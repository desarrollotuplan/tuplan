-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 28-03-2019 a las 21:07:19
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tu_plan`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calificacion`
--

CREATE TABLE `calificacion` (
  `CAL_ID` int(11) NOT NULL,
  `CAL_USU_ID` int(11) NOT NULL,
  `CAL_NEG_ID` int(11) DEFAULT NULL,
  `CAL_EVE_ID` int(11) DEFAULT NULL,
  `CAL_PRO_ID` int(11) DEFAULT NULL,
  `CAL_TIPO` int(11) NOT NULL,
  `CAL_PUNTO` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registraran los puntos de los usuarios se';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `CAT_ID` int(11) NOT NULL,
  `CAT_TP_ID` int(11) NOT NULL,
  `CAT_NOMBRE` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registraran las categorias de negocio.';

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`CAT_ID`, `CAT_TP_ID`, `CAT_NOMBRE`) VALUES
(1, 1, 'moteles'),
(2, 2, 'comidas'),
(3, 2, 'billares');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_producto`
--

CREATE TABLE `categoria_producto` (
  `CAPROD_ID` int(11) NOT NULL,
  `CAPROD_NEG_ID` int(11) NOT NULL,
  `CAPROD_NOMBRE` varchar(200) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registrara las categorias de los producto';

--
-- Volcado de datos para la tabla `categoria_producto`
--

INSERT INTO `categoria_producto` (`CAPROD_ID`, `CAPROD_NEG_ID`, `CAPROD_NOMBRE`) VALUES
(20, 3, 'comida'),
(21, 3, 'postre'),
(22, 3, 'mecato');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `CI_ID` int(11) NOT NULL,
  `CI_DE_ID` int(11) NOT NULL,
  `CI_CODIGO` int(11) NOT NULL,
  `CI_NOMBRE` varchar(45) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta tabla se egistraran todas las ciudades de cada pais con sus codigos correspondientes.';

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`CI_ID`, `CI_DE_ID`, `CI_CODIGO`, `CI_NOMBRE`) VALUES
(1, 1, 1, 'Abriaquí'),
(2, 16, 2, 'Acacías'),
(3, 12, 3, 'Acandí'),
(4, 13, 4, 'Acevedo'),
(5, 4, 5, 'Achí'),
(6, 13, 6, 'Agrado'),
(7, 11, 7, 'Agua de Dios'),
(8, 9, 8, 'Aguachica'),
(9, 21, 9, 'Aguada'),
(10, 6, 10, 'Aguadas'),
(11, 26, 11, 'Aguazul'),
(12, 9, 12, 'Agustín Codazzi'),
(13, 13, 13, 'Aipe'),
(14, 7, 14, 'Albania'),
(15, 14, 15, 'Albania'),
(16, 21, 16, 'Albania'),
(17, 11, 17, 'Albán'),
(18, 17, 18, 'Albán (San José)'),
(19, 24, 19, 'Alcalá'),
(20, 1, 20, 'Alejandria'),
(21, 15, 21, 'Algarrobo'),
(22, 13, 22, 'Algeciras'),
(23, 8, 23, 'Almaguer'),
(24, 5, 24, 'Almeida'),
(25, 23, 25, 'Alpujarra'),
(26, 13, 26, 'Altamira'),
(27, 12, 27, 'Alto Baudó (Pie de Pato)'),
(28, 4, 28, 'Altos del Rosario'),
(29, 23, 29, 'Alvarado'),
(30, 1, 30, 'Amagá'),
(31, 1, 31, 'Amalfi'),
(32, 23, 32, 'Ambalema'),
(33, 11, 33, 'Anapoima'),
(34, 17, 34, 'Ancuya'),
(35, 24, 35, 'Andalucía'),
(36, 1, 36, 'Andes'),
(37, 1, 37, 'Angelópolis'),
(38, 1, 38, 'Angostura'),
(39, 11, 39, 'Anolaima'),
(40, 1, 40, 'Anorí'),
(41, 6, 41, 'Anserma'),
(42, 24, 42, 'Ansermanuevo'),
(43, 23, 43, 'Anzoátegui'),
(44, 1, 44, 'Anzá'),
(45, 1, 45, 'Apartadó'),
(46, 11, 46, 'Apulo'),
(47, 20, 47, 'Apía'),
(48, 5, 48, 'Aquitania'),
(49, 15, 49, 'Aracataca'),
(50, 6, 50, 'Aranzazu'),
(51, 21, 51, 'Aratoca'),
(52, 25, 52, 'Arauca'),
(53, 25, 53, 'Arauquita'),
(54, 11, 54, 'Arbeláez'),
(55, 17, 55, 'Arboleda (Berruecos)'),
(56, 18, 56, 'Arboledas'),
(57, 1, 57, 'Arboletes'),
(58, 5, 58, 'Arcabuco'),
(59, 4, 59, 'Arenal'),
(60, 1, 60, 'Argelia'),
(61, 8, 61, 'Argelia'),
(62, 24, 62, 'Argelia'),
(63, 15, 63, 'Ariguaní (El Difícil)'),
(64, 4, 64, 'Arjona'),
(65, 1, 65, 'Armenia'),
(66, 19, 66, 'Armenia'),
(67, 23, 67, 'Armero (Guayabal)'),
(68, 4, 68, 'Arroyohondo'),
(69, 9, 69, 'Astrea'),
(70, 23, 70, 'Ataco'),
(71, 12, 71, 'Atrato (Yuto)'),
(72, 10, 72, 'Ayapel'),
(73, 12, 73, 'Bagadó'),
(74, 12, 74, 'Bahía Solano (Mútis)'),
(75, 12, 75, 'Bajo Baudó (Pizarro)'),
(76, 8, 76, 'Balboa'),
(77, 20, 77, 'Balboa'),
(78, 2, 78, 'Baranoa'),
(79, 13, 79, 'Baraya'),
(80, 17, 80, 'Barbacoas'),
(81, 1, 81, 'Barbosa'),
(82, 21, 82, 'Barbosa'),
(83, 21, 83, 'Barichara'),
(84, 16, 84, 'Barranca de Upía'),
(85, 21, 85, 'Barrancabermeja'),
(86, 14, 86, 'Barrancas'),
(87, 4, 87, 'Barranco de Loba'),
(88, 2, 88, 'Barranquilla'),
(89, 9, 89, 'Becerríl'),
(90, 6, 90, 'Belalcázar'),
(91, 1, 91, 'Bello'),
(92, 1, 92, 'Belmira'),
(93, 11, 93, 'Beltrán'),
(94, 5, 94, 'Belén'),
(95, 17, 95, 'Belén'),
(96, 12, 96, 'Belén de Bajirá'),
(97, 20, 97, 'Belén de Umbría'),
(98, 7, 98, 'Belén de los Andaquíes'),
(99, 5, 99, 'Berbeo'),
(100, 1, 100, 'Betania'),
(101, 5, 101, 'Beteitiva'),
(102, 1, 102, 'Betulia'),
(103, 21, 103, 'Betulia'),
(104, 11, 104, 'Bituima'),
(105, 5, 105, 'Boavita'),
(106, 18, 106, 'Bochalema'),
(107, 3, 107, 'Bogotá D.C.'),
(108, 11, 108, 'Bojacá'),
(109, 12, 109, 'Bojayá (Bellavista)'),
(110, 1, 110, 'Bolívar'),
(111, 8, 111, 'Bolívar'),
(112, 21, 112, 'Bolívar'),
(113, 24, 113, 'Bolívar'),
(114, 9, 114, 'Bosconia'),
(115, 5, 115, 'Boyacá'),
(116, 1, 116, 'Briceño'),
(117, 5, 117, 'Briceño'),
(118, 21, 118, 'Bucaramanga'),
(119, 18, 119, 'Bucarasica'),
(120, 24, 120, 'Buenaventura'),
(121, 5, 121, 'Buenavista'),
(122, 10, 122, 'Buenavista'),
(123, 19, 123, 'Buenavista'),
(124, 22, 124, 'Buenavista'),
(125, 8, 125, 'Buenos Aires'),
(126, 17, 126, 'Buesaco'),
(127, 24, 127, 'Buga'),
(128, 24, 128, 'Bugalagrande'),
(129, 1, 129, 'Burítica'),
(130, 5, 130, 'Busbanza'),
(131, 11, 131, 'Cabrera'),
(132, 21, 132, 'Cabrera'),
(133, 16, 133, 'Cabuyaro'),
(134, 11, 134, 'Cachipay'),
(135, 1, 135, 'Caicedo'),
(136, 24, 136, 'Caicedonia'),
(137, 22, 137, 'Caimito'),
(138, 23, 138, 'Cajamarca'),
(139, 8, 139, 'Cajibío'),
(140, 11, 140, 'Cajicá'),
(141, 4, 141, 'Calamar'),
(142, 31, 142, 'Calamar'),
(143, 19, 143, 'Calarcá'),
(144, 1, 144, 'Caldas'),
(145, 5, 145, 'Caldas'),
(146, 8, 146, 'Caldono'),
(147, 21, 147, 'California'),
(148, 24, 148, 'Calima (Darién)'),
(149, 8, 149, 'Caloto'),
(150, 24, 150, 'Calí'),
(151, 1, 151, 'Campamento'),
(152, 2, 152, 'Campo de la Cruz'),
(153, 13, 153, 'Campoalegre'),
(154, 5, 154, 'Campohermoso'),
(155, 10, 155, 'Canalete'),
(156, 2, 156, 'Candelaria'),
(157, 24, 157, 'Candelaria'),
(158, 4, 158, 'Cantagallo'),
(159, 12, 159, 'Cantón de San Pablo'),
(160, 11, 160, 'Caparrapí'),
(161, 21, 161, 'Capitanejo'),
(162, 1, 162, 'Caracolí'),
(163, 1, 163, 'Caramanta'),
(164, 21, 164, 'Carcasí'),
(165, 1, 165, 'Carepa'),
(166, 23, 166, 'Carmen de Apicalá'),
(167, 11, 167, 'Carmen de Carupa'),
(168, 1, 168, 'Carmen de Viboral'),
(169, 12, 169, 'Carmen del Darién (CURBARADÓ)'),
(170, 1, 170, 'Carolina'),
(171, 4, 171, 'Cartagena'),
(172, 7, 172, 'Cartagena del Chairá'),
(173, 24, 173, 'Cartago'),
(174, 32, 174, 'Carurú'),
(175, 23, 175, 'Casabianca'),
(176, 16, 176, 'Castilla la Nueva'),
(177, 1, 177, 'Caucasia'),
(178, 1, 178, 'Cañasgordas'),
(179, 21, 179, 'Cepita'),
(180, 10, 180, 'Cereté'),
(181, 5, 181, 'Cerinza'),
(182, 21, 182, 'Cerrito'),
(183, 15, 183, 'Cerro San Antonio'),
(184, 17, 184, 'Chachaguí'),
(185, 11, 185, 'Chaguaní'),
(186, 22, 186, 'Chalán'),
(187, 23, 187, 'Chaparral'),
(188, 21, 188, 'Charalá'),
(189, 21, 189, 'Charta'),
(190, 1, 190, 'Chigorodó'),
(191, 21, 191, 'Chima'),
(192, 9, 192, 'Chimichagua'),
(193, 10, 193, 'Chimá'),
(194, 5, 194, 'Chinavita'),
(195, 6, 195, 'Chinchiná'),
(196, 18, 196, 'Chinácota'),
(197, 10, 197, 'Chinú'),
(198, 11, 198, 'Chipaque'),
(199, 21, 199, 'Chipatá'),
(200, 5, 200, 'Chiquinquirá'),
(201, 9, 201, 'Chiriguaná'),
(202, 5, 202, 'Chiscas'),
(203, 5, 203, 'Chita'),
(204, 18, 204, 'Chitagá'),
(205, 5, 205, 'Chitaraque'),
(206, 5, 206, 'Chivatá'),
(207, 15, 207, 'Chivolo'),
(208, 11, 208, 'Choachí'),
(209, 11, 209, 'Chocontá'),
(210, 26, 210, 'Chámeza'),
(211, 11, 211, 'Chía'),
(212, 5, 212, 'Chíquiza'),
(213, 5, 213, 'Chívor'),
(214, 4, 214, 'Cicuco'),
(215, 21, 215, 'Cimitarra'),
(216, 19, 216, 'Circasia'),
(217, 1, 217, 'Cisneros'),
(218, 5, 218, 'Ciénaga'),
(219, 15, 219, 'Ciénaga'),
(220, 10, 220, 'Ciénaga de Oro'),
(221, 4, 221, 'Clemencia'),
(222, 1, 222, 'Cocorná'),
(223, 23, 223, 'Coello'),
(224, 11, 224, 'Cogua'),
(225, 13, 225, 'Colombia'),
(226, 22, 226, 'Colosó (Ricaurte)'),
(227, 27, 227, 'Colón'),
(228, 17, 228, 'Colón (Génova)'),
(229, 1, 229, 'Concepción'),
(230, 21, 230, 'Concepción'),
(231, 1, 231, 'Concordia'),
(232, 15, 232, 'Concordia'),
(233, 12, 233, 'Condoto'),
(234, 21, 234, 'Confines'),
(235, 17, 235, 'Consaca'),
(236, 17, 236, 'Contadero'),
(237, 21, 237, 'Contratación'),
(238, 18, 238, 'Convención'),
(239, 1, 239, 'Copacabana'),
(240, 5, 240, 'Coper'),
(241, 19, 241, 'Cordobá'),
(242, 8, 242, 'Corinto'),
(243, 21, 243, 'Coromoro'),
(244, 22, 244, 'Corozal'),
(245, 5, 245, 'Corrales'),
(246, 11, 246, 'Cota'),
(247, 10, 247, 'Cotorra'),
(248, 5, 248, 'Covarachía'),
(249, 22, 249, 'Coveñas'),
(250, 23, 250, 'Coyaima'),
(251, 25, 251, 'Cravo Norte'),
(252, 17, 252, 'Cuaspud (Carlosama)'),
(253, 16, 253, 'Cubarral'),
(254, 5, 254, 'Cubará'),
(255, 5, 255, 'Cucaita'),
(256, 11, 256, 'Cucunubá'),
(257, 18, 257, 'Cucutilla'),
(258, 5, 258, 'Cuitiva'),
(259, 16, 259, 'Cumaral'),
(260, 33, 260, 'Cumaribo'),
(261, 17, 261, 'Cumbal'),
(262, 17, 262, 'Cumbitara'),
(263, 23, 263, 'Cunday'),
(264, 7, 264, 'Curillo'),
(265, 21, 265, 'Curití'),
(266, 9, 266, 'Curumaní'),
(267, 1, 267, 'Cáceres'),
(268, 18, 268, 'Cáchira'),
(269, 18, 269, 'Cácota'),
(270, 11, 270, 'Cáqueza'),
(271, 12, 271, 'Cértegui'),
(272, 5, 272, 'Cómbita'),
(273, 4, 273, 'Córdoba'),
(274, 17, 274, 'Córdoba'),
(275, 18, 275, 'Cúcuta'),
(276, 1, 276, 'Dabeiba'),
(277, 24, 277, 'Dagua'),
(278, 14, 278, 'Dibulla'),
(279, 14, 279, 'Distracción'),
(280, 23, 280, 'Dolores'),
(281, 1, 281, 'Don Matías'),
(282, 20, 282, 'Dos Quebradas'),
(283, 5, 283, 'Duitama'),
(284, 18, 284, 'Durania'),
(285, 1, 285, 'Ebéjico'),
(286, 1, 286, 'El Bagre'),
(287, 15, 287, 'El Banco'),
(288, 24, 288, 'El Cairo'),
(289, 16, 289, 'El Calvario'),
(290, 18, 290, 'El Carmen'),
(291, 21, 291, 'El Carmen'),
(292, 12, 292, 'El Carmen de Atrato'),
(293, 4, 293, 'El Carmen de Bolívar'),
(294, 16, 294, 'El Castillo'),
(295, 24, 295, 'El Cerrito'),
(296, 17, 296, 'El Charco'),
(297, 5, 297, 'El Cocuy'),
(298, 11, 298, 'El Colegio'),
(299, 9, 299, 'El Copey'),
(300, 7, 300, 'El Doncello'),
(301, 16, 301, 'El Dorado'),
(302, 24, 302, 'El Dovio'),
(303, 5, 303, 'El Espino'),
(304, 21, 304, 'El Guacamayo'),
(305, 4, 305, 'El Guamo'),
(306, 14, 306, 'El Molino'),
(307, 9, 307, 'El Paso'),
(308, 7, 308, 'El Paujil'),
(309, 17, 309, 'El Peñol'),
(310, 4, 310, 'El Peñon'),
(311, 21, 311, 'El Peñon'),
(312, 11, 312, 'El Peñón'),
(313, 15, 313, 'El Piñon'),
(314, 21, 314, 'El Playón'),
(315, 31, 315, 'El Retorno'),
(316, 15, 316, 'El Retén'),
(317, 22, 317, 'El Roble'),
(318, 11, 318, 'El Rosal'),
(319, 17, 319, 'El Rosario'),
(320, 17, 320, 'El Tablón de Gómez'),
(321, 8, 321, 'El Tambo'),
(322, 17, 322, 'El Tambo'),
(323, 18, 323, 'El Tarra'),
(324, 18, 324, 'El Zulia'),
(325, 24, 325, 'El Águila'),
(326, 13, 326, 'Elías'),
(327, 21, 327, 'Encino'),
(328, 21, 328, 'Enciso'),
(329, 1, 329, 'Entrerríos'),
(330, 1, 330, 'Envigado'),
(331, 23, 331, 'Espinal'),
(332, 11, 332, 'Facatativá'),
(333, 23, 333, 'Falan'),
(334, 6, 334, 'Filadelfia'),
(335, 19, 335, 'Filandia'),
(336, 5, 336, 'Firavitoba'),
(337, 23, 337, 'Flandes'),
(338, 7, 338, 'Florencia'),
(339, 8, 339, 'Florencia'),
(340, 5, 340, 'Floresta'),
(341, 24, 341, 'Florida'),
(342, 21, 342, 'Floridablanca'),
(343, 21, 343, 'Florián'),
(344, 14, 344, 'Fonseca'),
(345, 25, 345, 'Fortúl'),
(346, 11, 346, 'Fosca'),
(347, 17, 347, 'Francisco Pizarro'),
(348, 1, 348, 'Fredonia'),
(349, 23, 349, 'Fresno'),
(350, 1, 350, 'Frontino'),
(351, 16, 351, 'Fuente de Oro'),
(352, 15, 352, 'Fundación'),
(353, 17, 353, 'Funes'),
(354, 11, 354, 'Funza'),
(355, 11, 355, 'Fusagasugá'),
(356, 11, 356, 'Fómeque'),
(357, 11, 357, 'Fúquene'),
(358, 11, 358, 'Gachalá'),
(359, 11, 359, 'Gachancipá'),
(360, 5, 360, 'Gachantivá'),
(361, 11, 361, 'Gachetá'),
(362, 2, 362, 'Galapa'),
(363, 22, 363, 'Galeras (Nueva Granada)'),
(364, 21, 364, 'Galán'),
(365, 11, 365, 'Gama'),
(366, 9, 366, 'Gamarra'),
(367, 5, 367, 'Garagoa'),
(368, 13, 368, 'Garzón'),
(369, 13, 369, 'Gigante'),
(370, 24, 370, 'Ginebra'),
(371, 1, 371, 'Giraldo'),
(372, 11, 372, 'Girardot'),
(373, 1, 373, 'Girardota'),
(374, 21, 374, 'Girón'),
(375, 9, 375, 'Gonzalez'),
(376, 18, 376, 'Gramalote'),
(377, 1, 377, 'Granada'),
(378, 11, 378, 'Granada'),
(379, 16, 379, 'Granada'),
(380, 21, 380, 'Guaca'),
(381, 5, 381, 'Guacamayas'),
(382, 24, 382, 'Guacarí'),
(383, 17, 383, 'Guachavés'),
(384, 8, 384, 'Guachené'),
(385, 11, 385, 'Guachetá'),
(386, 17, 386, 'Guachucal'),
(387, 1, 387, 'Guadalupe'),
(388, 13, 388, 'Guadalupe'),
(389, 21, 389, 'Guadalupe'),
(390, 11, 390, 'Guaduas'),
(391, 17, 391, 'Guaitarilla'),
(392, 17, 392, 'Gualmatán'),
(393, 15, 393, 'Guamal'),
(394, 16, 394, 'Guamal'),
(395, 23, 395, 'Guamo'),
(396, 21, 396, 'Guapota'),
(397, 8, 397, 'Guapí'),
(398, 22, 398, 'Guaranda'),
(399, 1, 399, 'Guarne'),
(400, 11, 400, 'Guasca'),
(401, 1, 401, 'Guatapé'),
(402, 11, 402, 'Guataquí'),
(403, 11, 403, 'Guatavita'),
(404, 5, 404, 'Guateque'),
(405, 21, 405, 'Guavatá'),
(406, 11, 406, 'Guayabal de Siquima'),
(407, 11, 407, 'Guayabetal'),
(408, 5, 408, 'Guayatá'),
(409, 21, 409, 'Guepsa'),
(410, 5, 410, 'Guicán'),
(411, 11, 411, 'Gutiérrez'),
(412, 20, 412, 'Guática'),
(413, 21, 413, 'Gámbita'),
(414, 5, 414, 'Gámeza'),
(415, 19, 415, 'Génova'),
(416, 1, 416, 'Gómez Plata'),
(417, 18, 417, 'Hacarí'),
(418, 4, 418, 'Hatillo de Loba'),
(419, 21, 419, 'Hato'),
(420, 26, 420, 'Hato Corozal'),
(421, 14, 421, 'Hatonuevo'),
(422, 1, 422, 'Heliconia'),
(423, 18, 423, 'Herrán'),
(424, 23, 424, 'Herveo'),
(425, 1, 425, 'Hispania'),
(426, 13, 426, 'Hobo'),
(427, 23, 427, 'Honda'),
(428, 23, 428, 'Ibagué'),
(429, 23, 429, 'Icononzo'),
(430, 17, 430, 'Iles'),
(431, 17, 431, 'Imúes'),
(432, 8, 432, 'Inzá'),
(433, 30, 433, 'Inírida'),
(434, 17, 434, 'Ipiales'),
(435, 13, 435, 'Isnos'),
(436, 12, 436, 'Istmina'),
(437, 1, 437, 'Itagüí'),
(438, 1, 438, 'Ituango'),
(439, 5, 439, 'Izá'),
(440, 8, 440, 'Jambaló'),
(441, 24, 441, 'Jamundí'),
(442, 1, 442, 'Jardín'),
(443, 5, 443, 'Jenesano'),
(444, 1, 444, 'Jericó'),
(445, 5, 445, 'Jericó'),
(446, 11, 446, 'Jerusalén'),
(447, 21, 447, 'Jesús María'),
(448, 21, 448, 'Jordán'),
(449, 2, 449, 'Juan de Acosta'),
(450, 11, 450, 'Junín'),
(451, 12, 451, 'Juradó'),
(452, 10, 452, 'La Apartada y La Frontera'),
(453, 13, 453, 'La Argentina'),
(454, 21, 454, 'La Belleza'),
(455, 11, 455, 'La Calera'),
(456, 5, 456, 'La Capilla'),
(457, 1, 457, 'La Ceja'),
(458, 20, 458, 'La Celia'),
(459, 17, 459, 'La Cruz'),
(460, 24, 460, 'La Cumbre'),
(461, 6, 461, 'La Dorada'),
(462, 18, 462, 'La Esperanza'),
(463, 1, 463, 'La Estrella'),
(464, 17, 464, 'La Florida'),
(465, 9, 465, 'La Gloria'),
(466, 9, 466, 'La Jagua de Ibirico'),
(467, 14, 467, 'La Jagua del Pilar'),
(468, 17, 468, 'La Llanada'),
(469, 16, 469, 'La Macarena'),
(470, 6, 470, 'La Merced'),
(471, 11, 471, 'La Mesa'),
(472, 7, 472, 'La Montañita'),
(473, 11, 473, 'La Palma'),
(474, 21, 474, 'La Paz'),
(475, 9, 475, 'La Paz (Robles)'),
(476, 11, 476, 'La Peña'),
(477, 1, 477, 'La Pintada'),
(478, 13, 478, 'La Plata'),
(479, 18, 479, 'La Playa'),
(480, 33, 480, 'La Primavera'),
(481, 26, 481, 'La Salina'),
(482, 8, 482, 'La Sierra'),
(483, 19, 483, 'La Tebaida'),
(484, 17, 484, 'La Tola'),
(485, 1, 485, 'La Unión'),
(486, 17, 486, 'La Unión'),
(487, 22, 487, 'La Unión'),
(488, 24, 488, 'La Unión'),
(489, 5, 489, 'La Uvita'),
(490, 8, 490, 'La Vega'),
(491, 11, 491, 'La Vega'),
(492, 5, 492, 'La Victoria'),
(493, 6, 493, 'La Victoria'),
(494, 24, 494, 'La Victoria'),
(495, 20, 495, 'La Virginia'),
(496, 18, 496, 'Labateca'),
(497, 5, 497, 'Labranzagrande'),
(498, 21, 498, 'Landázuri'),
(499, 21, 499, 'Lebrija'),
(500, 17, 500, 'Leiva'),
(501, 16, 501, 'Lejanías'),
(502, 11, 502, 'Lenguazaque'),
(503, 29, 503, 'Leticia'),
(504, 1, 504, 'Liborina'),
(505, 17, 505, 'Linares'),
(506, 12, 506, 'Lloró'),
(507, 10, 507, 'Lorica'),
(508, 10, 508, 'Los Córdobas'),
(509, 22, 509, 'Los Palmitos'),
(510, 18, 510, 'Los Patios'),
(511, 21, 511, 'Los Santos'),
(512, 18, 512, 'Lourdes'),
(513, 2, 513, 'Luruaco'),
(514, 23, 514, 'Lérida'),
(515, 23, 515, 'Líbano'),
(516, 8, 516, 'López (Micay)'),
(517, 5, 517, 'Macanal'),
(518, 21, 518, 'Macaravita'),
(519, 1, 519, 'Maceo'),
(520, 11, 520, 'Machetá'),
(521, 11, 521, 'Madrid'),
(522, 4, 522, 'Magangué'),
(523, 17, 523, 'Magüi (Payán)'),
(524, 4, 524, 'Mahates'),
(525, 14, 525, 'Maicao'),
(526, 22, 526, 'Majagual'),
(527, 2, 527, 'Malambo'),
(528, 17, 528, 'Mallama (Piedrancha)'),
(529, 2, 529, 'Manatí'),
(530, 14, 530, 'Manaure'),
(531, 9, 531, 'Manaure Balcón del Cesar'),
(532, 6, 532, 'Manizales'),
(533, 11, 533, 'Manta'),
(534, 6, 534, 'Manzanares'),
(535, 26, 535, 'Maní'),
(536, 16, 536, 'Mapiripan'),
(537, 4, 537, 'Margarita'),
(538, 1, 538, 'Marinilla'),
(539, 5, 539, 'Maripí'),
(540, 23, 540, 'Mariquita'),
(541, 6, 541, 'Marmato'),
(542, 6, 542, 'Marquetalia'),
(543, 20, 543, 'Marsella'),
(544, 6, 544, 'Marulanda'),
(545, 4, 545, 'María la Baja'),
(546, 21, 546, 'Matanza'),
(547, 1, 547, 'Medellín'),
(548, 11, 548, 'Medina'),
(549, 12, 549, 'Medio Atrato'),
(550, 12, 550, 'Medio Baudó'),
(551, 12, 551, 'Medio San Juan (ANDAGOYA)'),
(552, 23, 552, 'Melgar'),
(553, 8, 553, 'Mercaderes'),
(554, 16, 554, 'Mesetas'),
(555, 7, 555, 'Milán'),
(556, 5, 556, 'Miraflores'),
(557, 31, 557, 'Miraflores'),
(558, 8, 558, 'Miranda'),
(559, 20, 559, 'Mistrató'),
(560, 32, 560, 'Mitú'),
(561, 27, 561, 'Mocoa'),
(562, 21, 562, 'Mogotes'),
(563, 21, 563, 'Molagavita'),
(564, 10, 564, 'Momil'),
(565, 4, 565, 'Mompós'),
(566, 5, 566, 'Mongua'),
(567, 5, 567, 'Monguí'),
(568, 5, 568, 'Moniquirá'),
(569, 1, 569, 'Montebello'),
(570, 4, 570, 'Montecristo'),
(571, 10, 571, 'Montelíbano'),
(572, 19, 572, 'Montenegro'),
(573, 10, 573, 'Monteria'),
(574, 26, 574, 'Monterrey'),
(575, 4, 575, 'Morales'),
(576, 8, 576, 'Morales'),
(577, 7, 577, 'Morelia'),
(578, 22, 578, 'Morroa'),
(579, 11, 579, 'Mosquera'),
(580, 17, 580, 'Mosquera'),
(581, 5, 581, 'Motavita'),
(582, 10, 582, 'Moñitos'),
(583, 23, 583, 'Murillo'),
(584, 1, 584, 'Murindó'),
(585, 1, 585, 'Mutatá'),
(586, 18, 586, 'Mutiscua'),
(587, 5, 587, 'Muzo'),
(588, 21, 588, 'Málaga'),
(589, 1, 589, 'Nariño'),
(590, 11, 590, 'Nariño'),
(591, 17, 591, 'Nariño'),
(592, 23, 592, 'Natagaima'),
(593, 1, 593, 'Nechí'),
(594, 1, 594, 'Necoclí'),
(595, 6, 595, 'Neira'),
(596, 13, 596, 'Neiva'),
(597, 11, 597, 'Nemocón'),
(598, 11, 598, 'Nilo'),
(599, 11, 599, 'Nimaima'),
(600, 5, 600, 'Nobsa'),
(601, 11, 601, 'Nocaima'),
(602, 6, 602, 'Norcasia'),
(603, 4, 603, 'Norosí'),
(604, 12, 604, 'Novita'),
(605, 15, 605, 'Nueva Granada'),
(606, 5, 606, 'Nuevo Colón'),
(607, 26, 607, 'Nunchía'),
(608, 12, 608, 'Nuquí'),
(609, 13, 609, 'Nátaga'),
(610, 24, 610, 'Obando'),
(611, 21, 611, 'Ocamonte'),
(612, 18, 612, 'Ocaña'),
(613, 21, 613, 'Oiba'),
(614, 5, 614, 'Oicatá'),
(615, 1, 615, 'Olaya'),
(616, 17, 616, 'Olaya Herrera'),
(617, 21, 617, 'Onzaga'),
(618, 13, 618, 'Oporapa'),
(619, 27, 619, 'Orito'),
(620, 26, 620, 'Orocué'),
(621, 23, 621, 'Ortega'),
(622, 17, 622, 'Ospina'),
(623, 5, 623, 'Otanche'),
(624, 22, 624, 'Ovejas'),
(625, 5, 625, 'Pachavita'),
(626, 11, 626, 'Pacho'),
(627, 8, 627, 'Padilla'),
(628, 13, 628, 'Paicol'),
(629, 9, 629, 'Pailitas'),
(630, 11, 630, 'Paime'),
(631, 5, 631, 'Paipa'),
(632, 5, 632, 'Pajarito'),
(633, 13, 633, 'Palermo'),
(634, 6, 634, 'Palestina'),
(635, 13, 635, 'Palestina'),
(636, 21, 636, 'Palmar'),
(637, 2, 637, 'Palmar de Varela'),
(638, 21, 638, 'Palmas del Socorro'),
(639, 24, 639, 'Palmira'),
(640, 22, 640, 'Palmito'),
(641, 23, 641, 'Palocabildo'),
(642, 18, 642, 'Pamplona'),
(643, 18, 643, 'Pamplonita'),
(644, 11, 644, 'Pandi'),
(645, 5, 645, 'Panqueba'),
(646, 11, 646, 'Paratebueno'),
(647, 11, 647, 'Pasca'),
(648, 8, 648, 'Patía (El Bordo)'),
(649, 5, 649, 'Pauna'),
(650, 5, 650, 'Paya'),
(651, 26, 651, 'Paz de Ariporo'),
(652, 5, 652, 'Paz de Río'),
(653, 15, 653, 'Pedraza'),
(654, 9, 654, 'Pelaya'),
(655, 6, 655, 'Pensilvania'),
(656, 1, 656, 'Peque'),
(657, 20, 657, 'Pereira'),
(658, 5, 658, 'Pesca'),
(659, 1, 659, 'Peñol'),
(660, 8, 660, 'Piamonte'),
(661, 21, 661, 'Pie de Cuesta'),
(662, 23, 662, 'Piedras'),
(663, 8, 663, 'Piendamó'),
(664, 19, 664, 'Pijao'),
(665, 15, 665, 'Pijiño'),
(666, 21, 666, 'Pinchote'),
(667, 4, 667, 'Pinillos'),
(668, 2, 668, 'Piojo'),
(669, 5, 669, 'Pisva'),
(670, 13, 670, 'Pital'),
(671, 13, 671, 'Pitalito'),
(672, 15, 672, 'Pivijay'),
(673, 23, 673, 'Planadas'),
(674, 10, 674, 'Planeta Rica'),
(675, 15, 675, 'Plato'),
(676, 17, 676, 'Policarpa'),
(677, 2, 677, 'Polonuevo'),
(678, 2, 678, 'Ponedera'),
(679, 8, 679, 'Popayán'),
(680, 26, 680, 'Pore'),
(681, 17, 681, 'Potosí'),
(682, 24, 682, 'Pradera'),
(683, 23, 683, 'Prado'),
(684, 17, 684, 'Providencia'),
(685, 28, 685, 'Providencia'),
(686, 9, 686, 'Pueblo Bello'),
(687, 10, 687, 'Pueblo Nuevo'),
(688, 20, 688, 'Pueblo Rico'),
(689, 1, 689, 'Pueblorrico'),
(690, 15, 690, 'Puebloviejo'),
(691, 21, 691, 'Puente Nacional'),
(692, 17, 692, 'Puerres'),
(693, 27, 693, 'Puerto Asís'),
(694, 1, 694, 'Puerto Berrío'),
(695, 5, 695, 'Puerto Boyacá'),
(696, 27, 696, 'Puerto Caicedo'),
(697, 33, 697, 'Puerto Carreño'),
(698, 2, 698, 'Puerto Colombia'),
(699, 16, 699, 'Puerto Concordia'),
(700, 10, 700, 'Puerto Escondido'),
(701, 16, 701, 'Puerto Gaitán'),
(702, 27, 702, 'Puerto Guzmán'),
(703, 27, 703, 'Puerto Leguízamo'),
(704, 10, 704, 'Puerto Libertador'),
(705, 16, 705, 'Puerto Lleras'),
(706, 16, 706, 'Puerto López'),
(707, 1, 707, 'Puerto Nare'),
(708, 29, 708, 'Puerto Nariño'),
(709, 21, 709, 'Puerto Parra'),
(710, 7, 710, 'Puerto Rico'),
(711, 16, 711, 'Puerto Rico'),
(712, 25, 712, 'Puerto Rondón'),
(713, 11, 713, 'Puerto Salgar'),
(714, 18, 714, 'Puerto Santander'),
(715, 8, 715, 'Puerto Tejada'),
(716, 1, 716, 'Puerto Triunfo'),
(717, 21, 717, 'Puerto Wilches'),
(718, 11, 718, 'Pulí'),
(719, 17, 719, 'Pupiales'),
(720, 8, 720, 'Puracé (Coconuco)'),
(721, 23, 721, 'Purificación'),
(722, 10, 722, 'Purísima'),
(723, 6, 723, 'Pácora'),
(724, 5, 724, 'Páez'),
(725, 8, 725, 'Páez (Belalcazar)'),
(726, 21, 726, 'Páramo'),
(727, 11, 727, 'Quebradanegra'),
(728, 11, 728, 'Quetame'),
(729, 12, 729, 'Quibdó'),
(730, 19, 730, 'Quimbaya'),
(731, 20, 731, 'Quinchía'),
(732, 5, 732, 'Quipama'),
(733, 11, 733, 'Quipile'),
(734, 18, 734, 'Ragonvalia'),
(735, 5, 735, 'Ramiriquí'),
(736, 26, 736, 'Recetor'),
(737, 4, 737, 'Regidor'),
(738, 1, 738, 'Remedios'),
(739, 15, 739, 'Remolino'),
(740, 2, 740, 'Repelón'),
(741, 16, 741, 'Restrepo'),
(742, 24, 742, 'Restrepo'),
(743, 1, 743, 'Retiro'),
(744, 11, 744, 'Ricaurte'),
(745, 17, 745, 'Ricaurte'),
(746, 21, 746, 'Rio Negro'),
(747, 23, 747, 'Rioblanco'),
(748, 24, 748, 'Riofrío'),
(749, 14, 749, 'Riohacha'),
(750, 6, 750, 'Risaralda'),
(751, 13, 751, 'Rivera'),
(752, 17, 752, 'Roberto Payán (San José)'),
(753, 24, 753, 'Roldanillo'),
(754, 23, 754, 'Roncesvalles'),
(755, 5, 755, 'Rondón'),
(756, 8, 756, 'Rosas'),
(757, 23, 757, 'Rovira'),
(758, 5, 758, 'Ráquira'),
(759, 12, 759, 'Río Iró'),
(760, 12, 760, 'Río Quito'),
(761, 6, 761, 'Río Sucio'),
(762, 4, 762, 'Río Viejo'),
(763, 9, 763, 'Río de oro'),
(764, 1, 764, 'Ríonegro'),
(765, 12, 765, 'Ríosucio'),
(766, 21, 766, 'Sabana de Torres'),
(767, 2, 767, 'Sabanagrande'),
(768, 1, 768, 'Sabanalarga'),
(769, 2, 769, 'Sabanalarga'),
(770, 26, 770, 'Sabanalarga'),
(771, 15, 771, 'Sabanas de San Angel (SAN ANGEL)'),
(772, 1, 772, 'Sabaneta'),
(773, 5, 773, 'Saboyá'),
(774, 10, 774, 'Sahagún'),
(775, 13, 775, 'Saladoblanco'),
(776, 6, 776, 'Salamina'),
(777, 15, 777, 'Salamina'),
(778, 18, 778, 'Salazar'),
(779, 23, 779, 'Saldaña'),
(780, 19, 780, 'Salento'),
(781, 1, 781, 'Salgar'),
(782, 5, 782, 'Samacá'),
(783, 17, 783, 'Samaniego'),
(784, 6, 784, 'Samaná'),
(785, 22, 785, 'Sampués'),
(786, 13, 786, 'San Agustín'),
(787, 9, 787, 'San Alberto'),
(788, 21, 788, 'San Andrés'),
(789, 10, 789, 'San Andrés Sotavento'),
(790, 1, 790, 'San Andrés de Cuerquía'),
(791, 10, 791, 'San Antero'),
(792, 23, 792, 'San Antonio'),
(793, 11, 793, 'San Antonio de Tequendama'),
(794, 21, 794, 'San Benito'),
(795, 22, 795, 'San Benito Abad'),
(796, 11, 796, 'San Bernardo'),
(797, 17, 797, 'San Bernardo'),
(798, 10, 798, 'San Bernardo del Viento'),
(799, 18, 799, 'San Calixto'),
(800, 1, 800, 'San Carlos'),
(801, 10, 801, 'San Carlos'),
(802, 16, 802, 'San Carlos de Guaroa'),
(803, 11, 803, 'San Cayetano'),
(804, 18, 804, 'San Cayetano'),
(805, 4, 805, 'San Cristobal'),
(806, 9, 806, 'San Diego'),
(807, 5, 807, 'San Eduardo'),
(808, 4, 808, 'San Estanislao'),
(809, 4, 809, 'San Fernando'),
(810, 1, 810, 'San Francisco'),
(811, 11, 811, 'San Francisco'),
(812, 27, 812, 'San Francisco'),
(813, 21, 813, 'San Gíl'),
(814, 4, 814, 'San Jacinto'),
(815, 4, 815, 'San Jacinto del Cauca'),
(816, 1, 816, 'San Jerónimo'),
(817, 21, 817, 'San Joaquín'),
(818, 6, 818, 'San José'),
(819, 21, 819, 'San José de Miranda'),
(820, 1, 820, 'San José de Montaña'),
(821, 5, 821, 'San José de Pare'),
(822, 10, 822, 'San José de Uré'),
(823, 7, 823, 'San José del Fragua'),
(824, 31, 824, 'San José del Guaviare'),
(825, 12, 825, 'San José del Palmar'),
(826, 16, 826, 'San Juan de Arama'),
(827, 22, 827, 'San Juan de Betulia'),
(828, 4, 828, 'San Juan de Nepomuceno'),
(829, 17, 829, 'San Juan de Pasto'),
(830, 11, 830, 'San Juan de Río Seco'),
(831, 1, 831, 'San Juan de Urabá'),
(832, 14, 832, 'San Juan del Cesar'),
(833, 16, 833, 'San Juanito'),
(834, 17, 834, 'San Lorenzo'),
(835, 23, 835, 'San Luis'),
(836, 1, 836, 'San Luís'),
(837, 5, 837, 'San Luís de Gaceno'),
(838, 26, 838, 'San Luís de Palenque'),
(839, 22, 839, 'San Marcos'),
(840, 9, 840, 'San Martín'),
(841, 16, 841, 'San Martín'),
(842, 4, 842, 'San Martín de Loba'),
(843, 5, 843, 'San Mateo'),
(844, 21, 844, 'San Miguel'),
(845, 27, 845, 'San Miguel'),
(846, 5, 846, 'San Miguel de Sema'),
(847, 22, 847, 'San Onofre'),
(848, 4, 848, 'San Pablo'),
(849, 17, 849, 'San Pablo'),
(850, 5, 850, 'San Pablo de Borbur'),
(851, 1, 851, 'San Pedro'),
(852, 22, 852, 'San Pedro'),
(853, 24, 853, 'San Pedro'),
(854, 17, 854, 'San Pedro de Cartago'),
(855, 1, 855, 'San Pedro de Urabá'),
(856, 10, 856, 'San Pelayo'),
(857, 1, 857, 'San Rafael'),
(858, 1, 858, 'San Roque'),
(859, 8, 859, 'San Sebastián'),
(860, 15, 860, 'San Sebastián de Buenavista'),
(861, 1, 861, 'San Vicente'),
(862, 7, 862, 'San Vicente del Caguán'),
(863, 21, 863, 'San Vicente del Chucurí'),
(864, 15, 864, 'San Zenón'),
(865, 17, 865, 'Sandoná'),
(866, 15, 866, 'Santa Ana'),
(867, 1, 867, 'Santa Bárbara'),
(868, 21, 868, 'Santa Bárbara'),
(869, 17, 869, 'Santa Bárbara (Iscuandé)'),
(870, 15, 870, 'Santa Bárbara de Pinto'),
(871, 4, 871, 'Santa Catalina'),
(872, 1, 872, 'Santa Fé de Antioquia'),
(873, 12, 873, 'Santa Genoveva de Docorodó'),
(874, 21, 874, 'Santa Helena del Opón'),
(875, 23, 875, 'Santa Isabel'),
(876, 2, 876, 'Santa Lucía'),
(877, 15, 877, 'Santa Marta'),
(878, 5, 878, 'Santa María'),
(879, 13, 879, 'Santa María'),
(880, 4, 880, 'Santa Rosa'),
(881, 8, 881, 'Santa Rosa'),
(882, 20, 882, 'Santa Rosa de Cabal'),
(883, 1, 883, 'Santa Rosa de Osos'),
(884, 5, 884, 'Santa Rosa de Viterbo'),
(885, 4, 885, 'Santa Rosa del Sur'),
(886, 33, 886, 'Santa Rosalía'),
(887, 5, 887, 'Santa Sofía'),
(888, 5, 888, 'Santana'),
(889, 8, 889, 'Santander de Quilichao'),
(890, 18, 890, 'Santiago'),
(891, 27, 891, 'Santiago'),
(892, 1, 892, 'Santo Domingo'),
(893, 2, 893, 'Santo Tomás'),
(894, 1, 894, 'Santuario'),
(895, 20, 895, 'Santuario'),
(896, 17, 896, 'Sapuyes'),
(897, 25, 897, 'Saravena'),
(898, 18, 898, 'Sardinata'),
(899, 11, 899, 'Sasaima'),
(900, 5, 900, 'Sativanorte'),
(901, 5, 901, 'Sativasur'),
(902, 1, 902, 'Segovia'),
(903, 11, 903, 'Sesquilé'),
(904, 24, 904, 'Sevilla'),
(905, 5, 905, 'Siachoque'),
(906, 11, 906, 'Sibaté'),
(907, 27, 907, 'Sibundoy'),
(908, 18, 908, 'Silos'),
(909, 11, 909, 'Silvania'),
(910, 8, 910, 'Silvia'),
(911, 21, 911, 'Simacota'),
(912, 11, 912, 'Simijaca'),
(913, 4, 913, 'Simití'),
(914, 22, 914, 'Sincelejo'),
(915, 22, 915, 'Sincé'),
(916, 12, 916, 'Sipí'),
(917, 15, 917, 'Sitionuevo'),
(918, 11, 918, 'Soacha'),
(919, 5, 919, 'Soatá'),
(920, 5, 920, 'Socha'),
(921, 21, 921, 'Socorro'),
(922, 5, 922, 'Socotá'),
(923, 5, 923, 'Sogamoso'),
(924, 7, 924, 'Solano'),
(925, 2, 925, 'Soledad'),
(926, 7, 926, 'Solita'),
(927, 5, 927, 'Somondoco'),
(928, 1, 928, 'Sonsón'),
(929, 1, 929, 'Sopetrán'),
(930, 4, 930, 'Soplaviento'),
(931, 11, 931, 'Sopó'),
(932, 5, 932, 'Sora'),
(933, 5, 933, 'Soracá'),
(934, 5, 934, 'Sotaquirá'),
(935, 8, 935, 'Sotara (Paispamba)'),
(936, 17, 936, 'Sotomayor (Los Andes)'),
(937, 21, 937, 'Suaita'),
(938, 2, 938, 'Suan'),
(939, 13, 939, 'Suaza'),
(940, 11, 940, 'Subachoque'),
(941, 8, 941, 'Sucre'),
(942, 21, 942, 'Sucre'),
(943, 22, 943, 'Sucre'),
(944, 11, 944, 'Suesca'),
(945, 11, 945, 'Supatá'),
(946, 6, 946, 'Supía'),
(947, 21, 947, 'Suratá'),
(948, 11, 948, 'Susa'),
(949, 5, 949, 'Susacón'),
(950, 5, 950, 'Sutamarchán'),
(951, 11, 951, 'Sutatausa'),
(952, 5, 952, 'Sutatenza'),
(953, 8, 953, 'Suárez'),
(954, 23, 954, 'Suárez'),
(955, 26, 955, 'Sácama'),
(956, 5, 956, 'Sáchica'),
(957, 11, 957, 'Tabio'),
(958, 12, 958, 'Tadó'),
(959, 4, 959, 'Talaigua Nuevo'),
(960, 9, 960, 'Tamalameque'),
(961, 25, 961, 'Tame'),
(962, 17, 962, 'Taminango'),
(963, 17, 963, 'Tangua'),
(964, 32, 964, 'Taraira'),
(965, 1, 965, 'Tarazá'),
(966, 13, 966, 'Tarqui'),
(967, 1, 967, 'Tarso'),
(968, 5, 968, 'Tasco'),
(969, 26, 969, 'Tauramena'),
(970, 11, 970, 'Tausa'),
(971, 13, 971, 'Tello'),
(972, 11, 972, 'Tena'),
(973, 15, 973, 'Tenerife'),
(974, 11, 974, 'Tenjo'),
(975, 5, 975, 'Tenza'),
(976, 18, 976, 'Teorama'),
(977, 13, 977, 'Teruel'),
(978, 13, 978, 'Tesalia'),
(979, 11, 979, 'Tibacuy'),
(980, 5, 980, 'Tibaná'),
(981, 5, 981, 'Tibasosa'),
(982, 11, 982, 'Tibirita'),
(983, 18, 983, 'Tibú'),
(984, 10, 984, 'Tierralta'),
(985, 13, 985, 'Timaná'),
(986, 8, 986, 'Timbiquí'),
(987, 8, 987, 'Timbío'),
(988, 5, 988, 'Tinjacá'),
(989, 5, 989, 'Tipacoque'),
(990, 4, 990, 'Tiquisio (Puerto Rico)'),
(991, 1, 991, 'Titiribí'),
(992, 5, 992, 'Toca'),
(993, 11, 993, 'Tocaima'),
(994, 11, 994, 'Tocancipá'),
(995, 5, 995, 'Toguí'),
(996, 1, 996, 'Toledo'),
(997, 18, 997, 'Toledo'),
(998, 22, 998, 'Tolú'),
(999, 22, 999, 'Tolú Viejo'),
(1000, 21, 1000, 'Tona'),
(1001, 5, 1001, 'Topagá'),
(1002, 11, 1002, 'Topaipí'),
(1003, 8, 1003, 'Toribío'),
(1004, 24, 1004, 'Toro'),
(1005, 5, 1005, 'Tota'),
(1006, 8, 1006, 'Totoró'),
(1007, 26, 1007, 'Trinidad'),
(1008, 24, 1008, 'Trujillo'),
(1009, 2, 1009, 'Tubará'),
(1010, 10, 1010, 'Tuchín'),
(1011, 24, 1011, 'Tulúa'),
(1012, 17, 1012, 'Tumaco'),
(1013, 5, 1013, 'Tunja'),
(1014, 5, 1014, 'Tunungua'),
(1015, 4, 1015, 'Turbaco'),
(1016, 4, 1016, 'Turbaná'),
(1017, 1, 1017, 'Turbo'),
(1018, 5, 1018, 'Turmequé'),
(1019, 5, 1019, 'Tuta'),
(1020, 5, 1020, 'Tutasá'),
(1021, 26, 1021, 'Támara'),
(1022, 1, 1022, 'Támesis'),
(1023, 17, 1023, 'Túquerres'),
(1024, 11, 1024, 'Ubalá'),
(1025, 11, 1025, 'Ubaque'),
(1026, 11, 1026, 'Ubaté'),
(1027, 24, 1027, 'Ulloa'),
(1028, 11, 1028, 'Une'),
(1029, 12, 1029, 'Unguía'),
(1030, 12, 1030, 'Unión Panamericana (ÁNIMAS)'),
(1031, 1, 1031, 'Uramita'),
(1032, 16, 1032, 'Uribe'),
(1033, 14, 1033, 'Uribia'),
(1034, 1, 1034, 'Urrao'),
(1035, 14, 1035, 'Urumita'),
(1036, 2, 1036, 'Usiacuri'),
(1037, 1, 1037, 'Valdivia'),
(1038, 10, 1038, 'Valencia'),
(1039, 21, 1039, 'Valle de San José'),
(1040, 23, 1040, 'Valle de San Juan'),
(1041, 27, 1041, 'Valle del Guamuez'),
(1042, 9, 1042, 'Valledupar'),
(1043, 1, 1043, 'Valparaiso'),
(1044, 7, 1044, 'Valparaiso'),
(1045, 1, 1045, 'Vegachí'),
(1046, 23, 1046, 'Venadillo'),
(1047, 1, 1047, 'Venecia'),
(1048, 11, 1048, 'Venecia (Ospina Pérez)'),
(1049, 5, 1049, 'Ventaquemada'),
(1050, 11, 1050, 'Vergara'),
(1051, 24, 1051, 'Versalles'),
(1052, 21, 1052, 'Vetas'),
(1053, 11, 1053, 'Viani'),
(1054, 1, 1054, 'Vigía del Fuerte'),
(1055, 24, 1055, 'Vijes'),
(1056, 18, 1056, 'Villa Caro'),
(1057, 8, 1057, 'Villa Rica'),
(1058, 5, 1058, 'Villa de Leiva'),
(1059, 18, 1059, 'Villa del Rosario'),
(1060, 27, 1060, 'Villagarzón'),
(1061, 11, 1061, 'Villagómez'),
(1062, 23, 1062, 'Villahermosa'),
(1063, 6, 1063, 'Villamaría'),
(1064, 4, 1064, 'Villanueva'),
(1065, 14, 1065, 'Villanueva'),
(1066, 21, 1066, 'Villanueva'),
(1067, 26, 1067, 'Villanueva'),
(1068, 11, 1068, 'Villapinzón'),
(1069, 23, 1069, 'Villarrica'),
(1070, 16, 1070, 'Villavicencio'),
(1071, 13, 1071, 'Villavieja'),
(1072, 11, 1072, 'Villeta'),
(1073, 11, 1073, 'Viotá'),
(1074, 5, 1074, 'Viracachá'),
(1075, 16, 1075, 'Vista Hermosa'),
(1076, 6, 1076, 'Viterbo'),
(1077, 21, 1077, 'Vélez'),
(1078, 11, 1078, 'Yacopí'),
(1079, 17, 1079, 'Yacuanquer'),
(1080, 13, 1080, 'Yaguará'),
(1081, 1, 1081, 'Yalí'),
(1082, 1, 1082, 'Yarumal'),
(1083, 1, 1083, 'Yolombó'),
(1084, 1, 1084, 'Yondó (Casabe)'),
(1085, 26, 1085, 'Yopal'),
(1086, 24, 1086, 'Yotoco'),
(1087, 24, 1087, 'Yumbo'),
(1088, 4, 1088, 'Zambrano'),
(1089, 21, 1089, 'Zapatoca'),
(1090, 15, 1090, 'Zapayán (PUNTA DE PIEDRAS)'),
(1091, 1, 1091, 'Zaragoza'),
(1092, 24, 1092, 'Zarzal'),
(1093, 5, 1093, 'Zetaquirá'),
(1094, 11, 1094, 'Zipacón'),
(1095, 11, 1095, 'Zipaquirá'),
(1096, 15, 1096, 'Zona Bananera (PRADO - SEVILLA)'),
(1097, 18, 1097, 'Ábrego'),
(1098, 13, 1098, 'Íquira'),
(1099, 5, 1099, 'Úmbita'),
(1100, 11, 1100, 'Útica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colores`
--

CREATE TABLE `colores` (
  `COL_ID` int(11) NOT NULL,
  `COL_CAT_ID` int(11) NOT NULL,
  `COL_CODIGO` varchar(45) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registraran los colores asignados a cada categoria de negocios. para diseñar cada interfas distinta';

--
-- Volcado de datos para la tabla `colores`
--

INSERT INTO `colores` (`COL_ID`, `COL_CAT_ID`, `COL_CODIGO`) VALUES
(13, 1, '#dc3545');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `COM_ID` int(11) NOT NULL,
  `COM_NEG_ID` int(11) DEFAULT NULL,
  `COM_USU_ID` int(11) NOT NULL,
  `COM_EVE_ID` int(11) DEFAULT NULL,
  `COM_FECHA` date NOT NULL,
  `COM_COMENTARIO` varchar(500) CHARACTER SET utf8 NOT NULL,
  `COM_ESTADO` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'publicado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='en esta entidad se registraran los comentarios acerca de la ';

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`COM_ID`, `COM_NEG_ID`, `COM_USU_ID`, `COM_EVE_ID`, `COM_FECHA`, `COM_COMENTARIO`, `COM_ESTADO`) VALUES
(40, 3, 2, NULL, '2018-12-10', 'muy buen sitio, me gusto mucho lo recomendare.', 'publicado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `DE_ID` int(11) NOT NULL,
  `DE_P_ID` int(11) NOT NULL,
  `DE_CODIGO` int(11) NOT NULL,
  `DE_NOMBRE` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta tabla se registraran los departamentos de cada pais y sus respectivos codigos.';

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`DE_ID`, `DE_P_ID`, `DE_CODIGO`, `DE_NOMBRE`) VALUES
(1, 1, 5, 'ANTIOQUIA'),
(2, 1, 8, 'ATLÁNTICO'),
(3, 1, 11, 'BOGOTÁ, D.C.'),
(4, 1, 13, 'BOLÍVAR'),
(5, 1, 15, 'BOYACÁ'),
(6, 1, 17, 'CALDAS'),
(7, 1, 18, 'CAQUETÁ'),
(8, 1, 19, 'CAUCA'),
(9, 1, 20, 'CESAR'),
(10, 1, 23, 'CÓRDOBA'),
(11, 1, 25, 'CUNDINAMARCA'),
(12, 1, 27, 'CHOCÓ'),
(13, 1, 41, 'HUILA'),
(14, 1, 44, 'LA GUAJIRA'),
(15, 1, 47, 'MAGDALENA'),
(16, 1, 50, 'META'),
(17, 1, 52, 'NARIÑO'),
(18, 1, 54, 'NORTE DE SANTANDER'),
(19, 1, 63, 'QUINDIO'),
(20, 1, 66, 'RISARALDA'),
(21, 1, 68, 'SANTANDER'),
(22, 1, 70, 'SUCRE'),
(23, 1, 73, 'TOLIMA'),
(24, 1, 76, 'VALLE DEL CAUCA'),
(25, 1, 81, 'ARAUCA'),
(26, 1, 85, 'CASANARE'),
(27, 1, 86, 'PUTUMAYO'),
(28, 1, 88, 'ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CA'),
(29, 1, 91, 'AMAZONAS'),
(30, 1, 94, 'GUAINÍA'),
(31, 1, 95, 'GUAVIARE'),
(32, 1, 97, 'VAUPÉS'),
(33, 1, 99, 'VICHADA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pedido`
--

CREATE TABLE `detalle_pedido` (
  `DETA_ID` int(11) NOT NULL,
  `DETA_PE_ID` int(11) NOT NULL,
  `DETA_PROD_ID` int(11) NOT NULL,
  `DETA_PRECIO_UNITARIO` int(11) NOT NULL,
  `DETA_CANTIDAD` int(11) NOT NULL DEFAULT '1',
  `DETA_PRECIO_TOTAL` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registrara a detalle el pedido realizado ';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dias`
--

CREATE TABLE `dias` (
  `DIA_ID` int(11) NOT NULL,
  `DIA_CODIGO` int(11) NOT NULL,
  `DIA_NOMBRE` varchar(10) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta tabla se registraran los dias de la semana para el horario eventos o promociones de ser nesesario.';

--
-- Volcado de datos para la tabla `dias`
--

INSERT INTO `dias` (`DIA_ID`, `DIA_CODIGO`, `DIA_NOMBRE`) VALUES
(1, 0, 'Lunes'),
(2, 1, 'Martes'),
(3, 2, 'Miercoles'),
(4, 3, 'Jueves'),
(5, 4, 'Viernes'),
(6, 5, 'Sabado'),
(7, 6, 'Domingo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio`
--

CREATE TABLE `domicilio` (
  `DOM_ID` int(11) NOT NULL,
  `DOM_NEG_ID` int(11) NOT NULL,
  `DOM_SERVICIO` char(2) CHARACTER SET utf8 NOT NULL DEFAULT 'no',
  `DOM_VALOR_MINIMO` int(11) DEFAULT NULL,
  `DOM_VALOR_DOMICILIO` int(11) DEFAULT NULL,
  `DOM_HORA_INICIO_WEB` time NOT NULL,
  `DOM_HORA_FIN_WEB` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registrara los datos de los negocios los ';

--
-- Volcado de datos para la tabla `domicilio`
--

INSERT INTO `domicilio` (`DOM_ID`, `DOM_NEG_ID`, `DOM_SERVICIO`, `DOM_VALOR_MINIMO`, `DOM_VALOR_DOMICILIO`, `DOM_HORA_INICIO_WEB`, `DOM_HORA_FIN_WEB`) VALUES
(2, 3, 'si', 15000, 5000, '09:00:00', '16:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

CREATE TABLE `eventos` (
  `EVE_ID` int(11) NOT NULL,
  `EVE_NEG_ID` int(11) NOT NULL,
  `EVE_CA_ID` int(11) NOT NULL,
  `EVE_CI_ID` int(11) NOT NULL,
  `EVE_NOMBRE` varchar(200) CHARACTER SET utf8 NOT NULL,
  `EVE_DESCRIPCION` varchar(500) CHARACTER SET utf8 NOT NULL,
  `EVE_INFORMACION_ADICIONAL` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `EVE_DIRECCION` varchar(200) CHARACTER SET utf8 NOT NULL,
  `EVE_TELEFONO` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `EVE_CELULAR` varchar(20) CHARACTER SET utf8 NOT NULL,
  `EVE_FECHA_INICIO` date NOT NULL,
  `EVE_HORA_INICIO` time NOT NULL,
  `EVE_FECHA_FIN` date NOT NULL,
  `EVE_HORA_FIN` time NOT NULL,
  `EVE_FOTO_1` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `EVE_FOTO_2` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `EVE_ESTADO` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'publicado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se podra registrar todas las eventos hechos por los usuarios dueños de negocios.';

--
-- Volcado de datos para la tabla `eventos`
--

INSERT INTO `eventos` (`EVE_ID`, `EVE_NEG_ID`, `EVE_CA_ID`, `EVE_CI_ID`, `EVE_NOMBRE`, `EVE_DESCRIPCION`, `EVE_INFORMACION_ADICIONAL`, `EVE_DIRECCION`, `EVE_TELEFONO`, `EVE_CELULAR`, `EVE_FECHA_INICIO`, `EVE_HORA_INICIO`, `EVE_FECHA_FIN`, `EVE_HORA_FIN`, `EVE_FOTO_1`, `EVE_FOTO_2`, `EVE_ESTADO`) VALUES
(25, 3, 1, 428, 'ee', 'ee', 'ee', '', '', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 'Vista/Files/Files-negocios/1234567890123456789/2018091501345793141.png', 'Vista/Files/Files-negocios/1234567890123456789/2018091721115986819.png', 'Activo'),
(26, 3, 1, 428, 'ww', '', '', '', '', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 'Vista/Files/Files-examples/Slide/img1.jpg', 'Vista/Files/Files-examples/Slide/img2.jpg', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favoritos`
--

CREATE TABLE `favoritos` (
  `FAV_ID` int(11) NOT NULL,
  `FAV_NEG_ID` int(11) NOT NULL,
  `FAV_USU_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='en esta entidad el usuario va poder registrar sus sitios fav';

--
-- Volcado de datos para la tabla `favoritos`
--

INSERT INTO `favoritos` (`FAV_ID`, `FAV_NEG_ID`, `FAV_USU_ID`) VALUES
(37, 3, 5),
(38, 3, 4),
(41, 3, 4),
(48, 4, 2),
(58, 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `GEN_ID` int(11) NOT NULL,
  `GEN_NOMBRE` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `GEN_SEUDO` char(1) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta tabla se registraran todos los generos.';

--
-- Volcado de datos para la tabla `genero`
--

INSERT INTO `genero` (`GEN_ID`, `GEN_NOMBRE`, `GEN_SEUDO`) VALUES
(1, 'Hombre', 'M'),
(2, 'Mujer', 'F');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `HOR_ID` int(11) NOT NULL,
  `HOR_NEG_ID` int(11) NOT NULL,
  `HOR_DIA` int(11) NOT NULL,
  `HOR_HT_ID` int(11) NOT NULL,
  `HOR_J1_HORA_INICIO` time DEFAULT NULL,
  `HOR_J1_HORA_FIN` time DEFAULT NULL,
  `HOR_J2_HORA_INICIO` time DEFAULT NULL,
  `HOR_J2_HORA_FIN` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registrara el horario de cada negocio esp';

--
-- Volcado de datos para la tabla `horario`
--

INSERT INTO `horario` (`HOR_ID`, `HOR_NEG_ID`, `HOR_DIA`, `HOR_HT_ID`, `HOR_J1_HORA_INICIO`, `HOR_J1_HORA_FIN`, `HOR_J2_HORA_INICIO`, `HOR_J2_HORA_FIN`) VALUES
(2, 3, 7, 1, '44:00:00', '24:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario_tipo`
--

CREATE TABLE `horario_tipo` (
  `HT_ID` int(11) NOT NULL,
  `HT_NOMBRE` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `HT_DESCRIPCION` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registrara los tipos de horarios disponibles para los negocios.';

--
-- Volcado de datos para la tabla `horario_tipo`
--

INSERT INTO `horario_tipo` (`HT_ID`, `HT_NOMBRE`, `HT_DESCRIPCION`) VALUES
(1, 'Continuo', 'Jornada laboral continua.'),
(2, '24 Horas', 'Los 7 dias de la semana.'),
(3, 'Doble Jornada', 'Jornada laboral en la mañana y en la tarde');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `intereses`
--

CREATE TABLE `intereses` (
  `IN_ID` int(11) NOT NULL,
  `IN_USU_ID` int(11) NOT NULL,
  `IN_CA_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta tabla se registraran los intereses de los usuarios con el unico objetivo de recomendarle sitios de su gusto.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `MEN_ID` int(11) NOT NULL,
  `MEN_USU_ID` int(11) NOT NULL,
  `MEN_NEG_ID` int(11) NOT NULL,
  `MEN_MENSAJE` varchar(500) CHARACTER SET utf8 NOT NULL,
  `MEN_FECHA` date NOT NULL,
  `MEN_HORA` time NOT NULL,
  `MEN_ESTADO` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT 'enviado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta tabla se registraran todos los mensajes entre los usuarios y los negocios y respuestas de los pedidos.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes_automaticos`
--

CREATE TABLE `mensajes_automaticos` (
  `MEN_ID` int(11) NOT NULL,
  `MEN_NEG` int(11) NOT NULL,
  `MEN_MENSAJE` varchar(500) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta tabla se registraran todos los mensajes automaticos de los sitios.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `negocio`
--

CREATE TABLE `negocio` (
  `NEG_ID` int(11) NOT NULL,
  `NEG_SUB_CATEGORIA_ID` int(11) NOT NULL,
  `NEG_CI_ID` int(11) NOT NULL,
  `NEG_USU_ID` int(11) NOT NULL,
  `NEG_LOGO` varchar(300) CHARACTER SET utf8 NOT NULL,
  `NEG_NOMBRE` varchar(100) CHARACTER SET utf8 NOT NULL,
  `NEG_ESLOGAN` varchar(50) CHARACTER SET utf8 NOT NULL,
  `NEG_DESCRIPCION` varchar(500) CHARACTER SET utf8 NOT NULL,
  `NEG_DIRECCION` varchar(300) CHARACTER SET utf8 NOT NULL,
  `NEG_TELEFONO` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `NEG_CELULAR` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `NEG_WHATSAPP` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `NEG_SERVICIOS` varchar(500) CHARACTER SET utf8 NOT NULL,
  `NEG_LATITUD` varchar(255) CHARACTER SET utf8 NOT NULL,
  `NEG_LONGITUD` varchar(255) CHARACTER SET utf8 NOT NULL,
  `NEG_FOTO_1` varchar(300) CHARACTER SET utf8 NOT NULL,
  `NEG_FOTO_2` varchar(300) CHARACTER SET utf8 NOT NULL,
  `NEG_FOTO_3` varchar(300) CHARACTER SET utf8 NOT NULL,
  `NEG_NIT` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `NEG_RUT` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `NEG_ULT_UPDATE` date DEFAULT NULL,
  `NEG_ESTADO` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'activo',
  `NEG_VISITAS` int(11) DEFAULT '0',
  `NEG_FOLDER` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='en esta entidad se registraran, editaran y eliminaran todos los negocios con su debida ciudad e Informacion nesesaria.';

--
-- Volcado de datos para la tabla `negocio`
--

INSERT INTO `negocio` (`NEG_ID`, `NEG_SUB_CATEGORIA_ID`, `NEG_CI_ID`, `NEG_USU_ID`, `NEG_LOGO`, `NEG_NOMBRE`, `NEG_ESLOGAN`, `NEG_DESCRIPCION`, `NEG_DIRECCION`, `NEG_TELEFONO`, `NEG_CELULAR`, `NEG_WHATSAPP`, `NEG_SERVICIOS`, `NEG_LATITUD`, `NEG_LONGITUD`, `NEG_FOTO_1`, `NEG_FOTO_2`, `NEG_FOTO_3`, `NEG_NIT`, `NEG_RUT`, `NEG_ULT_UPDATE`, `NEG_ESTADO`, `NEG_VISITAS`, `NEG_FOLDER`) VALUES
(3, 1, 428, 1, 'Recursos/Files/Files-negocios/1234567890123456789/2018092700025057468.png', 'el corral', 'Las comidas mas ricas de la ciudad', 'nos especializammos en hacer las hamburguesas mas ricas de la ciudad.', 'mz j casa 6 terrazas boqeuron', '54345345', '4325354', '546354345', 'parqueadero.', '4.4137851', '-75.2638788', 'Recursos/Files/Files-negocios/1234567890123456789/2018092700045510351.png', 'Recursos/Files/Files-negocios/1234567890123456789/2018092700053230070.png', 'Recursos/Files/Files-negocios/1234567890123456789/2018092700055751345.png', '232326321', '313521351', '0000-00-00', '', 0, '1234567890123456789'),
(4, 2, 428, 5, '', 'gsdgvsda', 'sdgsadfsda', '', '', NULL, NULL, NULL, '', '', '', '', '', '', NULL, NULL, NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opciones_pago`
--

CREATE TABLE `opciones_pago` (
  `OPT_ID` int(11) NOT NULL,
  `OPT_NEG_ID` int(11) NOT NULL,
  `OPT_TIP_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registrara todas las opciones de pago que acepten los negocios.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `PAG_ID` int(11) NOT NULL,
  `PAG_NEG_ID` int(11) NOT NULL,
  `PAG_SUS_ID` int(11) NOT NULL,
  `PAG_FECHA_PAGO` date NOT NULL,
  `PAG_HORA_PAGO` time NOT NULL,
  `PAG_FECHA_TERMINO` date NOT NULL,
  `PAG_HORA_TERMINO` time NOT NULL,
  `PAG_ESTADO` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'activo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `P_ID` int(11) NOT NULL,
  `P_CODIGO` int(11) NOT NULL,
  `P_NOMBRE` varchar(40) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta tabla se registraran todos los paises.';

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`P_ID`, `P_CODIGO`, `P_NOMBRE`) VALUES
(1, 169, 'COLOMBIA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `PE_ID` int(11) NOT NULL,
  `PE_NEG_ID` int(11) NOT NULL,
  `PE_USU_ID` int(11) NOT NULL,
  `PE_ID_GENERADO` varchar(50) CHARACTER SET utf8 NOT NULL,
  `PE_FECHA` date NOT NULL,
  `PE_HORA` time NOT NULL,
  `PE_TOTAL` int(11) NOT NULL,
  `PE_VALOR_DOMICILIO` varchar(45) CHARACTER SET utf8 NOT NULL,
  `PE_COMENTARIO_USU` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `PE_DIRECCION` varchar(300) CHARACTER SET utf8 NOT NULL,
  `PE_ESTADO` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'pedido',
  `PE_BARRIO` varchar(100) CHARACTER SET utf8 NOT NULL,
  `PE_TELEFONO` int(10) NOT NULL,
  `PE_TIPO_PAGO` varchar(20) CHARACTER SET utf8 NOT NULL,
  `PE_EFECTIVO_PAGO` int(11) DEFAULT NULL,
  `PE_FACTURA_EMPRESA` char(2) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `PE_NOMBRE_EMPRESA` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `PE_NIT` char(30) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se van a registrar todos los pedidos que se realizen por parte del usuario con rol ''usuario''.';

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`PE_ID`, `PE_NEG_ID`, `PE_USU_ID`, `PE_ID_GENERADO`, `PE_FECHA`, `PE_HORA`, `PE_TOTAL`, `PE_VALOR_DOMICILIO`, `PE_COMENTARIO_USU`, `PE_DIRECCION`, `PE_ESTADO`, `PE_BARRIO`, `PE_TELEFONO`, `PE_TIPO_PAGO`, `PE_EFECTIVO_PAGO`, `PE_FACTURA_EMPRESA`, `PE_NOMBRE_EMPRESA`, `PE_NIT`) VALUES
(44, 3, 2, '17861224032', '2018-11-22', '23:11:42', 110000, '5000', '', 'vjvbjbhjbj', 'pedido', 'hnkubnkbkb', 546543, 'Efectivo', 150000, 'no', '', ''),
(45, 3, 2, '10030013217', '2018-11-24', '17:11:57', 43978, '5000', 'bfdgdf', 'dfgdfgdffgsdfgsdf', 'pedido', 'fgdfgdfsgsdf', 1131353, 'Efectivo', 49000, 'no', '', ''),
(52, 3, 2, '38643021252', '2018-12-10', '22:12:42', 21998, '5000', 'sadasd', 'asdasdasdsdasdas', 'pedido', 'asdasdasdasd', 111111, 'Efectivo', 121212121, 'no', '', ''),
(53, 3, 2, '69022201238', '2018-12-15', '00:12:33', 21978, '5000', '', 'dsfsdfssdfsdfsd', 'registrado', 'sdfsdfdasf', 51531, 'Efectivo', 2147483647, 'no', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `PROD_ID` int(11) NOT NULL,
  `PROD_CAP_ID` int(11) NOT NULL,
  `PROD_NOMBRE` varchar(100) CHARACTER SET utf8 NOT NULL,
  `PROD_PRECIO` int(11) NOT NULL,
  `PROD_ESPECIFICACIONES` varchar(300) CHARACTER SET utf8 NOT NULL,
  `PROD_INFO_AD` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `PROD_ESTADO` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'activo',
  `PROD_FOTO` varchar(300) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta tabla se podran registrar todos los productos de los';

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`PROD_ID`, `PROD_CAP_ID`, `PROD_NOMBRE`, `PROD_PRECIO`, `PROD_ESPECIFICACIONES`, `PROD_INFO_AD`, `PROD_ESTADO`, `PROD_FOTO`) VALUES
(19, 20, 'coco', 22, 'sadasd', 'sadas', 'Activo', 'Recursos/Files/Files-negocios/1234567890123456789/2018091301224682742.png'),
(20, 20, 'shh', 2, 's', 's', 'Activo', 'Recursos/Files/Files-negocios/1234567890123456789/2018091301355713544.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promociones`
--

CREATE TABLE `promociones` (
  `PRO_ID` int(11) NOT NULL,
  `PRO_NEG_ID` int(11) NOT NULL,
  `PRO_CA_ID` int(11) NOT NULL,
  `PRO_NOMBRE` varchar(100) CHARACTER SET utf8 NOT NULL,
  `PRO_DESCRIPCION` varchar(500) CHARACTER SET utf8 NOT NULL,
  `PRO_FECHA_INICIO` date NOT NULL,
  `PRO_HORA_INICIO` time NOT NULL,
  `PRO_FECHA_FIN` date NOT NULL,
  `PRO_HORA_FIN` time NOT NULL,
  `PRO_FOTO_1` char(255) CHARACTER SET utf8 NOT NULL,
  `PRO_FOTO_2` char(255) CHARACTER SET utf8 NOT NULL,
  `PRO_ESTADO` char(30) CHARACTER SET utf8 NOT NULL DEFAULT 'activo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='en esta entidad los negocios podran registrar todas las prom';

--
-- Volcado de datos para la tabla `promociones`
--

INSERT INTO `promociones` (`PRO_ID`, `PRO_NEG_ID`, `PRO_CA_ID`, `PRO_NOMBRE`, `PRO_DESCRIPCION`, `PRO_FECHA_INICIO`, `PRO_HORA_INICIO`, `PRO_FECHA_FIN`, `PRO_HORA_FIN`, `PRO_FOTO_1`, `PRO_FOTO_2`, `PRO_ESTADO`) VALUES
(21, 3, 1, 'DSD', '', '2019-01-02', '00:00:00', '0000-00-00', '00:00:00', 'Vista/Files/Files-examples/Slide/img1.jpg', 'Vista/Files/Files-negocios/1234567890123456789/2018091302462173194.jpeg', 'Activa'),
(22, 3, 2, 'oo', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 'Vista/Files/Files-examples/Slide/img1.jpg', 'Vista/Files/Files-examples/Slide/img2.jpg', 'Activa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puntos`
--

CREATE TABLE `puntos` (
  `PUN_ID` int(11) NOT NULL,
  `PUN_PUNTOS` int(11) NOT NULL,
  `PUN_NOMBRE` varchar(45) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registrara los puntos disponibles para calificar los sitios promociones o eventos.';

--
-- Volcado de datos para la tabla `puntos`
--

INSERT INTO `puntos` (`PUN_ID`, `PUN_PUNTOS`, `PUN_NOMBRE`) VALUES
(1, 1, 'Muy malo'),
(2, 2, 'Malo'),
(3, 3, 'Regular'),
(4, 4, 'Bueno'),
(5, 5, 'Muy bueno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recomendados`
--

CREATE TABLE `recomendados` (
  `RE_ID` int(11) NOT NULL,
  `RE_NEG_ID` int(11) NOT NULL,
  `RE_USU_ID` int(11) NOT NULL,
  `RE_SI` int(1) DEFAULT '0',
  `RE_NO` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `recomendados`
--

INSERT INTO `recomendados` (`RE_ID`, `RE_NEG_ID`, `RE_USU_ID`, `RE_SI`, `RE_NO`) VALUES
(13, 3, 1, 1, 0),
(17, 3, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `redes_sociales`
--

CREATE TABLE `redes_sociales` (
  `RED_ID` int(11) NOT NULL,
  `RED_NEG_ID` int(11) NOT NULL,
  `RED_FACEBOOK` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `RED_YOUTUBE` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `RED_INSTAGRAM` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `RED_TWITTER` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `RED_WEB_SITE` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `RED_APP_ANDROID` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `RED_APP_IOS` varchar(300) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registraran las redes sociales de cada ne';

--
-- Volcado de datos para la tabla `redes_sociales`
--

INSERT INTO `redes_sociales` (`RED_ID`, `RED_NEG_ID`, `RED_FACEBOOK`, `RED_YOUTUBE`, `RED_INSTAGRAM`, `RED_TWITTER`, `RED_WEB_SITE`, `RED_APP_ANDROID`, `RED_APP_IOS`) VALUES
(2, 3, '1', '4', '3', '2', '5', '6', '7');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE `reserva` (
  `RES_ID` int(11) NOT NULL,
  `RES_NEG_ID` int(11) NOT NULL,
  `RES_USU_ID` int(11) NOT NULL,
  `RES_RD_ID` int(11) NOT NULL,
  `RES_TR_ID` int(11) NOT NULL,
  `RES_FECHA` date NOT NULL,
  `RES_HORA` time NOT NULL,
  `RES_NUMERO_PERSONAS` int(11) NOT NULL,
  `RES_NOMBRE` varchar(45) CHARACTER SET utf8 NOT NULL,
  `RES_INFORMACION_AD` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `RES_ESTADO` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registraran las reservas hechas por los clientes a los negocios.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservas_disponibles`
--

CREATE TABLE `reservas_disponibles` (
  `RD_ID` int(11) NOT NULL,
  `RD_NOMBRE` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `RD_INFORMACION` varchar(200) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registrara el tipo de reserva que pueden registrar los negocios, ya sea una mesa o una cancha de futbol etc.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `RO_ID` int(11) NOT NULL,
  `RO_NOMBRE` varchar(45) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta tabla se registraran los roles disponibles para los usuarios del sistema con sus respectivos permisos.';

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`RO_ID`, `RO_NOMBRE`) VALUES
(1, 'usuario'),
(2, 'cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategoria`
--

CREATE TABLE `subcategoria` (
  `SUB_ID` int(11) NOT NULL,
  `SUB_CAT_ID` int(11) NOT NULL,
  `SUB_NOMBRE` char(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registraran todas las subcategorias';

--
-- Volcado de datos para la tabla `subcategoria`
--

INSERT INTO `subcategoria` (`SUB_ID`, `SUB_CAT_ID`, `SUB_NOMBRE`) VALUES
(1, 1, 'motel'),
(2, 2, 'comidas rapidas'),
(3, 3, 'billar bar'),
(4, 1, 'apartahotel'),
(5, 1, 'hostal');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `suscripcion`
--

CREATE TABLE `suscripcion` (
  `SUS_ID` int(11) NOT NULL,
  `SUS_NOMBRE` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `SUS_DESCRIPCION` varchar(500) COLLATE utf8_spanish2_ci NOT NULL,
  `SUS_PRECIO` int(11) NOT NULL,
  `SUS_DIAS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registrara todo tipo de suscripcion para los negocio, como free, premium, bronce, gold, etc.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_pago`
--

CREATE TABLE `tipos_pago` (
  `TIP_ID` int(11) NOT NULL,
  `TIP_NOMBRE` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `TIP_ICON` varchar(45) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En estta tabla se registraran todos los tipos de pago.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_reserva`
--

CREATE TABLE `tipos_reserva` (
  `TR_ID` int(11) NOT NULL,
  `TR_NOMBRE` varchar(40) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registraran los tipos de reservas disponibles al reservar mesa.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_calificacion`
--

CREATE TABLE `tipo_calificacion` (
  `TIPO_ID` int(11) NOT NULL,
  `TIPO_NOMBRE` varchar(45) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta tabla se registrara todos los tipos de calificacion posibles.';

--
-- Volcado de datos para la tabla `tipo_calificacion`
--

INSERT INTO `tipo_calificacion` (`TIPO_ID`, `TIPO_NOMBRE`) VALUES
(1, 'Ambiente'),
(2, 'Productos'),
(3, 'Atencion'),
(4, 'General');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_publico`
--

CREATE TABLE `tipo_publico` (
  `TP_ID` int(11) NOT NULL,
  `TP_NOMBRE` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `TP_EDAD_MIN` int(11) NOT NULL,
  `TP_EDAD_MAX` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registrara todos los tipos de publicos para cada categoria y para poder filtrar contenido de acuerdo a la edad y sexo.';

--
-- Volcado de datos para la tabla `tipo_publico`
--

INSERT INTO `tipo_publico` (`TP_ID`, `TP_NOMBRE`, `TP_EDAD_MIN`, `TP_EDAD_MAX`) VALUES
(1, 'Adultos hombres y mujeres', 18, 150),
(2, 'Todas las edades', 1, 150),
(3, 'Adultos hombres', 18, 150),
(4, 'Adultos mujeres', 18, 150),
(5, 'Adolecentes', 12, 20),
(6, 'Niños', 1, 12),
(7, 'Adultos mayores', 50, 150);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `USU_ID` int(11) NOT NULL,
  `USU_RO_ID` int(11) NOT NULL,
  `USU_GEN_ID` int(11) NOT NULL,
  `USU_CI_ID` int(11) NOT NULL,
  `USU_NOMBRE` varchar(30) CHARACTER SET utf8 NOT NULL,
  `USU_APELLIDO` varchar(30) CHARACTER SET utf8 NOT NULL,
  `USU_FECHA_NACIMIENTO` date NOT NULL,
  `USU_EMAIL` varchar(100) CHARACTER SET utf8 NOT NULL,
  `USU_MOVIL` varchar(30) CHARACTER SET utf8 NOT NULL,
  `USU_PASS` varchar(200) CHARACTER SET utf8 NOT NULL,
  `USU_PALABRA_CLAVE` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `USU_FOTO` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `USU_ESTRATO` int(11) DEFAULT NULL,
  `USU_VISITAS` int(11) DEFAULT NULL,
  `USU_ESTADO` varchar(20) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='en esta entidad se registrara, editaran y eliminaran  los us';

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`USU_ID`, `USU_RO_ID`, `USU_GEN_ID`, `USU_CI_ID`, `USU_NOMBRE`, `USU_APELLIDO`, `USU_FECHA_NACIMIENTO`, `USU_EMAIL`, `USU_MOVIL`, `USU_PASS`, `USU_PALABRA_CLAVE`, `USU_FOTO`, `USU_ESTRATO`, `USU_VISITAS`, `USU_ESTADO`) VALUES
(1, 2, 1, 248, 'jhonattan', 'rojas', '1994-05-18', 'jh@j', '0351351', '$2y$10$fnX1yWt2TdJamUxMBDtZ0Oh0eGtO3YVZM4Il.WigTnpMOc0mYWhKm', 'jh', 'Vista/Files/Files-examples/Usuario/Profile.png', NULL, 1, 'registrado'),
(2, 1, 1, 248, 'Santiago', 'rojas', '2018-11-28', 'san@s', '112254', '$2y$10$63sS6EQVjd/uZerT0cQ5nO1SeJMFZMDiQeLK6UEKhdnDfVU/zqjKW', 'san', 'Recursos/Files/Files-usuarios/2018121916590237509.png', NULL, 1, NULL),
(3, 2, 1, 248, 'TMX', 'rojas', '0000-00-00', 'tmxj995@gmail.com', '', '$2y$10$acXBtDqW/u5APW/u.w/KauBk.HCzJAqg5.PSZRHpUqvdphxw0SDMq', NULL, 'Vista/Files/Files-examples/Usuario/Profile.png', NULL, 0, 'registrado'),
(4, 1, 1, 248, 'alejo', '', '1965-12-09', '', '', '', NULL, NULL, NULL, NULL, NULL),
(5, 1, 2, 248, 'lisa', '', '1994-09-04', '', '', '', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas`
--

CREATE TABLE `visitas` (
  `VIS_ID` int(11) NOT NULL,
  `VIS_USU_ID` int(11) NOT NULL,
  `VIS_NEG_ID` int(11) DEFAULT NULL,
  `VIS_PRO_ID` int(11) DEFAULT NULL,
  `VIS_EVE_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='En esta entidad se registraran visitas o ''vistos'' de los u';

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `calificacion`
--
ALTER TABLE `calificacion`
  ADD PRIMARY KEY (`CAL_ID`),
  ADD KEY `FK_EVE_CAL` (`CAL_EVE_ID`),
  ADD KEY `FK_NEG_CAL` (`CAL_NEG_ID`),
  ADD KEY `FK_PRO_CAL` (`CAL_PRO_ID`),
  ADD KEY `FK_USU_CAL` (`CAL_USU_ID`),
  ADD KEY `FK_TIPO_CAL` (`CAL_TIPO`),
  ADD KEY `FK_PUN_CAL` (`CAL_PUNTO`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`CAT_ID`),
  ADD KEY `FK_TP_CAT` (`CAT_TP_ID`);

--
-- Indices de la tabla `categoria_producto`
--
ALTER TABLE `categoria_producto`
  ADD PRIMARY KEY (`CAPROD_ID`),
  ADD KEY `FK_NEG_CATPRO` (`CAPROD_NEG_ID`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`CI_ID`),
  ADD KEY `FK_DE_CI` (`CI_DE_ID`);

--
-- Indices de la tabla `colores`
--
ALTER TABLE `colores`
  ADD PRIMARY KEY (`COL_ID`),
  ADD KEY `FK_CAT_COL` (`COL_CAT_ID`);

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`COM_ID`),
  ADD KEY `FK_NEG_COM` (`COM_NEG_ID`),
  ADD KEY `FK_RELATIONSHIP_16` (`COM_EVE_ID`),
  ADD KEY `FK_USU_COM` (`COM_USU_ID`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`DE_ID`),
  ADD KEY `FK_P_DE` (`DE_P_ID`);

--
-- Indices de la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  ADD PRIMARY KEY (`DETA_ID`),
  ADD KEY `FK_PE_DETA` (`DETA_PE_ID`),
  ADD KEY `FK_PROD_DETA` (`DETA_PROD_ID`);

--
-- Indices de la tabla `dias`
--
ALTER TABLE `dias`
  ADD PRIMARY KEY (`DIA_ID`);

--
-- Indices de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD PRIMARY KEY (`DOM_ID`),
  ADD KEY `FK_NEG_DOM` (`DOM_NEG_ID`);

--
-- Indices de la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`EVE_ID`),
  ADD KEY `FK_NEG_EVE` (`EVE_NEG_ID`),
  ADD KEY `FK_CI_EVE` (`EVE_CI_ID`),
  ADD KEY `FK_CA_EVE` (`EVE_CA_ID`);

--
-- Indices de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  ADD PRIMARY KEY (`FAV_ID`),
  ADD KEY `FK_NEG_FAV` (`FAV_NEG_ID`),
  ADD KEY `FK_USU_FAV` (`FAV_USU_ID`);

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`GEN_ID`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`HOR_ID`),
  ADD KEY `FK_NEG_HOR` (`HOR_NEG_ID`),
  ADD KEY `FK_DIA_HOR` (`HOR_DIA`),
  ADD KEY `FK_TIPO_HOR` (`HOR_HT_ID`);

--
-- Indices de la tabla `horario_tipo`
--
ALTER TABLE `horario_tipo`
  ADD PRIMARY KEY (`HT_ID`);

--
-- Indices de la tabla `intereses`
--
ALTER TABLE `intereses`
  ADD PRIMARY KEY (`IN_ID`),
  ADD KEY `FK_USU_IN_idx` (`IN_USU_ID`),
  ADD KEY `FK_CA_IN_idx` (`IN_CA_ID`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`MEN_ID`),
  ADD KEY `FK_NEG_MEN` (`MEN_NEG_ID`),
  ADD KEY `FK_USU_MEN` (`MEN_USU_ID`);

--
-- Indices de la tabla `mensajes_automaticos`
--
ALTER TABLE `mensajes_automaticos`
  ADD PRIMARY KEY (`MEN_ID`),
  ADD KEY `FK_NEG` (`MEN_NEG`);

--
-- Indices de la tabla `negocio`
--
ALTER TABLE `negocio`
  ADD PRIMARY KEY (`NEG_ID`),
  ADD KEY `FK_CI_NEG` (`NEG_CI_ID`),
  ADD KEY `FK_USU_NEG` (`NEG_USU_ID`),
  ADD KEY `FK_SUB_NEG` (`NEG_SUB_CATEGORIA_ID`);

--
-- Indices de la tabla `opciones_pago`
--
ALTER TABLE `opciones_pago`
  ADD PRIMARY KEY (`OPT_ID`),
  ADD KEY `FK_TIP_OPT` (`OPT_TIP_ID`),
  ADD KEY `FK_NEG_OPT` (`OPT_NEG_ID`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`PAG_ID`),
  ADD KEY `FK_NEG_PAG` (`PAG_NEG_ID`),
  ADD KEY `FK_SUS_PAG` (`PAG_SUS_ID`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`P_ID`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`PE_ID`),
  ADD KEY `FK_USU_PE` (`PE_USU_ID`),
  ADD KEY `FK_NEG_PE` (`PE_NEG_ID`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`PROD_ID`),
  ADD KEY `FK_CAP_PROD` (`PROD_CAP_ID`);

--
-- Indices de la tabla `promociones`
--
ALTER TABLE `promociones`
  ADD PRIMARY KEY (`PRO_ID`),
  ADD KEY `FK_NEG_PROMO` (`PRO_NEG_ID`),
  ADD KEY `FK_CA_ID` (`PRO_CA_ID`);

--
-- Indices de la tabla `puntos`
--
ALTER TABLE `puntos`
  ADD PRIMARY KEY (`PUN_ID`);

--
-- Indices de la tabla `recomendados`
--
ALTER TABLE `recomendados`
  ADD PRIMARY KEY (`RE_ID`),
  ADD KEY `FK_NEG_REC` (`RE_NEG_ID`),
  ADD KEY `FK_USU_REC` (`RE_USU_ID`);

--
-- Indices de la tabla `redes_sociales`
--
ALTER TABLE `redes_sociales`
  ADD PRIMARY KEY (`RED_ID`),
  ADD KEY `FK_NEG_RED` (`RED_NEG_ID`);

--
-- Indices de la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`RES_ID`),
  ADD KEY `FK_NEG_RES` (`RES_NEG_ID`),
  ADD KEY `FK_USU_RES` (`RES_USU_ID`),
  ADD KEY `FK_TR_RES` (`RES_TR_ID`),
  ADD KEY `FK_RD_RES` (`RES_RD_ID`);

--
-- Indices de la tabla `reservas_disponibles`
--
ALTER TABLE `reservas_disponibles`
  ADD PRIMARY KEY (`RD_ID`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`RO_ID`);

--
-- Indices de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD PRIMARY KEY (`SUB_ID`),
  ADD KEY `FK_CAT_SUB` (`SUB_CAT_ID`);

--
-- Indices de la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
  ADD PRIMARY KEY (`SUS_ID`);

--
-- Indices de la tabla `tipos_pago`
--
ALTER TABLE `tipos_pago`
  ADD PRIMARY KEY (`TIP_ID`);

--
-- Indices de la tabla `tipos_reserva`
--
ALTER TABLE `tipos_reserva`
  ADD PRIMARY KEY (`TR_ID`);

--
-- Indices de la tabla `tipo_calificacion`
--
ALTER TABLE `tipo_calificacion`
  ADD PRIMARY KEY (`TIPO_ID`);

--
-- Indices de la tabla `tipo_publico`
--
ALTER TABLE `tipo_publico`
  ADD PRIMARY KEY (`TP_ID`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`USU_ID`),
  ADD KEY `FK_CI_USU` (`USU_CI_ID`),
  ADD KEY `FK_RO_USU` (`USU_RO_ID`),
  ADD KEY `FK_GEN_USU` (`USU_GEN_ID`);

--
-- Indices de la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD PRIMARY KEY (`VIS_ID`),
  ADD KEY `FK_EVE_VIS` (`VIS_EVE_ID`),
  ADD KEY `FK_NEG_VIS` (`VIS_NEG_ID`),
  ADD KEY `FK_PRO_VIS` (`VIS_PRO_ID`),
  ADD KEY `FK_USU_VIS` (`VIS_USU_ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `calificacion`
--
ALTER TABLE `calificacion`
  MODIFY `CAL_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `CAT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `categoria_producto`
--
ALTER TABLE `categoria_producto`
  MODIFY `CAPROD_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `CI_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1101;

--
-- AUTO_INCREMENT de la tabla `colores`
--
ALTER TABLE `colores`
  MODIFY `COL_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `COM_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `DE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  MODIFY `DETA_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dias`
--
ALTER TABLE `dias`
  MODIFY `DIA_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  MODIFY `DOM_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `eventos`
--
ALTER TABLE `eventos`
  MODIFY `EVE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  MODIFY `FAV_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `genero`
--
ALTER TABLE `genero`
  MODIFY `GEN_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `horario`
--
ALTER TABLE `horario`
  MODIFY `HOR_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `horario_tipo`
--
ALTER TABLE `horario_tipo`
  MODIFY `HT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `intereses`
--
ALTER TABLE `intereses`
  MODIFY `IN_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `MEN_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mensajes_automaticos`
--
ALTER TABLE `mensajes_automaticos`
  MODIFY `MEN_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `negocio`
--
ALTER TABLE `negocio`
  MODIFY `NEG_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `opciones_pago`
--
ALTER TABLE `opciones_pago`
  MODIFY `OPT_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `PAG_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `P_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `PE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `PROD_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `promociones`
--
ALTER TABLE `promociones`
  MODIFY `PRO_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `puntos`
--
ALTER TABLE `puntos`
  MODIFY `PUN_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `recomendados`
--
ALTER TABLE `recomendados`
  MODIFY `RE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `redes_sociales`
--
ALTER TABLE `redes_sociales`
  MODIFY `RED_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `reserva`
--
ALTER TABLE `reserva`
  MODIFY `RES_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reservas_disponibles`
--
ALTER TABLE `reservas_disponibles`
  MODIFY `RD_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `RO_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  MODIFY `SUB_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
  MODIFY `SUS_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipos_pago`
--
ALTER TABLE `tipos_pago`
  MODIFY `TIP_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipos_reserva`
--
ALTER TABLE `tipos_reserva`
  MODIFY `TR_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_calificacion`
--
ALTER TABLE `tipo_calificacion`
  MODIFY `TIPO_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_publico`
--
ALTER TABLE `tipo_publico`
  MODIFY `TP_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `USU_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `visitas`
--
ALTER TABLE `visitas`
  MODIFY `VIS_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `calificacion`
--
ALTER TABLE `calificacion`
  ADD CONSTRAINT `FK_EVE_CAL` FOREIGN KEY (`CAL_EVE_ID`) REFERENCES `eventos` (`EVE_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_NEG_CAL` FOREIGN KEY (`CAL_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_PRO_CAL` FOREIGN KEY (`CAL_PRO_ID`) REFERENCES `promociones` (`PRO_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_PUNTO_CAL` FOREIGN KEY (`CAL_TIPO`) REFERENCES `puntos` (`PUN_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TIPO_CAL` FOREIGN KEY (`CAL_TIPO`) REFERENCES `tipo_calificacion` (`TIPO_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_USU_CAL` FOREIGN KEY (`CAL_USU_ID`) REFERENCES `usuarios` (`USU_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD CONSTRAINT `FK_TP_CAT` FOREIGN KEY (`CAT_TP_ID`) REFERENCES `tipo_publico` (`TP_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `categoria_producto`
--
ALTER TABLE `categoria_producto`
  ADD CONSTRAINT `FK_NEG_CATPRO` FOREIGN KEY (`CAPROD_NEG_ID`) REFERENCES `negocio` (`NEG_ID`);

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `FK_DE_CI` FOREIGN KEY (`CI_DE_ID`) REFERENCES `departamento` (`DE_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `colores`
--
ALTER TABLE `colores`
  ADD CONSTRAINT `FK_CAT_COL` FOREIGN KEY (`COL_CAT_ID`) REFERENCES `categoria` (`CAT_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `FK_NEG_COM` FOREIGN KEY (`COM_NEG_ID`) REFERENCES `negocio` (`NEG_ID`),
  ADD CONSTRAINT `FK_RELATIONSHIP_16` FOREIGN KEY (`COM_EVE_ID`) REFERENCES `eventos` (`EVE_ID`),
  ADD CONSTRAINT `FK_USU_COM` FOREIGN KEY (`COM_USU_ID`) REFERENCES `usuarios` (`USU_ID`);

--
-- Filtros para la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD CONSTRAINT `FK_P_DE` FOREIGN KEY (`DE_P_ID`) REFERENCES `pais` (`P_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  ADD CONSTRAINT `FK_PE_DETA` FOREIGN KEY (`DETA_PE_ID`) REFERENCES `pedido` (`PE_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_PROD_DETA` FOREIGN KEY (`DETA_PROD_ID`) REFERENCES `productos` (`PROD_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD CONSTRAINT `FK_NEG_DOM` FOREIGN KEY (`DOM_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD CONSTRAINT `FK_CA_EVE` FOREIGN KEY (`EVE_CA_ID`) REFERENCES `categoria` (`CAT_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_CI_EVE` FOREIGN KEY (`EVE_CI_ID`) REFERENCES `ciudad` (`CI_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_NEG_EVE` FOREIGN KEY (`EVE_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `favoritos`
--
ALTER TABLE `favoritos`
  ADD CONSTRAINT `FK_NEG_FAV` FOREIGN KEY (`FAV_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_USU_FAV` FOREIGN KEY (`FAV_USU_ID`) REFERENCES `usuarios` (`USU_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `horario`
--
ALTER TABLE `horario`
  ADD CONSTRAINT `FK_DIA_HOR` FOREIGN KEY (`HOR_DIA`) REFERENCES `dias` (`DIA_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_NEG_HOR` FOREIGN KEY (`HOR_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TIPO_HOR` FOREIGN KEY (`HOR_HT_ID`) REFERENCES `horario_tipo` (`HT_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `intereses`
--
ALTER TABLE `intereses`
  ADD CONSTRAINT `FK_CA_IN` FOREIGN KEY (`IN_CA_ID`) REFERENCES `categoria` (`CAT_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_USU_IN` FOREIGN KEY (`IN_USU_ID`) REFERENCES `usuarios` (`USU_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD CONSTRAINT `FK_NEG_MEN` FOREIGN KEY (`MEN_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_USU_MEN` FOREIGN KEY (`MEN_USU_ID`) REFERENCES `usuarios` (`USU_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `mensajes_automaticos`
--
ALTER TABLE `mensajes_automaticos`
  ADD CONSTRAINT `FK_NEG` FOREIGN KEY (`MEN_NEG`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `negocio`
--
ALTER TABLE `negocio`
  ADD CONSTRAINT `FK_CI_NEG` FOREIGN KEY (`NEG_CI_ID`) REFERENCES `ciudad` (`CI_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_SUB_NEG` FOREIGN KEY (`NEG_SUB_CATEGORIA_ID`) REFERENCES `subcategoria` (`SUB_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_USU_NEG` FOREIGN KEY (`NEG_USU_ID`) REFERENCES `usuarios` (`USU_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `opciones_pago`
--
ALTER TABLE `opciones_pago`
  ADD CONSTRAINT `FK_NEG_OPT` FOREIGN KEY (`OPT_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TIP_OPT` FOREIGN KEY (`OPT_TIP_ID`) REFERENCES `tipos_pago` (`TIP_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD CONSTRAINT `FK_NEG_PAG` FOREIGN KEY (`PAG_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_SUS_PAG` FOREIGN KEY (`PAG_SUS_ID`) REFERENCES `suscripcion` (`SUS_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `FK_NEG_PE` FOREIGN KEY (`PE_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_USU_PE` FOREIGN KEY (`PE_USU_ID`) REFERENCES `usuarios` (`USU_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `FK_CAP_PROD` FOREIGN KEY (`PROD_CAP_ID`) REFERENCES `categoria_producto` (`CAPROD_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `promociones`
--
ALTER TABLE `promociones`
  ADD CONSTRAINT `FK_CA_ID` FOREIGN KEY (`PRO_CA_ID`) REFERENCES `categoria` (`CAT_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_NEG_PROMO` FOREIGN KEY (`PRO_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `recomendados`
--
ALTER TABLE `recomendados`
  ADD CONSTRAINT `FK_NEG_RE` FOREIGN KEY (`RE_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_USU_RE` FOREIGN KEY (`RE_USU_ID`) REFERENCES `usuarios` (`USU_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `redes_sociales`
--
ALTER TABLE `redes_sociales`
  ADD CONSTRAINT `FK_NEG_RED` FOREIGN KEY (`RED_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `FK_NEG_RES` FOREIGN KEY (`RES_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_RD_RES` FOREIGN KEY (`RES_RD_ID`) REFERENCES `reservas_disponibles` (`RD_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TR_RES` FOREIGN KEY (`RES_TR_ID`) REFERENCES `tipos_reserva` (`TR_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_USU_RES` FOREIGN KEY (`RES_USU_ID`) REFERENCES `usuarios` (`USU_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD CONSTRAINT `FK_CAT_SUB` FOREIGN KEY (`SUB_CAT_ID`) REFERENCES `categoria` (`CAT_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `FK_CI_USU` FOREIGN KEY (`USU_CI_ID`) REFERENCES `ciudad` (`CI_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_GEN_USU` FOREIGN KEY (`USU_GEN_ID`) REFERENCES `genero` (`GEN_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_RO_USU` FOREIGN KEY (`USU_RO_ID`) REFERENCES `rol` (`RO_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD CONSTRAINT `FK_EVEN_VIS` FOREIGN KEY (`VIS_EVE_ID`) REFERENCES `eventos` (`EVE_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_NEG_VIS` FOREIGN KEY (`VIS_NEG_ID`) REFERENCES `negocio` (`NEG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_PRO_VIS` FOREIGN KEY (`VIS_PRO_ID`) REFERENCES `promociones` (`PRO_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_USU_VIS` FOREIGN KEY (`VIS_USU_ID`) REFERENCES `usuarios` (`USU_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
