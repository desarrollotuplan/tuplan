//Variables para almacenar los datos para redimensionar y recortar imagenes
var x,y,w,h,input,width,height,maxWidth,maxHeight,resizedImage,croppedImage,jcrop;

var jx, jy, ratio, jtype;

$(document).ready(function(){
    
    $(document).on('click','.load-images',function(){
        var input = $(this).attr('data-input');
        $(input).click();
    });

    $(document).on('click','.btn-change-image',function(){
        saveImage($(this));
    });


    /*Función para detectar cambio del valor del input tipo file y validar si es un archivo de imagen y si cumple con la resolución minima obligatoria*/
    $(document).on('change','.img-jcrop',function(){
        if(this.files && this.files[0]){
            var input = this,
                width = '',
                height = '',
                _URL = window.URL || window.webkitURL,
                file = this.files[0],
                type = this.files[0].type;

            if($.inArray(type,jtype) != -1){
                var image = new Image();
                image.onload = function(){
                    width = this.width;
                    height = this.height;
                    if(this.width >= jx && this.height >= jy){
                        giveData();
                    }else{
                        var a = {type:'swal',title:'Atención',text:'Archivo no válido, solo se aceptan archivos de minimo anchjo de '+jx+'px y minimo de alto de '+jy+'px',icon:'warning'};
                        output(a);
                    }
                    
                };
                image.src = _URL.createObjectURL(file);

                function giveData(){
                    var data = {type:type,width:width,height:height,input:input};
                    validateImage(data);
                }
            }else{
                var a = {type:'swal',title:'Atención',text:'Archivo no válido, solo se aceptan archivos de imagen .JPG o .PNG',icon:'warning'};
                output(a);
            }
        }
    });
});
// Setear los requisitos de las imagenes
function setValuesJcrop(data){
    jx = data.width;
    jy = data.height;
    jtype = data.typeImages;
    ratio = data.aspectRatio;
}


//Función para validar la resolución de la imagen y pasar datos para hacer el cross X Y
function validateImage(data){
    if(jx < data.width && jy < data.height){
        if(data.width > 1000){
            var width = 1000,
                height = 0;
        }else{
            var width = data.width,
                height = data.height;
        }
        var data2 = {
            input:data.input,
            width:width,
            height:height,
            minWidth:jx,
            minHeight:jy,
            aspectRatio:ratio
        };
        imagePreview(data2);
    }else{
        var a = {type:'swal',title:'Atención',text:'La imagen debe tener una resolución mínima de 300 x 150 píxeles.',icon:'warning'};
        output(a);
    }

}


//Función para visualizar imagen que se selecciono en el input tipo file y hacer el cross para recortar la imagen
function imagePreview(data){
    if(jx > data.width){
        width = jx;
    }else{
        width = data.width;
    }
    input = data.input;
    height = data.height;
    maxWidth = data.minWidth;
    maxHeight = data.minHeight;
    var reader = new FileReader();
    reader.onload = function(e){
        $('.load-image-js').html('<img src="'+e.target.result+'" id="img-upload" width="'+width+'" height="auto" alt="Imagen"/>');
    }
    reader.readAsDataURL(data.input.files[0]);
    
    setTimeout(function(){
        $('#img-upload').Jcrop({
            onSelect: showCoords,
            minSize: [data.minWidth,data.minHeight],
            setSelect: [0,0,data.minWidth,data.minHeight],
            allowSelect: false,
            aspectRatio: data.aspectRatio,
        });
    },200);
    
    //Función para obtener las coordenadas del cross en la imagen
    function showCoords(c){
        // Se puede acceder a las variables asi:
        // cx, cy, c.x2, c.y2, cw, ch
        x = c.x,
        y = c.y,
        w = c.w,
        h = c.h;
    };
}

//Función para cerrar el modal en el que se recorta la imagen y ejecutar la redimensión y recorte de la imagen
function saveImage(inputFile){
    var inputVal = $(inputFile).attr('data-input');
    var imgUp = $(inputFile).attr('data-img');
    var data = {input:input,width:width,height:height};
    resizeImage(data);
    setTimeout(function(){
        var data2 = {image:resizedImage,x:x,y:y,w:w,h:h,mW:maxWidth,mH:maxHeight};
            cutImage(data2);
        setTimeout(function(){
            if($(imgUp).attr('data-background') == 'true'){
                $(imgUp).css('background-image', 'url('+croppedImage+')');
            }else{
                $(imgUp).attr('src',croppedImage);
            }
            $(inputVal).attr('value',croppedImage);
        },300);
    },300);
    output({closeModal:true});
    if($(inputFile).hasClass('sendLoad')){
        setTimeout(function(){
            $(inputFile).parents('form').submit();
        },700);
    }
}

//Función para redimensionar imagenes con canvas
function resizeImage(data){
	$('body').append('<div><canvas id="canvas" class="d-none"></canvas></div>');
	file = data.input.files[0];
	reader = new FileReader();
	reader.readAsDataURL(file);
	reader.onloadend = function () {
		tmpImg = new Image();
        tmpImg.src = reader.result;
        tmpImg.onload = function() {
        	var MAX_WIDTH = data.width;
            var MAX_HEIGHT = data.height;
        	var tempW = tmpImg.width;
            var tempH = tmpImg.height;
             if(tempW > tempH){
                if(tempW > MAX_WIDTH){
                    tempH *= MAX_WIDTH / tempW;
                    tempW = MAX_WIDTH;
                }
            }else{
                if(tempH > MAX_HEIGHT){
                    tempW *= MAX_HEIGHT / tempH;
                    tempH = MAX_HEIGHT;
                }
            }
			var canvas = document.getElementById('canvas');
		    canvas.width = tempW;
		    canvas.height = tempH;
		    var ctx = canvas.getContext("2d");
		    ctx.drawImage(this, 0, 0, tempW, tempH);
		    resizedImage = canvas.toDataURL();
            $('body #canvas').parents('div').remove();
		}
	}
}

//Función para recortar imagenes con canvas
function cutImage(data){
	$('body').append('<div><canvas id="canvas" class="d-none"></canvas></div>');
	var canvas = document.getElementById('canvas'),
        context = canvas.getContext("2d"),
        img = new Image();
    canvas.width = data.mW;
    canvas.height = data.mH;
    img.src = data.image;
    img.onload = function(){
        context.drawImage(this,data.x,data.y,data.w,data.h,0,0,data.mW,data.mH);
        croppedImage = canvas.toDataURL();
    }
    $('body #canvas').parents('div').remove();
}
