/*
 ========================================================= 
    codigo escrito por jhonattan rojas montoya 
 ========================================================= 
    TU PLAN V:1.8 SEPTIEMBRE 4 DE MARZO DEL 2018
 ========================================================= 
*/

$(document).ready(function(){
    
    var select = null;
    
    // INPUTS Y TEXTAREAS
    $('body').on('focus','input[type="text"], input[type="number"], input[type="email"],  input[type="password"], textarea',function(){
        $(this).parents('.form-group-tpl').children('label').addClass('label active focus');
    });
    
    $('body').on('blur','.form-group-tpl input, .form-group-tpl textarea',function(){
        $(this).parents('.form-group-tpl').children('label').removeClass('focus');
        if(this.value === ''){
            if(!$(this).parents('.form-group-tpl').children('label').hasClass('placeholder')){
                $(this).parents('.form-group-tpl').children('label').removeClass('active');
            }
        }
    });
    
    $('body').on('click','.form-group-tpl .checkbox, .form-group-tpl .radio',function(){
        $(this).parents('.form-group-tpl').children('input[type="checkbox"], input[type="radio"]').click();
    });
    
    // MOSTRAR SELECTS OPTIONS
    $('body').on('click','.form-group-tpl .select',function(){
        if(!$(this).attr('disabled')){
            var c = $('.form-group-tpl .select.active');
            if(select !== $(this)){
                c.removeClass('active').children('.options').removeClass('active');
                c.children('span').removeClass('icon-minus').addClass('icon-plus');
                $(this).addClass('active').children('.options').addClass('active');
                $(this).children('span').removeClass('icon-plus').addClass('icon-minus');
                select = $(this);
            }
        }
    });
    
    // ESCONDER OPTIONS
    $('body').on('click',function(e){
        var c = $('.form-group-tpl .select.active');
        if(select && !select.is(e.target) && select.has(e.target).length === 0){
            c.removeClass('active').children('.options').removeClass('active');
            c.children('span').removeClass('icon-minus').addClass('icon-plus');
        }
    });
    
    // ESCONDER OPTIONS
    $('body').on('click','.form-group-tpl .select.active',function(e){
        var c = $('.form-group-tpl .select.active');
        $(c).removeClass('active').children('.options').removeClass('active');
        $(c).children('span').removeClass('icon-minus').addClass('icon-plus');
    });
    
    // ESCOJER OPCION DE LOS SELECT
    $('body').on('click','.select ol li',function(){
        var o = $(this).attr('data-list');
        var text = $(this).html();
        $(this).parents('.select').children('label').html(text);
        $(this).parents('.form-group-tpl').children('input').val(o).change();
    });
    
    // QUITAR COLOR ROJO DE LOS INPUT ERROR
    $('body').on('change','input.error',function(){
        $(this).removeClass('error').closest('.form-group-tpl').removeClass('error');
    });
    // QUITAR COLOR ROJO DE LOS INPUT ERROR
    $('body').on('keyup','input.error',function(){
        $(this).removeClass('error').closest('.form-group-tpl').removeClass('error');
    });
    
});