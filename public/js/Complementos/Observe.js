const observer = new MutationObserver((mutationList) => {
    mutationList.forEach((mutation) => {
        if(mutation.addedNodes.length > 0){
            // CODIGO PENDIENTE POR SI SE QUIERE AÑADOR Y DAR UNA NUMERACION A UN TR
        }
        if(mutation.removedNodes.length > 0){
            if(mutation.target.nodeName.toUpperCase() == 'TBODY'){
                var table = mutation.target.offsetParent;
                enumTable(table);
            }
        }
    });
});

// Elementos
const tables = document.querySelector('table.dinamic');
const observerOptions = {
    attributes: true,
    childList: true,
    subtree: true
}
if(tables){
    observer.observe(tables,observerOptions);
}