$(document).ready(function(){
    // Saber si entro por un  link o navego desde el inicio
    var prevUrl = document.referrer;
    
    // Si encuentra el host nuestro en el historial anterior devolvera la cadena y si no evolvera un -1
    var origin = prevUrl.indexOf(window.location.host);
    
    // AGREGAR URL AL LINK
    $('body').on('click','.addLink',function(){
        if($(this).attr('data-route')){
            var add = $(this).attr('data-route');
        }else{
            var add = $(this).attr('data-route-link');
        }
        var loc = window.location;
        var url = loc.pathname.substring(0, loc.pathname.length)+'/'+add;
        history.pushState(null,'',url);
    });
    
    // NUEVA URL SIN RECARGAR
    $('body').on('click','.newLink',function(){
        if($(this).attr('data-route')){
            var add = $(this).attr('data-route');
        }else{
            var add = $(this).attr('data-route-link');
        }
        var url = window.laravel.url+'/'+add;
        history.pushState(null,'',url);
    });

    // DEVOLVER LINK
    $('body').on('click','.historyback',function(){
        console.log(origin);
        if( origin === -1) {
            // Ir a la página anterior
            var loc = window.location;
            // Extraigo la ultima parte del slash de la url
            var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/'));
            // Remplazar la url
            history.replaceState(null,'',pathName);

            origin = 1;
        }else{
            window.history.back(-1);
        }
    });
});