$(document).ready(function(){
    
    $('.animation-scroll').scroll(function(){

        var windowHeight = $(window).height() - 30;
        var contenido = $(".transition-obj");
        
        if(contenido.length > 0){
            for (var i = 0; i < contenido.length; i++) {
                
                var obj = $(contenido[i]);
                var position = obj.offset();
                var animation = contenido[i].dataset.animateType;
                
                if(windowHeight >= position.top){
                    obj.addClass(animation);
                }else{
                    obj.removeClass(animation);
                }
            }
        }

    });
    
    // ANIMACIONES EN GENERAL
    $('body').on('click','[data-animate="true"]',function(){
        var i = $(this);
        var elem = i.find('[data-animate-type]');
        var from = elem.attr('data-animate-type');
        elem.addClass(from);
        setTimeout(function(){
            i.attr('data-animate','false');
        },20);
    });
    
     // QUITAR ANIMACION
    $('body').on('click',function(e){
        var c = $('[data-animate="false"]');
        if(!c.is(e.target) && c.has(e.target).length === 0){
            var elem = c.find('[data-animate-type]');
            var from = elem.attr('data-animate-type');
            elem.removeClass(from);
            c.attr('data-animate','true');
        }
    });
    
});
