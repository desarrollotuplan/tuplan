$(document).ready(function(){
    // MOSTRAR MENU
    $('body').on('click','.menuIcon[data-expanded="false"]',function(){
        $('body').css('overflow','hidden');
        var span = $(this).children('span');
        $('.menuHidden').addClass('menuHeight');
        setTimeout(function(){
            span.removeClass('icon-home').addClass('icon-cross');
            $('.menuHidden').addClass('menuWidth');
            $('.nav').addClass('navWidth');
            $('main.main').addClass('mainOpacity');
            $('.menuIcon').attr('data-expanded','true');
        },500);
    });
    
    // OCULTAR MENU
    $('body').on('click','.menuIcon[data-expanded="true"]',function(){
        var v = $('.menuIcon').attr('data-expanded');
        var s = $('body').attr('data-area');
        if(v == 'true'){
            $('.menuHidden').removeClass('menuWidth');
            $('.nav').removeClass('navWidth');
            setTimeout(function(){
                $('.menuHidden').removeClass('menuHeight');
                $('main.main').removeClass('mainOpacity');
                span.removeClass('icon-cross ocultar').addClass('icon-home');
                $('.menuIcon').attr('data-expanded','false');
                if(s == 'false' || s == undefined){
                    $('body').css('overflow','auto');
                }
            },300);
            var span = $('.menuIcon').children('span');
        }
        
    });
    
    // LIST-GROUP
    $('body').on('click','.list-group-dark .list-group-item',function(){
        $('.list-group-dark').find('.list-group-item').removeClass('active');
        $(this).addClass('active');
    });
    
    // MENU SIDEBAR - MAIN PANEL
    $('body').on('click','.main-panel .close-sidebar',function(){
        if($(this).attr('aria-expanded') != 'true') {
            $(this).attr('aria-expanded','true');
            $('.menu-sidebar').addClass('sidebar-mini');
            $('.main-panel').addClass('expanded');
        }else{
            $(this).attr('aria-expanded','false');
            $('.main-panel').removeClass('expanded');
            $('.menu-sidebar').removeClass('sidebar-mini');
        }
    });
    
    // MENU SIDEBAR - MAIN PANEL
    $('body').on('click','.load-details',function(){
        if($('.main-panel .close-sidebar').attr('aria-expanded') == 'true') {
            if($(window).width() <= 500){
                $('.main-panel .close-sidebar').attr('aria-expanded','false');
                $('.main-panel').removeClass('expanded');
                $('.menu-sidebar').removeClass('sidebar-mini');
            }
        }
    });
    
});