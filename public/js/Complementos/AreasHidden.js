$(document).ready(function(){
   
    $('body').on('click','[data-show-area]',function(){
        var  d = {
            area : $(this).attr('data-show-area'),
            direction : $(this).attr('data-show-direction'),
            background : $(this).attr('data-show-background'),
        }
        areaShow(d);
    });
    
    $('body').on('click','.close-area',function(){
        var datos = {
            area : $(this).attr('data-hidden-area'),
            attr : $(this).attr('data-hidden-attr'),
            background : $(this).attr('data-hidden-background'),
        }
        areaHidden(datos);
    });
});

// FUNCION PARA MOSTTRAR AREAS ESCONDIAS / AGREGAR MAS FUNCIONES Y ATRIBUTOS DE SER NESESARIO
function areaShow(datos){
    $(datos.area).fadeIn(100,function(){
        var ele = $(datos.area).children('[data-hidden-transition]');
        
        if(ele.length > 0){
            for (var i = 0; i < ele.length; i++) {
                var d = {
                    nele : $(ele[i]),
                    time : parseInt(ele[i].dataset.hiddenTransition),
                    flex : ele[i].dataset.hiddenFlex,
                }
                showLoad(d);
            }
        }
    });
    $(datos.area).addClass(datos.direction);
    setTimeout(function(){
       $(datos.area).addClass(datos.background);
    },400);
    $(datos.area).find('.close-area').attr('data-hidden-attr',datos.direction).attr('data-hidden-background',datos.background);
    
    var scroll = $(datos.area).attr('data-scroll-body');
    
    if(scroll == 'true'){
        $('body').css('overflow','hidden');
    }
    
    $('body').attr('data-area','true')
}

// FUNCION PARA OCULTAR AREAS
function areaHidden(datos){
    $(datos.area).removeClass(datos.attr).fadeOut(2000);
    $(datos.area).children('[data-hidden-transition],[data-hidden-load]').fadeOut();
    setTimeout(function(){
       $(datos.area).removeClass(datos.background);
    },100);
    $(datos.area).find('.close-area').removeAttr('data-hidden-attr data-hidden-background');
    $(datos.area).find('[data-hidden-cont]').removeAttr('data-hidden-cont');
    
    var scroll = $(datos.area).attr('data-scroll-body');
    
    if(scroll == 'true'){
        $('body').css('overflow','auto');
    }
    
    $('body').attr('data-area','false');
}

// MOSTRAR CONTENIDO CON SUAVIDAD O ANIMACION
function showLoad(datos){
    setTimeout(function(){
        if(datos.flex == 'true'){
            $(datos.nele).fadeIn().css('display','flex');
        }else{
            $(datos.nele).fadeIn();
        }
    },datos.time);
}

/*
    =============================================
    codigo escrito por jhonattan rojas montoya 
    =============================================
*/