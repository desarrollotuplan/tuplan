$(document).ready(function(){
    
    // CARGAR CONTENIDO POST - URL
    $('body').on('click','.loadPost',function(){
        loadPost($(this));
    });
    
    // CARGAR CONTENIDO GET - URL
    $('body').on('click','.loadGet',function(){
        loadGet($(this));
    });
    
    // EVITAR AGREGAR #
    $('body').on('click','.uri',function(e){ e.preventDefault(); });
    
    // ENVIAR FORMULARIOS POR AJAX
    $('body').on('submit','.sendAjax',function(e){
        e.preventDefault();
        sendForm($(this));
    });
    
    // ENVIAR FORMULARIOS POR AJAX CUANDO CAMBIEN
    $('body').on('change','.sendChangeAjax',function(e){
        e.preventDefault();
        sendForm($(this));
    });
    
    // METHODOS PUT Y DELETE
    $('body').on('click','[data-method]',function(){ confirmMethodsForm($(this)) });
    
    // ACTIVAR FORMULARIO
    $('body').on('click','.active-form',function(){ 
        $(this).parents('form').find('input, textarea, button, select, .select').removeAttr('disabled');
    });
    
    // CAMBIAR COLOR Y TEXTO DE LOS BOTONES DE FORMULARIOS INACTIVOS Y EL TIPO
    $('body').on('click','.active-form',function(){
        var t = $(this);
        t.removeClass($(this).attr('data-remove')+' active-form').addClass('btn-success').text('Guardar');
        setTimeout(function(){  t.attr('type','submit');   },300);
    });
    
    // Eliminar contenido del modal tanpronto de cierre
    $("#modalAsinc").on('hidden.bs.modal', function () {
        remove = $(this).find('.modal-dialog').attr('data-remove');
        $(this).find('.modal-dialog').removeClass(remove).attr('data-remove',null).find('.modal-content').html(null);
    });
    
});