var alerts = {
    makeAlert : function(data){
        var options = {};
        var screen = $(window).width();
        options.type = data.typeAlert;
        
        if(data.title){
            options.title = data.title;
        }
        
        if(data.text){
            options.text = data.text;
        }
        
        switch(data.position){
            case 'right-top':
                options.from = 'top', options.align = 'right';
            break;
        }
        
        switch(data.typeAlert){
            case 'success':
                options.icon = 'icon-checkmark';
            break;
            case 'info':
                options.icon = 'icon-info';
            break;
            case 'warning':
                options.icon = 'icon-warning';
            break;
            case 'danger':
                options.icon = 'icon-cross';
            break;
        }
        
        if(screen <= 500){
           options.from = 'bottom', options.align = 'center';
        }
        
        alerts.showAlert(options);
    },
    showAlert : function(options){
        $.notify({
            icon: options.icon,
            title: options.title,
            message: options.text,
            url: options.url,
            target: options.target
        }, {
            type: options.type,
            timer: 300,
            placement: {
                from: options.from,
                align: options.align
            }
        });
    }
}