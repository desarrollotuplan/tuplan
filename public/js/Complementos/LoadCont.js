// ENVIAR FORMULARIOS POR POST AJAX
function sendForm(form){
    if(form.hasClass('route-link')){
        var loc = window.location;
        var pathname = loc.pathname.substring(0, loc.pathname.length)+'/'+form.attr('action');
    }else{
        var pathname = '/'+form.attr('action');
    }
    $.ajax({
        url : window.laravel.url+pathname,
        type : 'POST',
        data : form.serialize(),
        headers : { 'X-CSRF-TOKEN' : window.laravel.token }
    })
    .done(function(r){
        if($.isArray(r)){
            for(var i = 0; i < r.length; i++){
                output(r[i],form);
            }
        }else{
            output(r,form);
        }
    })
    .fail(function(r){
        console.log(r);
    });
}

// CARGAR CONTENIDO POR METODO POST
function loadPost(data){
    if(data.attr('data-route-link')){
        var loc = window.location;
        var pathname = loc.pathname.substring(0, loc.pathname.length)+'/'+data.attr('data-route-link');
    }else{
        var pathname = '/'+data.attr('data-route');
    }
    $.ajax({
        url : window.laravel.url+pathname,
        type : 'POST',
        data : {data: data.attr('data-dinf')},
        headers: { 'X-CSRF-TOKEN': window.laravel.token }
    })
    .done(function(r){
        if($.isArray(r)){
            for(var i = 0; i < r.length; i++){
                output(r[i],data);
            }
        }else{
            output(r,data);
        }
    })
    .fail(function(r){
        console.log(r);
    });
}

// CARGAR CONTENIDO POR GET
function loadGet(data){
    if(data.attr('data-route-link')){
        var loc = window.location;
        var pathname = loc.pathname.substring(0, loc.pathname.length)+'/'+data.attr('data-route-link');
    }else{
        var pathname = '/'+data.attr('data-route');
    }
    $.ajax({
        url : window.laravel.url+pathname,
        type : 'GET',
        data : {data: data.attr('data-dinf')},
        headers: { 'X-CSRF-TOKEN': window.laravel.token }
    })
    .done(function(r){
        if($.isArray(r)){
            for(var i = 0; i < r.length; i++){
                output(r[i],data);
            }
        }else{
            output(r,data);
        }
    })
    .fail(function(r){
        console.log(r);
    });
}

// SALIDA RESPUESTA DEL SERVIDOR
function output(r,form = null){
    switch(r.type){
        
        case 'modal':
            if(!r.modalType){ r.modalType = 'modal-dark'; }
            $('#modalAsinc .modal-dialog').addClass(r.modalType).attr('data-remove',r.modalType).find('.modal-content').html(r.view);
            $('#modalAsinc').modal('show');
        break;
        
        case 'div':
            $(r.idDiv).html(r.view);
        break;
            
        case 'addCont':
            $(r.div).append(r.view);
        break;
            
        case 'deleteCont':
            $(r.div).remove();
        break;
            
        case 'swal':
            makeSwal(r);
        break;
            
        case 'redirect':
            if(r.route == '/'){
                window.location = window.laravel.url;
            }else{
                window.location = window.laravel.url+'/'+r.route;
            }
        break;
            
        case 'alertView':
            alerts.makeAlert(r);
            $(r.idDiv).html(r.view);
        break;
            
        case 'alert':
            alerts.makeAlert(r);
        break;
            
        case 'jcrop':
            setValuesJcrop(r);
        break;
    }
    if(r.errorInput){ errorInputs(r.errorInput); }
    if(r.closeModal){ $('.modal').modal('hide'); }
    if(r.disabledForm){
        form.find('input, textarea, select, .select, button[type=button]').attr('disabled',true); 
        var clas = form.find('button[data-remove]').attr('data-remove');
        form.find('button[data-remove]').removeClass('btn-success').addClass(clas+' active-form').attr('type','button').text('Editar');
    }
    if(r.resetForm){ 
        form.find('input, textarea').not('input[type="hidden"]').val(null);
    }
}

// ALERTAS SWAL
function makeSwal(data){
    swal({
        title: data.title,
        text: data.text,
        icon: data.icon
    })
}

// ERROR EN LOS INPUT
function errorInputs(data){
    for (var i = 0; i < data.length; i++) {
        $('input[name="'+data[i]+'"]').addClass('error').closest('.form-group-tpl').addClass('error');
    }
}

// CONFIRMAR FORMULARIOS DINAMICOS
function confirmMethodsForm(method){
    var confirm = method.attr('data-confirm');
    if(confirm){
        swal({
            title : 'Atencion',
            icon : 'warning',
            text : confirm,
            buttons : true,
            dangerMode: true
        })
        .then((willDelete) => {
            if(willDelete) {
                createForm(method);
            }else{
                return;
            }
        });
    }else{
        createForm(method);
    }
}

// CREAR FORMULARIOS DINAMICOS
function createForm(link){
    var httpMethod = link.attr('data-method').toUpperCase();

    if ( $.inArray(httpMethod, ['PUT', 'DELETE']) === - 1 ) {
        return;
    }
    
    if(link.attr('data-route')){
        var url = link.attr('data-route');
        var clase = '';
    }else{
        var url = link.attr('data-route-link');
        var clase = 'route-link';
    }

    // Se crea el formulario
    var form = 
        $('<form>', {
            'method': 'POST',
            'action': url,
            'class' : 'sendAjax hid delete '+clase,
        });

    var token = 
        $('<input>', {
            'type': 'hidden',
            'name': 'csrf_token',
            'value': window.laravel.token
        });

    var hiddenInput =
        $('<input>', {
            'name': '_method',
            'type': 'hidden',
            'value': link.attr('data-method').toUpperCase()
        });

    form.append(token, hiddenInput).appendTo('body').submit();
    
    $('form.delete').remove();
    if(link.attr('data-remove')){
        link.remove();
    }
}

// Poner clases a objetos del dom
function adClass(obj,clase){
    $(obj).addClass(clase);
}

// Actualizar numerologia de una tabla tras ser borrado un registros / tr
function enumTable(table){
    var tr = $(table).find('tr');
    var count = 1;
    for(var i = 1; i < tr.length; i++){
        var td = $(tr)[i].children[0].textContent = count;
        count = count+1;
    }
}
