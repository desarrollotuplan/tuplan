<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Ruta pagina inicial de la app
Route::get('/','Inicio\InicioController@index')->name('/');

// Ruta para redireccionar a la pagina inicial de la app si se detecta cualquier solicitud a la ruta get /login
Route::match(['get'], 'login', function(){ return redirect('/'); });


// FORMULARIO LOGIN
Route::post('formLogin','Inicio\InicioController@formLogin')->name('formLogin');
    
// LOGIN
Route::post('login','Auth\LoginController@login');

// Login facebook
Route::get('loginFb','Auth\LoginController@facebook');

// Login google
Route::get('loginGoogle','Auth\LoginController@google');


// Pagina de negocios/sitios - usuario
Route::group(['middleware'=>'MNegocios'],function(){
    
    Route::prefix('Buscar/Sitios')->group(function(){
        
        // Detalles del negocio por post
        Route::post('{negocio}','General\NegocioController@detallesNegocio')->name('buscar.sitios');

        // Alternativa get  del sitio
        Route::get('{negocio}','General\NegocioController@sitios')->name('detalleSitio');

        // Informacion del negocio
        Route::post('{negocio}/informacionSitio','General\NegocioController@informacionSitio')->name('informacionSitio');

        // Horario negocio
        Route::post('{negocio}/horario','General\HorarioController@horarioNegocio')->name('horario');

        // Redes sociales
        Route::post('{negocio}/redesSociales','General\RedesSocialesController@redesNegocio')->name('redesSociales');

        // Productos
        Route::post('{negocio}/productos','General\ProductosController@productos')->name('productos');

        // Reseñas
        Route::post('{negocio}/resenasSitio','General\ResenasController@resenasSitio')->name('resenasSitio');

        // Mapa
        Route::post('{negocio}/mapa','viewController@mapa')->name('mapa');

        // Reservas
        Route::post('{negocio}/reservasSitio','General\NegocioReservaController@reservasNegocio')->name('reservasSitio');

        // Promociones y eventos
        Route::post('{negocio}/promosEventos','General\NegocioController@promosEventos');

        //Filtrar comentarios del negocio - Ordenar
        Route::post('{negocio}/filtrarComentarios','General\ResenasController@filtroComentarios');
    });
    
});

// Rutas con login
Route::group(['middleware' => ['auth']], function(){
    
    // LOGOUT
    Route::get('logout','Auth\LoginController@cerrarSesion')->name('logout');
     /*
    |----------------------------------------------------------------------
    | RUTAS DEL USUARIO - USUARIO
    |----------------------------------------------------------------------
    */
    Route::group(['middleware' => 'MUsuario'], function(){

        // Recomiendame
        Route::resource('Recomiendame','General\RecomendadosController');

        // Sitios
        Route::get('Sitios','General\NegocioController@sitios')->name('Sitios');

        // Promociones
        Route::get('Promociones','General\PromocionesController@promocionesUsuario')->name('Promociones');

        // Eventos
        Route::get('Eventos','General\EventosController@eventosUsuario')->name('Eventos');

        // Favoritos
        Route::get('Favoritos','General\FavoritosController@index')->name('Favoritos');
        
        // Guardados
        Route::get('Guardados','Usuario\GuardadosController@index')->name('Guardados');

        // Pedidos
        Route::get('Pedidos','viewController@pedidos')->name('Pedidos');

        // Datos personales
        Route::get('PerfilUser','General\UsuarioController@datosUsuario')->name('PerfilUser');

        // Pagina de recomendados - usuarios
        Route::group(['middleware'=>'MNegocios'],function(){

            // Detalles de los sitios recomendados
            Route::post('detallesRecomendado/{negocio}','General\RecomendadosController@detallesRecomendado');

        });

        // Middleware para filtros de busquedas - usuario
        Route::group(['middleware'=>'MFiltros'],function(){

            // Filtrar recomendados
            Route::post('buscarRecomendados','General\RecomendadosController@filtrar');

            // Filtrar sitios - post
            Route::post('Sitios/Buscar','General\NegocioController@filtrar');
            
            // Filtrar promociones - post
            Route::post('Promociones/Buscar','General\PromocionesController@filtrar');
            
            // Filtrar eventos - post
            Route::post('Eventos/Buscar','General\EventosController@filtrar');
            
            // Filtrar favoritos - post
            Route::post('Favoritos/Buscar','General\FavoritosController@filtrar');
            
        });
        
        // Filtrar sitios - get
        Route::get('Sitios/Filtrar/{ciudad}/{categoria}/{busqueda?}','General\NegocioController@buscarSitios');
        
        // Filtrar promociones - get
        Route::get('Promociones/Filtrar/{ciudad}/{categoria}','General\PromocionesController@buscarPromo');
        
        // Filtrar eventos - get 
        Route::get('Eventos/Filtrar/{ciudad}/{categoria}','General\EventosController@buscarEvento');
        
        // Filtrar favoritos - get 
        Route::get('Favoritos/Filtrar/{ciudad}/{categoria}/{busqueda?}','General\FavoritosController@buscarFavorito');
        

        // Pagina de negocios/sitios - usuario
        Route::group(['middleware'=>'MNegocios'],function(){

            Route::prefix('Sitios')->group(function(){

                // Detalles del negocio por post
                Route::post('{negocio}','General\NegocioController@detallesNegocio')->name('sitios');

                // Alternativa get  del sitio
                Route::get('{negocio}','General\NegocioController@sitios')->name('detalleSitio');

                // Informacion del negocio
                Route::post('{negocio}/informacionSitio','General\NegocioController@informacionSitio')->name('informacionSitio');

                // Horario negocio
                Route::post('{negocio}/horario','General\HorarioController@horarioNegocio')->name('horario');

                // Redes sociales
                Route::post('{negocio}/redesSociales','General\RedesSocialesController@redesNegocio')->name('redesSociales');

                // Productos
                Route::post('{negocio}/productos','General\ProductosController@productos')->name('productos');

                // Reseñas
                Route::post('{negocio}/resenasSitio','General\ResenasController@resenasSitio')->name('resenasSitio');

                // Mapa
                Route::post('{negocio}/mapa','viewController@mapa')->name('mapa');

                // Reservas
                Route::post('{negocio}/reservasSitio','General\NegocioReservaController@reservasNegocio')->name('reservasSitio');

                // Promociones y eventos
                Route::post('{negocio}/promosEventos','General\NegocioController@promosEventos');

                //Filtrar comentarios del negocio - Ordenar
                Route::post('{negocio}/filtrarComentarios','General\ResenasController@filtroComentarios');
                
            });
        });

        // Productos - Usuario
        Route::group(['middleware'=>'MProductos'],function(){

            // Detalles producto
            Route::post('Sitios/{negocio}/detallesProducto/{producto}','General\ProductosController@detallesProducto')->name('detallesProducto');

        });
        
        // Reservas - Usuario
        Route::group(['middleware'=>'MReservasDisponibles'],function(){
            
            // Realizar reserva - mostrar detalles - usuario
            Route::post('Sitios/{negocio}/detallesReservaDisponible/{reserva}','General\ReservasDisponiblesController@detallesReservaDisponible')->name('detallesReservaDisponible');

            // Buscar horas disponibles de la reserva - usuario
            Route::post('Sitios/{negocio}/buscarHorasReserva/{reserva}','General\HoraReservaController@horaReserva')->name('buscarReserva');

            // Mostrar formulario de la reserva - usuario
            Route::post('Sitios/{negocio}/formularioReserva/{reserva}','General\ReservaController@formularioReserva')->name('formularioReserva');
            

        });
        
        Route::group(['middleware'=>'MReservas'],function(){
            
            // Mostrar detalles de una reserva
            Route::resource('reserva','General\ReservaController');
            
            // Datos personales
            Route::get('Reservas','General\ReservaController@index')->name('Reservas');
            
        });
        
        // Realizar una reserva - usuario
        Route::post('reservar','General\ReservaController@reservar')->name('reservar');
        
        // Guardar datos del usuario - usuario
        Route::post('usuarioSave','General\UsuarioController@usuarioSave')->name('usuarioSave');


        // Pagina de promociones
        Route::group(['middleware'=>'MPromos'], function(){

            Route::prefix('Promociones')->group(function(){
                
                // Detalles de las promociones por post  - usuario
                Route::post('{promocion}','General\PromocionesController@detallesPromocion');

                // Detalles de las promociones por get - usuario
                Route::get('{promocion}','General\PromocionesController@promocionesUsuario')->name('promocionesGet');

                // Formulario para guardar promo - usuario
                Route::post('{promocion}/formSavePromo','Usuario\GuardadosController@formSavePromo')->name('formSavePromo');

                // Guardar promocion - usuario
                Route::post('{promocion}/savePromotion','Usuario\GuardadosController@savePromo')->name('savePromo');

            });
        });


        // Pagina de eventos - usuario
        Route::group(['middleware'=>'MEventos'], function(){
            
            Route::prefix('Eventos')->group(function(){

                // Detalles post de los eventos - usuario
                Route::post('{evento}','General\EventosController@detallesEvento')->name('detallesEvento');

                // Detalles get de los eventos - usuario
                Route::get('{evento}','General\EventosController@eventosUsuario')->name('eventosGet');

                // Formulario para guardar evento - usuario
                Route::post('{evento}/formSaveEvento','Usuario\GuardadosController@formSaveEvento')->name('formSaveEvento');

                // Guardar evento - usuario
                Route::post('{evento}/saveEvento','Usuario\GuardadosController@saveEvento')->name('saveEvento');

            });
        });

        // Middleware y rutas de promociones guardadas - usuario
        Route::group(['middleware'=>'MGuardados'],function(){

            // sitios guardados - usuario
            Route::resource('Sitios/{negocio}/guardado','Usuario\GuardadosController');
            
            // Eliminar promocion guardada - usuario
            Route::post('deleteSavePromo/{guardado}','Usuario\GuardadosController@deleteSavePromo')->name('deleteSavePromo');
            
            // Eliminar evento guardada - usuario
            Route::post('deleteSaveEvento/{guardado}','Usuario\GuardadosController@deleteSaveEvento')->name('deleteSaveEvento');
            
            // Editar guardados en general - pagina guardados
            Route::post('Guardados/edit/{guardado}','Usuario\GuardadosController@edit')->name('editarGuardado');
            
            // Actualizar guardados en general - pagina guardados
            Route::put('Guardados/{guardado}','Usuario\GuardadosController@update')->name('actualizarGuardado');
            
            // Actualizar guardados en general - pagina guardados
            Route::delete('Guardados/delete/{guardado}/{div}','Usuario\GuardadosController@eliminarGuardado')->name('eliminarGuardado');
            

        });
        
        // Filtro de guardados
        Route::post('Guardados/Buscar','Usuario\GuardadosController@filtrar');
        
        // Filtro por get
        Route::get('Guardados/Filtrar/{tipo}/{busqueda?}','Usuario\GuardadosController@buscar');

        // Middleware de favoritos del usuario
        Route::group(['middleware'=>'MFavoritos'],function(){
            
            // Favoritos
            Route::resource('Sitios/{negocio}/favoritos','General\FavoritosController');
            
        });

        // Usuario
        Route::resource('usuario','General\UsuarioController');

        // Intereses
        Route::resource('intereses','Usuario\InteresesController');
        
        // Comentarios
        Route::group(['middleware'=>'MComentario'], function(){
            
            // Comentarios
            Route::resource('Sitios/{negocio}/comentario','General\ComentariosController');
            
        });
        
        // Comentarios
        Route::group(['middleware'=>'MCalificacion'], function(){
            
            // Comentarios
            Route::resource('Sitios/{negocio}/calificacion','General\CalificacionController');
            
        });
    });

    
    /*
    |----------------------------------------------------------------------
    | RUTAS DEL USUARIO-CLIENTE
    |----------------------------------------------------------------------
    */
    
    // Rutas unicamente del cliente
    Route::group(['middleware'=>'MClient'],function(){
        
        // Sitio del cliente
        Route::get('MiNegocio','General\NegocioController@miSitio')->name('miNegocio');
        
        // Guardar imagenes de portada y logo del negocio - cliente
        Route::post('MiNegocio/saveImages','General\NegocioController@guardarImagenes');
        
        // eliminar imagenes del negocios albun cliente - cliente
        Route::delete('MiNegocio/deleteImage/{imagen}','General\NegocioController@eliminarImagenes');
        
        // Guardar imagenes del album del negocio
        Route::post('MiNegocio/saveAlbum','General\NegocioController@guardarAlmbum');
            
        // Actualizar el negocio - cliente
        Route::post('MiNegocio/Actualizar','General\NegocioController@actualizarNegocio');
        
        // Informacion del sitio - cliente
        Route::get('MiNegocio/Informacion','General\NegocioController@index');
        
        // Categoria-producto del sitio - cliente
        Route::resource('CategoriaProducto','General\CategoriaProductoController');
        
        // Redes sociales del sitio - cliente
        Route::get('MiNegocio/RedesSociales','General\RedesSocialesController@index');
        
        // Ruta perdonalizada para actualizar las redes sociales del sitio
        Route::post('MiNegocio/RedesSociales/Actualizar','General\RedesSocialesController@update');
        
        // Ruta domicilio del sitio - cliente
        Route::get('MiNegocio/Domicilios','Cliente\DomicilioController@index');
        
        // Ruta personalizada para actualizar los domicilios
        Route::put('MiNegocio/Domicilios/Actualizar','Cliente\DomicilioController@update');
        
        // Ruta reservas del sitio - cliente
        Route::get('MiNegocio/Reservas','General\NegocioReservaController@index');
        
        // Ruta personalizada para actualizar las reservas
        Route::put('MiNegocio/Reservas/Actualizar','General\NegocioReservaController@update');
        
        
        // Middleware de las categorias productos
        Route::group(['middleware'=>'MCcategoriaProducto'],function(){
            
            // Ruta get para editar categoria y parametro adicional
            Route::get('CategoriaProducto/{categoria}/edit/{data}','General\CategoriaProductoController@edit');
            
            // Agregar producto con parametro adicional
            Route::get('MiNegocio/Productos/{categoria}/{code}','General\ProductosController@create');
            
        });
        
        // Middleware de los productos
        Route::group(['middleware'=>'MCproductos'],function(){
            
            // Productos del sitio - cliente
            Route::resource('MiNegocio/Productos','General\ProductosController');
            
            // Editar producto personalizado
            Route::get('MiNegocio/Productos/{producto}/edit/{code}','General\ProductosController@edit');
            
        });
        
        // Middleware pata las horas de reserva del sitio - cliente
        Route::group(['middleware'=>'MChoraReserva'],function(){
            
            // Ruta para las horas de reserva del sitio - cliente
            Route::resource('HoraReserva','General\HoraReservaController');
            
        });
        
        
        // Middleware de los tipos de reserva disponible cliente
        Route::group(['middleware'=>'MCreservaDisponible'],function(){
            
            // Rutas para los tipos de reserva del sitio - cliente
            Route::resource('MiNegocio/ReservaDisponible','General\ReservasDisponiblesController');

            // Rutas para los tipos de reserva del sitio - cliente
            Route::get('MiNegocio/ReservaDisponible/{ReservaDisponible}/edit/{code}','General\ReservasDisponiblesController@edit');

        });
        
        
        // Middelware de las promociones - cliente
        Route::group(['middleware'=>'MCpromociones'],function(){
            
            // Ruta promociones del sitio - cliente
            Route::resource('MiNegocio/Promociones','General\PromocionesController');

            // Ruta personalzada para editar promociones
            Route::get('MiNegocio/Promociones/{Promociones}/edit/{code}','General\PromocionesController@edit');
            
        });
        
        Route::group(['middleware'=>'MCeventos'],function(){
            
            // Ruta de los eventos del sitio - cliente
            Route::resource('MiNegocio/Eventos','General\EventosController');

            // Ruta personalzada para editar eventos
            Route::get('MiNegocio/Eventos/{Eventos}/edit/{code}','General\EventosController@edit');
            
        });
        
        // Middleware del horario - cliente
        Route::group(['middleware'=>'MChorario'],function(){
            
            // Ruta del horario de atencion del sitio - negocio
            Route::resource('MiNegocio/HorarioAtencion','General\HorarioController');

            // Ruta para crear un horario para un dia de la semana
            Route::get('MiNegocio/HorarioAtencion/create/{dia}/{code}','General\HorarioController@create');

            // Confirmar el tipo de horario para agregar el horario de un dia
            Route::post('MiNegocio/HorarioAtencion/Confirmar','General\HorarioController@confirmar');

            // Ruta para editar el horario - cliente / personalizado
            Route::get('MiNegocio/HorarioAtencion/{HorarioAtencion}/edit/{code}','General\HorarioController@edit');
        });
        
        // Pagina de servicios - cliente
        Route::get('Servicios','General\NegocioController@servicios')->name('servicios');
        
        // Pagina de pedidos - cliente
        Route::get('Servicios/Pedidos','General\PedidosController@index');
        
        // Pagina de reservas - cliente
        Route::get('Servicios/Reservas','General\ReservaController@listaReservas');
        
        
        // Midleware de las reservas 
        Route::group(['middleware'=>'MCreserva'],function(){
            
            // Mostrar detalles de una reserva
            Route::post('Servicios/Reservas/detalles/{reserva}/{code}','General\ReservaController@detallesReserva');
        });
        
        
        // Perfil del cliente
        Route::get('Perfil','General\UsuarioController@datosCliente')->name('perfil');
        
        // Guardar datos del cliente
        Route::post('clienteSave','General\UsuarioController@usuarioSave')->name('clienteSave');
        
        // Carga de imagen usuario
        Route::post('perfilUsuario','General\ImagenesController@perfilUsuario');
        
        // Carga del logo del negocio
        Route::post('logo','General\ImagenesController@logo');
        
        // Carga del logo del negocio
        Route::post('portada','General\ImagenesController@portada');
        
        // Carga de las imagenes del usuario
        Route::post('fotoNegocio','General\ImagenesController@imagenesNegocio');
        
    });
    
});