<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="viewport" content="initial-scale=1">
    <title>@yield('titulo')</title>
    <link rel="stylesheet" href="{{ asset('css/Bootstrap/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/Usuario/Usuario.css') }}">
    <link rel="stylesheet" href="{{ asset('css/Animate/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/Fotorama/fotorama.css') }}">
    <link rel="stylesheet" href="{{ asset('Iconos/style.css') }}">
    @include('Layout.Plugin')
</head>
<body>
    <div class="contenedor">
        <main class="main dark text-white">
            @include('Layout.Usuario.Nav')
            @yield('contenido') <!--variable-->
        </main>
    </div>
    @include('Complementos.Modal')
    @include('Complementos.loadjs')
    @yield('masterjs')
    
    @if(session('message'))
        <script>$(document).ready(function(){ output({!! json_encode(session('message')) !!}); })</script>
    @endif
</body>
</html>