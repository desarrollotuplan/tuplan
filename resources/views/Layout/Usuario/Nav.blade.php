<header class="header-tpl light-blue">
    <div class="navMenu">
        <section class="menuHidden menu-sport-dark" role="navigation">
            <div class="menuIcon" data-expanded="false">
                <span class="icon-home"></span>
            </div>
            <nav class="nav">
                <h5 class="w-100">Menu</h5>
                <ul class="menu">
                    <li><a href="/"><span class="icon-home"></span> Inicio</a></li>
                    <li><a href="{{ route('Recomiendame.index') }}"><span class="icon-star-full"></span> Recomiendame <span class="icon-circle-left circle"></span></a></li>
                    <li><a href="{{ route('Sitios') }}"><span class="icon-office"></span> Sitios</a></li>
                    <li><a href="{{ route('Promociones') }}"><span class="icon-price-tags"></span> Promociones</a></li>
                    <li><a href="{{ route('Eventos') }}"><span class="icon-calendar"></span> Eventos</a></li>
                    <li><a href="{{ route('Favoritos') }}"><span class="icon-heart"></span> Favoritos</a></li>
<!--                    <li><a href="{{ route('Pedidos') }}"><span class="icon-cart"></span> Pedidos</a></li>-->
                    <li><a href="{{ route('Guardados') }}"><span class="icon-folder"></span> Guardados</a></li>
                    <li><a href="{{ route('Reservas') }}"><span class="icon-book"></span> Reservas</a></li>
                    <li><a href="{{ route('PerfilUser') }}"><span class="icon-user"></span> Mis datos</a></li>
                    <li><a href="#"><span class="icon-info"></span> Ayuda</a></li>
                    <li><a href="{{route('logout')}}" class="logout"><span class="icon-switch"></span> Cerrar Sesion</a></li>
                </ul>
            </nav>
        </section>
    </div>
    <div class="title">
        <h4>Tu Plan</h4>
    </div>
    <div class="img">
        <img src="{{ asset(User::foto()) }}" alt="Imagen de usuario" class="imgUser">
    </div>
</header>