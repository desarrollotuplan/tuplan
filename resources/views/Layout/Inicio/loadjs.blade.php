<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/SweetAlert/sweetalert.min.js') }}"></script>
<script src="{{ asset('js/Complementos/Menus.js') }}"></script>
<script src="{{ asset('js/Complementos/LoadCont.js') }}"></script>
<script src="{{ asset('js/Complementos/General.js') }}"></script>
<script src="{{ asset('js/Complementos/Inputs.js') }}"></script>
<script src="{{ asset('js/Complementos/Animates.js') }}"></script>
<script src="{{ asset('js/Complementos/AreasHidden.js') }}"></script>