<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>@yield('titulo')</title>
    <link rel="stylesheet" href="{{asset('css/Bootstrap/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/Inicio/Inicio.css')}}">
    <link rel="stylesheet" href="{{asset('Iconos/style.css')}}">
    @include('Layout.Plugin')
</head>
<body>
    <div class="contenedor">
        <main class="mainInicio dark text-white">
            @yield('contenido')
        </main>
    </div>
    @include('Complementos.Modal')
    @yield('masterjs')
     
    @if(session('message'))
        <script>$(document).ready(function(){ output({!! json_encode(session('message')) !!}); })</script>
    @endif
</body>
</html>