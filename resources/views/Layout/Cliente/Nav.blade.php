<nav class="navbar navbar-expand-sm navbar-dark light-blue fixed-top" style="min-height:52px">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand" href="{{ route('/') }}">
            <img src="{{ asset('Imagenes/Mascota90.png') }}" alt="tuPlan" class="t-40">
             Tu Plan
        </a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link inicio" href="{{ route('/') }}">Inicio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link miNegocio" href="{{ route('miNegocio') }}">Mi negocio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link servicios" href="{{ route('servicios') }}">Servicios</a>
            </li>
            <li class="nav-item">
                <a class="nav-link estadisticas" href="#">Estadisticas</a>
            </li>
        </ul>
        <div class="btn-group">
            <button type="button" class="btn btn-outline-light dropdown-toggle" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false">
            Opciones
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="{{ route('perfil') }}" class="dropdown-item">Perfil</a>
                <a href="" class="dropdown-item">Configuracion</a>
                <div class="dropdown-divider"></div>
                <a href="{{ route('logout') }}" class="dropdown-item">Cerrar sesion</a>
            </div>
        </div>
    </div>
</nav>