<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="viewport" content="initial-scale=1">
    <title>@yield('titulo')</title>
    <link rel="stylesheet" href="{{ asset('css/Bootstrap/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/Cliente/Cliente.css') }}">
    <link rel="stylesheet" href="{{ asset('css/Animate/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/Fotorama/fotorama.css') }}">
    <link rel="stylesheet" href="{{ asset('css/Jcrop/jquery.Jcrop.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Iconos/style.css') }}">
    @include('Layout.Plugin')
</head>
<body>
    <div class="contenedor">
        <main class="main dark-sl text-white mt-52">
            @include('Layout.Cliente.Nav')
            @yield('contenido') <!--variable-->
            @include('Layout.Cliente.Footer')
        </main>
    </div>
    @include('Complementos.Modal')
    
    @include('Complementos.loadjs')
    
    @yield('masterjs')
    
    @if(session('message'))
        <script>$(document).ready(function(){ output({!! json_encode(session('message')) !!}); })</script>
    @endif
</body>
</html>