<div class="modal-body p-4">
   <header class="mb-4">
       <h3 class="text-center title">Inicio de sesion</h3>
   </header>
    <form action="login" method="post" class="sendAjax">
        @csrf
        
        <div class="form-group form-group-tpl form-group-dark mb-4">
            <input type="text" name="user" id="user" class="form-control" autocomplete="on">
            <label class="label label-lg">Correo electronico</label>
        </div>
        
        <div class="form-group form-group-tpl form-group-dark mb-4">
            <input type="password" name="password" id="password" class="form-control" autocomplete="cc-csc">
            <label class="label label-lg">Contraseña</label>
        </div>
        

        <div class="form-group">
            <button type="submit" class="btn btn-success btn-sm btn-block mb-3">Entrar</button>
            <a href="#" class="link-pass">¿Olvidaste la contraseña?</a>
        </div>
        <div class="mb-3">
            <hr class="hrdark">
            <a href="{{ $GGurl }}" class="btn btn-danger btn-social mb-2 d-block">
                <span class="icon icon-google icon-fb"></span> Continuar con Google 
            </a>
            <a href="{{ $FBurl }}" class="btn btn-primary btn-social mb-2 d-block">
                <span class="icon icon-facebook2 icon-fb"></span> Continuar con Facebook 
            </a>
        </div>
        
        <div class="form-group">
            <button type="button" class="btn btn-indigo btn-sm float-right" data-dismiss="modal">Cerrar</button>
        </div>
   </form>
</div>