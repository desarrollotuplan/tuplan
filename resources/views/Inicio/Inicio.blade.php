@extends('Layout.Inicio.master')
@section('titulo','Tu Plan')
@section('contenido')
<div class="row m-0 h-100">
    <div class="col-lg-3 col-md-4 d-md-block p-0 h-100">

        <section class="brightBlue p-4 h-100-vh text-white">
            <div class="content-logo mb-4">
                <img src="{{asset('Imagenes/Mascota200.png')}}" alt="Tu Plan-mascota" class="pet-inicio img-center">
                <h1 class="title text-center">Tu Plan</h1>
            </div>
            <div class="content-form">
                <form id="search">
                   
                    <div class="form-group mb-5">
                        <h6 class="title text-center mb-3">¿Donde te encunetras?</h6>
                        
                        <div class="form-group form-group-tpl form-group-dark">
                            <input type="text" name="categoria" class="select-tpl">
                            <div class="select select-lg">
                                <label>Selecione</label>
                                <span class="icon-plus"></span>
                                <div class="options scroll-white">
                                    <ol><!--EL DATA-LIST ES EL VALOR DEL OPTION-SELECT-->
                                        <li data-list="">Seleccione</li>
                                        <li data-list="">Ibague</li>
                                    </ol>
                               </div>
                            </div>
                       </div>
                       
                    </div>
                    
                    <div class="form-group mb-5">
                       
                        <h6 class="title text-center mb-3">¿Qué buscas?</h6>
                        
                        <div class="form-group form-group-tpl form-group-dark">
                            <input type="text" name="categoria" class="select-tpl">
                            <div class="select select-lg">
                                <label>Selecione</label>
                                <span class="icon-plus"></span>
                                <div class="options scroll-white">
                                    <ol><!--EL DATA-LIST ES EL VALOR DEL OPTION-SELECT-->
                                        <li data-list="">Seleccione</li>
                                        <li data-list="">Sitios</li>
                                        <li data-list="">Promos</li>
                                        <li data-list="">Eventos</li>
                                    </ol>
                               </div>
                            </div>
                       </div>
                       
                    </div>
                    
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-sm btn-block">Buscar</button>
                        <button type="button" class="btn btn-outline-success btn-sm btn-block loadPost" data-route="formLogin">Iniciar sesion</button>
                    </div>
                </form>

                <div class="form-group text-center mb-5">
                    <a href="#registro"data-toggle="modal">Registrarse</a>
                </div>

                <div class="form-group text-center">
                    <small>¿Eres dueño de un negocio?</small><br>
                    <a href="">Registrate aqui</a>
                </div>
            </div>
        </section>
    </div>
    <div class="col-lg-9 col-md-8 p-0 h-100">
        <div id="carouselExampleIndicators" class="carousel slide carousel-fade h-100-vh" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="8"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="9"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="10"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="11"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="12"></li>
            </ol>

            <div class="carousel-inner h-100">
                <div class="carousel-item h-100 active">
                    <img src="{{asset('Imagenes/relaxing.jpg')}}" class="d-block fit-contain h-100 w-100">
                    <div class="carousel-caption d-none d-md-block caption-slide">
                        <h2>Piscinas</h2>
                    </div>
                </div>
                <div class="carousel-item h-100">
                    <img src="{{asset('Imagenes/Tenis.jpg')}}" class="d-block fit-contain h-100 w-100">
                    <div class="carousel-caption d-none d-md-block caption-slide">
                        <h2>Tenis</h2>
                    </div>
                </div>
                <div class="carousel-item h-100">
                    <img src="{{asset('Imagenes/Paintball.jpg')}}" class="d-block fit-contain h-100 w-100">
                    <div class="carousel-caption d-none d-md-block caption-slide">
                        <h2>Paintball</h2>
                    </div>
                </div>
                <div class="carousel-item h-100">
                    <img src="{{asset('Imagenes/swimming-pool-2386261_1920.jpg')}}" class="d-block fit-contain h-100 w-100">
                    <div class="carousel-caption d-none d-md-block caption-slide">
                        <h2>Piscinas</h2>
                    </div>
                </div>
                <div class="carousel-item h-100">
                    <img src="{{asset('Imagenes/Chatarra.jpg')}}" alt="" class="d-block fit-contain h-100 w-100">
                    <div class="carousel-caption d-none d-md-block caption-slide">
                        <h2>Comidas</h2>
                    </div>
                </div>
                <div class="carousel-item h-100">
                    <img src="{{asset('Imagenes/summer.jpg')}}" class="d-block fit-contain h-100 w-100">
                    <div class="carousel-caption d-none d-md-block caption-slide">
                        <h2>Clubes</h2>
                    </div>
                </div>
                <div class="carousel-item h-100">
                    <img src="{{asset('Imagenes/evento.jpg')}}" class="d-block fit-contain h-100 w-100">
                    <div class="carousel-caption d-none d-md-block caption-slide">
                        <h2>Discotecas</h2>
                    </div>
                </div>
                <div class="carousel-item h-100">
                    <img src="{{asset('Imagenes/cupcakes.jpg')}}" class="d-block fit-contain h-100 w-100">
                    <div class="carousel-caption d-none d-md-block caption-slide">
                        <h2>Pastelerias</h2>
                    </div>
                </div>
                <div class="carousel-item h-100">
                    <img src="{{asset('Imagenes/billar.jpg')}}" class="d-block fit-contain h-100 w-100">
                    <div class="carousel-caption d-none d-md-block caption-slide">
                        <h2>Billares</h2>
                    </div>
                </div>
                <div class="carousel-item h-100">
                    <img src="{{asset('Imagenes/EVE1.jpg')}}" class="d-block fit-contain h-100 w-100">
                    <div class="carousel-caption d-none d-md-block caption-slide">
                        <h2>Eventos</h2>
                    </div>
                </div>
                <div class="carousel-item h-100">
                    <img src="{{asset('Imagenes/sport-2620593_1920.jpg')}}" class="d-block fit-contain h-100 w-100">
                    <div class="carousel-caption d-none d-md-block caption-slide">
                        <h2>Canchas sinteticas</h2>
                    </div>
                </div>
                <div class="carousel-item h-100">
                    <img src="{{asset('Imagenes/swimming-pool-918593_1920.jpg')}}" class="d-block fit-contain h-100 w-100">
                    <div class="carousel-caption d-none d-md-block caption-slide">
                        <h2>Fincas</h2>
                    </div>
                </div>
                <div class="carousel-item h-100">
                    <img src="{{asset('Imagenes/love-1137271_1920.jpg')}}" class="d-block fit-contain h-100 w-100">
                    <div class="carousel-caption d-none d-md-block caption-slide">
                        <h2>Moteles</h2>
                    </div>
                </div>
            </div>

            <a href="#carouselExampleIndicators" class="carousel-control-prev" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a href="#carouselExampleIndicators" class="carousel-control-next" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
    </div>
    <div class="container">
        
            <div class="row align-items-center section">
                <div class="col-lg-5">
                    <div class="p-4 br-5">
                        <h1 class="title-header">Encuentra <br> los mejores <span class="c-orange">Sitios </span><br> en tu <span class="c-blue">ciudad!</span></h1>
                        <div class="line line-30 line-white"></div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <img src="{{ asset('svg/destination_blue.svg') }}" class="w-100">
                </div>
            </div>
        
       
            <div class="row align-items-center section">
                <div class="col-lg-7">
                    <img src="{{ asset('svg/eventos.svg') }}" class="w-100">
                </div>
                <div class="col-lg-5">
                    <div class="p-4 br-5">
                        <h1 class="title-header">Enterate de los <span class="c-orange">Eventos </span> que habran!</h1>
                        <div class="line line-30 line-white "></div>
                    </div>
                </div>
            </div>
        
        
            <div class="row align-items-center section">
                <div class="col-lg-5">
                    <div class="p-4 br-5">
                        <h1 class="title-header">Agrega a <span class="c-pas"> favoritos </span> los sitios que te gustan</h1>
                        <div class="line line-30 line-white"></div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <img src="{{ asset('svg/favorites.svg') }}" class="w-100">
                </div>
            </div>
        
            <div class="row align-items-center section">
                <div class="col-lg-7">
                    <img src="{{ asset('svg/reservas.svg') }}" class="w-100">
                </div>
                <div class="col-lg-5">
                    <div class="p-4 br-5">
                        <h1 class="title-header">Realiza cualquier tipo de <span class="c-blue"> reservas </span></h1>
                        <div class="line line-30 line-white"></div>
                    </div>
                </div>
            </div>
        
            <div class="row align-items-center section">
                <div class="col-lg-5">
                    <div class="p-4 br-5">
                        <h1 class="title-header">Haz tus pedidos<span class="c-teal"> online </span></h1>
                        <div class="line line-30 line-white"></div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <img src="{{ asset('svg/productos.svg') }}" class="w-100">
                </div>
            </div>
        
            <div class="row align-items-center section">
                <div class="col-lg-7">
                    <img src="{{ asset('svg/recomendaciones.svg') }}" class="w-100">
                </div>
                <div class="col-lg-5">
                    <div class="p-4 br-5">
                        <h1 class="title-header">Pide recomendaciones segun tus<span class="c-blue"> Intereses</span></h1>
                        <div class="line line-30 line-white"></div>
                    </div>
                </div>
            </div>
        
            <div class="row align-items-center section">
                <div class="col-lg-5">
                    <div class="p-4 br-5">
                        <h1 class="title-header">Recibe notificaciones de tus sitios <span class="c-pas">favoritos</span></h1>
                        <div class="line line-30 line-white"></div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <img src="{{ asset('svg/notificaciones.svg') }}" class="w-100">
                </div>
            </div>
        
    </div>
</div>
@endsection

@section('masterjs')
    @include('Layout.Inicio.loadjs')
@endsection