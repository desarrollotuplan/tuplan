@extends('Layout.Cliente.master')
@section('titulo','Tu Plan | Servicios')
@section('contenido')
    <div class="menu-sidebar darkBlue scroll-white">
        <div class="header-sidebar">
            <h4 class="m-0">Menu</h4>
        </div>
        <ul class="nav-sidebar">
            <li>
                <a href="#" class="uri newLink loadPost" data-route="Servicios">
                    <span class="icon-home"></span>
                    <bb>Inicio</bb>
                </a>
            </li>
            <li>
                <a href="#" class="uri newLink loadGet" data-route="Servicios/Reservas">
                    <span class="icon-clipboard"></span>
                    <bb>Reservas</bb>
                </a>
            </li>
            <li>
                <a href="#" class="uri newLink loadGet" data-route="Servicios/Pedidos">
                    <span class="icon-truck"></span>
                    <bb>Pedidos</bb>
                </a>
            </li>
            <li>
                <a href="#" class="uri newLink loadGet" data-route="Servicios/Chat">
                    <span class="icon-bubbles4"></span>
                    <bb>Chat</bb>
                </a>
            </li>
            <li>
                <a href="#" class="uri newLink loadGet" data-route="Servicios/Preguntas">
                    <span class="icon-users"></span>
                    <bb>Atencion al cliente</bb>
                </a>
            </li>
        </ul>
    </div>
    <div class="main-panel">
        <div class="header-main">
            <h4 class="title-header">
                <button class="btn btn-sm btn-success close-sidebar" aria-expanded="false">
                    <span class="icon-list2"></span>
                </button>
                <span class="icon-file-text"></span>
                Servicios
            </h4>
        </div>
        <div class="cont-main p-3 load-details h-100-vh">
            @if(!isset($informacion))
            <div class="p-3 darkBlue">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem eos magnam officia, atque vel quis laudantium soluta repellendus, error veniam sequi recusandae nobis excepturi! Modi labore aspernatur numquam praesentium quasi!
            </div>
            @endif
                 
        </div>
    </div>
    <a href="{{ route('logout') }}">salir</a>
@endsection
@section('masterjs')
<script> adClass('.servicios','active')</script>
@if(isset($informacion))
<script>$(document).ready(function(){ output({!! json_encode($informacion) !!}); });</script>
@endif
@endsection