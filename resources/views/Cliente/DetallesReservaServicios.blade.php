@if($reserva)
<div class="modal-body">
    <div class="border-tooltip p-3 mb-3">
        <h4>Tipo de reserva</h4>
        <hr class="hrdark">
        <img src="{{ asset($reserva->imagen) }}" alt="reserva" class="w-100 fit-cover mb-2" style="height:200px;">
        <div class="mb-4">
            <h4>{{ $reserva->nombre_reserva }}</h4>
            <p>{{ $reserva->informacion }}</p>
        </div>
        <h4>Datos</h4>
        <hr class="hrdark">
        <div class="row m-0 align-items-center">
            <div class="col-12 col-sm-6">
                Reservado por:
            </div>
            <div class="col-12 col-sm-6">
                <span class="badge badge-info-lighten badge-strong">{{ $reserva->nombre_usuario }}</span>
            </div>
        
            <div class="col-12 col-sm-6">
                Hora de reserva:
            </div>
            <div class="col-12 col-sm-6">
                <span class="badge badge-info-lighten badge-strong">{{ GSD::getTimeFormat($reserva->hora_reserva) }}</span>
            </div>
        
            <div class="col-12 col-sm-6">
                Para el dia:
            </div>
            <div class="col-12 col-sm-6">
                <span class="badge badge-info-lighten badge-strong">{{ GSD::getDateSpanishFull($reserva->fecha) }}</span>
            </div>
        
            <div class="col-12 col-sm-6">
                Personas que asistiran:
            </div>
            <div class="col-12 col-sm-6">
                <span class="badge badge-info-lighten badge-strong">{{ $reserva->numero_personas }}</span>
            </div>
        
            <div class="col-12 col-sm-6">
                Tipo de reserva:
            </div>
            <div class="col-12 col-sm-6">
                <span class="badge badge-info-lighten badge-strong">{{ $reserva->nombre_tipo_reserva }}</span>
            </div>
        
            <div class="col-12 col-sm-6">
                Informacion adicional:
            </div>
            <div class="col-12 col-sm-6">
                <span class="badge badge-info-lighten badge-strong">{{ $reserva->informacion_adicional }}</span>
            </div>
        
            <div class="col-12 col-sm-6">
                Estado:
            </div>
            <div class="col-12 col-sm-6">
                @if($reserva->estado == 'Reservado')
                    @php $color = 'badge-success-lighten'; @endphp
                @elseif($reserva->estado == 'Revisado')
                    @php $color = 'badge-info-lighten'; @endphp
                @else
                   @php $color = 'badge-danger-lighten'; @endphp
                @endif
                <span class="badge {{ $color }} badge-strong">{{ $reserva->estado }}</span>
            </div>
        </div>
    </div>
    <div>
        <button class="btn btn-sm btn-indigo float-right" data-dismiss="modal">Cerrar</button>
    </div>
</div>
@endif