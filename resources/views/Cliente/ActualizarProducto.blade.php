@if($producto)
<div class="col-lg-2 col-md-3 col-sm-3">
    @if(!$producto->imagenes->isEmpty())
    <img src="{{ asset($producto->imagenes['0']->ruta) }}" alt="" class="fit-cover w-100 h-100p">
    @endif
</div>
<div class="col-lg-8 col-md-7 col-sm-7">
    <h5>{{ $producto->nombre }}</h5>
    <p class="h-65 overflow-h">{{ $producto->especificaciones }}</p>
</div>
<div class="col-lg-2 col-md-2 col-sm-2">
    <h4 class="text-right">
        <badge class="badge badge-warning-lighten h6">
            {{ number_format($producto->precio,'0',',','.') }}
        </badge>
    </h4>
    <div class="option text-right">
        <button class="btn btn-outline-light btn-sm loadGet" data-route="MiNegocio/Productos/{{ $producto->id }}/edit/{{ $code }}">
            <span class="icon-pencil2"></span>
        </button>
    </div>
</div>
@endif