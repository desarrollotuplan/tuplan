@if($categoria) @php $code = User::randomString(5); @endphp
<div class="darkBlue row m-0 p-3 mb-2 ">
    <div class="col-lg-10 col-sm-10 col-8">
        <button data-toggle="collapse" data-target="#{{ $code }}" data-code="{{ $code }}" class="btn btn-link text-white m-0">{{ $categoria->nombre }}
            <small class="badge badge-success-lighten ml-2">{{ $categoria->productos->count() }}</small>
        </button>
   </div>
   <div class="col-lg-2 col-sm-2 col-4" data-key="{{ $code }}">
        <button type="button" class="btn btn-outline-success btn-sm float-right ml-2 mb-1 loadGet" data-toggle="collapse" data-route="MiNegocio/Productos/{{ $categoria->id }}" title="Agregar Producto">
            <span class="icon-plus"></span>
        </button>

        <button class="btn btn-outline-primary btn-sm float-right loadGet mb-1" data-route="CategoriaProducto/{{ $categoria->id }}/edit/{{ $code }}" Title="Editar Categoria">
            <span class="icon-pencil2"></span>
        </button>
   </div>
   <div class="col-12 p-0">
        <div class="collapse" id="{{ $code }}">
       </div>
    </div>
</div>

@endif