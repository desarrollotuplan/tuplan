@if(isset($categoria))
<div class="modal-body">
    <div class="header mb-4">
        <h4>Editar categoria</h4>
        <hr class="hrdark d-block">
    </div>
    <form action="CategoriaProducto/{{ $categoria->id }}" method="post" class="sendAjax">
        <div class="hid">
            @csrf
            @method('PUT')
            <input type="hidden" name="code" value="{{ $code }}" class="hid">
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="text" name="categoria" class="form-control form-control-sm" value="{{ $categoria->nombre }}">
            <label class="label label-sm active">Nombre de la categoria</label>
        </div>
        <button type="submit" class="btn btn-success btn-sm float-right">Guardar</button>
        <button type="button" class="btn btn-danger btn-sm float-left" data-dismiss="modal">Cerrar</button>
    </form>
</div>
@endif