@if(is_array($hora))
    @foreach($hora as $hr)
        <button class="btn btn-sm btn-outline-light mb-2" data-method="delete" data-route="HoraReserva/{{ $hr->id }}" data-confirm="Seguro que desea eliminar esta hora?">{{ GSD::getTimeFormat($hr->hora) }}</button>
    @endforeach
@elseif($hora)
    <button class="btn btn-sm btn-outline-light mb-2" data-method="delete" data-route="HoraReserva/{{ $hr->id }}" data-confirm="Seguro que desea eliminar esta hora?">{{ GSD::getTimeFormat($hr->hora) }}</button>
@endif