@extends('Layout.Cliente.master')
@section('titulo','Tu Plan | Mi Sitio')
@section('contenido')
    <div class="menu-sidebar darkBlue scroll-white">
        <div class="header-sidebar">
            <h4 class="m-0">Menu</h4>
        </div>
        <ul class="nav-sidebar">
            <li>
                <a href="#" class="uri newLink loadPost" data-route="MiNegocio">
                    <span class="icon-home"></span>
                    <bb>Inicio</bb>
                </a>
            </li>
            <li>
                <a href="#" class="uri newLink loadGet" data-route="MiNegocio/Informacion">
                    <span class="icon-folder-open"></span>
                    <bb>Informacion</bb>
                </a>
            </li>
            <li>
                <a href="#" class="uri newLink loadGet" data-route="MiNegocio/HorarioAtencion">
                    <span class="icon-clock"></span>
                    <bb>Horario de atencion</bb>
                </a>
            </li>
            <li>
                <a href="#" class="uri newLink loadGet" data-route="MiNegocio/Productos">
                    <span class="icon-cart"></span>
                    <bb>Productos</bb>
                </a>
            </li>
            <li>
                <a href="#" class="uri newLink loadGet" data-route="MiNegocio/RedesSociales">
                    <span class="icon-facebook2"></span>
                    <bb>Redes sociales</bb>
                </a>
            </li>
            <li>
                <a href="#" class="uri newLink loadGet" data-route="MiNegocio/Domicilios">
                    <span class="icon-truck"></span>
                    <bb>Domicilios</bb>
                </a>
            </li>
            <li>
                <a href="#" class="uri newLink loadGet" data-route="MiNegocio/Reservas">
                    <span class="icon-clipboard"></span>
                    <bb>Reservas</bb>
                </a>
            </li>
            <li>
                <a href="#" class="uri newLink loadGet" data-route="MiNegocio/Promociones">
                    <span class="icon-price-tags"></span>
                    <bb>Promociones</bb>
                </a>
            </li>
            <li>
                <a href="#" class="uri newLink loadGet" data-route="MiNegocio/Eventos">
                    <span class="icon-calendar"></span>
                    <bb>Eventos</bb>
                </a>
            </li>
        </ul>
    </div>
    <div class="main-panel">
        <div class="header-main">
            <h4 class="title-header">
                <button class="btn btn-sm btn-success close-sidebar" aria-expanded="false">
                    <span class="icon-list2"></span>
                </button>
                <span class="icon-pencil2"></span>
                Mi Negocio
                <a href="{{ Route('buscar.sitios',['negocio'=>Ddunico::negocio(User::negocioId())]) }}" target="_blank" class="btn btn-primary btn-sm float-right">Vista Previa</a>
            </h4>
        </div>
        <div class="cont-main p-3 load-details">
            @if(!isset($informacion))
            <div class="p-3 darkBlue">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem eos magnam officia, atque vel quis laudantium soluta repellendus, error veniam sequi recusandae nobis excepturi! Modi labore aspernatur numquam praesentium quasi!
            </div>
            @endif
            
        </div>
    </div>
    <a href="{{ route('logout') }}">salir</a>
@endsection
@section('masterjs')
<script> adClass('.miNegocio','active')</script>
<script src="{{ asset('js/Jcrop/jquery.Jcrop.min.js') }}"></script>
<script src="{{ asset('js/Image/image.js') }}"></script>
@if(isset($informacion))
<script>$(document).ready(function(){ output({!! json_encode($informacion) !!}); });</script>
@endif
@endsection