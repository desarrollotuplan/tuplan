<div class="jumbotron jumb-tuplan m-0 mb-3 portada" style="background-image: url('{{ asset(User::negocio()->folder.User::negocio()->foto_perfil) }}'); background-size: cover; background-color: rgba(0,0,0,0.2); background-blend-mode: soft-light; background-position: center; background-size: cover;" data-background="true">
    <img src="{{ asset(User::negocio()->folder.User::negocio()->logo) }}" alt="" style="height:100px; width:100px;" class="mb-2 logo-image">
    <form action="MiNegocio/saveImages" class="options sendAjax" method="post">
        <div class="hid">
            <input type="text" name="logo" id="img-logo-upload" hidden>
            <input type="text" name="portada" id="img-portada-upload" hidden>
        </div>
        <button type="button" class="btn btn-sm btn-success loadPost" data-route="logo" disabled>Cambiar Logo</button>
        <button type="button" class="btn btn-sm btn-light loadPost" data-route="portada" disabled>Cambiar portada</button>
        <button type="button" class="btn btn-sm btn-indigo float-right active-form" data-remove="btn-indigo">Editar</button>
    </form>
</div>
   

<div class="darkBlue p-3 mb-3">
    <div class="header">
        <h4>Imagenes
            <button type="button" class="btn btn-primary btn-sm float-right loadPost mr-1" data-route="fotoNegocio">
                <apan class="icon-plus"></apan> Agregar imagen
            </button>
        </h4>
        <hr class="hrdark">
    </div>
    <div class="load-files">
        <div class="fotorama w-100" data-nav="thumbs" data-allowfullscreen="native" data-autoplay="4000" data-click="false">
            @if(isset($imagenes) && !$imagenes->isEmpty())
                @foreach($imagenes as $i)
            <div data-img="{{ asset($i->ruta) }}" alt="" class="row m-0 justifi-content-cemter">
                <div class="col-12 text-center">
                    <button type="button" class="btn btn-danger btn-sm float-right float-none" data-route="MiNegocio/deleteImage/{{ $i->id }}" data-method="delete" data-confirm="Esta seguro que desea eliminar esta imagen?">
                        <apan class="icon-bin"></apan>
                    </button>
                </div>
            </div>
                @endforeach
            @else
                Sin fotos del sitio.
            @endif
        </div>
        <script>$('.fotorama').fotorama();</script>
    </div>
</div>

<form action="MiNegocio/Actualizar" class="darkBlue p-3 sendAjax mb-3">
    <div class="header">
        <h4>Infomacion <button type="button" class="btn btn-primary btn-sm float-right active-form" data-remove="btn-primary">Editar</button></h4>
        <hr class="hrdark">
    </div>
    <div class="row m-0">
        <div class="col-lg-12 form-group form-group-tpl form-group-dark">
            <input type="text" name="nombre" class="form-control form-control-sm" value="{{ User::negocio()->nombre }}" disabled>
            <label class="label label-lg active">Nombre</label>
        </div>
    
        <div class="col-lg-12 form-group form-group-tpl form-group-dark">
            <input type="text" name="eslogan" class="form-control form-control-sm" value="{{ User::negocio()->eslogan }}" disabled>
            <label class="label label-lg active">Eslogan</label>
        </div>
    
        <div class="col-lg-12 form-group form-group-tpl form-group-dark">
            <textarea name="descripcion" rows="6" class="form-control form-control-sm resize" disabled>{{ User::negocio()->descripcion }}</textarea>
            <label class="label label-lg active">Descripcion</label>
        </div>
    
        <div class="col-lg-12 form-group form-group-tpl form-group-dark">
            <input type="text" name="direccion" class="form-control form-control-sm" value="{{ User::negocio()->direccion }}" disabled>
            <label class="label label-lg active">Direccion</label>
        </div>
    
        <div class="col-lg-5 form-group form-group-tpl form-group-dark">
            <input type="text" name="latitud" class="form-control form-control-sm" value="{{ User::negocio()->latitud }}" disabled>
            <label class="label label-lg active">Latitud</label>
        </div>
    
        <div class="col-lg-5 form-group form-group-tpl form-group-dark">
            <input type="text" name="longitud" class="form-control form-control-sm" value="{{ User::negocio()->longitud }}" disabled>
            <label class="label label-lg active">Longitud</label>
        </div>
    
        <div class="col-lg-2 form-group form-group-tpl form-group-dark">
            <button type="button" class="btn btn-outline-success btn-sm btn-block"><span class="icon-location"></span>Ubicar</button>
        </div>
    
        <div class="col-lg-4 form-group form-group-tpl form-group-dark">
            <input type="text" name="telefono" class="form-control form-control-sm" value="{{ User::negocio()->telefono }}" disabled>
            <label class="label label-lg active">Telefono</label>
        </div>
    
        <div class="col-lg-4 form-group form-group-tpl form-group-dark">
            <input type="text" name="celular" class="form-control form-control-sm" value="{{ User::negocio()->celular }}" disabled>
            <label class="label label-lg active">Celular</label>
        </div>
    
        <div class="col-lg-4 form-group form-group-tpl form-group-dark">
            <input type="text" name="whatsapp" class="form-control form-control-sm" value="{{ User::negocio()->whatsapp }}" disabled>
            <label class="label label-lg active">WhatsApp</label>
        </div>
    
        <div class="col-lg-6 form-group form-group-tpl form-group-dark">
            <input type="text" name="nit" class="form-control form-control-sm" value="{{ User::negocio()->whatsapp }}" disabled>
            <label class="label label-lg active">Nit (Opcional)</label>
        </div>
    
        <div class="col-lg-6 form-group form-group-tpl form-group-dark">
            <input type="text" name="rut" class="form-control form-control-sm" value="{{ User::negocio()->whatsapp }}" disabled>
            <label class="label label-lg active">Rut (Opcional)</label>
        </div>
    </div>
</form>