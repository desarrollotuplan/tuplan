<div class="darkBlue bs-m-dark p-3 mb-5">
    <h4>
        Categorias
        <button class="btn btn-success btn-sm float-right btn-icon loadGet" data-route="CategoriaProducto/create">
            <span class="icon-plus"></span>
            Crear categoria
        </button>
    </h4>
</div>
<div class="categorias">
    @if(isset($categorias))
        @foreach($categorias as $c) @php $code  = User::randomString(5); @endphp
        <div class="darkBlue bs-m-dark row m-0 p-3 mb-2 ">
            <div class="col-lg-10 col-sm-10 col-8">
                <button data-toggle="collapse" data-target="#{{ $code }}" data-code="{{ $code }}" class="btn btn-link text-white m-0">{{ $c->nombre }}
                    <small class="badge badge-success-lighten ml-2">{{ $c->productos->count() }}</small>
                </button>
           </div>
           <div class="col-lg-2 col-sm-2 col-4">
                <button type="button" class="btn btn-outline-success btn-sm float-right ml-2 mb-1 loadGet" data-route="MiNegocio/Productos/{{ $c->id }}/{{ $code }}" title="Agregar Producto">
                    <span class="icon-plus"></span>
                </button>
                
                <button class="btn btn-outline-primary btn-sm float-right loadGet mb-1" data-route="CategoriaProducto/{{ $c->id }}/edit/{{ $code }}">
                    <span class="icon-pencil2"></span>
                </button>
           </div>
           <div class="col-12 p-0">
                <div class="collapse show" id="{{ $code }}">
                    @if(!$c->productos->isEmpty())
                        @foreach($c->productos as $p) @php $codeP  = User::randomString(8); @endphp
                <div class="dark br-5 mb-2 p-3 row m-0" data-code="{{ $codeP }}">
                    <div class="col-lg-2 col-md-3 col-sm-3">
                        @if(!$p->imagenes->isEmpty())
                        <img src="{{ asset($p->imagenes['0']->ruta) }}" alt="" class="fit-cover w-100 h-100p">
                        @endif
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-7">
                        <h5>{{ $p->nombre }}</h5>
                        <p class="h-65 overflow-h">{{ $p->especificaciones }}</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <h4 class="text-right">
                            <badge class="badge badge-warning-lighten h6">
                                {{ number_format($p->precio,'0',',','.') }}
                            </badge>
                        </h4>
                        <div class="option text-right">
                            <button class="btn btn-outline-light btn-sm loadGet" data-route="MiNegocio/Productos/{{ $p->id }}/edit/{{ $codeP }}">
                                <span class="icon-pencil2"></span>
                            </button>
                        </div>
                    </div>
                </div>
                        @endforeach
                    @endif
               </div>
           </div>
        </div>
        @endforeach
    @endif
</div>