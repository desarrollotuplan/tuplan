<div class="modal-body">
    <div class="header mb-4">
        <h4>Crear Promo</h4>
        <hr class="hrdark d-block">
    </div>
    <form action="MiNegocio/Promociones" method="post" class="sendAjax">
        <div class="form-group form-group-tpl form-group-dark">
            <input type="text" name="nombre" class="form-control form-control-sm">
            <label class="label label-sm">Titulo de la promo</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <textarea name="descripcion" rows="3" class="form-control resize"></textarea>
            <label class="label label-sm">Descripcion</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <textarea name="informacion" rows="3" class="form-control resize"></textarea>
            <label class="label label-sm">Informacion Adicional</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="date" name="fecha_inicio" class="form-control form-control-sm">
            <label class="label label-sm placeholder">Fecha de inicio</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="time" name="hora_inicio" class="form-control form-control-sm">
            <label class="label label-sm placeholder">Hora Inicio</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="date" name="fecha_fin" class="form-control form-control-sm">
            <label class="label label-sm placeholder">Fecha de finalizacion</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="time" name="hora_fin" class="form-control form-control-sm">
            <label class="label label-sm placeholder">Hora de finalizacion</label>
        </div>
        <h4>Falta foto</h4>
        <button type="submit" class="btn btn-success btn-sm float-right">Crear</button>
        <button type="button" class="btn btn-danger btn-sm float-left" data-dismiss="modal">Cerrar</button>
    </form>
</div>