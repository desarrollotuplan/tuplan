@if($dia && $code && $tipo)
<div class="modal-body">
    <div class="header mb-4">
        <h4>Crear horario del dia {{ $dia->nombre }}</h4>
        <hr class="hrdark d-block">
    </div>
    <form action="MiNegocio/HorarioAtencion/Confirmar" method="post" class="sendAjax">
        <div class="form-group form-group-tpl form-group-dark">
            <input type="text" name="tipo" class="select-tpl" value="si">
            <div class="select select-lg">
                <label>Seleccione</label>
                <span class="icon-plus"></span>
                <div class="options scroll-white">
                    <ol><!--EL DATA-LIST ES EL VALOR DEL OPTION-SELECT-->
                        <li data-list="">Seleccione</li>
                    @if(!$tipo->isEmpty())
                        @foreach($tipo as $t)
                        <li data-list="{{ $t->id }}">{{ $t->nombre }}</li>
                        @endforeach
                    @endif
                    </ol>
               </div>
            </div>
            <label class="label label-lg active">Seleccione el tipo de horario</label>
        </div>
        <div class="form-group mb-5">
            <button type="submit" class="btn btn-success btn-sm btn-block">Confirmar</button>
        </div>
    </form>
    <form action="MiNegocio/HorarioAtencion" method="post" class="sendAjax">
        <div class="hid">
            @csrf
            <input type="hidden" name="dia" value="{{ $dia->id }}">
            <input type="hidden" name="code" value="{{ $code }}">
       </div>
       <div class="loadHorario">
            <button class="btn btn-danger btn-sm float-left" data-dismiss="modal">Cancelar</button>
       </div>
    </form>
</div>
@endif