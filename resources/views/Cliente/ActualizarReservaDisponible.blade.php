@if($reserva && $code)
<div class="bs-m-dark card card-dark card-tpl">
    <div class="card-img-content">
        <img src="{{ asset($reserva->imagen) }}" alt="reserva-disponible" class="card-img fit-cover">
    </div>
    <div class="card-body">
        <h5 class="card-title">{{ $reserva->nombre }}</h5>
        <p class="card-description h-87">{{ $reserva->informacion }}</p>
    </div>
    <div class="card-footer">

        <button type="button" class="btn btn-primary btn-sm loadGet" data-route="MiNegocio/ReservaDisponible/{{ $reserva->id }}">Ver horario</button>

        <button type="button" class="btn btn-outline-light btn-sm float-right loadGet" data-route="MiNegocio/ReservaDisponible/{{ $reserva->id }}/edit/{{ $code }}">
            <span class="icon-pencil"></span>
        </button>

    </div>
@endif