@extends('Layout.Cliente.master')
@section('titulo','Tu Plan | Inicio')
@section('contenido')
    <div class="jumbotron jumb-tuplan jumb-office">
        <div class="jumb-cont">
            <h4>Hola {{ User::primerNombre() }}</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem reiciendis consectetur ab voluptatum odit blanditiis, ratione modi, animi, voluptate rem corporis. Praesentium minus soluta asperiores ab, alias deserunt adipisci iste!</p>
            <div class="text-right">
                <button class="btn btn-sm btn-primary">Aprender mas...</button>
            </div>
        </div>
    </div>
    <a href="{{ route('logout') }}">salir</a>
@endsection
@section('masterjs')
<script>adClass('.inicio','active')</script>
@endsection