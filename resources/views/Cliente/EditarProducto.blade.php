@if($producto)
<div class="modal-body">
    <form action="MiNegocio/Productos/{{ $producto->id }}" method="post" class="sendAjax">
        <div class="-header">
            <h4>Editar Producto</h4>
            <hr class="hrdark mb-5">
        </div>
        <div class="hid">
            @csrf
            @method('PUT')
            <input type="hidden" name="code" value="{{ $code }}" hidden>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="text" name="nombre" class="form-control form-control-sm" value="{{ $producto->nombre }}">
            <label class="label label-sm active">Nombre del producto</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <textarea name="especificaciones" rows="3" class="form-control form-control-sm resize">{{ $producto->especificaciones }}</textarea>
            <label for="" class="label label-sm active">Especificaciones</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <textarea name="informacion_adicional" rows="3" class="form-control form-control-sm resize">{{ $producto->informacion_adicional }}</textarea>
            <label class="label label-sm active">Informacion adicional</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="number" name="precio" class="form-control form-control-sm"  value="{{ $producto->precio }}">
            <label class="label label-sm active">Precio</label>
        </div>
        <div class="">
            <button type="submit" class="btn btn-success btn-sm float-right">Guardar</button>
            <button type="button" class="btn btn-orange btn-sm" data-dismiss="modal">Cancelar</button>
        </div>
    </form>
</div>
@endif