@if($reserva)
<form action="MiNegocio/Reservas/Actualizar" class="p-3 darkBlue sendAjax mb-3 bs-m-dark" method="post">
    <div class="hid">
        @csrf
        @method('PUT')
    </div>
    <div class="header">
        <h4>Reservas
            <button type="button" class="btn btn-primary btn-sm float-right active-form" data-remove="btn-primary">Editar</button>
        </h4>
        <hr class="hrdark mb-5">
    </div>
    <div class="row m-0">
        <div class="col-lg-4 form-group form-group-tpl form-group-dark">
            <input type="text" name="reserva" class="select-tpl" value="{{ $reserva->reserva }}">
            <div class="select select-sm" disabled>
                <label>{{ ucwords($reserva->reserva) }}</label>
                <span class="icon-plus"></span>
                <div class="options scroll-white">
                    <ol><!--EL DATA-LIST ES EL VALOR DEL OPTION-SELECT-->
                        <li data-list="">Seleccione</li>
                        <li data-list="si">Si</li>
                        <li data-list="no">No</li>
                    </ol>
               </div>
            </div>
            <label class="label label-lg active">Cuenta con servicio de reservas?</label>
        </div>
        <div class="col-lg-4 form-group form-group-tpl form-group-dark">
            <input type="time" name="hora_inicio" class="form-control form-control-sm" value="{{ $reserva->hora_inicio }}" disabled="">
            <label class="label label-lg active placeholder">Hora de minima para reservar</label>
        </div>
        <div class="col-lg-4 form-group form-group-tpl form-group-dark">
            <input type="time" name="hora_final" class="form-control form-control-sm" value="{{ $reserva->hora_final }}" disabled="">
            <label class="label label-lg active placeholder">Hora de maxima para reservar</label>
        </div>
    </div>
</form>
@endif


<div class="darkBlue p-3 mb-2 bs-m-dark">
    <h4>Tipos de reservas disponibles
        <button type="button" class="btn btn-success btn-sm float-right loadGet" data-route="MiNegocio/ReservaDisponible">Agregar</button>
    </h4>
</div>
<div class="reservas row m-0">
    @if(!$reservasDisponibles->isEmpty()) 
        @foreach($reservasDisponibles as $rd) @php $code = User::randomString(6); @endphp
            <div class="col-lg-4" data-code="{{ $code }}">
                <div class="bs-m-dark card card-dark card-tpl">
                    <div class="card-img-content">
                        <img src="{{ asset($rd->imagen) }}" alt="reserva-disponible" class="card-img fit-cover">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title" title="">{{ $rd->nombre }}</h5>
                        <p class="card-description h-87">{{ $rd->informacion }}</p>
                    </div>
                    <div class="card-footer">

                        <button type="button" class="btn btn-primary btn-sm loadGet" data-route="MiNegocio/ReservaDisponible/{{ $rd->id }}">Ver horario</button>

                        <button type="button" class="btn btn-outline-light btn-sm float-right loadGet" data-route="MiNegocio/ReservaDisponible/{{ $rd->id }}/edit/{{ $code }}">
                            <span class="icon-pencil"></span>
                        </button>

                    </div>
                </div>
            </div>
        @endforeach 
    @endif 
</div>