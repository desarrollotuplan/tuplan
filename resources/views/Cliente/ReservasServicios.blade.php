<div class="darkBlue bs-m-dark table-tpl table-dark">
    <div class="header">
        <bb>Reservas</bb>
    </div>
    <div class="table-responsive scroll-white">
        <table class="table table-striped text-center m-0 scroll-white">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Hora</th>
                    <th scope="col">Tipo reserva</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Ver</th>
                </tr>
            </thead>
            <tbody>
            @if(!$reservas->isEmpty())
                @foreach($reservas as $r) @php $id = User::randomString(5) @endphp
                <tr id="{{ $id }}">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ GSD::getDateSpanishFull($r->fecha) }}</td>
                    <td>{{ GSD::getTimeFormat($r->hora) }}</td>
                    <td>{{ $r->nombre_reserva }}</td>
                    @if($r->estado == 'Reservado')
                        @php $color = 'badge-success-lighten'; @endphp
                    @elseif($r->estado == 'Revisado')
                        @php $color = 'badge-info-lighten'; @endphp
                    @else
                       @php $color = 'badge-danger-lighten'; @endphp
                    @endif
                    <td><span class="badge {{ $color }} badge-strong p-2">{{ $r->estado }}</span></td>
                    <td><button class="btn btn-outline-primary btn-sm loadPost" data-route-link="detalles/{{ $r->id }}/{{ $id }}">Ver</button></td>
                </tr>
                @endforeach
            @else
                <tr>
                    <th>1</th>
                    <td>2019-08-09</td>
                    <td>08:00 am</td>
                    <td>$30.000</td>
                    <td><span class="badge badge-success-lighten p-2">Ejemplo</span></td>
                    <td><button class="btn btn-outline-primary btn-sm">Ver</button></td>
                </tr>
                @endif
            </tbody>
        </table>    
    </div>
</div>