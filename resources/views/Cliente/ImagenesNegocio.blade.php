<div class="fotorama w-100" data-nav="thumbs" data-allowfullscreen="native" data-autoplay="4000" data-click="false">
    @if(!$imagenes->isEmpty())
        @foreach($imagenes as $i)
        <div data-img="{{ asset($i->ruta) }}" alt="" class="row m-0 justifi-content-cemter">
            <div class="col-12 text-center">
                <button type="button" class="btn btn-danger btn-sm float-right float-none" data-route="MiNegocio/deleteImage/{{ $i->id }}" data-method="delete" data-confirm="Esta seguro que desea eliminar esta imagen?">
                    <apan class="icon-bin"></apan>
                </button>
            </div>
        </div>
        @endforeach
    @else
        Sin fotos del sitio.
    @endif
</div>
<script>$('.fotorama').fotorama();</script>