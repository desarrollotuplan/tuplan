@if($redes)
<div class="darkBlue p-3 bs-m-dark">
    <form action="MiNegocio/RedesSociales/Actualizar" class="row sendAjax m-0" method="post">
        <div class="header col-12">
            <h4>Redes Sociales <button type="button" class="btn btn-primary btn-sm float-right active-form" data-remove="btn-primary">Editar</button></h4>
            <hr class="hrdark mb-5">
        </div>
        <div class="form-group form-group-tpl form-group-dark col-lg-6 col-md-6 col-sm-6 col-12">
            <input type="text" name="facebook" class="form-control" value="{{ $redes->facebook }}" placeholder="www.facebook.com" disabled >
            <label class="label label-lg active placeholder"> <span class="icon-facebook2 icon-right"></span>Facebook</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark col-lg-6 col-md-6 col-sm-6 col-12">
            <input type="text" name="instagram" class="form-control" value="{{ $redes->instagram }}" disabled>
            <label class="label label-lg active placeholder"> <span class="icon-instagram icon-right"></span>Instagram</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark col-lg-6 col-md-6 col-sm-6 col-12">
            <input type="text" name="youtube" class="form-control" value="{{ $redes->youtube }}" disabled>
            <label class="label label-lg active placeholder"> <span class="icon-youtube icon-right"></span>YouTube</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark col-lg-6 col-md-6 col-sm-6 col-12">
            <input type="text" name="twitter" class="form-control" value="{{ $redes->twitter }}" disabled>
            <label class="label label-lg active placeholder"> <span class="icon-twitter icon-right"></span>Twitter</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark col-lg-6 col-md-6 col-sm-6 col-12">
            <input type="text" name="website" class="form-control" value="{{ $redes->website }}" disabled>
            <label class="label label-sm active placeholder"> <span class="icon-chrome icon-right"></span>Sitio Web</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark col-lg-6 col-md-6 col-sm-6 col-12">
            <input type="text" name="app_android" class="form-control" value="{{ $redes->app_android }}" disabled>
            <label class="label label-lg active placeholder"> <span class="icon-android icon-right"></span>App Android</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark col-lg-6 col-md-6 col-sm-6 col-12">
            <input type="text" name="app_ios" class="form-control" value="{{ $redes->app_ios }}" disabled>
            <label class="label label-lg active placeholder"> <span class="icon-appleinc icon-right"></span>App IOS</label>
        </div>
    </form>
</div>
@endif