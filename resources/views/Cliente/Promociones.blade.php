<div class="darkBlue bs-m-dark p-3 mb-4">
    <h4>
        Promociones
        <button class="btn btn-success btn-sm float-right btn-icon loadGet" data-route="MiNegocio/Promociones/create">
            <span class="icon-plus"></span>
            Crear promo
        </button>
    </h4>
</div>


<div class="row m-0 promociones">
@if($promociones && !$promociones->isEmpty())
    @foreach($promociones as $pr) @php $code = User::randomString(6); @endphp
    <div class="col-lg-4 col-md-6 col-sm-6 col-12" data-code="{{ $code }}">
        <div class="card card-tpl card-dark bs-m-dark">
            <div class="card-img-content" style="height:250px;">
                <img src="{{ asset($pr->imagen) }}" alt="foto-negocio" class="card-img fit-cover" style="height:250px;">
            </div>
            <div class="card-body">
                <h5 class="card-title" title="">{{ $pr->nombre }}</h5>
                <p class="card-description h-87 title">{{ $pr->descripcion }}</p>
            </div>
            <div class="card-footer">
                <button class="btn btn-sm btn-outline-success btn-option-xs">Vista Previa</button>
                <button class="btn btn-sm btn-primary btn-option-xs float-right loadGet" data-route="MiNegocio/Promociones/{{ $pr->id }}/edit/{{ $code }}">Editar</button>
            </div>
        </div>
    </div>
    @endforeach
@else
@endif
</div>