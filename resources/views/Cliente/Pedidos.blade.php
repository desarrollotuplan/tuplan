@if($pedidos)
<div class="darkBlue bs-m-dark table-tpl table-dark">
    <div class="header">
        <bb>Pedidos</bb>
    </div>
    <div class="table-responsive">
        <table class="table table-striped text-center m-0">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Hora</th>
                    <th scope="col">Total</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Detalles</th>
                </tr>
            </thead>
            <tbody>
               @if(!$pedidos->isEmpty())
                   @foreach($pedidos as $p)
                <tr>
                    <th>{{ $loop->iteration }}</th>
                    <td>{{ $p->fecha }}</td>
                    <td>{{ $p->hora }}</td>
                    <td>{{ $p->total }}</td>
                    <td><span class="badge badge-success-lighten p-2">{{ $p->estado->nombre }}</span></td>
                    <td><button class="btn btn-outline-primary btn-sm">Ver</button></td>
                </tr>
                    @endforeach
                @else
                <tr>
                    <th>1</th>
                    <td>2019-08-09</td>
                    <td>08:00 am</td>
                    <td>$30.000</td>
                    <td><span class="badge badge-success-lighten p-2">Ejemplo</span></td>
                    <td><button class="btn btn-outline-primary btn-sm">Ver</button></td>
                </tr>
                @endif
            </tbody>
        </table>    
    </div>
</div>
@endif