<div class="modal-body">
    <form action="MiNegocio/Productos" method="post" class="sendAjax">
        <div class="header">
            <h4>Crear Producto</h4>
            <hr class="hrdark mb-5">
        </div>
        <div class="hid">
            @csrf
            <input type="hidden" name="categoria" value="{{ $categoria }}" hidden>
            <input type="hidden" name="code" value="{{ $code }}" hidden>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="text" name="nombre" class="form-control form-control-sm">
            <label class="label label-sm">Nombre del producto</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <textarea name="especificaciones" rows="3" class="form-control form-control-sm resize"></textarea>
            <label class="label label-sm">Especificaciones</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <textarea name="informacion_adicional" rows="3" class="form-control form-control-sm resize"></textarea>
            <label class="label label-sm">Informacion adicional</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="number" name="precio" class="form-control form-control-sm">
            <label class="label label-sm">Precio</label>
        </div>
        <div class="">
            <button type="submit" class="btn btn-success btn-sm float-right">Agregar</button>
            <button type="button" class="btn btn-orange btn-sm" data-dismiss="modal">Cancelar</button>
        </div>
    </form>
</div>