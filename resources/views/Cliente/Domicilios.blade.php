@if($domicilio)
<form action="MiNegocio/Domicilios/Actualizar" class="p-3 darkBlue sendAjax bs-m-dark" method="post">
    <div class="hid">
        @csrf
        @method('PUT')
    </div>
    <div class="header">
        <h4>Domicilios
            <button type="button" class="btn btn-primary btn-sm float-right active-form" data-remove="btn-primary">Editar</button>
        </h4>
        <hr class="hrdark mb-5">
    </div>
    <div class="row m-0">
        <div class="col-lg-4 form-group form-group-tpl form-group-dark">
            <input type="text" name="servicio" class="select-tpl" value="{{ $domicilio->servicio }}">
            <div class="select select-sm" disabled>
                <label>{{ ucwords($domicilio->servicio) }}</label>
                <span class="icon-plus"></span>
                <div class="options scroll-white">
                    <ol><!--EL DATA-LIST ES EL VALOR DEL OPTION-SELECT-->
                        <li data-list="">Seleccione</li>
                        <li data-list="si">Si</li>
                        <li data-list="no">No</li>
                    </ol>
               </div>
            </div>
            <label class="label label-lg active">Cuenta con servicio domicilio?</label>
        </div>
        <div class="col-lg-4 form-group form-group-tpl form-group-dark">
            <input type="text" name="valor_minimo" class="form-control form-control-sm" value="{{ $domicilio->valor_minimo }}" disabled="">
            <label class="label label-lg active placeholder">Valor minimo del pedido</label>
        </div>
        <div class="col-lg-4 form-group form-group-tpl form-group-dark">
            <input type="text" name="valor_domicilio" class="form-control form-control-sm" value="{{ $domicilio->valor_domicilio }}" disabled="">
            <label class="label label-lg active placeholder">Valor del domicilio</label>
        </div>
        <div class="col-lg-4 form-group form-group-tpl form-group-dark">
            <input type="time" name="hora_inicio" class="form-control form-control-sm" value="{{ $domicilio->hora_inicio }}" disabled="">
            <label class="label label-lg active placeholder">Hora de inicio de atencion online</label>
        </div>
        <div class="col-lg-4 form-group form-group-tpl form-group-dark">
            <input type="time" name="hora_final" class="form-control form-control-sm" value="{{ $domicilio->hora_final }}" disabled="">
            <label class="label label-lg active placeholder">Hora de fin de atencion online</label>
        </div>
    </div>
</form>
@endif