@if($reserva)
    <img src="{{ asset($reserva->imagen) }}" alt="reserva" class="w-100 fit-cover" style="height:200px;">
    <div class="modal-body">
        <h4>{{ $reserva->nombre }}</h4>
        <p>{{ $reserva->informacion }}</p>
        
        <form action="HoraReserva" method="post" class="row m-0 mb-4 p-2 border sendAjax">
            <div class="hid">
                @csrf
                <input type="hidden" name="reserva" value="{{ $reserva->id }}" class="hid">
            </div>
            <div class="col-lg-12">
                <h3>Agregar Hora</h3>
                <hr class="hrdark mb-3">
            </div>
           
            <div class="col-lg-5 form-group form-group-tpl form-group-dark">
                <input type="text" name="dia" class="select-tpl">
                <div class="select select-sm">
                    <label>Seleccione</label>
                    <span class="icon-plus"></span>
                    <div class="options scroll-white">
                        <ol><!--EL DATA-LIST ES EL VALOR DEL OPTION-SELECT-->
                            <li data-list="">Seleccione</li>
                            @if(!$dias->isEmpty())
                                @foreach($dias as $d)
                                    <li data-list="{{ $d->id }}">{{ $d->nombre }}</li>
                                @endforeach
                            @endif
                        </ol>
                   </div>
                </div>
                <label class="label label-lg active">Dia de la semana</label>
            </div>
            
            <div class="col-lg-4 form-group form-group-tpl form-group-dark">
                <input type="time" name="hora" class="form-control form-control-sm">
                <label class="label label-lg active placeholder">Hora de Reserva</label>
            </div>
            
            <div class="col-lg-3 form-group form-group-tpl form-group-dark">
                <input type="number" name="cantidad" class="form-control form-control-sm">
                <label class="label label-lg active placeholder">Numero de reservas</label>
            </div>
            
            <div class="col-lg-12 text-right">
                <button type="submit" class="btn btn-success btn-sm">Agregar</button>
            </div>
            
        </form>
        
        <h5>NOTA: hacer click encima de la hora que desea eliminar.</h5>
        
        @if(!$dias->isEmpty())
            @foreach($dias as $d)
                <div class="p-3 border-info mb-2">
                    <h5>{{ $d->nombre }}</h5>
                    <div class="{{ $d->nombre }}">
                        @if(!$d->horasReservas->isEmpty())
                            @foreach($d->horasReservas as $hr)
                                <button class="btn btn-sm btn-outline-light mb-2" data-method="delete" data-route="HoraReserva/{{ $hr->id }}" data-confirm="Seguro que desea eliminar esta hora?">{{ GSD::getTimeFormat($hr->hora) }}</button>
                            @endforeach
                        @endif
                    </div>
                </div>
            @endforeach
        @endif
        
    </div>
    <div class="p-3">
        <button class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
    </div>
@endif