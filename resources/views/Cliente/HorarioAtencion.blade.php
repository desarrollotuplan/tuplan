@if(!$horario->isEmpty())
<div class="row m-0">
    @foreach($horario as $hr) @php $code = User::randomString(5); @endphp
    <div class="col-lg-6">
        <div class="darkBlue p-3 bs-m-dark h-100">
            <h4>{{ $hr->nombre }}</h4>
            <h5>Tipo de horario</h5>
            <div data-code="{{ $code }}">
                @if(!$hr->horario->isEmpty()) 
                <span class="badge badge-success-lighten">{{ $hr->horario[0]->horarioTipo->nombre }}</span>
                <a href="#{{ $code }}" class="uri" data-toggle="collapse">Detalles</a>
                <div id="{{ $code }}" class="collapse p-2">
                    @if($hr->horario[0]->horarioTipo->nombre == 'Doble jornada')
                    <h6>
                        Primera jornada Desde: <span class="badge badge-info-lighten">{{ GSD::getTimeFormat($hr->horario[0]->hora_inicio_1) }}</span>
                        Hasta: <span class="badge badge-warning-lighten">{{ GSD::getTimeFormat($hr->horario[0]->hora_fin_1) }}</span>
                    </h6>
                    <h6>
                        Segunda jornada Desde: <span class="badge badge-info-lighten">{{ GSD::getTimeFormat($hr->horario[0]->hora_inicio_2) }}</span>
                        Hasta: <span class="badge badge-warning-lighten">{{ GSD::getTimeFormat($hr->horario[0]->hora_fin_2) }}</span>
                        <a href="#" class="uri text-info loadGet" data-route="MiNegocio/HorarioAtencion/{{ $hr->horario[0]->id }}/edit/{{ $code }}">Editar</a>
                        <a href="#" class="uri text-info loadGet float-right text-warning" data-route="MiNegocio/HorarioAtencion/{{ $hr->horario[0]->id }}/edit/{{ $code }}">Eliminar</a>
                    </h6>
                    @else
                    <h6>
                        Desde: <span class="badge badge-info-lighten">{{ GSD::getTimeFormat($hr->horario[0]->hora_inicio_1) }}</span>
                        Hasta: <span class="badge badge-warning-lighten">{{ GSD::getTimeFormat($hr->horario[0]->hora_fin_1) }}</span>
                        <a href="#" class="uri text-info loadGet" data-route="MiNegocio/HorarioAtencion/{{ $hr->horario[0]->id }}/edit/{{ $code }}">Editar</a>
                        
                        <a href="#" class="uri text-info loadGet float-right text-warning" data-route="MiNegocio/HorarioAtencion/{{ $hr->horario[0]->id }}/edit/{{ $code }}">Eliminar</a>
                    </h6>
                    @endif
                </div>
                @else
                <span class="badge badge-danger-lighten">Sin definir</span>
                <a href="#" class="loadGet uri" data-route="MiNegocio/HorarioAtencion/create/{{ $hr->id }}/{{ $code }}">Agregar Horario</a>
                @endif
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif