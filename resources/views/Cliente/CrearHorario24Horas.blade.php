@if($horario)
<div class="form-group">
    <h5>Jornada de 24 horas</h5>
    <hr class="hrdark mb-4">
    <p class="mb-4">No es necesario realizar ninguna accion simplemente de click en guardar para continuar.</p>
</div>
<div class="hid">
    <input type="hidden" class="hid" name="tipo" value="{{ $horario->id }}">
</div>
<div class="form-group">
    <button type="button" class="btn btn-danger btn-sm float-left" data-dismiss="modal">Cancelar</button>
    <button tyoe="submit" class="btn btn-success btn-sm float-right">Guardar</button>
</div>
@endif