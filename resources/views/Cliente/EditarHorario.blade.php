@if(isset($horario) && isset($code) && isset($tipo))
<div class="modal-body">
    <div class="header mb-4">
        <h4>Editar horario del dia {{ $horario->dia->nombre }}</h4>
        <hr class="hrdark d-block">
    </div>
    <form action="MiNegocio/HorarioAtencion/Confirmar" method="post" class="sendAjax">
        <div class="form-group form-group-tpl form-group-dark">
            <input type="text" name="tipo" class="select-tpl" value="si">
            <div class="select select-lg">
                <label>{{ $horario->horarioTipo->nombre }}</label>
                <span class="icon-plus"></span>
                <div class="options scroll-white">
                    <ol><!--EL DATA-LIST ES EL VALOR DEL OPTION-SELECT-->
                        <li data-list="">Seleccione</li>
                    @if(!$tipo->isEmpty())
                        @foreach($tipo as $t)
                        <li data-list="{{ $t->id }}">{{ $t->nombre }}</li>
                        @endforeach
                    @endif
                    </ol>
               </div>
            </div>
            <label class="label label-lg active">Seleccione el tipo de horario</label>
        </div>
        <div class="form-group mb-5">
            <button type="submit" class="btn btn-success btn-sm btn-block">Confirmar</button>
        </div>
    </form>
    <form action="MiNegocio/HorarioAtencion/{{ $horario->id }}" method="post" class="sendAjax">
        <div class="hid">
            @csrf
            @method('PUT')
            <input type="hidden" name="dia" value="{{ $horario->dia->id }}">
            <input type="hidden" name="code" value="{{ $code }}">
       </div>
       <div class="loadHorario">
            @if($horario->horarioTipo->nombre == 'Doble jornada')
            <div class="form-group">
                <h5>Primera jornada</h5>
                <hr class="hrdark mb-4">
            </div>
            <div class="hid">
                <input type="hidden" class="hid" name="tipo" value="{{ $horario->horarioTipo->id }}">
            </div>
            <div class="form-group form-group-tpl form-group-dark">
                <input type="time" name="hora_inicio_1" class="form-control form-control-sm" value="{{ $horario->hora_inicio_1 }}">
                <label class="label label-lg placeholder">Desde</label>
            </div>
            <div class="form-group form-group-tpl form-group-dark">
                <input type="time" name="hora_fin_1" class="form-control form-control-sm" value="{{ $horario->hora_fin_1 }}">
                <label class="label label-lg placeholder">Hasta</label>
            </div>
            <div class="form-group">
                <h5>Segunda jornada</h5>
                <hr class="hrdark mb-4">
            </div>
            <div class="form-group form-group-tpl form-group-dark">
                <input type="time" name="hora_inicio_2" class="form-control form-control-sm" value="{{ $horario->hora_inicio_2 }}">
                <label class="label label-lg placeholder">Desde</label>
            </div>
            <div class="form-group form-group-tpl form-group-dark">
                <input type="time" name="hora_fin_2" class="form-control form-control-sm" value="{{ $horario->hora_fin_2 }}">
                <label class="label label-lg placeholder">Hasta</label>
            </div>
            @elseif($horario->horarioTipo->nombre == '24 Horas')
            <div class="form-group">
                <h5>Jornada de 24 horas</h5>
                <hr class="hrdark mb-4">
                <p class="mb-4">No es necesario realizar ninguna accion simplemente de click en guardar para continuar.</p>
            </div>
            <div class="hid">
                <input type="hidden" class="hid" name="tipo" value="{{ $horario->id }}">
            </div>
            @else
            <div class="form-group">
                <h5>{{ $horario->horarioTipo->nombre }}</h5>
                <hr class="hrdark mb-4">
            </div>
            <div class="hid">
                <input type="hidden" class="hid" name="tipo" value="{{ $horario->horarioTipo->id }}">
            </div>
            <div class="form-group form-group-tpl form-group-dark">
                <input type="time" name="hora_inicio_1" class="form-control form-control-sm" value="{{ $horario->hora_inicio_1 }}">
                <label class="label label-lg placeholder">Desde</label>
            </div>
            <div class="form-group form-group-tpl form-group-dark">
                <input type="time" name="hora_fin_1" class="form-control form-control-sm" value="{{ $horario->hora_fin_1 }}">
                <label class="label label-lg placeholder">Hasta</label>
            </div>
            @endif
            <div class="form-group">
                <button type="button" class="btn btn-danger btn-sm float-left" data-dismiss="modal">Cancelar</button>
                <button tyoe="submit" class="btn btn-success btn-sm float-right">Guardar</button>
            </div>
       </div>
    </form>
</div>
@endif