@extends('Layout.Cliente.master')
@section('titulo','Tu Plan | Perfil')
@section('contenido')
<form action="clienteSave" class="container p-3 sendAjax disabled-success">
    <div class="row m-0 p-3 darkBlue br-5 bs-m-dark align-items-center">
        <div class="col-lg-12">
            <h4>Datos personales <button type="button" class="btn btn-sm btn-orange float-right active-form" data-remove="btn-orange">Editar</button></h4>
            <hr class="hrdark mb-4">
        </div>
        <div class="col-lg-3 text-center">
            <div class="hid">
                <input type="text" name="imageUser" id="img-user-upload" class="hid" hidden>
            </div>
            <div class="load-image-upload">
                <img src="{{ User::foto() }}" alt="imagen usuario" class="image-user">
            </div>
            <button type="button" class="btn btn-success btn-sm loadPost" data-route="perfilUsuario" disabled>Editar Imagen</button>
        </div>
        <div class="col-lg-9">
            <div class="row m-0">
                <div class="col-lg-6">
                    <div class="form-group form-group-tpl form-group-dark">
                        <input type="text" name="primer_nombre" class="form-control" value="{{ User::primerNombre() }}" disabled>
                        <label class="label label-lg active">Primer nombre</label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-tpl form-group-dark">
                        <input type="text" name="segundo_nombre" class="form-control" value="{{ User::segundoNombre() }}" disabled>
                        <label class="label label-lg active">Segundo nombre</label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-tpl form-group-dark">
                        <input type="text" name="primer_apellido" class="form-control" value="{{ User::primerApellido() }}" disabled>
                        <label class="label label-lg active">Primer apellido</label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-tpl form-group-dark">
                        <input type="text" name="segundo_apellido" class="form-control" value="{{ User::segundoApellido() }}" disabled>
                        <label class="label label-lg active">Segundo apellido</label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-tpl form-group-dark">
                        <input type="date" name="fecha_nacimiento" class="form-control" value="{{ User::fechaNacimiento() }}" disabled>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <h4>Datos de contacto</h4>
            <hr class="hrdark mb-4">
        </div>
        <div class="col-lg-4">
            <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="celular" class="form-control" value="{{ User::celular() }}" disabled>
                <label class="label label-lg active">Celular</label>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="ciudad" class="select-tpl" value="{{ User::ciudadId() }}" disabled>
                <div class="select select-lg" role="button" tabindex="0" disabled>
                    <label>{{ User::ciudadNombre() }}</label>
                    <span class="icon-plus"></span>
                    <div class="options scroll-white">
                        <ol>
                            @foreach($ciudades as $c)
                            <li data-list="{{ $c->id }}">{{ $c->nombre }}</li>
                            @endforeach
                        </ol>
                   </div>
                </div>
           </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="direccion" class="form-control" value="{{ User::direccion() }}" disabled>
                <label class="label label-lg active">Direccion</label>
            </div>
        </div>
    </div>
</form>
@endsection
@section('masterjs')
<script src="{{ asset('js/Jcrop/jquery.Jcrop.min.js') }}"></script>
<script src="{{ asset('js/Image/image.js') }}"></script>
@endsection