@if($reserva && $code)
<div class="modal-body">
    <div class="header mb-4">
        <h4>Crear tipo de reserva</h4>
        <hr class="hrdark d-block">
    </div>
    <form action="MiNegocio/ReservaDisponible/{{ $reserva->id }}" method="post" class="sendAjax">
        <div class="hid">
            @csrf
            @method('PUT')
            <input type="hidden" class="hid" name="code" value="{{ $code }}">
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="text" name="nombre" class="form-control form-control-sm" value="{{ $reserva->nombre }}">
            <label class="label label-sm active">Nombre del tipo</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <textarea name="informacion" rows="3" class="form-control resize">{{ $reserva->informacion }}</textarea>
            <label class="label label-sm active">Informacion del tipo de reserva</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="number" name="minimo" class="form-control form-control-sm" value="{{ $reserva->numero_minimo_personas }}">
            <label class="label label-sm active">Numero minimo de personas</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="number" name="maximo" class="form-control form-control-sm" value="{{ $reserva->numero_maximo_personas }}">
            <label class="label label-sm active">Numero maximo de personas</label>
        </div>
        <h4>Falta foto</h4>
        <button type="submit" class="btn btn-success btn-sm float-right">Guardar</button>
        <button type="button" class="btn btn-danger btn-sm float-left" data-dismiss="modal">Cerrar</button>
    </form>
</div>
@endif