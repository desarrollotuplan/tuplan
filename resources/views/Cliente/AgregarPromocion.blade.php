@if($promo) @php $code = User::randomString(6); @endphp
<div class="col-lg-4 col-md-6 col-sm-6 col-12" data-code="{{ $code }}">
    <div class="card card-tpl card-dark bs-m-dark">
        <div class="card-img-content" style="height:250px;">
            <img src="{{ asset($promo->imagen) }}" alt="foto-negocio" class="card-img fit-cover" style="height:250px;">
        </div>
        <div class="card-body">
            <h5 class="card-title" title="">{{ $promo->nombre }}</h5>
            <p class="card-description h-87 title">{{ $promo->descripcion }}</p>
        </div>
        <div class="card-footer">
            <button class="btn btn-sm btn-outline-success btn-option-xs">Vista Previa</button>
            <button class="btn btn-sm btn-primary btn-option-xs float-right loadGet" data-route="MiNegocio/Promociones/{{ $promo->id }}/edit/{{ $code }}">Editar</button>
        </div>
    </div>
</div>
@endif