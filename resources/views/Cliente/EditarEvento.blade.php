@if($eve && $code)
<div class="modal-body">
    <div class="header mb-4">
        <h4>Editar Evento</h4>
        <hr class="hrdark d-block">
    </div>
    <form action="MiNegocio/Eventos/{{ $eve->id }}" method="post" class="sendAjax">
        <div class="hid">
            @csrf
            @method('PUT')
            <input type="hidden" name="code" value="{{ $code }}" class="hid">
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="text" name="nombre" class="form-control form-control-sm" value="{{ $eve->nombre }}">
            <label class="label label-sm placeholder">Titulo del evento</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <textarea name="descripcion" rows="3" class="form-control resize">{{ $eve->descripcion }}</textarea>
            <label class="label label-sm placeholder">Descripcion</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <textarea name="informacion" rows="3" class="form-control resize">{{ $eve->informacion_adicional }}</textarea>
            <label class="label label-sm placeholder">Informacion Adicional</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="date" name="fecha_inicio" class="form-control form-control-sm" value="{{ $eve->fecha_inicio }}">
            <label class="label label-sm placeholder">Fecha de inicio</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="time" name="hora_inicio" class="form-control form-control-sm" value="{{ $eve->hora_inicio }}">
            <label class="label label-sm placeholder">Hora Inicio</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="date" name="fecha_fin" class="form-control form-control-sm" value="{{ $eve->fecha_fin }}">
            <label class="label label-sm placeholder">Fecha de finalizacion</label>
        </div>
        <div class="form-group form-group-tpl form-group-dark">
            <input type="time" name="hora_fin" class="form-control form-control-sm" value="{{ $eve->hora_fin }}">
            <label class="label label-sm placeholder">Hora de finalizacion</label>
        </div>
        <h4>Falta foto</h4>
        <button type="submit" class="btn btn-success btn-sm float-right">Actualizar</button>
        <button type="button" class="btn btn-danger btn-sm float-left" data-dismiss="modal">Cerrar</button>
    </form>
</div>
@endif