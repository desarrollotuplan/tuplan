<div class="modal-body">
    <div class="header mb-4">
        <h4>Crear categoria</h4>
        <hr class="hrdark d-block">
    </div>
    <form action="CategoriaProducto" method="post" class="sendAjax">
        <div class="form-group form-group-tpl form-group-dark">
            <input type="text" name="categoria" class="form-control form-control-sm">
            <label class="label label-sm">Nombre de la categoria</label>
        </div>
        <button type="submit" class="btn btn-success btn-sm float-right">Crear</button>
        <button type="button" class="btn btn-danger btn-sm float-left" data-dismiss="modal">Cerrar</button>
    </form>
</div>