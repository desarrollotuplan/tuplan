@if($horario && $code && $tipo) 
<span class="badge badge-success-lighten">{{ $tipo->nombre }}</span>
<a href="#{{ $code }}" class="uri" data-toggle="collapse">Detalles</a>
<div id="{{ $code }}" class="collapse p-2 show">
    @if($tipo->nombre == 'Doble jornada')
    <h6>
        Primera jornada Desde: <span class="badge badge-info-lighten">{{ GSD::getTimeFormat($horario->hora_inicio_1) }}</span>
        Hasta: <span class="badge badge-warning-lighten">{{ GSD::getTimeFormat($horario->hora_fin_1) }}</span>
    </h6>
    <h6>
        Segunda jornada Desde: <span class="badge badge-info-lighten">{{ GSD::getTimeFormat($horario->hora_inicio_2) }}</span>
        Hasta: <span class="badge badge-warning-lighten">{{ GSD::getTimeFormat($horario->hora_fin_2) }}</span>
        <a href="#" class="uri text-info loadGet" data-route="MiNegocio/HorarioAtencion/{{ $horario->id }}/edit/{{ $code }}">Editar</a>
        <a href="#" class="uri text-info loadGet float-right text-warning" data-route="MiNegocio/HorarioAtencion/{{ $horario->id }}/edit/{{ $code }}">Eliminar</a>
    </h6>
    @else
    <h6>
        Desde: <span class="badge badge-info-lighten">{{ GSD::getTimeFormat($horario->hora_inicio_1) }}</span>
        Hasta: <span class="badge badge-warning-lighten">{{ GSD::getTimeFormat($horario->hora_fin_1) }}</span>
        <a href="#" class="uri text-info loadGet" data-route="MiNegocio/HorarioAtencion/{{ $horario->id }}/edit/{{ $code }}">Editar</a>
        <a href="#" class="uri text-info loadGet float-right text-warning" data-route="MiNegocio/HorarioAtencion/{{ $horario->id }}/edit/{{ $code }}">Eliminar</a>
    </h6>
    @endif
</div>
@else
<span class="badge badge-danger-lighten">Sin definir</span>
<a href="#" class="loadGet uri" data-route="MiNegocio/HorarioAtencion/create/{{ $hr->id }}/{{ $code }}">Agregar Horario</a>
@endif