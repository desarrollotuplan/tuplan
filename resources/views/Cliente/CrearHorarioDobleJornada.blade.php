@if($horario)
<div class="form-group">
    <h5>Primera jornada</h5>
    <hr class="hrdark mb-4">
</div>
<div class="hid">
    <input type="hidden" class="hid" name="tipo" value="{{ $horario->id }}">
</div>
<div class="form-group form-group-tpl form-group-dark">
    <input type="time" name="hora_inicio_1" class="form-control form-control-sm">
    <label class="label label-lg placeholder">Desde</label>
</div>
<div class="form-group form-group-tpl form-group-dark">
    <input type="time" name="hora_fin_1" class="form-control form-control-sm">
    <label class="label label-lg placeholder">Hasta</label>
</div>
<div class="form-group">
    <h5>Segunda jornada</h5>
    <hr class="hrdark mb-4">
</div>
<div class="form-group form-group-tpl form-group-dark">
    <input type="time" name="hora_inicio_2" class="form-control form-control-sm">
    <label class="label label-lg placeholder">Desde</label>
</div>
<div class="form-group form-group-tpl form-group-dark">
    <input type="time" name="hora_fin_2" class="form-control form-control-sm">
    <label class="label label-lg placeholder">Hasta</label>
</div>
<div class="form-group">
    <button type="button" class="btn btn-danger btn-sm float-left" data-dismiss="modal">Cancelar</button>
    <button tyoe="submit" class="btn btn-success btn-sm float-right">Guardar</button>
</div>
@endif