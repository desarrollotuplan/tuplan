@if($categoria)
    {{ $categoria->nombre }}
    <small class="badge badge-success-lighten ml-2">{{ $categoria->productos()->count() }}</small>
@endif