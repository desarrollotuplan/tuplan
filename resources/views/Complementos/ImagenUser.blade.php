<div class="modal-body">
    <div class="hid">
        <input type="file" id="img" class="hid img-jcrop">
    </div>
    <div class="load-image-js mb-3 scroll-white p-2">
        <div class="load-images" data-input="#img">
            <img src="{{  asset('svg/images.svg') }}" alt="cargar imagenes">
            <h4>Cargar imagen</h4>
        </div>
    </div>
    <div>
        <button class="btn btn-danger btn-sm float-left" data-dismiss="modal">Cancelar</button>
        <button class="btn btn-success btn-sm float-right btn-change-image" data-input="{{ $input }}" data-img="{{ $image }}" data-dismiss="modal">Cargar</button>
    </div>
</div>