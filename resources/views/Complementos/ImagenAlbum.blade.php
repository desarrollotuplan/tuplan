<form action="{{ $route }}" method="post" class="sendAjax">
    <div class="hid">
        <input type="file" id="img" class="hid img-jcrop">
        <input type="text" name="imageAlbum" id="album" class="hid">
    </div>
    <div class="load-image-js mb-3 scroll-white p-2">
        <div class="load-images" data-input="#img">
            <img src="{{  asset('svg/images.svg') }}" alt="cargar imagenes">
            <h4>Cargar imagen</h4>
        </div>
    </div>
    <div class="text-right">
        <button type="button" class="btn btn-success btn-sm btn-change-image sendLoad" data-input="#album">Cargar</button>
    </div>
</div>