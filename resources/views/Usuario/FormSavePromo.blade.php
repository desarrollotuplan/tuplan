@if(isset($guardado))
    <div class="text-white p-3">
        <h6>Ya se encuentra guardada</h6>
        <div class="text-right">
            <button class="btn-option-xs btn btn-sm btn-orange float-right loadPost" data-route="deleteSavePromo/{{ $guardado->id }}">Dejar de guardar</button>
        </div>
    </div>
@elseif(isset($promo))
    <form action="savePromotion" class="p-3 w-100 sendAjax route-link">
       <div class="hid">
           @csrf
       </div>
        <div class="form-group form-group-tpl form-group-dark">
            <label class="label label-sm active">Nombre</label>
            <input type="text" name="nombre" class="form-control form-control-sm" value="{{ $promo->nombre }}" autocomplete="off">
        </div>
        <button type="submit" class="btn btn-primary btn-sm btn-option-xs float-right">Guardar</button>
    </form>
@endif
