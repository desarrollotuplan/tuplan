<div class="modal-body">
    <form action="comentario" class="sendAjax route-link" method="post">
        <div class="form-group">
            <h4>Comentar</h4>
            <hr class="hrdark mb-4">
        </div>
        <div class="hid">
            @csrf
        </div>
       <div class="form-group form-group-tpl form-group-dark">
            <textarea name="comentario" rows="3" class="form-control resize scroll-white"></textarea>
            <label class="label label-sm">Comentario</label>
        </div>
        <div>
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-success btn-sm float-right">Guardar</button>
        </div>
    </form>
</div>