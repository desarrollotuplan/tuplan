@if(!$comentarios->isEmpty())
    @foreach($comentarios as $co)
        <div class="col-lg-12">
            <div class="p-2 dark">
                <div class="darkBlue p-1" data-key="{{ $co->id }}">
                    <div class="row m-0">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-4 text-center">
                            <img src="{{ asset($co->usuario->foto) }}" alt="" class="rounded-circle img-70 img-center mb-1">
                            <small>2017/05/06</small>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-8">
                            <h6 class="title">{{ $co->usuario->primer_nombre }}</h6>
                            <p class="h-60 overflow-h">{{ $co->comentario }}</p>
                            <h6>
                            @php 
                                foreach($calificacionUsuarios as $cu){
                                    if($cu['usuario'] == $co->usuario_id){
                                        foreach($cu['calificacion'] as $c){
                            @endphp
                            <span class="">{{ $c->nombre }}:</span><span class="text-success title"> {{ $c->puntos }}</span>
                            @php }  }  }  @endphp
                           </h6>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-12">
                            <button class="btn-option-xs btn btn-sm btn-outline-success float-right" title="Util">
                                <span class="icon-plus m-0"></span> 0
                            </button>
                            <button class="btn-option-xs btn btn-sm btn-outline-warning float-right" title="Denunciar">
                                <span class="icon-flag m-0"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif