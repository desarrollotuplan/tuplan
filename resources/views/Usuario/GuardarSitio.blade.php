@if(isset($negocio))
<form action="Sitios/{{ $negocio->id_unico }}/guardado" class="sendAjax p-3" method="post">
    <header>
        <h5>Guardar sitio como</h5> <hr class="hrdark">
    </header>
    <div class="form-group m-0">
        @csrf
    </div>
    <div class="form-group form-group-tpl form-group-dark mb-4">
        <label class="label label-sm active">Nombre</label>
        <input type="text" name="nombre" class="form-control form-control-sm" value="{{ $negocio->nombre }}">
    </div>
    <div class="form-group">
        <button type="button" class="btn btn-sm btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-sm btn-success btn-sm float-right">Guardar</button>
    </div>
</form>
@endif
