<section class="reservas p-3">
   
    <div class="row m-0">
        <div class="col-lg-12">
            <header>
                <h4>Reservas</h4>
                <hr class="hrdark">
            </header>
        </div>
    </div>
    
    <div class="row m-0">
        @if(isset($reserva))
            @if(!$rDisponibles->isEmpty())
                @foreach($rDisponibles as $rd)
                    <div class="col-lg-4 col-md-4 col-sm-6 cpl-6">
                        <div class="card darkBlue border-0">
                            <img src="{{ asset($rd->imagen) }}" class="card-img-top fit-cover" style="height: 150px;">
                            <div class="card-body">
                                <h5 class="card-title text-black">{{ $rd->nombre }}</h5>
                                <p class="d-block text-truncate">{{ $rd->informacion }}</p>
                                <button class="btn btn-sm btn-success btn-option-xs loadPost" data-route-link="detallesReservaDisponible/{{ $rd->id }}">Reservar</button>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-lg-12 text-center">
                    Este sitio aun no registra los tipos de reserva disponibles.
                </div>
            @endif
        @else
            <div class="col-lg-12 text-center">
                Este sitio no cuenta con ningun tipo de reserva
            </div>
        @endif
    </div>
    
</section>
   
