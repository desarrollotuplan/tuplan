<div class="modal-body">
   <div class="row m-0">
        <div class="col-lg-2 col-md-2 col-sm-2 col-auto">
            <img src="{{ asset($promo->negocio->folder.$promo->negocio->logo) }}" alt="" style="height:50px; width:50px;" class="mb-2">
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10 col-9">
            <a href="{{ route('detalleSitio',['negocio'=>$promo->negocio->id_unico]) }}" class="text-white" target="_blank">
                <h5 class="text-truncate">{{ $promo->negocio->nombre }}</h5>
            </a>
            <small class="d-block text-truncate text-white-50">{{ $promo->negocio->eslogan }}</small>
        </div>
    </div>
    <div class="fotorama w-100 mb-2" data-nav="thumbs" data-allowfullscreen="native" data-autoplay="4000">
        <img src="{{ asset($promo->imagen) }}" class="w-100">
        @if(isset($imagenes) && !$imagenes->isEmpty())
            @foreach($imagenes as $i)
                <img src="{{ asset($i->ruta) }}" alt="">
            @endforeach
        @endif
    </div>
    <script>$('.fotorama').fotorama();</script>
   
    
    <h4>{{ $promo->nombre }}</h4>
    <p class="text-white-50">{{ $promo->descripcion }}</p>
    <p class="text-white-50">{{ $promo->informacion_adicional }}</p>
    <div class="options-promotion">
        <button id="savePromotion" class="btn btn-sm btn-success btn-option-xs mb-2 dropdown-toggle loadPost" data-route-link="formSavePromo" data-toggle="dropdown">Guardar promo</button>
        <div class="dropdown-menu savePromo btooltip" aria-labelledby="savePromotion">
            
        </div>
    </div>
    <ul class="list-group list-group-flush list-group-darkBlue list-group-icon">
       
        <li class="list-group-item">
            <span class="icon-clock"></span>
            <h6 class="badge badge-success-lighten">Desde</h6><br>
            {{ GSD::getDateSpanishFull($promo->fecha_inicio) }} Desde las {{ GSD::getTimeFormat($promo->hora_inicio) }}
        </li>
        
        <li class="list-group-item">
            <span class="icon-clock"></span>
            <h6 class="badge badge-danger-lighten">Hasta</h6><br>
            {{ GSD::getDateSpanishFull($promo->fecha_fin) }} Hasta las {{ GSD::getTimeFormat($promo->hora_fin) }}
        </li>
        
        <li class="list-group-item">
            <span class="icon-phone"></span>
            <h6 class="badge badge-info-lighten">Telefono</h6><br>
            {{ $promo->negocio->telefono }}
        </li>
        
        <li class="list-group-item">
            <span class="icon-location"></span>
            <h6 class="badge badge-info-lighten">Direccion</h6><br>
            {{ $promo->negocio->direccion }}
        </li>
        
    </ul>
</div>
<div class="modal-footer border-0">
    <button class="btn btn-sm btn-danger historyback" data-dismiss="modal">Cerrar</button>
</div>