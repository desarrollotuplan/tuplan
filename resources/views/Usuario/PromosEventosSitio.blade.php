<div class="p-2 dark">
    <div class="row m-0">
         @if(!$promociones->isEmpty())
            @foreach($promociones as $pr)
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                <div class="card card-tpl card-dark bs-m-dark" data-key="{{ $pr->id_unico }}">
                    <div class="tipo p-1">
                        <h4>Promocion</h4>
                    </div>
                    <div class="card-img-content"  style="height:250px;">
                        <img src="{{ asset($pr->imagen) }}" alt="foto-negocio" class="card-img fit-cover" style="height:250px;">
                    </div>
                    <div class="card-body">
                        <small class="card-category text-success"><span class="icon-spoon-knife"></span> {{ $pr->nombre_categoria }}</small>
                        <h5 class="card-title" title="">{{ $pr->nombre }}</h5>
                        <p class="card-description h-87 title">{{ $pr->descripcion }}</p>
                    </div>
                    <div class="card-footer">
                        <a  href="{{ Route('promocionesGet',['promocion'=>$pr->id_unico]) }}" target="_blank" class=" btn btn-sm btn-outline-primary btn-option-xs float-right">Ver mas...</a>
                    </div>
                </div>
            </div>
            @endforeach
        @endif
        
        @if(!$eventos->isEmpty())
            @foreach($eventos as $ev)
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                <div class="card card-tpl card-dark bs-m-dark">
                   <div class="tipo p-1">
                        <h4>Evento</h4>
                    </div>
                    <div class="card-img-content"  style="height:250px;">
                        <img src="{{ asset($ev->imagen) }}" alt="foto-negocio" class="card-img fit-cover" style="height:250px;">
                    </div>
                    <div class="card-body">
                        <small class="card-category text-success"><span class="icon-spoon-knife"></span> {{ $ev->nombre_categoria }}</small>
                        <h5 class="card-title" title="">{{ $ev->nombre }}</h5>
                        <p class="card-description h-87 title">{{ $ev->descripcion }}</p>
                    </div>
                    <div class="card-footer">
                        <a  href="{{ Route('eventosGet',['evento'=>$ev->id_unico]) }}" target="_blank" class=" btn btn-sm btn-outline-primary btn-option-xs float-right">Ver mas...</a>
                    </div>
                </div>
            </div>
            @endforeach
        @endif
    </div>
</div>