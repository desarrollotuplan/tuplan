@if(!$recomendados->isEmpty())
    @foreach($recomendados as $r)
        <li class="loadPost" data-route="detallesRecomendado/{{ $r->id_unico }}">
            <div class="row align-items-center m-0">
                <div class="col-4">
                    <img src="{{ $r->folder.$r->logo }}" alt="foto-negocio" class="fit-cover img-select">
                </div>
                <div class="col-8 h-mini">
                    <h6 class="title-option">{{ $r->nombre }}</h6>
                    <small class="{{ $r->color }} d-block text-truncate">{{ $r->categoria }}</small>
                </div>
            </div>
        </li>
    @endforeach
@endif