@if(!$intereses->isEmpty())
    @foreach($intereses as $i)
        <button class="btn btn-sm btn-success mb-2" data-method="delete" data-route="intereses/{{ $i->id }}" data-confirm="Seguro que desea eliminar la categoria {{ $i->categoria->nombre }}">{{ $i->categoria->nombre }}</button>
    @endforeach
@endif