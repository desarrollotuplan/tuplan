<div id="loadInformation" class="darkBlue bs-m-dark br-5">
    <div class="informacionSitio p-3 mb-3">
        <div class="eslogan">
           
            <div class="row m-0">
                <div class="col-lg-12">
                    <h4>{{ $negocio->nombre }}</h4>
                    <hr class="hrdark">
                    <h6 class="mb-3">Etiquetas</h6>
                    <p>{{ $negocio->eslogan }}</p>
                    <p>{{ $negocio->descripcion }}</p>
                    <h4>Fotos</h4>
                    <hr class="hrdark">
                    <div class="fotorama w-100" data-nav="thumbs" data-allowfullscreen="native" data-autoplay="4000">
                        <img src="{{ asset($negocio->folder.$negocio->foto_perfil) }}" alt="">
                        @if(!$imagenes->isEmpty())
                            @foreach($imagenes as $i)
                                <img src="{{ asset($i->ruta) }}" alt="" class="w-100">
                            @endforeach
                        @else
                            Sin fotos del sitio.
                        @endif
                    </div>
                    <script>$('.fotorama').fotorama();</script>
                    <h4>Mapa</h4>
                    <hr class="hrdark">
                </div>
            </div>

        </div>
    </div>
</div>