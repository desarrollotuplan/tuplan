@extends('Layout.Usuario.master')
@section('titulo','Tu Plan | Promociones')
@section('contenido')
<div class="p-4 darkBlue bs-md-dark">
   <form action="Guardados/Buscar" method="post" class="row m-0 align-items-center sendAjax">
        <div class="col-lg-3 col-md-12 col-sm-12">
            <div class="form-goup">
                <h3 class="title">Guardados <img src="{{ asset('Imagenes/Favicon.ico') }}" alt="search" class="faviconSearch"></h3>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
           <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="tipo" class="select-tpl" value="todas">
                <div class="select select-sm" role="button" tabindex="0">
                    <label>Todos</label>
                    <span class="icon-plus"></span>
                    <div class="options scroll-white">
                        <ol>
                            <li data-list="todos">Todos</li>
                            <li data-list="sitios">Sitios</li>
                            <li data-list="promociones">Promociones</li>
                            <li data-list="eventos">Eventos</li>
                        </ol>
                   </div>
                </div>
                <label class="label label-sm placeholder">Tipo</label>
           </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-10">
            <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="nombre" class="form-control form-control-sm">
                <label class="label label-sm">Nombre del negocio</label>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-2 text-center">
            <button class="btn btn-sm btn-success">Buscar</button>
        </div>
    </form>
</div>

<section class="sitiosRecomendados p-3">
   <div class="row m-0">
       @if(!$guardados->isEmpty())
        <div class="col-lg-12">
            <div class="darkBlue bs-m-dark table-tpl table-dark">
                <div class="header">
                    <bb>Historial</bb>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped text-center m-0 dinamic">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Creado</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Editar</th>
                                <th scope="col">Eliminar</th>
                                <th scope="col">Ver</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($guardados as $g) @php $div = User::randomString(5); @endphp
                            <tr id="{{ $div }}">
                                <th>{{ $loop->iteration }}</th>
                                <td>{{ $g->nombre }}</td>
                                <td>{{ $g->created_at }}</td>
                                <td>
                               @if($g->negocio_id)
                                   <span class="badge badge-warning-lighten badge-strong p-2">Sitio</span>
                                    @php  $ruta = route('detalleSitio',['negocio'=>Ddunico::negocio($g->negocio_id)]); @endphp
                               @elseif($g->evento_id)
                                   <span class="badge badge-success-lighten badge-strong p-2">Evento</span>
                                    @php  $ruta = route('eventosGet',['evento'=>Ddunico::evento($g->evento_id)]); @endphp
                               @elseif($g->promocion_id)
                                   <span class="badge badge-info-lighten badge-strong p-2">Promo</span>
                                   @php  $ruta = route('promocionesGet',['promocion'=>Ddunico::promo($g->promocion_id)]); @endphp
                               @endif
                                </td>
                                
                                <td><button class="btn btn-outline-info btn-option-xs btn-sm loadPost" data-route="Guardados/edit/{{ $g->id }}">Editar</button></td>
                                <td><button class="btn btn-outline-danger btn-option-xs btn-sm" data-confirm="Esta seguro que desea eliminar este registro con el nombre de {{ $g->nombre }}" data-method="delete" data-route="Guardados/delete/{{ $g->id }}/{{ $div }}">Eliminar</button></td>
                                <td><a href="{{ $ruta }}" target="_blank" class="btn btn-outline-primary btn-sm">Ver</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>    
                </div>
            </div>
        </div>
       @endif
    </div>
</section>

@endsection