<div class="p-3 darkBlue">
    <div class="row m-0">
        <div class="col-lg-12">
            <header>
                <h4>Redes sociales</h4>
                <hr class="hrdark">
            </header>
        </div>
        <div class="col-lg-12 text-center">
            @if($redes)
               
                @if($redes->facebook)
                    <a href="{{ $redes->facebook }}" target="_blank" class="btn btn-social btn-facebook">
                        <span class="icon-facebook2"></span> Facebook
                    </a>
                @endif
                @if($redes->instagram)
                    <a href="{{ $redes->instagram }}" target="_blank" class="btn btn-social btn-instagram">
                        <span class="icon-instagram"></span> Instagram
                    </a>
                @endif
                @if($redes->twitter)
                    <a href="{{ $redes->twitter }}" target="_blank" class="btn btn-social btn-twitter">
                        <span class="icon-twitter"></span> Twitter
                    </a>
                @endif
                @if($redes->youtube)
                    <a href="{{ $redes->youtube }}" target="_blank" class="btn btn-social btn-youtube">
                        <span class="icon-youtube"></span> Youtube
                    </a>
                @endif
                @if($redes->website)
                    <a href="{{ $redes->website }}" target="_blank" class="btn btn-social btn-facebook">
                        <span class="icon-sphere"></span> Sitio web
                    </a>
                @endif
                @if($redes->app_android)
                    <a href="{{ $redes->app_android }}" target="_blank" class="btn btn-social btn-android">
                        <span class="icon-android"></span> Play store
                    </a>
                @endif
                @if($redes->app_ios)
                    <a href="{{ $redes->app_ios }}" target="_blank" class="btn btn-social btn-apple">
                        <span class="icon-appleinc"></span> App store
                    </a>
                @endif
               
            @else
                <div class="p-3">Este sitio no cuenta con redes sociales.</div>
            @endif
            
           
        </div>
    </div>
</div>