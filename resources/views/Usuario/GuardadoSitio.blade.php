@if(isset($negocio))
    @if(!isset($guardado))
        <button class="btn btn-sm btn-success btn-option-xs mt-2 loadGet" data-route="Sitios/{{ $negocio }}/guardado/create">
            <span class="icon-bookmark"></span> Guardar
        </button>
    @else
        <button class="btn btn-sm btn-success btn-option-xs mt-2" data-method="delete" data-route="Sitios/{{ $negocio }}/guardado/{{ $guardado->id }}">
            <span class="icon-bookmark"></span> No guardar
        </button>
    @endif
@endif