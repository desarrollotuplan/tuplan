@extends('Layout.Usuario.master')
@section('titulo','Tu Plan | Sitios')
@section('contenido')

<section class="headerSitios darkBlue p-3 bs-m-dark">
    <form action="Sitios/Buscar" method="post" class="row m-0 align-items-center sendAjax">
        <div class="col-lg-2 col-md-12 col-sm-12">
            <div class="form-goup">
                <h2 class="title">Sitios <img src="{{ asset('Imagenes/Favicon.ico') }}" alt="search" class="faviconSearch"></h2>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
           <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="ciudad" class="select-tpl" value="{{ User::ciudadId() }}">
                <div class="select select-sm" role="button" tabindex="0">
                    <label>{{ User::ciudadNombre() }}</label>
                    <span class="icon-plus"></span>
                    <div class="options scroll-white">
                        <ol>
                            <li data-list="todas">Todas</li>
                            @foreach($ciudades as $c)
                                <li data-list="{{ $c->id }}">{{ $c->nombre }}</li>
                            @endforeach
                        </ol>
                   </div>
                </div>
                <label class="label label-sm placeholder">Ciudad</label>
           </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
           <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="categoria" class="select-tpl" value="todas">
                <div class="select select-sm" role="button" tabindex="0">
                    <label>Todas</label>
                    <span class="icon-plus"></span>
                    <div class="options scroll-white">
                        <ol>
                            <li data-list="todas">Todas</li>
                            @foreach($categorias as $c)
                                <li data-list="{{ $c->id }}">{{ $c->nombre }}</li>
                            @endforeach
                        </ol>
                   </div>
                </div>
                <label class="label label-sm placeholder">Categoria</label>
           </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-10">
            <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="nombre" class="form-control form-control-sm">
                <label class="label label-sm">Nombre del negocio</label>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-2 text-center">
            <button class="btn btn-sm btn-success">Buscar</button>
        </div>
<!-- EJEMPLOS DE LOS CHEBOX QUE DEBO HACER-->
<!--
        <div class="col-lg-2">
            <div class="form-group form-group-tpl form-group-dark radio">
               <input type="radio">
                <label class="checkbox">hola gente</label>
            </div>
            <div class="form-group form-group-tpl form-group-dark checkbox">
               <input type="checkbox">
                <label class="checkbox">hola gente</label>
            </div>
        </div>
-->
    </form>
</section>
<section class="sitiosRecomendados p-3">
   <div class="row m-0">
       <div class="col-lg-12">
            <h3>Recomendados</h3>
            <hr class="hrdark">
       </div>
       @foreach($intereses as $i)
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="card card-tpl card-dark bs-m-dark">
                <div class="card-img-content">
                    <img src="{{ asset($i->folder.$i->foto_perfil) }}" alt="foto-negocio" class="card-img fit-cover">
                </div>
                <div class="card-body">
                    <small class="card-category {{ $i->color }}"><span class="{{ $i->icono }}"></span> {{ $i->categoria }}</small>
                    <h5 class="card-title" title="">{{ $i->nombre }}</h5>
                    <p class="card-description h-87">{{ $i->descripcion }}</p>
                </div>
                <div class="card-footer">
                    @if(HorarioDias::getHorarioDia($i->id) === 'Abierto')
                        <span class="badge badge-success-lighten badge-strong">Abierto</span>
                    @else
                        <span class="badge badge-danger-lighten badge-strong">Cerrado</span>
                    @endif
                    <a href="#" class="uri btn btn-option-xs btn-outline-info loadPost newLink float-right" title="Ver mas" data-show-area="#info-negocios" data-show-direction="show-left" data-show-background="dark" data-route="Sitios/{{ $i->id_unico }}">
                        <span class="icon-eye-plus"></span> Ver mas...
                    </a>
                </div>
            </div>
       </div>
       @endforeach
    </div>
</section>
<section id="info-negocios" class="area-right-100 scroll-white animation-scroll" data-scroll-body="true">
    <nav class="navbar navbar-expand-md navbar-transparent-dark" data-hidden-transition="1000">
        <a class="navbar-brand close-area historyback" data-hidden-area="#info-negocios" >
            <span class="icon-circle-left"></span> Volver
        </a>
        <div class="option float-right btn-group ts-none">
            <button type="button" class="btn btn-sm btn-light btn-option-xs dropdown-toggle loadPost" data-toggle="dropdown" data-route="saveSitio"> 
                <span class="icon-cog"></span>
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-tpl">
                <a class="dropdown-item uri" href="#"><span class="icon-flag"></span> Denunciar sitio</a>
            </div>
        </div>
    </nav>
    <section id="loadContSitio" data-hidden-transition="1200"></section>
</section>
@endsection
@section('masterjs')
    @if(isset($negocio))
    <script>areaShow({ area: '#info-negocios', direction: 'show-left', background: 'dark' }); output({!! json_encode($negocio) !!});</script>
    @endif
@endsection



