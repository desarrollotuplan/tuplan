<section class="p-3 darkBlue br-5">
   <div class="row m-0 mb-2">
       
        <div class="col-lg-12">
            <header>
                <h4>Calificaciones</h4>
                <hr class="hrdark">
                <p class="mb-4">Esta calificacion se ha sacado del promedio de {{ $votantes }} personas</p>
            </header>
        </div>
        @if($calificacion)
            @foreach($calificacion as $c)                 
                @php $media = $c->cantidadPuntos / $votantes; @endphp
                @if($media >= 4 ) 
                    @php $color = 'bg-success'; @endphp
                @elseif($media == 3 && $media < 4)
                    @php $color = 'bg-primary'; @endphp
                @elseif($media < 3 )
                    @php $color = 'bg-danger'; @endphp
                @endif
                
                <div class="col-lg-3 col-sm-6 col-12">
                    <div class="card-icon light-blue bs-m-dark h-100">
                        <h2 class="title">{{ $media }}<span class="icon-star-empty h3"></span></h2>
                        <small class="title">{{ $c->nombre }}</small>
                        <div class="icon icon-right {{ $color }} br-5 bs-m-dark">
                            <span class="{{ $c->icono }}"></span>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
        
    </div>
    
    
    <div class="filtros row m-0 align-items-center">
       
        <div class="col-lg-12">
            <header>
                <h4>Comentarios</h4>
                <hr class="hrdark">
            </header>
        </div>
        
        <div class="col-lg-5">
            <form action="filtrarComentarios" method="get" class="sendChangeAjax route-link">
                <div class="form-group form-group-tpl form-group-dark mb-3">
                    <input type="text" name="filtro" class="select-tpl">
                    <div class="select select-sm">
                        <label>Recientes</label>
                        <span class="icon-plus"></span>
                        <div class="options scroll-white">
                            <ol><!--EL DATA-LIST ES EL VALOR DEL OPTION-SELECT-->
                                <li data-list="recientes">Recientes</li>
                                <li data-list="antiguos">Antiguos</li>
                            </ol>
                       </div>
                    </div>
                    <label class="label label-sm placeholder">Ordenar por</label>
               </div>
            </form>
        </div>
        
        <div class="col-lg-2">
<!--            <button class="btn btn-sm btn-success">Filtrar</button>-->
        </div>
        
        <div class="col-lg-5 text-right">
            @if(isset($calificacion_user))
                <button class="btn btn-sm btn-indigo loadGet" data-route-link="calificacion/{{ $calificacion_user->id }}/edit">Editar calificacion</button>
            @else
                <button class="btn btn-sm btn-indigo loadGet" data-route-link="calificacion/create">Calificar</button>
            @endif
            @if(isset($comentario_user))
                <button class="btn btn-sm btn-orange loadGet" data-route-link="comentario/{{ $comentario_user->id }}/edit">Editar comentario</button>
            @else
                <button class="btn btn-sm btn-orange loadGet" data-route-link="comentario/create">Comentar</button>
            @endif
        </div>
        
    </div>
        
        
    <div class="comentarios row m-0">
        
        @if(!$comentarios->isEmpty())
            @foreach($comentarios as $co)
                <div class="col-lg-12">
                    <div class="p-2 dark">
                        <div class="darkBlue p-1" data-key="{{ $co->id }}">
                            <div class="row m-0">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-4 text-center">
                                    <img src="{{ asset($co->usuario->foto) }}" alt="" class="rounded-circle img-70 img-center mb-1">
                                    <small>2017/05/06</small>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-8">
                                    <h6 class="title">{{ $co->usuario->primer_nombre }}</h6>
                                    <p class="h-60 overflow-h">{{ $co->comentario }}</p>
                                    <h6>
                                    @php 
                                        foreach($calificacionUsuarios as $cu){
                                            if($cu['usuario'] == $co->usuario_id){
                                                foreach($cu['calificacion'] as $c){
                                    @endphp
                                    <span class="">{{ $c->nombre }}:</span><span class="text-success title"> {{ $c->puntos }}</span>
                                    @php }  }  }  @endphp
                                   </h6>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-12">
                                    <button class="btn-option-xs btn btn-sm btn-outline-success float-right" title="Util">
                                        <span class="icon-plus m-0"></span> 0
                                    </button>
                                    <button class="btn-option-xs btn btn-sm btn-outline-warning float-right" title="Denunciar">
                                        <span class="icon-flag m-0"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
        
    </div>
</section>