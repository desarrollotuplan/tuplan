<div class="p-3">
    <div class="row m-0">
       
        <div class="col-lg-12">
            <header>
                <h4>Productos y servicios</h4>
                <hr class="hrdark">
            </header>
        </div>
        @if(!$categorias->isEmpty())
            @foreach($categorias as $c)
                <div class="col-lg-12">
                    <div class="darkBlue" data-key="{{ $c->id }}">
                        <div class="category p-3 transparent-dark-1">
                            <a href="#ui{{ $loop->iteration }}" data-toggle="collapse" aria-expanded="true">{{ $c->nombre }}</a>
                        </div>
                        <div id="ui{{ $loop->iteration }}" class="collapse show">

                            <div class="row m-0 p-1">
                               
                                @if(!$c->productos->isEmpty())
                                    @foreach($c->productos as $p)
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="card-mini br-0 border-tooltip darkBlue row m-0">
                                                <div class="col-lg-4 col-md-4 col-sm-3">
                                                    @foreach($p->imagenes as $i)
                                                        <img src="{{ asset($i->ruta) }}" alt="" class="h-90 fit-cover w-100">
                                                    @endforeach
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-9">
                                                    <h6 class="text-truncate">{{ $p->nombre }}</h6>
                                                    <p class="text-muted h-65 overflow-h m-0">{{ $p->especificaciones }}</p>
                                                </div>
                                                <div class="col-lg-12 col-md-12">
                                                    <dd class="text-success d-inline-block p-1 m-0" title="Precio">$ {{ number_format($p->precio,'0',',','.') }}</dd>
                                                    <button class="btn btn-sm btn-outline-info btn-option-xs float-right loadPost" data-route-link="detallesProducto/{{ $p->id }}">Ver mas</button>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="p-2">No hay productos registrados en esta categoria.</div>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            @endforeach
        @else
            No se encontraron productos registrados.
        @endif
    </div> 
</div>