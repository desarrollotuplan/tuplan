@extends('Layout.Usuario.master')
@section('titulo','Tu Plan | Reservas')
@section('contenido')
<div class="p-4 darkBlue bs-md-dark">
   <div class="row m-0 align-items-center">
        <div class="col-lg-3 col-md-12 col-sm-12">
            <div class="form-goup">
                <h3 class="title">Reservas <img src="{{ asset('Imagenes/Favicon.ico') }}" alt="search" class="faviconSearch"></h3>
            </div>
        </div>
    </div>
</div>
<section class="favoritos p-3">
    <div class="row m-0">
       @if(!$reservas->isEmpty())
        <div class="col-lg-12">
            <div class="darkBlue bs-m-dark table-tpl table-dark">
                <div class="header">
                    <bb>Historial</bb>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped text-center m-0 dinamic">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Sitio</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Tipo reserva</th>
                                <th scope="col">Estado</th>
                                <th scope="col">Ver</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($reservas as $r)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $r->nombre_negocio }}</td>
                                <td>{{ $r->fecha }}</td>
                                <td>{{ $r->nombre_reserva }}</td>
                                @if($r->estado == 'Reservado')
                                    @php $color = 'badge-success-lighten'; @endphp
                                @elseif($r->estado == 'Revisado')
                                    @php $color = 'badge-info-lighten'; @endphp
                                @else
                                   @php $color = 'badge-danger-lighten'; @endphp
                                @endif
                                <td><span class="badge {{ $color }} badge-strong p-2">{{ $r->estado }}</span></td>
                                <td><button class="btn btn-outline-light btn-sm loadGet" data-route="reserva/{{ $r->id }}">Ver</button></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>    
                </div>
            </div>
        </div>
        @endif
    </div>
</section>
@endsection