@extends('Layout.Usuario.master')
@section('titulo','Tu Plan | Favoritos')
@section('contenido')
<div class="p-4 darkBlue bs-md-dark">
   <form action="Favoritos/Buscar" method="post" class="row m-0 align-items-center sendAjax">
        <div class="col-lg-3 col-md-12 col-sm-12">
            <div class="form-goup">
                <h3 class="title">Favoritos <img src="{{ asset('Imagenes/Favicon.ico') }}" alt="search" class="faviconSearch"></h3>
            </div>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-6">
           <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="ciudad" class="select-tpl" value="{{ User::ciudadId() }}">
                <div class="select select-sm" role="button" tabindex="0">
                    <label>{{ User::ciudadNombre() }}</label>
                    <span class="icon-plus"></span>
                    <div class="options scroll-white">
                        <ol>
                            <li data-list="todas">Todas</li>
                            @foreach($ciudades as $c)
                                <li data-list="{{ $c->id }}">{{ $c->nombre }}</li>
                            @endforeach
                        </ol>
                   </div>
                </div>
                <label class="label label-sm placeholder">Ciudad</label>
           </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
           <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="categoria" class="select-tpl" value="todas">
                <div class="select select-sm" role="button" tabindex="0">
                    <label>Todas</label>
                    <span class="icon-plus"></span>
                    <div class="options scroll-white">
                        <ol>
                            <li data-list="todas">Todas</li>
                            @foreach($categorias as $c)
                                <li data-list="{{ $c->id }}">{{ $c->nombre }}</li>
                            @endforeach
                        </ol>
                   </div>
                </div>
                <label class="label label-sm placeholder">Categoria</label>
           </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-10">
            <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="nombre" class="form-control form-control-sm">
                <label class="label label-sm">Nombre del negocio</label>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-2 text-center">
            <button class="btn btn-sm btn-success">Buscar</button>
        </div>
    </form>
</div>

<section class="favoritos p-3">
    <div class="row m-0">
     @foreach($favoritos as $i)
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="card card-tpl card-dark bs-m-dark">
                <div class="card-img-content">
                    <img src="{{ asset($i->folder.$i->foto_perfil) }}" alt="foto-negocio" class="card-img fit-cover">
                </div>
                <div class="card-body">
                    <small class="card-category {{ $i->color }}"><span class="{{ $i->icono }}"></span> {{ $i->categoria }}</small>
                    <h5 class="card-title" title="">{{ $i->nombre }}</h5>
                    <p class="card-description h-87">{{ $i->descripcion }}</p>
                </div>
                <div class="card-footer">
                    @if(HorarioDias::getHorarioDia($i->id) === 'Abierto')
                        <span class="badge badge-success-lighten badge-strong">Abierto</span>
                    @else
                        <span class="badge badge-danger-lighten badge-strong">Cerrado</span>
                    @endif
                    <a href="{{ route('detalleSitio',['negocio'=>$i->id_unico]) }}" target="_blank" class="btn btn-option-xs btn-outline-info float-right" title="Ver mas">
                        <span class="icon-eye-plus"></span> Ver mas...
                    </a>
                </div>
            </div>
       </div>
       @endforeach
    </div>
</section>
@endsection