@if(isset($negocio))
    @if(!isset($favorito))
        <button class="btn btn-sm btn-danger btn-option-xs mt-2 loadPost" data-route="Sitios/{{ $negocio }}/favoritos">
            <span class="icon-heart"></span> Agregar
        </button>
    @else
        <button class="btn btn-sm btn-danger btn-option-xs mt-2" data-method="delete" data-route="Sitios/{{ $negocio }}/favoritos/{{ $favorito->id }}">
            <span class="icon-heart"></span> Quitar
        </button>
    @endif
@endif