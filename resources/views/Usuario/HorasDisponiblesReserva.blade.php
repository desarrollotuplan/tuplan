@if(!$horas->isEmpty())
    <h6 class="title text-success">Horas disponibles para el {{ $fechaString }}</h6><hr class="hrdark">
    <form action="formularioReserva/{{ $reserva }}" method="post" class="mb-3 sendAjax route-link">
        <div class="mb-3">
            <div class="form hid">
                <input type="date" name="fecha" value="{{ $date }}" class="hid" hidden>
            </div>
            @foreach($horas as $h)
                <div class="form-group form-group-tpl form-group-dark radio d-inline-block ">
                   <input type="radio" name="hora" value="{{ $h->id }}">
                    <label class="checkbox">{{ date('g:i a',strtotime($h->hora)) }}</label>
                </div>
            @endforeach
        </div>
        <div class="form-group text-right">
            <button type="submit" class="btn btn-sm btn-outline-primary">Reservar</button>
        </div>
    </form>
@else
    <h6 class="title text-warning">No hay horas disponibles para {{ $fechaString }}</h6>
@endif