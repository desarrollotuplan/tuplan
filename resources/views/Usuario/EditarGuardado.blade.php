@if(isset($guardado))
<form action="Guardados/{{ $guardado->id }}" class="sendAjax p-3" method="post">
    <header>
        <h5>Guardar como</h5> <hr class="hrdark mb-4">
    </header>
    <div class="form-group m-0">
        @csrf
        @method('PUT')
    </div>
    <div class="form-group form-group-tpl form-group-dark mb-4">
        <label class="label label-sm active">Nombre</label>
        <input type="text" name="nombre" class="form-control form-control-sm" value="{{ $guardado->nombre }}">
    </div>
    <div class="form-group">
        <button type="button" class="btn btn-sm btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-sm btn-success btn-sm float-right">Guardar</button>
    </div>
</form>
@endif