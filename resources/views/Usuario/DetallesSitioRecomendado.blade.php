@if(isset($negocio))
<div class="jumbotron jumb-tuplan m-0" style="background-image: url('{{ asset($negocio->folder.$negocio->foto_perfil) }}'); background-size: cover; background-color: rgba(0,0,0,0.2); background-blend-mode: soft-light; background-position: center; background-size: cover;" data-key="{{ $negocio->id_unico }}">
    <img src="{{ asset($negocio->folder.$negocio->logo) }}" alt="" style="height:100px; width:100px;" class="mb-2">
    <a href="http://localhost:8000/Sitios/{{ $negocio->id_unico }}" target="_blank" class="text-white">
        <h1>{{ $negocio->nombre }}</h1>
    </a>
    <small class="{{ $negocio->color }} d-block">
        <span class="{{ $negocio->icono }}"></span> {{ $negocio->categoria }}
    </small>
    <div class="favorito d-inline-block">
       
        @if(!isset($favorito))
            <button class="btn btn-sm btn-danger btn-option-xs mt-2 loadPost" data-route="Sitios/{{ $negocio->id_unico }}/favoritos">
                <span class="icon-heart"></span> Agregar
            </button>
        @else
            <button class="btn btn-sm btn-danger btn-option-xs mt-2" data-method="delete" data-route="Sitios/{{ $negocio->id_unico }}/favoritos/{{ $favorito->id }}">
                <span class="icon-heart"></span> Quitar
            </button>
        @endif
    </div>
    <div class="save d-inline-block">
        @if(!isset($guardado))
            <button class="btn btn-sm btn-success btn-option-xs mt-2 loadGet" data-route="Sitios/{{ $negocio->id_unico }}/guardado/create">
                <span class="icon-bookmark"></span> Guardar
            </button>
        @else
            <button class="btn btn-sm btn-success btn-option-xs mt-2" data-method="delete" data-route="Sitios/{{ $negocio->id_unico }}/guardado/{{ $guardado->id }}">
                <span class="icon-bookmark"></span> No guardar
            </button>
        @endif
    </div>
</div>
<div class="row m-0">
    <div class="col-lg-1"></div>
    <div class="col-lg-8">
        <div id="loadInformation" class="darkBlue bs-m-dark br-5">
            <div class="informacionSitio p-3 mb-3">
                <div class="eslogan">
                    <div class="row m-0">
                        <div class="col-lg-12">
                            <h4>{{ $negocio->nombre }}</h4>
                            <hr class="hrdark">
                            <h6 class="mb-3">Etiquetas</h6>
                            <p>{{ $negocio->eslogan }}</p>
                            <p>{{ $negocio->descripcion }}</p>
                            <h4>Fotos</h4>
                            <hr class="hrdark">
                            
                           <div class="fotorama dark p-3 w-100" data-nav="thumbs" data-allowfullscreen="native" data-autoplay="4000">
                               <img src="{{ asset($negocio->folder.$negocio->foto_perfil) }}" alt="">
                                @if(!$imagenes->isEmpty())
                                    @foreach($imagenes as $i)
                                        <img src="{{ asset($i->ruta) }}" alt="">
                                    @endforeach
                                @else
                                    Sin fotos del sitio.
                                @endif
                           </div>
                           <script>$('.fotorama').fotorama();</script>
                           
                            <h4>Mapa</h4>
                            <hr class="hrdark">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="row m-0">
            <div class="col-lg-12 col-md-4 col-sm-6 col-6">
                <div class="card-mini darkBlue bs-m-dark">
                    <h6 class="title">Telefono: </h6>{{ $negocio->telefono }}
               </div>
            </div>
            <div class="col-lg-12 col-md-4 col-sm-6 col-6">
                <div class="card-mini darkBlue bs-m-dark">
                    <h6 class="title">Celular: </h6>{{ $negocio->celular }}
                </div>
            </div>
            <div class="col-lg-12 col-md-4 col-sm-6 col-6">
                <div class="card-mini darkBlue bs-m-dark">
                    <h6 class="title">Whatsapp: </h6>{{ $negocio->whatsapp }}
                </div>
            </div>
            <div class="col-lg-12 col-md-4 col-sm-6 col-6">
                <div class="card-mini darkBlue bs-m-dark">
                    <h6 class="title">S. de reservas: </h6>{{ $reserva->reserva }}
                </div>
            </div>
            <div class="col-lg-12 col-md-4 col-sm-6 col-6">
                <div class="card-mini darkBlue bs-m-dark">
                    <h6 class="title">S. domicilio: </h6>{{ $domicilio->servicio }}
                </div>
            </div>
            <div class="col-lg-12 col-md-4 col-sm-6 col-6">
                <div class="card-mini darkBlue bs-m-dark">
                    <h6 class="title">Tipos de pago: </h6>Master Card
                </div>
            </div>
        </div>
    </div>
</div>
@endif