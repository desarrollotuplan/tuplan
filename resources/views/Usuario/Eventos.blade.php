@extends('Layout.Usuario.master')
@section('titulo','Tu Plan | Eventos')
@section('contenido')
<div class="p-3 darkBlue bs-md-dark">
   <form action="Eventos/Buscar" method="post" class="row m-0 align-items-center sendAjax">
        <div class="col-lg-3 col-md-12 col-sm-12">
            <div class="form-goup">
                <h3 class="title">Eventos <img src="{{ asset('Imagenes/Favicon.ico') }}" alt="search" class="faviconSearch"></h3>
            </div>
        </div>
        <div class="col-lg-4 col-md-5 col-sm-5">
           <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="ciudad" class="select-tpl" value="{{ User::ciudadId() }}">
                <div class="select select-sm" role="button" tabindex="0">
                    <label>{{ User::ciudadNombre() }}</label>
                    <span class="icon-plus"></span>
                    <div class="options scroll-white">
                        <ol>
                            <li data-list="todas">Todas</li>
                            @foreach($ciudades as $c)
                                <li data-list="{{ $c->id }}">{{ $c->nombre }}</li>
                            @endforeach
                        </ol>
                   </div>
                </div>
                <label class="label label-sm placeholder">Ciudad</label>
           </div>
        </div>
        <div class="col-lg-4 col-md-5 col-sm-5">
           <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="categoria" class="select-tpl" value="todas">
                <div class="select select-sm" role="button" tabindex="0">
                    <label>Todas</label>
                    <span class="icon-plus"></span>
                    <div class="options scroll-white">
                        <ol>
                            <li data-list="todas">Todas</li>
                            @foreach($categorias as $c)
                                <li data-list="{{ $c->id }}">{{ $c->nombre }}</li>
                            @endforeach
                        </ol>
                   </div>
                </div>
                <label class="label label-sm placeholder">Categoria</label>
           </div>
        </div>
        <div class="col-lg-1 col-md-2 col-sm-2 text-center">
            <button class="btn btn-sm btn-success">Buscar</button>
        </div>
    </form>
</div>

<div class="p-3">
    <div class="row m-0">
        @if(!$eventos->isEmpty())
            @foreach($eventos as $ev)
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-tpl card-dark bs-m-dark">
                    <div class="row m-0">
                        <div class="col-lg-auto col-md-auto col-sm-auto col-auto">
                            <img src="{{ asset($ev->folder.$ev->logo) }}" alt="" style="height:50px; width:50px;" class="mb-2">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                            <a href="{{ route('detalleSitio',['negocio'=>$ev->id_negocio]) }}" class="text-white" target="_blank">
                                <h5 class="text-truncate">{{ $ev->nombre_negocio }}</h5>
                            </a>
                            <small class="d-block text-truncate text-white-50">{{ $ev->eslogan }}</small>
                        </div>
                    </div>
                    <div class="card-img-content"  style="height:250px;">
                        <img src="{{ asset($ev->imagen) }}" alt="foto-negocio" class="card-img fit-cover" style="height:250px;">
                    </div>
                    <div class="card-body">
                        <small class="card-category text-success"><span class="icon-spoon-knife"></span> {{ $ev->nombre_categoria }}</small>
                        <h5 class="card-title" title="">{{ $ev->nombre }}</h5>
                        <p class="card-description h-87 title">{{ $ev->descripcion }}</p>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-outline-primary btn-option-xs float-right loadPost newLink" data-route="Eventos/{{ $ev->id_unico }}">Ver mas...</button>
                    </div>
                </div>
            </div>
            @endforeach
        @endif
    </div>
</div>

@endsection

@section('masterjs')
    @if(isset($detallesPromo))
        <script>$(document).ready(function(){ output({!! json_encode($detallesPromo) !!}); })</script>
    @endif
@endsection