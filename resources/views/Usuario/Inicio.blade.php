@extends('Layout.Usuario.master')
@section('titulo','Tu Plan | Inicio')
@section('contenido')
    <section class="headerHome bs-dark">
        <div class="jumbotron jumb-tuplan">
            <div class="contJumbInicio">
                <h4>Bienvenido </h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore tenetur iure eveniet praesentium fuga, illum ab modi dolorum at perspiciatis, provident quaerat et officiis velit libero accusantium recusandae atque eos.</p>
                <button class="btn btn-success btn-sm">Descargar App</button>
                <button class="btn btn-primary btn-sm">Aprender mas...</button>
            </div>
        </div>
    </section>
@endsection