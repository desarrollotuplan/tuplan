<div class="modal-body">
    @if($comentario)
    <form action="comentario/{{ $comentario->id }}" class="sendAjax route-link">
        <div class="form-group">
            <h4>Editar comentario</h4>
            <hr class="hrdark mb-4">
        </div>
        <div class="hid">
            @csrf
            @method('PUT')
        </div>
       <div class="form-group form-group-tpl form-group-dark">
            <textarea name="comentario" rows="3" class="form-control resize scroll-white">{{ $comentario->comentario }}</textarea>
            <label class="label label-sm active">Comentario</label>
        </div>
        <div>
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-success btn-sm float-right">Guardar</button>
        </div>
    </form>
    @endif
</div>