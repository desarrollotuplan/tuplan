<div class="p-3">
    <div class="row m-0">
        <div class="col-lg-12">
            <header>
                <h4>Horario de atencion</h4>
                <hr class="hrdark">
            </header>
        </div>
        @if($horario)
            @foreach($horario as $h)
                <div class="col-lg-4 col-md-3 col-sm-4 col-12">
                    <div class="card-border p-3 darkBlue h-100 br-5 border-info">
                        <h4>{{ $h->dia->nombre }}</h4>
                            <p>
                                <small class="badge badge-success-lighten badge-strong">{{ $h->horarioTipo->nombre }}</small>
                            </p>
                        @if($h->horarioTipo->nombre == 'Doble jornada')
                            <h6>Primera jornada</h6>
                            <h6>
                                <b>Desde: </b>
                                <small class="badge badge-info-lighten">{{ GSD::getTimeFormat($h->hora_inicio_1) }}</small>
                            </h6>
                            <h6 class="mb-3">
                                <b>Hasta: </b>
                                <small class="badge badge-warning-lighten">{{ GSD::getTimeFormat($h->hora_fin_1) }}</small>
                            </h6> 
                            <h6>Segunda jornada</h6>
                            <h6>
                                <b>Desde: </b>
                                <small class="badge badge-info-lighten">{{ GSD::getTimeFormat($h->hora_inicio_2) }}</small>
                            </h6>
                            <h6>
                                <b>Hasta: </b> 
                                <small class="badge badge-warning-lighten">{{ GSD::getTimeFormat($h->hora_fin_2) }}</small>
                            </h6>
                            </p>
                        @else
                            <h6>
                                <b>Desde: </b>
                                <small class="badge badge-info-lighten">{{ GSD::getTimeFormat($h->hora_inicio_1) }}</small>
                            </h6>
                            <h6>
                                <b>Hasta: </b>
                                <small class="badge badge-warning-lighten">{{ GSD::getTimeFormat($h->hora_fin_1) }}</small>
                            </h6> 
                        @endif
                    </div>
                </div>
            @endforeach
        @else
            No se encontro un horario de este sitio.
        @endif
        
    </div>
</div>