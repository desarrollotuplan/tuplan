@if(isset($producto))
    <div class="p-3">
        <h3>{{ $producto->nombre }} <hr class="hrdark"></h3>
        <div class="fotorama w-100 mb-2" data-nav="thumbs" data-allowfullscreen="native" data-autoplay="4000">
            @if(!$producto->imagenes->isEmpty())
                @foreach($producto->imagenes as $i)
                    <img src="{{ asset($i->ruta) }}" alt="" class="w-100">
                @endforeach
            @else
                No hay imagenes de este producto o servicio.
            @endif
        </div>
        <script>$('.fotorama').fotorama();</script>
        <p>{{ $producto->especificaciones }}</p>
        <p>{{ $producto->informacion_adicional }}</p>
        <h2>Precio: <small class="text-success">{{ number_format($producto->precio, 0, ',', '.') }}</small></h2>
        <div>
            <button class="btn btn-sm btn-danger float-right" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
@else
    Ha ocurrido un error intentelo de nuevo o actualize la pagina.
@endif
