@extends('Layout.Usuario.master')
@section('titulo','Tu Plan | Perfil')
@section('contenido')
<form action="usuarioSave" class="container p-3 sendAjax disabled-success">
    <div class="row m-0 p-3 darkBlue br-5 bs-m-dark">
        <div class="col-lg-12">
            <h4>Datos personales <button type="button" class="btn btn-sm btn-orange float-right active-form" data-remove="btn-orange">Editar</button></h4>
            <hr class="hrdark mb-4">
        </div>
        <div class="col-lg-3">
            <img src="{{ User::foto() }}" alt="imagen usuario" class="mb-2 d-block m-auto">
        </div>
        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group form-group-tpl form-group-dark">
                        <input type="text" name="primer_nombre" class="form-control" value="{{ User::primerNombre() }}" disabled>
                        <label class="label label-lg active">Primer nombre</label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-tpl form-group-dark">
                        <input type="text" name="segundo_nombre" class="form-control" value="{{ User::segundoNombre() }}" disabled>
                        <label class="label label-lg active">Segundo nombre</label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-tpl form-group-dark">
                        <input type="text" name="primer_apellido" class="form-control" value="{{ User::primerApellido() }}" disabled>
                        <label class="label label-lg active">Primer apellido</label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-tpl form-group-dark">
                        <input type="text" name="segundo_apellido" class="form-control" value="{{ User::segundoApellido() }}" disabled>
                        <label class="label label-lg active">Segundo apellido</label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-tpl form-group-dark">
                        <input type="date" name="fecha_nacimiento" class="form-control" value="{{ User::fechaNacimiento() }}" disabled>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <h4>Datos de contacto</h4>
            <hr class="hrdark mb-4">
        </div>
        <div class="col-lg-4">
            <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="celular" class="form-control" value="{{ User::celular() }}" disabled>
                <label class="label label-lg active">Celular</label>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="ciudad" class="select-tpl" value="{{ User::ciudadId() }}" disabled>
                <div class="select select-lg" role="button" tabindex="0" disabled>
                    <label>{{ User::ciudadNombre() }}</label>
                    <span class="icon-plus"></span>
                    <div class="options scroll-white">
                        <ol>
                            @foreach($ciudades as $c)
                            <li data-list="{{ $c->id }}">{{ $c->nombre }}</li>
                            @endforeach
                        </ol>
                   </div>
                </div>
           </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group form-group-tpl form-group-dark">
                <input type="text" name="direccion" class="form-control" value="{{ User::direccion() }}" disabled>
                <label class="label label-lg active">Direccion</label>
            </div>
        </div>
    </div>
</form>

<div class="p-3 container">
    <div class="row p-3 m-0 darkBlue bs-m-dark br-5">
        <div class="col-lg-12">
            <h4>Mis intereses</h4>
            <p class="text-info">Haga click en las categorias que desee eliminar.</p>
            <hr class="hrdark">
        </div>
        <div class="col-lg-12 mb-2">
            <div class="intereses">
                @if(!$intereses->isEmpty())
                    @foreach($intereses as $i)
                    <button class="btn btn-sm btn-success mb-2" data-method="delete" data-route="intereses/{{ $i->id }}" data-confirm="Seguro que desea eliminar la categoria {{ $i->categoria->nombre }}">{{ $i->categoria->nombre }}</button>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="col-lg-12">
            <button class="btn btn-sm btn-primary float-right loadGet" data-route="intereses/create">Agregar Intereses</button>
        </div>
    </div>
</div>
@endsection