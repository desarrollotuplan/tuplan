<div class="modal-body">
    <div class="">
        <h4>Calificacion</h4>
        <hr class="hrdark">
    </div>
    @foreach($tipo_calificacion as $tp)
        <form method="post" action="calificacion" class="sendChangeAjax route-link">
            <div class="hid">
                @csrf
                <input type="text" name="tipo_calificacion" value="{{ $tp->id }}">
            </div>
            <div class="row align-items-center m-0">
                <div class="col-5">
                    <h6>{{ $tp->nombre }}</h6>
                </div>
                <div class="col-7">
                    <p class="score text-right">
                        @foreach($puntos as $p) @php $id = User::randomString(4); @endphp
                        <input id="{{ $id }}" type="radio" name="puntos" value="{{ $p->id }}">
                        <label for="{{ $id }}"><span class="icon-star-full"></span></label>
                        @endforeach
                    </p>
                </div>
            </div>
        </form>
    @endforeach
    <div>
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
    </div>
</div>