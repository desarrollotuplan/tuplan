<div class="p-3">
    <header>
        <h4>Categorias</h4>
        <hr class="hrdark">
    </header>
    <form action="intereses" class="sendAjax">
       <p>Selecciona las caegorias que deseas agregar a tus intereses.</p>
        <div class="row m-0">
            @foreach($categorias as $c)
            <div class="col-lg-6 col-sm-6">
                <div class="form-group form-group-tpl form-group-dark checkbox">
                   <input type="checkbox" value="{{ $c->id }}" name="{{ $c->nombre }}">
                    <label class="checkbox">{{ $c->nombre }}</label>
                </div>
            </div>
            @endforeach
        </div>
        <div class="p-2">
            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-sm btn-indigo float-right">Guardar</button>
        </div>
    </form>
</div>
