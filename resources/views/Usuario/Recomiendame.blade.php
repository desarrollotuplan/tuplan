@extends('Layout.Usuario.master')
@section('titulo','Tu Plan | Recomiendame')
@section('contenido')
    <div class="menu-sidebar darkBlue scroll-white">
        <div class="header-sidebar">
        <h4 class="m-0">
            <img src="{{ asset('Imagenes/Mascota90.png') }}" class="pet">
        </div>
        
        <ul class="option-selected">
        @if(!$recomendados->isEmpty())
            @foreach($recomendados as $r)
                <li class="loadPost" data-route="detallesRecomendado/{{ $r->id_unico }}">
                    <div class="row align-items-center m-0">
                        <div class="col-4">
                            <img src="{{ $r->folder.$r->logo }}" alt="foto-negocio" class="fit-cover img-select">
                        </div>
                        <div class="col-8 h-mini">
                            <h6 class="title-option">{{ $r->nombre }}</h6>
                            <small class="{{ $r->color }} d-block text-truncate">{{ $r->categoria }}</small>
                        </div>
                    </div>
                </li>
            @endforeach
        @endif
        </ul>
    </div>
    <div class="main-panel scroll-white">
        <nav class="navbar navbar-expand-md navbar-transparent-dark d-block">
            <a class="navbar-brand close-area close-sidebar">
                <span class="icon-brand82 before-white"></span>
            </a>
            <button class="btn btn-success btn-sm float-right btn-icon" data-target="#filter" data-toggle="modal">
                <span class="icon-equalizer"></span>Filtrar
            </button>
        </nav>
        <div class="load-details">
            
        </div>
    </div>
    
    <div class="modal fade" id="filter" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dark">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-head">
                        <h3>Filtrar por</h3>
                        <hr class="hrdark">
                    </div>
                    <form action="buscarRecomendados" method="post" class="filter-form p-2 sendAjax">
                        <div class="form-group form-group-tpl form-group-dark">
                            <input type="text" name="ciudad" class="select-tpl" value="{{ User::ciudadId() }}">
                            <div class="select select-sm" role="button" tabindex="0">
                                <label>{{ User::ciudadNombre() }}</label>
                                <span class="icon-plus"></span>
                                <div class="options scroll-white">
                                    <ol><!--EL DATA-LIST ES EL VALOR DEL OPTION-SELECT-->
                                        @if($ciudades)
                                            @foreach($ciudades as $c)
                                                <li data-list="{{ $c->id }}">{{ $c->nombre }}</li>
                                            @endforeach
                                        @endif
                                    </ol>
                                </div>
                            </div>
                            <label class="label label-sm placeholder">Ciudad</label>
                       </div>
                        <div class="form-group form-group-tpl form-group-dark">
                            <input type="text" name="categoria" class="select-tpl" value="todas">
                            <div class="select select-sm" role="button" tabindex="0">
                                <label>Todas</label>
                                <span class="icon-plus"></span>
                                <div class="options scroll-white">
                                    <ol><!--EL DATA-LIST ES EL VALOR DEL OPTION-SELECT-->
                                        <li data-list="todas">Todas</li>
                                        @if($categorias)
                                            @foreach($categorias as $c)
                                                <li data-list="{{ $c->id }}">{{ $c->nombre }}</li>
                                            @endforeach
                                        @endif
                                    </ol>
                                </div>
                            </div>
                            <label class="label label-sm placeholder">Categorias</label>
                       </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-sm float-left" data-dismiss="modal">Cerrar</button>
                            <button class="btn btn-success btn-sm float-right btn-icon"> <span class="icon-search"></span>Buscar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('masterjs')
    @if($info)
    <script>output({!! json_encode($info) !!});</script>
    @endif
@endsection

