<form action="reservar" method="post" class="sendAjax">
    <header>
        <h5 class="title">Reserva</h5>
        
        <hr class="hrdark">
    </header>
    <div class="details mb-5 btooltip bs-m-dark">
        <table class="table table-borderless">
            <tr>
                <th class="w-25 text-info">Hora</th>
                <td>{{ date('g:i a',strtotime($horaView)) }}</td>
            </tr>
            <tr>
                <th class="w-25 text-info">Dia</th>
                <td>{{ $fechaView }}</td>
            </tr>
            
        </table>
    </div>
    <div class="form-group hid">
        <input type="hidden" name="hora" value="{{ $hora }}" hidden>
        <input type="hidden" name="fecha" value="{{ $fecha }}" hidden>
    </div>
    <div class="form-group form-group-tpl form-group-dark mb-4">
        <input type="text" name="nombre" class="form-control" autocomplete="off">
        <label class="label label-lg">Nombre de quien reserva</label>
    </div>
    <div class="form-group form-group-tpl form-group-dark mb-4">
        <input type="text" name="personas" class="select-tpl" value="{{ $personas->minimo }}">
        <div class="select select-lg" role="button" tabindex="0">
            <label>{{ $personas->minimo }} Personas</label>
            <span class="icon-plus"></span>
            <div class="options scroll-white">
                <ol><!--EL DATA-LIST ES EL VALOR DEL OPTION-SELECT-->
                    @for($i = $personas->minimo; $i < $personas->maximo; $i++)
                        <li data-list="{{ $i }}">{{ $i }} Personas</li>
                    @endfor
                </ol>
           </div>
        </div>
        <label class="label label-lg placeholder">Numero de personas</label>
    </div>
    <div class="form-group form-group-tpl form-group-dark mb-4">
        <input type="text" name="ocacion" class="select-tpl">
        <div class="select select-lg" role="button" tabindex="0">
            <label>Seleciona una ocacion (opcional)</label>
            <span class="icon-plus"></span>
            <div class="options scroll-white">
                <ol><!--EL DATA-LIST ES EL VALOR DEL OPTION-SELECT-->
                    <li data-list="">Seleccione una ocacion (opcional)</li>
                    @foreach($tipoR as $t)
                        <li data-list="{{ $t->id }}">{{ $t->nombre }}</li>
                    @endforeach
                </ol>
           </div>
        </div>
        <label class="label label-lg placeholder">Ocacion</label>
    </div>
    <div class="form-group form-group-tpl form-group-dark mb-3">
        <textarea name="informacion" rows="3" class="form-control resize"></textarea>
        <label class="label label-lg">¿Desea agregar algo?</label>
    </div>
    <div class="form-group mb-2">
        <button class="btn btn-block btn-sm btn-outline-success">Realizar reserva</button>
    </div>
</form>