<img src="{{ asset($reserva->imagen) }}" alt="reserva" class="w-100 fit-cover" style="height:200px;">
<div class="p-3">
    <div class="mb-4">
        <h4>{{ $reserva->nombre }}</h4>
        <p>{{ $reserva->informacion }}</p>
    </div>
    <h6 class="title">Seleccione una fecha <hr class="hrdark"></h6>
    <form action="buscarHorasReserva/{{ $reserva->id }}" class="route-link sendAjax mb-5">
        <div class="form-group form-group-tpl form-group-dark mb-3">
           @csrf
            <input type="date" name="fecha" class="form-control form-control-sm">
        </div>
        <div class="form-group text-right">
            <button type="submit" class="btn btn-sm btn-orange">Buscar reserva</button>
        </div>
    </form>
    
    <div class="horas-disponibles mb-3"></div>
    <div class="p-1">
        <button class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
    </div>
    
</div>
