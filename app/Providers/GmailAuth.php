<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class GmailAuth extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path('/Helpers/GmailAuth.php');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
