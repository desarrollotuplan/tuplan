<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class JessengersDate extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path('/Helpers/JessengersDate.php');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
