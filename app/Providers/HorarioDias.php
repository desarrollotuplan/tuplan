<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HorarioDias extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path('/Helpers/HorarioDias.php');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
