<?php

namespace App\Http\Middleware\Cliente;

use Closure;
use App\Models\General\Horario as H;
use App\Models\General\HorarioTipo as HT;
use App\Models\General\Dias as D;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class Horario
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        // Verficamos que exista el horario
        if($req->route()->HorarioAtencion){
            $horario = H::find($req->route()->HorarioAtencion);
            if($horario){
                // Si hay un tipo de horario se verifica que exista
                if($req->tipo){
                    $tipo = HT::find($req->tipo);
                    if($tipo){
                        return $next($req);
                    }else{
                        return response(RSP::swalError());
                    }
                }else{
                    return $next($req);
                }
                
            }else{
                return response(RSP::swalError());
            }
        }else{
            return $next($req);
        }
    }
}
