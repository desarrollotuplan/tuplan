<?php

namespace App\Http\Middleware\Cliente;

use Closure;
use App\Helpers\User;
use App\Helpers\Response as RSP;
use App\Models\General\Producto as P;
use App\Models\General\CategoriaProducto as CAP;

class Productos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if(isset($req->id)){
            $id = $req->id;
        }elseif($req->route()->Producto){
            $id = $req->route()->Producto;
        }
        
        if(isset($id)){
            // Se accede al id del negocio al cual esta enlasado a este usuario
            $negocio = User::negocioId();

            // Seleccionamos la categoria del producto al cual se intenta acceder
            $categoria = P::find($id);

            // Si existe el producto se verifica que esa categoria sea de dicho negocio
            if(isset($categoria->categoria_id)){

                // Verificamos que la categoria pertenezca a dicho negocio
                $true = CAP::where(['id'=>$categoria->categoria_id,'negocio_id'=>$negocio])->first();

                if($true){
                    return $next($req);
                }else{
                    return response(RSP::swalError());
                }
            }else{
                return response(RSP::swalError());
            }
        }else{
            return $next($req);
        }
    }
}
