<?php

namespace App\Http\Middleware\Cliente;

use Closure;
use App\Helpers\User;
use App\Helpers\Response as RSP;
use App\Models\General\CategoriaProducto as CAP;

class CategoriaProducto
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if(isset($req->id)){
            $id = $req->id;
        }elseif($req->route()->categoria){
            $id = $req->route()->categoria;
        }
        
        $negocio = User::negocioId();
        
        $categoria = CAP::where(['negocio_id'=>$negocio,'id'=>$id]);
        
        if(isset($categoria)){
            return $next($req);
        }else{
            return RSP::swalError();
        }
        
    }
}
