<?php

namespace App\Http\Middleware\Cliente;

use Closure;
use App\Models\General\ReservasDisponibles as RD;
use App\Helpers\Response as RSP;
use App\Helpers\User;


class ReservaDisponible
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        
        if($req->route()->ReservaDisponible){
            $id = $req->route()->ReservaDisponible;
            $rd = RD::where(['id'=>$id,'negocio_id'=>User::negocioId()])->first();
            if($rd){
                return $next($req);
            }else{
                return response(RSP::swalError());
            }
        }else{
            return $next($req);
        }
        
    }
}
