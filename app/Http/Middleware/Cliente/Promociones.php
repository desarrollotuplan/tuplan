<?php

namespace App\Http\Middleware\Cliente;

use Closure;
use App\Models\General\Promociones as PR;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class Promociones
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if($req->route()->Promociones){
            $id = $req->route()->Promociones;
            $rd = PR::where(['id'=>$id,'negocio_id'=>User::negocioId()])->first();
            if($rd){
                return $next($req);
            }else{
                return response(RSP::swalError());
            }
        }else{
            return $next($req);
        }
    }
}
