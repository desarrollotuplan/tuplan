<?php

namespace App\Http\Middleware\Cliente;

use Closure;
use App\Models\General\HorasReserva as HR;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class HoraReserva
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if($req->route()->HoraReserva){
            $id = $req->route()->HoraReserva;
            $hr = HR::with(['reservasDisponible'=>function($q){ $q->where(['negocio_id'=>User::negocioId()]); }])->where(['id'=>$id])->first();
            if($hr){
                return $next($req);
            }else{
                return response(RSP::swalError());
            }
        }else{
            return $next($req);
        }
    }
}
