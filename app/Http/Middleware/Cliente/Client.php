<?php

namespace App\Http\Middleware\Cliente;

use Closure;
use App\Helpers\User;
use App\Helpers\Response as RSP;
use App\Models\General\Rol;

class Client
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Se consulta el id del rol 'cliente'
        $rol = Rol::where('nombre','cliente')->first()->id;
        
        // Si coincide con el id del rol de la persona logeada lo dejara pasar
        if(User::rolId() == $rol){
            return $next($request);
        }else{
            if($req->method() == 'POST'){
                return response(RSP::swal('Error'));
            }else{
                return RSP::redirectWith('/',[]);
            }
        }
    }
}
