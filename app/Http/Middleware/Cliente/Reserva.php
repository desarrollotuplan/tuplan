<?php

namespace App\Http\Middleware\Cliente;

use Closure;
use Illuminate\Support\Facades\DB;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class Reserva
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if($req->route()->reserva){
            $id = $req->route()->reserva;
            $r = DB::table('reserva as r')
            ->select('r.*')
            ->join('horas_reserva as hr','hr.id','=','r.hora_reserva_id')
            ->join('reservas_disponibles as rd','rd.id','=','hr.reserva_disponible_id')
            ->where(['rd.negocio_id'=>User::negocioId(),'r.id'=>$req->route()->reserva])
            ->first();
            
            if($r){
                return $next($req);
            }else{
                return response(RSP::swalError());
            }
        }else{
            return $next($req);
        }
    }
}
