<?php

namespace App\Http\Middleware\Usuario;

use Closure;
use App\Models\General\EstadoTablas as ET;
use App\Models\General\Calificacion as CA;
use App\Models\General\Negocio as N;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class Calificacion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if($req->route()->negocio){
            
            $id = $req->route()->negocio;
            
            // Se busca el id de los negocios que se pueden acceder
            $estado = ET::getEstadoTablaId('negocio','Activo');
        
            // Se consulta el negocio
            $negocio = N::where(['id_unico'=>$id,'estado_id'=>$estado->id])->first();
            
            // Si existe se deja pasar si no arroja error
            if($negocio){
                
                if($req->route()->calificacion){
                    
                    // Se verifica que exista la calificacion
                    $calificacion = CA::where(['negocio_id'=>$negocio->id,'id'=>$req->route()->calificacion,'usuario_id'=>User::id()])->first();
                    
                    if($calificacion){
                        return $next($req);
                    }else{
                        return response(RSP::swalError());
                    }
                }else{
                    return $next($req);
                }
            }else{
                return response(RSP::swalError());
            }
        }else{
            return response(RSP::swalError());
        }
    }
}
