<?php

namespace App\Http\Middleware\Usuario;

use Closure;
use App\Helpers\User;
use App\Helpers\Response as RSP;
use App\Models\General\Rol;

class Usuario
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        // Se consulta el id del rol 'usuario'
        $rol = Rol::where('nombre','usuario')->first()->id;
        
        // Si coincide con el id del rol de la persona logeada lo dejara pasar
        if(User::rolId() == $rol){
            return $next($req);
        }else{
            if($req->method() == 'POST'){
                return response(RSP::swal('Error'));
            }else{
                return RSP::redirectWith('/',[]);
            }
        }
    }
}
