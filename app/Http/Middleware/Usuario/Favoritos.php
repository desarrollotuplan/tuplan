<?php

namespace App\Http\Middleware\Usuario;

use Closure;
use App\Models\General\Negocio as NE;
use App\Models\General\Favoritos as FA;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class Favoritos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if($req->route()->negocio){
            
            // id del negocio
            $negocio_id = $req->route()->negocio;
            
            // Se busca el id del negocio al cual se desea acceder para verificar que exista
            $negocio = NE::select('id')->where('id_unico',$negocio_id)->first();
            
            // Si hay un id de un favorito se verifica que exista 
            if(isset($req->route()->favorito) && isset($negocio)){
                // id del favorito
                $id = $req->route()->favorito;
                
                // Se verifica que exista el favorito
                $fav = FA::where(['usuario_id'=>User::id(),'id'=>$id])->first();
                
                // Si el favorito no existe se crea una variable $false
                if(!$fav){
                    $false = true;
                }
            }
            
            
            // Si el negocio existe y el id del favorito se encuentra y tambien existe se deja pasar si no vota error
            if(isset($negocio->id)){
                if(!isset($false)){
                    return $next($req);
                }else{
                    return response(RSP::swalError());
                }
            }else{
                return response(RSP::swalError());
            }
        }
    }
}
