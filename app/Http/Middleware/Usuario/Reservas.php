<?php

namespace App\Http\Middleware\Usuario;

use Closure;
use App\Models\General\Reserva as RE;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class Reservas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if($req->route()->reserva){
            $reserva = RE::where(['id'=>$req->route()->reserva,'usuario_id'=>User::id()])->first();
            if($reserva){
                return $next($req);
            }else{
                return response(RSP::swalError());
            }
        }else{
            return $next($req);
        }
    }
}
