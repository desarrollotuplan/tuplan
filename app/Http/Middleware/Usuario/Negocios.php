<?php

namespace App\Http\Middleware\Usuario;

use Closure;
use App\Models\General\EstadoTablas as ET;
use App\Models\General\Negocio as N;
use App\Helpers\Response as RSP;

class Negocios
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        
        // Se valida si llega el id unico / para verificar que el negocio exista
        if($req->route()->negocio){
            $id = $req->route()->negocio;
        }
        
        // Se busca el id de los negocios que se pueden acceder
        $estado = ET::getEstadoTablaId('negocio','Activo');
        
        
        // Se consulta el negocio
        $negocio = N::where(['id_unico'=>$id,'estado_id'=>$estado->id])->first();
        
        
        if($negocio){
            return $next($req);
        }else{
             if($req->method() == 'POST')
            {
                return response(RSP::swalError());
                
            }elseif($req->method() == 'GET')
            {
                return RSP::redirectWith('Sitios',RSP::swal('Atencion','El sitio que intenta consultar no existe o no se encuentra disponible.','info'));
            }
        }
        
    }
}
