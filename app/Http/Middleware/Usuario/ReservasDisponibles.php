<?php

namespace App\Http\Middleware\Usuario;

use Closure;
use App\Models\General\ReservasDisponibles as RD;
use App\Models\General\Negocio as N;
use App\Helpers\Response as RSP;

class ReservasDisponibles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if($req->route()->negocio && $req->route()->reserva){
            // Id verdadero del negocio
            $negocio = N::getId($req->route()->negocio);
            // Id de la reserva disponible
            $rd = $req->route()->reserva;
            
            if($negocio){
                
                // Se valida que exista dicha reserva
                $reserva = RD::where(['negocio_id'=>$negocio->id,'id'=>$rd])->first();
                
                // Si existe deja pasar y si no arroja error
                if($reserva){
                    return $next($req);
                    
                }else{
                    return response(RSP::swalError());
                }
            }else{
                return response(RSP::swalError());
            }
        }else{
            return response(RSP::swalError());
        }
    }
}
