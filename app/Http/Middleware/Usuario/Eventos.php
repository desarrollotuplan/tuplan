<?php

namespace App\Http\Middleware\Usuario;

use Closure;
use App\Models\General\EstadoTablas as ET;
use App\Models\General\Eventos as EV;
use App\Helpers\Response as RSP;

class Eventos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if(isset($req->data)){
            $id = $req->data;
        }elseif(isset($req->id)){
            $id = $req->id;
        }elseif($req->route()->evento){
            $id = $req->route()->evento;
        }
        // OBTENER EL ID DEL ESTADO PARA LOS EVENTOS VIGENTES
        $estado = ET::getEstadoTablaId('eventos','Activo');
        
        $eve = EV::where(['id_unico'=>$id,'estado_id'=>$estado->id])->first();
        
        if($eve){
            $req->evento = $eve;
            return $next($req);
        }else{
            if($req->method() == 'POST')
            {
                return response(RSP::swalError());
                
            }elseif($req->method() == 'GET')
            {
                return RSP::redirectWith('sitios',RSP::swal('Atencion','El evento que intenta consultar no existe o no se encuentra disponible.','info'));
            }
        }
        
        
    }
}
