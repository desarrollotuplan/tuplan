<?php

namespace App\Http\Middleware\Usuario;

use Closure;
use App\Models\General\Ciudad as CI;
use App\Models\General\Categoria as CA;
use App\Helpers\Response as RSP;


class Filtros
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if(isset($req->ciudad)){
            
            // Se verifica que la ciudad exista
            $ciudad = CI::find($req->ciudad);
            
            // Si es correcto sigue el proceso y si no vota error
            if($ciudad || $req->ciudad == 'todas'){
                
                // Si existe categoria se verifica que exista
                if(isset($req->categoria)){
                        
                    // se verifica que exista
                    $categoria = CA::find($req->categoria);

                    if($categoria || $req->categoria == 'todas'){
                        return $next($req);
                    }else{
                        return response(RSP::swalError());
                    } 
                }else{
                    return response(RSP::swalError());
                }
            }else{
                return response(RSP::swalError());
            }
        }else{
            return response(RSP::swalError());
        }
    }
}
