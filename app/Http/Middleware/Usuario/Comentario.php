<?php

namespace App\Http\Middleware\Usuario;

use Closure;
use App\Models\General\EstadoTablas as ET;
use App\Models\General\Comentarios as CO;
use App\Models\General\Negocio as N;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class Comentario
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if($req->route()->negocio){
            
            $id = $req->route()->negocio;
            
            // Se busca el id de los negocios que se pueden acceder
            $estado = ET::getEstadoTablaId('negocio','Activo');
        
            // Se consulta el negocio
            $negocio = N::where(['id_unico'=>$id,'estado_id'=>$estado->id])->first();
            
            // Si existe se deja pasar si no arroja error
            if($negocio){
                
                // Si existe la variable comentario en el link se consulta
                if($req->route()->comentario){
                    $id_com = $req->route()->comentario;
                    
                    // Se busca el id de los comentarios que se pueden acceder
                    $estado_com = ET::getEstadoTablaId('comentarios','Activo');
                    
                    // Se verifica que exista el comentario
                    $comentario = CO::where(['id'=>$id_com,'negocio_id'=>$negocio->id,'usuario_id'=>User::id(),'estado_id'=>$estado_com->id])->first();
                    
                    if($comentario){
                        return $next($req);
                    }else{
                        return response(RSP::swalError());
                    }
                    
                }else{
                    return $next($req);
                }
            }else{
                return response(RSP::swalError());
            }
        }else{
            return response(RSP::swalError());
        }
    }
}
