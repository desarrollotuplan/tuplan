<?php

namespace App\Http\Middleware\Usuario;

use Closure;
use App\Models\Usuario\Guardados as G;
use App\Models\General\EstadoTablas as ET;
use App\Models\General\Negocio as N;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class Guardados
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if($req->route()->negocio){
            $id = $req->route()->guardado;
            
            // Se busca el id de los negocios que se pueden acceder
            $estado = ET::getEstadoTablaId('negocio','Activo');

            // Se consulta el negocio
            $negocio = N::where(['id_unico'=>$req->route()->negocio,'estado_id'=>$estado->id])->first();
            
            // Si existe 
            if($negocio){
                // si existe un id de guardado de consulta y si no se deja pasar
                if($id){
                    $g = G::where(['id'=>$id,'usuario_id'=>User::id()])->first();
                    if($g){
                        return $next($req);
                    }else{
                        return response(RSP::swalError());
                    }
                }else{
                    return $next($req);
                }
            }else{
                return response(RSP::swalError());
            }
        }elseif($req->route()->guardado){
            
            $g = G::where(['id'=>$req->route()->guardado,'usuario_id'=>User::id()])->first();
            
            if($g){
                return $next($req);
            }else{
                return response(RSP::swalError());
            }
        }else{
            return response(RSP::swalError());
        }
        
        
        
    }
}
