<?php

namespace App\Http\Middleware\Usuario;

use Closure;
use App\Models\General\Producto as P;
use App\Models\General\CategoriaProducto as CPR;
use App\Models\General\EstadoTablas as ET;
use App\Models\General\Negocio as N;
use App\Helpers\Response as RSP;

class Productos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        if($req->route()->producto && $req->route()->negocio){
            
            $id = $req->route()->producto;
            
            // Se obtiene el id del negocio
            $negocio = N::getId($req->route()->negocio);
            
            if($negocio->id){
                
                // Se busca el id de los productos que se pueden acceder osea activos
                $estado = ET::getEstadoTablaId('productos','Activo');

                // Se valida la categoria del producto
                $cat = CPR::select('id')->where('negocio_id',$negocio->id)->first();

                if($cat){
                    
                    // Se valida el producto
                    $pro = P::where(['id'=>$id,'estado_id'=>$estado->id,'categoria_id'=>$cat->id])->first();
                    
                    if($pro){
                        return $next($req);
                    }else{
                        return response(RSP::swalError());    
                    }
                    
                }else{
                    return response(RSP::swalError());
                }
                
            }else{
                return response(RSP::swalError());
            }
        }else{
            return response(RSP::swalError());
        }
        
        
    }
}
