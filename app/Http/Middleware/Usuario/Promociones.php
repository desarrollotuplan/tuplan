<?php

namespace App\Http\Middleware\Usuario;

use Closure;
use App\Models\General\Promociones as PR;
use App\Models\General\EstadoTablas as ET;
use App\Helpers\Response as RSP;

class Promociones
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {
        $id = $req->route()->promocion;
        
        // OBTENER EL ID DEL ESTADO PARA LAS PROMOCIONES VIGENTES
        $estado = ET::getEstadoTablaId('promociones','Activo');
        
        // Verificamos que exista promocion
        $promo = PR::where(['id_unico'=>$id,'estado_id'=>$estado->id])->first();
        
        if($promo){
            $req->promo = $promo;
            return $next($req);
        }else{
            // Si no existe la promocion se valida que tipo de peticion se hizo para asi mismo dar respuesta
            if($req->method() == 'POST')
            {
                return response(RSP::swalError());    
                
            }elseif($req->method() == 'GET')
            {
                return RSP::redirectWith('Promociones',RSP::swal('Atencion','La promocion que intenta consultar no esta disponible.','warning'));
            }
        }
    }
    
}
