<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \App\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        
        // Middleware del usuario
        'MUsuario'  =>  \App\Http\Middleware\Usuario\Usuario::class,
        'MPromos' => \App\Http\Middleware\Usuario\Promociones::class,
        'MNegocios' =>  \App\Http\Middleware\Usuario\Negocios::class,
        'MProductos' =>  \App\Http\Middleware\Usuario\Productos::class,
        'MReservasDisponibles'    =>  \App\Http\Middleware\Usuario\ReservasDisponibles::class,
        'MEventos' =>  \App\Http\Middleware\Usuario\Eventos::class,
        'MGuardados' =>  \App\Http\Middleware\Usuario\Guardados::class,
        'MFiltros'  =>  \App\Http\Middleware\Usuario\Filtros::class,
        'MFavoritos'    =>  \App\Http\Middleware\Usuario\Favoritos::class,
        'MComentario'    =>  \App\Http\Middleware\Usuario\Comentario::class,
        'MCalificacion'    =>  \App\Http\Middleware\Usuario\Calificacion::class,
        'MReservas'    =>  \App\Http\Middleware\Usuario\Reservas::class,
        
        // Middleware del cliente
        'MClient' => \App\Http\Middleware\Cliente\Client::class,
        'MCcategoriaProducto' => \App\Http\Middleware\Cliente\CategoriaProducto::class,
        'MCproductos' => \App\Http\Middleware\Cliente\Productos::class,
        'MCreservaDisponible' => \App\Http\Middleware\Cliente\ReservaDisponible::class,
        'MChoraReserva' => \App\Http\Middleware\Cliente\HoraReserva::class,
        'MCpromociones' => \App\Http\Middleware\Cliente\Promociones::class,
        'MCeventos' => \App\Http\Middleware\Cliente\Eventos::class,
        'MChorario' => \App\Http\Middleware\Cliente\Horario::class,
        'MCreserva' => \App\Http\Middleware\Cliente\Reserva::class,
        
    ];

    /**
     * The priority-sorted list of middleware.
     *
     * This forces non-global middleware to always be in the given order.
     *
     * @var array
     */
    protected $middlewarePriority = [
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\Authenticate::class,
        \Illuminate\Session\Middleware\AuthenticateSession::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
        \Illuminate\Auth\Middleware\Authorize::class,
    ];
}
