<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Usuario\Intereses as I;
use App\Models\General\Ciudad as C;
use App\Models\General\Categoria as CA;
use App\Models\General\Usuario as U;
use App\Models\General\Genero as G;
use App\Models\General\EstadoTablas as ET;
use App\Models\General\UsuarioRol as UR;
use App\Models\General\Rol as R;
use App\Models\General\GenerateImage as GI;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class UsuarioController extends Controller
{
    
    // Datos del usuario y sus intereses - usuario
    public function datosUsuario()
    {
        $ciudades = C::all();
        $intereses = I::with('categoria')->where('usuario_id',User::id())->get();
        $var = [
            'intereses' => $intereses,
            'ciudades' => $ciudades,
        ];
        return view('Usuario.DatosPersonales',$var);
    }
    
    // Guardar datos del usuario con cualquier rol
    public function usuarioSave(Request $req)
    {
        $user = U::find(User::id());
        $image = GI::convertToImageFile($req->imageUser,'imagenes/usuarios/','.jpg',$user->foto);
        $user->primer_nombre = $req->primer_nombre;
        $user->segundo_nombre = $req->segundo_nombre;
        $user->primer_apellido = $req->primer_apellido;
        $user->segundo_apellido = $req->segundo_apellido;
        $user->telefono_movil = $req->celular;
        $user->direccion = $req->direccion;
        $user->ciudad_id = $req->ciudad;
        $user->fecha_nacimiento = $req->fecha_nacimiento;
        if($image){
            $user->foto = $image;
        }
        if($user->save()){
            return RSP::swalSuccess('Se guardo la informacion correctamente',false,false,true);
        }else{
            return RSP::swalError();
        }
    }
    
    // Registrar usuario de facebook
    public static function RegistrarUsuarioFacebook($user){
        $rol = R::where('nombre','Usuario')->first();
        if($rol){
            $estado = ET::getEstadoTablaId('usuarios','Activo');
            $genero = G::where('nombre','Indefinido')->first();
            $usuario  = new U;
            $usuario->facebook_id = $user['id'];
            $usuario->email = $user['email'];
            $usuario->genero_id = $genero->id;
            $usuario->estado_id = $estado->id;
            $usuario->primer_nombre = $user['first_name'];
            $usuario->segundo_nombre = $user['last_name'];
            $usuario->password = bcrypt($user['first_name'].$user['last_name'].'1234567890');
            $usuario->email = $user['email'];
            $usuario->foto = 'storage/imagenes/usuarios/20190422104331507.jpg';
            
            if($usuario->save()){
                $rolUser = new UR;
                $rolUser->usuario_id = $usuario->id;
                $rolUser->rol_id = $rol->id;
                if($rolUser->save()){
                    return $usuario;          
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    // Actualizar el id del facebook del usuario ya registrado
    public static function registrarIdFacebookUser($user){
        $usuario = U::getEmail($user['email']);
        $usuario->facebook_id = $user['id'];
        if($usuario->save()){
            return $usuario;
        }else{
            return false;
        }
    }
    
    // Registrar los usuarios de google
    public static function registrarUsuarioGoogle($user){
        $rol = R::where('nombre','Usuario')->first();
        if($rol){
            $estado = ET::getEstadoTablaId('usuarios','Activo');
            $genero = G::where('nombre','Indefinido')->first();
            $usuario  = new U;
            $usuario->google_id = $user['sub'];
            $usuario->email = $user['email'];
            $usuario->genero_id = $genero->id;
            $usuario->estado_id = $estado->id;
            $usuario->primer_nombre = $user['given_name'];
            $usuario->primer_apellido = $user['family_name'];
            $usuario->password = bcrypt($user['given_name'].$user['family_name'].'1234567890');
            $usuario->email = $user['email'];
            $usuario->foto = 'storage/imagenes/usuarios/20190422104331507.jpg';
            
            if($usuario->save()){
                $rolUser = new UR;
                $rolUser->usuario_id = $usuario->id;
                $rolUser->rol_id = $rol->id;
                if($rolUser->save()){
                    return $usuario;          
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    
    // Actualizar el id de google del usuario ya registrado
    public static function registrarIdGoogleUser($user){
        $usuario = U::getEmail($user['email']);
        $usuario->google_id = $user['sub'];
        if($usuario->save()){
            return $usuario;
        }else{
            return false;
        }
    }
    
    // Perfil del cliente
    public function datosCliente(){
        $ciudades = C::all();
        return view('Cliente.Perfil',['ciudades'=>$ciudades]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
