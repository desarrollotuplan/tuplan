<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\General\DatosLogin as DL;
use App\Helpers\User;

class DatosLoginController extends Controller
{
  // Registrar datos de inicio de sesion
    public static function datosLogin(){
        $d = DL::getDataLogin();
        $dl = new DL;
        $dl->usuario_id = User::id();
        $dl->ip = $d['ip'];
        $dl->navegador_id = $d['browser'];
        $dl->dispositivo_id = $d['device'];
        $dl->sistema_operativo = $d['so'];
        $dl->save();
    }
}
