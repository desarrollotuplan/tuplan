<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\General\CategoriaProducto as CAP;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class CategoriaProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return RSP::viewModal('Cliente.CrearCategoria');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $cat = new CAP;
        $cat->negocio_id = User::negocioId();
        $cat->nombre = $req->categoria;
        $cat->save();
        
        $categoria = CAP::find($cat->id);
        
        return [
            RSP::alert('success','right-top','Hecho','Se registro la categoria.'),
            RSP::addCont('.categorias','Cliente.AgregarCategoriaProducto',['categoria'=>$categoria],true)
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$data)
    {
        $cat = CAP::find($id);
        return RSP::viewModal('Cliente.EditarCategoria',['categoria'=>$cat,'code'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $cat = CAP::where(['id'=>$id,'negocio_id'=>User::negocioId()])->first();
        if($cat){
            $cat->nombre = $req->categoria;
            $cat->save();

            $categoria = CAP::find($id);

            return [
                RSP::alert('success','right-top','Hecho','Categoria editada exitosamente'),
                RSP::viewDiv('[data-code="'.$req->code.'"]','Cliente.ActualizarCategoriaProducto',['categoria'=>$categoria],true)
            ];
            
        }else{
            return RSP::swalError();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
