<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\General\Comentarios as CO;
use App\Models\General\EstadoTablas as ET;
use App\Models\General\Negocio as N;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class ComentariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        $negocio = N::getId($req->route()->negocio);
        $com = CO::where(['usuario_id'=>User::id(),'negocio_id'=>$negocio->id])->first();
        
        if(!$com){
            return RSP::viewModal('Usuario.FormularioComentario');
        }else{
            return RSP::viewModal('Usuario.EditarComentario',['comentario'=>$com]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $negocio = N::getId($req->route()->negocio);
        
        $com = CO::where(['usuario_id'=>User::id(),'negocio_id'=>$negocio->id])->first();
        
        if(!$com){
            // Se busca el id de los negocios que se pueden acceder
            $estado = ET::getEstadoTablaId('comentarios','Activo');
            
            $com = new CO;
            $com->usuario_id = User::id();
            $com->estado_id = $estado->id;
            $com->negocio_id = $negocio->id;
            $com->comentario = $req->comentario;
            $com->save();
            return RSP::alert('success','right-top','Hecho','Has comentado un sitio',true);
        }else{
            return RSP::swalError();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $req)
    {
        $negocio = N::getId($req->route()->negocio);
            
        $id = $req->route()->comentario;
        
        $comentario = CO::where(['id'=>$id,'negocio_id'=>$negocio->id,'usuario_id'=>User::id()])->first();
        
        return RSP::viewModal('Usuario.EditarComentario',['comentario'=>$comentario]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        $negocio = N::getId($req->route()->negocio);
            
        $id = $req->route()->comentario;
        
        $com = CO::where(['id'=>$id,'negocio_id'=>$negocio->id,'usuario_id'=>User::id()])->first();;
        $com->comentario = $req->comentario;
        $com->save();
        
        return RSP::alert('success','right-top','Hecho','Has actualizado un comentario',true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
