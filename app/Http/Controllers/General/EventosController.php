<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\General\Eventos as EV;
use App\Models\General\Categoria as CA;
use App\Models\General\EstadoTablas as ET;
use App\Helpers\CategoriasCiudades as CC;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class EventosController extends Controller
{
    
    // Pagina principal de eventos del usuario
    public function eventosUsuario($id = null)
    {
        // OBTENER TODAS LAS CATEGORIAS PARA MOSTRARLAS
        $categorias = CC::getCategorias();
        
        // OBTENER LAS CIUDADES PARA MOSTRARLAS
        $ciudades= CC::getCiudades();
        
        // OBTENER EL ID DEL ESTADO PARA LOS EVENTOS VIGENTES
        $estado = ET::getEstadoTablaId('eventos','Activo');
        
        // OBTENER LOS EVENTOS CON ESE ESTADO VIGENTE
        $eventos = DB::table('eventos as e')
            ->join('negocio as n','n.id','=','e.negocio_id')
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->select('n.id','n.nombre as nombre_negocio','n.logo','n.eslogan','n.folder','n.id_unico as id_negocio','e.*','s.nombre as nombre_categoria')
            ->where(['e.estado_id'=>$estado->id])->get();
        
        // Si existe un id de una promo se trae para mostrar
        if(isset($id)){
            $eve = $this->detallesEvento($id);
        }else{
            $eve = null;
        }
        
        // datos
        $var = [
            'categorias'=>$categorias,
            'ciudades'=>$ciudades,
            'eventos'=>$eventos,
            'detallesPromo'=>$eve
        ];
        
        return view('Usuario.Eventos',$var);
    }
    
    // Detalles del evento por post y get
    public function detallesEvento($req){
        
        // Obtener detalles
        $evento = EV::with(['negocio'=>function($q){ $q->select('id','nombre','logo','eslogan','folder','id_unico','telefono','direccion'); }])->where(['id_unico'=>$req])->first();
        
        // Mostrar detalles
        return RSP::viewModal('Usuario.DetallesEventos',['evento'=>$evento]);
    }
    
    
    
    // Filtro por post - Usuario
    public function filtrar(Request $req){
        
        // Obtenemos el nombre de la ciudad por su id
        if($req->ciudad !== 'todas'){
            $ciudad = CC::getNameCiudad($req->ciudad);
        }else{
            $ciudad = 'Todas';
        }
        
        // Si existe una categoria que no sea 'todas' se busca su nombre
        if($req->categoria !== 'todas'){
            $categoria = CC::getNameCategoria($req->categoria);
        }else{
            $categoria = 'Todas';
        }
        
        // Se redirecciona para mostrar resultados
        return RSP::redirect('Eventos/Filtrar/'.$ciudad.'/'.$categoria);
    }
    
    // Filtro por get - usuario
    public function buscarEvento(Request $req){
        
        // Obtener la ciudad por su nombre
        if($req->route()->ciudad !== 'Todas'){
            $ciudad = CC::getIdCiudad($req->route()->ciudad);
        }
        
        // Obtener la categoria por su nombre
        if(!$req->route()->categoria !== 'Todas'){
            $categoria = CC::getIdCategoria($req->route()->categoria);
        }
        
        // OBTENER EL ID DEL ESTADO PARA LAS PROMOCIONES VIGENTES
        $estado = ET::getEstadoTablaId('Eventos','Activo');
        
        // OBTENER LOS EVENTOS CON ESE ESTADO VIGENTE
        $busqueda = DB::table('eventos as e')
            ->join('negocio as n','n.id','=','e.negocio_id')
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->select('n.id','n.nombre as nombre_negocio','n.logo','n.eslogan','n.folder','n.id_unico as id_negocio','e.*','s.nombre as nombre_categoria')
            ->where(['e.estado_id'=>$estado->id]);
        
        if(isset($categoria)){
            $busqueda = $busqueda->where(['c.id'=>$categoria->id]);
        }
        
        if(isset($ciudad)){
            $busqueda = $busqueda->where(['n.ciudad_id'=>$ciudad->id]);
        }
        
        $filtro = $busqueda->get();
        
        // Obtener ciudades para la vista
        $ciudades = CC::getCiudades();
        
        // Obtener categorias para la vista
        $categorias = CC::getCategorias();
        
        // detalles de promocion no existente - requisito de la vista
        $eve = null;
        // datos
        $var = [
            'categorias'=>$categorias,
            'ciudades'=>$ciudades,
            'eventos'=>$filtro,
            'detallesEvento'=>$eve
        ];
        
        return view('Usuario.Eventos',$var);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventos = EV::where(['negocio_id'=>User::negocioId()])->get();
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
            return RSP::viewDiv('.cont-main','Cliente.Eventos',['eventos'=>$eventos]);
        }else{
            return view('Cliente.MiSitio',['informacion'=>RSP::viewDiv('.cont-main','Cliente.Eventos',['eventos'=>$eventos])]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return RSP::viewModal('Cliente.CrearEvento');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        // Obtener el id del estado activo de la tabla eventos
        $estado = ET::getEstadoTablaId('eventos','Activo');
        
        $eve = new EV;
        $eve->negocio_id = User::negocioId();
        $eve->estado_id = $estado->id;
        $eve->id_unico = md5(uniqid(mt_rand(), true));
        $eve->nombre = $req->nombre;
        $eve->descripcion = $req->descripcion;
        $eve->informacion_adicional = $req->informacion;
        $eve->fecha_inicio = $req->fecha_inicio;
        $eve->hora_inicio = $req->hora_inicio;
        $eve->fecha_fin = $req->fecha_fin;
        $eve->hora_fin = $req->hora_fin;
        
        if($eve->save()){
            return [
                RSP::alert('success','right-top','Hecho','Se ha registrado correctamente el evento',true),
                RSP::addCont('.eventos','Cliente.AgregarEvento',['evento'=>$eve])
            ];
        }else{
            return RSP::swalError();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$code)
    {
        $eve = EV::find($id);
        return RSP::viewModal('Cliente.EditarEvento',['eve'=>$eve,'code'=>$code]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $eve = EV::find($id);
        $eve->nombre = $req->nombre;
        $eve->descripcion = $req->descripcion;
        $eve->informacion_adicional = $req->informacion;
        $eve->fecha_inicio = $req->fecha_inicio;
        $eve->hora_inicio = $req->hora_inicio;
        $eve->fecha_fin = $req->fecha_fin;
        $eve->hora_fin = $req->hora_fin;
        
        if($eve->save()){
            return [
                RSP::alert('success','right-top','Hecho','Se ha actualizado correctamente el evento',true),
                RSP::viewDiv('[data-code="'.$req->code.'"]','Cliente.ActualizarEvento',['evento'=>$eve,'code'=>$req->code])
            ];
        }else{
            return RSP::swalError();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
