<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\General\Reserva as R;
use App\Models\General\ReservasDisponibles as RD;
use App\Models\General\HorasReserva as HR;
use App\Models\General\EstadoTablas as ET;
use App\Models\General\TipoReserva as TR;
use App\Helpers\JessengersDate as JD;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class ReservaController extends Controller
{
    
    public function formularioReserva(Request $req)
    {
        // Validamos que haya seleccionado una hora para la reserva
        if($req->hora){
            // Seleccionamos el id del estado "reservado"
            $estado = ET::getEstadoTablaId('reservas','Reservado');
            
            // validamos que aun este disponible la hora de reserva
            $h = R::select('id')->where(['hora_reserva_id'=>$req->hora,'fecha'=>$req->fecha,'estado_id'=>$estado->id])->first();
            
            if(!$h){
                
                // Seleccionar la hora de la reserva para mostrarla y el id
                $hora = HR::select('reserva_disponible_id','hora')->find($req->hora);
                
                // Seleccionamos el numero minimo y el maximo de personas para el formulario
                $nm = RD::select('numero_minimo_personas as minimo','numero_maximo_personas as maximo')->find($hora->reserva_disponible_id);
                
                // Seleccionamos todos los tipos de reserva para mostrar en el formulario
                $tr = TR::all();
                
                // Traducir fecha a español y string
                $fecha = JD::getDateSpanishFull($req->fecha);
                
                $var = [
                    'hora'=>$req->hora,
                    'horaView'=>$hora->hora,
                    'fechaView'=>$fecha,
                    'fecha'=>$req->fecha,
                    'personas'=>$nm,
                    'tipoR' => $tr
                ];
                
                // Redirijimos al formulario de la reserva.
                return RSP::viewDiv('.horas-disponibles','Usuario.FormularioReserva',$var);
                
            }else{
                return RSP::swal('Ups!','Parece que alguien se te adelanto y reservo antes que tu, escoje otra hora para reservar.','info');
            }
            
        }else{
            return RSP::swal('Atencion','Seleccione una hora  para seguir con la reserva.','info');
        }
    }
    
    public function reservar(Request $req) 
    {
        // Seleccionamos el id del estado "reservado"
        $estado = ET::getEstadoTablaId('reservas','Reservado');
        // Validamos que aun este disponible la hora y la fecha para hacer la reserva
        $hora = R::select('id')->where(['hora_reserva_id'=>$req->hora,'fecha'=>$req->fecha,'estado_id'=>$estado->id])->first();
        
        if(!$hora){
            
            // Si no han reservado para esa fecha y hora procedemos a hacer la reserva
            $r = new R;
            $r->usuario_id = User::id();
            $r->nombre_usuario = $req->nombre;
            $r->hora_reserva_id = $req->hora;
            $r->fecha = $req->fecha;
            $r->numero_personas = $req->personas;
            $r->tipo_reserva_id = $req->ocacion;
            $r->informacion_adicional = $req->informacion;
            $r->estado_id = $estado->id;
            $r->save();
            
            return RSP::swalSuccess('Se ha hecho la reserva correctamente, puede revisar en sus reservas el estado y ver mucho mas.', false, true);
            
        }else{
            return RSP::swal('Ups!','Parece que alguien se te adelanto y reservo antes, escoje otra hora e intentalo de nuevo','info');
        }
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservas = DB::table('reserva as r')
            ->select('r.*','n.id as id_negocio','n.nombre as nombre_negocio','rd.nombre as nombre_reserva','es.nombre as estado')
            ->join('horas_reserva as hr','hr.id','=','r.hora_reserva_id')
            ->join('reservas_disponibles as rd','rd.id','=','hr.reserva_disponible_id')
            ->join('negocio as n','n.id','=','rd.negocio_id')
            ->join('estado-tablas as et','et.id','=','r.estado_id')
            ->join('estado as es','es.id','=','et.estado_id')
            ->where('r.usuario_id',User::id())
            ->orderBy('created_at','desc')
            ->get();
        
        return view('Usuario.Reservas',['reservas'=>$reservas]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $req)
    {
        $reserva = DB::table('reserva as r')
            ->select('r.*','n.id as id_negocio','n.nombre as nombre_negocio','n.logo','n.folder','n.foto_perfil','rd.nombre as nombre_reserva','rd.imagen','rd.informacion','rd.nombre as nombre_reserva','es.nombre as estado','hr.hora as hora_reserva','tr.nombre as nombre_tipo_reserva')
            ->join('horas_reserva as hr','hr.id','=','r.hora_reserva_id')
            ->join('reservas_disponibles as rd','rd.id','=','hr.reserva_disponible_id')
            ->join('negocio as n','n.id','=','rd.negocio_id')
            ->join('estado-tablas as et','et.id','=','r.estado_id')
            ->join('estado as es','es.id','=','et.estado_id')
            ->leftJoin('tipo_reserva as tr','tr.id','=','r.tipo_reserva_id')
            ->where(['r.usuario_id'=>User::id(),'r.id'=>$req->route()->reserva])
            ->first();
        
        return RSP::viewModal('Usuario.DetallesReservaUsuario',['reserva'=>$reserva]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        // Buscar el id del estado cancelado de la tabla reservas
        $estado = ET::getEstadoTablaId('reservas','Cancelado');
        // Seleccionar la reserva
        $reserva = R::where(['usuario_id'=>User::id(),'id'=>$req->route()->reserva])->first();
        //Cambiar el estado
        $reserva->estado_id = $estado->id;
        $reserva->save();
        return [RSP::redirect('Reservas'),RSP::redirectWith('Reservas',RSP::swal('Hecho','Se cancelo la reserva','success'))];
    }

    
    // Lista de reservas del cliente
    public function listaReservas(){
        $reservas = DB::table('reserva as r')
            ->select('r.*','n.id as id_negocio','rd.nombre as nombre_reserva','es.nombre as estado','hr.hora')
            ->join('horas_reserva as hr','hr.id','=','r.hora_reserva_id')
            ->join('reservas_disponibles as rd','rd.id','=','hr.reserva_disponible_id')
            ->join('negocio as n','n.id','=','rd.negocio_id')
            ->join('estado-tablas as et','et.id','=','r.estado_id')
            ->join('estado as es','es.id','=','et.estado_id')
            ->where('n.id',User::negocioId())
            ->orderBy('created_at','desc')
            ->get();
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
            return RSP::viewDiv('.cont-main','Cliente.ReservasServicios',['reservas'=>$reservas]);
        }else{
            return view('Cliente.Servicios',['informacion'=>RSP::viewDiv('.cont-main','Cliente.ReservasServicios',['reservas'=>$reservas])]);
        }
    }

    // Pagina de reservas - cliente
    public function reservas(){
        // Recogemos los tipos de reserva del negocio - cliente
        $tipos = RD::where(['negocio_id'=>User::negocioId()])->pluck('id');
        
        // Recogemos las reservas hechas a dichos tipos
        $reservas = R::whereIn('tipo_reserva_id',$tipos)->get();
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
            return RSP::viewDiv('.cont-main','Cliente.ReservasServicios',['reservas'=>$reservas]);
        }else{
            return view('Cliente.Servicios',['informacion'=>RSP::viewDiv('.cont-main','Cliente.ReservasServicios',['reservas'=>$reservas])]);
        }
        
    }
    
    // Detalles reserva - cliente
    public function detallesReserva(Request $req){
        $reserva = DB::table('reserva as r')
            ->select('r.*','rd.nombre as nombre_reserva','rd.imagen','rd.informacion','rd.nombre as nombre_reserva','es.nombre as estado','hr.hora as hora_reserva','tr.nombre as nombre_tipo_reserva')
            ->join('horas_reserva as hr','hr.id','=','r.hora_reserva_id')
            ->join('reservas_disponibles as rd','rd.id','=','hr.reserva_disponible_id')
            ->join('negocio as n','n.id','=','rd.negocio_id')
            ->join('estado-tablas as et','et.id','=','r.estado_id')
            ->join('estado as es','es.id','=','et.estado_id')
            ->leftJoin('tipo_reserva as tr','tr.id','=','r.tipo_reserva_id')
            ->where(['n.id'=>User::negocioId(),'r.id'=>$req->route()->reserva])
            ->first();
        
        return RSP::viewModal('Cliente.DetallesReservaServicios',['reserva'=>$reserva]);
    }
    
}