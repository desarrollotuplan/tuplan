<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\General\Negocio as N;
use App\Models\General\NegocioReserva as NR;
use App\Models\General\ReservasDisponibles as RD;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class NegocioReservaController extends Controller
{
    // Reservas del sitios-usuario
    public function reservasNegocio(Request $req){
        
        // Id del negocio
        $neg = N::getId($req->route()->negocio);
        
        // Se busca la informacion de reserva del sitio
        $reserva = NR::where('negocio_id',$neg->id)->first();
        
        // Se valida si el sitio cuenta con algun tipo de reserva
        if($reserva && $reserva->reserva == 'si'){
            
            // Si cuenta con reservas se consultan los tipos existentes
            $rDisponibles = RD::where('negocio_id',$neg->id)->get();
            
            return RSP::viewDiv('#loadInformation','Usuario.ReservasSitio',['reserva'=>$reserva,'rDisponibles'=>$rDisponibles]);
            
        }else{
            return RSP::viewDiv('#loadInformation','Usuario.ReservasSitio');
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reserva = NR::where('negocio_id',User::negocioId())->first();
        
        $tiposReserva = RD::where('negocio_id',User::negocioId())->get();
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
            return RSP::viewDiv('.cont-main','Cliente.Reservas',['reserva'=>$reserva,'reservasDisponibles'=>$tiposReserva]);
        }else{
            return view('Cliente.MiSitio',['informacion'=>RSP::viewDiv('.cont-main','Cliente.Reservas',['reserva'=>$reserva,'reservasDisponibles'=>$tiposReserva])]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        $NR = NR::where('negocio_id',User::negocioId())->first();
        $NR->reserva = $req->reserva;
        $NR->hora_inicio = $req->hora_inicio;
        $NR->hora_final = $req->hora_final;
        $NR->save();
        
        return RSP::alert('success','right-top','Hecho','Informacion guardada correctamente',false,true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
