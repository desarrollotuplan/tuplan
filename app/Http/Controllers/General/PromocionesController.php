<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\General\Categoria as CA;
use App\Models\General\Promociones as PR;
use App\Models\General\EstadoTablas as ET;
use App\Helpers\CategoriasCiudades as CC;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class PromocionesController extends Controller
{
    // Pagina de promociones
    public function promocionesUsuario($id = null)
    {
        // OBTENER TODAS LAS CATEGORIAS PARA MOSTRARLAS
        $categorias = CC::getCategorias();
        
        // OBTENER LAS CIUDADES PARA MOSTRARLAS
        $ciudades= CC::getCiudades();
        
        // OBTENER EL ID DEL ESTADO PARA LAS PROMOCIONES VIGENTES
        $estado = ET::getEstadoTablaId('promociones','Activo');
        
        // OBTENER LAS PROMOCIONES CON ESE ESTADO VIGENTE
        $promociones = DB::table('promociones as p')
            ->join('negocio as n','n.id','=','p.negocio_id')
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->select('n.id','n.nombre as nombre_negocio','n.logo','n.eslogan','n.folder','n.id_unico as id_negocio','p.*','s.nombre as nombre_categoria')
            ->where(['p.estado_id'=>$estado->id])->get();
        
        // Si existe un id de una promo se trae para mostrar
        if(isset($id)){
            $pro = $this->detallesPromocion($id);
        }else{
            $pro = null;
        }
        
        // datos
        $var = [
            'categorias'=>$categorias,
            'ciudades'=>$ciudades,
            'promociones'=>$promociones,
            'detallesPromo'=>$pro
        ];
        
        return view('Usuario.Promociones',$var);
    }
    
    // Destalles de la promocion
    public function detallesPromocion($req){
        
        if(isset($req->data)){
            $id = $req->data;
        }elseif($req){
            $id = $req;
        }
        
        // Obtener detalles
        $promo = PR::with(['negocio'=>function($q){ $q->select('id','nombre','logo','eslogan','folder','id_unico','telefono','direccion'); }])->where(['id_unico'=>$id])->first();
        
        // Mostrar detalles
        return RSP::viewModal('Usuario.DestallesPromocion',['promo'=>$promo]);
        
    }
    
    // Filtro por post - Usuario
    public function filtrar(Request $req){
        
        // Obtenemos el nombre de la ciudad por su id
        if($req->ciudad !== 'todas'){
            $ciudad = CC::getNameCiudad($req->ciudad);
        }else{
            $ciudad = 'Todas';
        }
        
        // Si existe una categoria que no sea 'todas' se busca su nombre
        if($req->categoria !== 'todas'){
            $categoria = CC::getNameCategoria($req->categoria);
        }else{
            $categoria = 'Todas';
        }
        
        // Se redirecciona para mostrar resultados
        return RSP::redirect('Promociones/Filtrar/'.$ciudad.'/'.$categoria);
    }
    
    // Filtro por get - usuario
    public function buscarPromo(Request $req){
        // Obtener la ciudad por su nombre
        if($req->route()->ciudad !== 'todas'){
            $ciudad = CC::getIdCiudad($req->route()->ciudad);
        }
        
        // Obtener la categoria por su nombre
        if(!$req->route()->categoria !== 'todas'){
            $categoria = CC::getIdCategoria($req->route()->categoria);
        }
        
        // OBTENER EL ID DEL ESTADO PARA LAS PROMOCIONES VIGENTES
        $estado = ET::getEstadoTablaId('promociones','Activo');
        
        // OBTENER LAS PROMOCIONES CON ESE ESTADO VIGENTE
        $busqueda = DB::table('promociones as p')
            ->join('negocio as n','n.id','=','p.negocio_id')
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->select('n.id','n.nombre as nombre_negocio','n.logo','n.eslogan','n.folder','n.id_unico as id_negocio','p.*','s.nombre as nombre_categoria')
            ->where(['p.estado_id'=>$estado->id]);
        
        if(isset($categoria)){
            $busqueda = $busqueda->where(['c.id'=>$categoria->id]);
        }
        
        if(isset($ciudad)){
            $busqueda = $busqueda->where(['n.ciudad_id'=>$ciudad->id]);
        }
        
        $filtro = $busqueda->get();
        
        // Obtener ciudades para la vista
        $ciudades = CC::getCiudades();
        
        // Obtener categorias para la vista
        $categorias = CC::getCategorias();
        
        // detalles de promocion no existente - requisito de la vista
        $pro = null;
        
        // datos
        $var = [
            'categorias'=>$categorias,
            'ciudades'=>$ciudades,
            'promociones'=>$filtro,
            'detallesPromo'=>$pro
        ];
        
        return view('Usuario.Promociones',$var);
    }
    
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promos = PR::where(['negocio_id'=>User::negocioId()])->get();
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
            return RSP::viewDiv('.cont-main','Cliente.Promociones',['promociones'=>$promos]);
        }else{
            return view('Cliente.MiSitio',['informacion'=>RSP::viewDiv('.cont-main','Cliente.Promociones',['promociones'=>$promos])]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return RSP::viewModal('Cliente.CrearPromocion');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        // Obtener el id del estado registrado
        $estado = ET::getEstadoTablaId('promociones','Activo');
        
        $pro = new PR;
        $pro->negocio_id = User::negocioId();
        $pro->estado_id = $estado->id;
        $pro->id_unico = md5(uniqid(mt_rand(), true));
        $pro->nombre = $req->nombre;
        $pro->descripcion = $req->descripcion;
        $pro->informacion_adicional = $req->informacion;
        $pro->fecha_inicio = $req->fecha_inicio;
        $pro->hora_inicio = $req->hora_inicio;
        $pro->fecha_fin = $req->fecha_fin;
        $pro->hora_fin = $req->hora_fin;
        
        if($pro->save()){
            return [
                RSP::alert('success','right-top','Hecho','Se ha registrado correctamente la promocion',true),
                RSP::addCont('.promociones','Cliente.AgregarPromocion',['promo'=>$pro])
            ];
        }else{
            return RSP::swalError();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$code)
    {
        $promo = PR::find($id);
        return RSP::viewModal('Cliente.EditarPromocion',['promo'=>$promo,'code'=>$code]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $pro = PR::find($id);
        $pro->nombre = $req->nombre;
        $pro->descripcion = $req->descripcion;
        $pro->informacion_adicional = $req->informacion;
        $pro->fecha_inicio = $req->fecha_inicio;
        $pro->hora_inicio = $req->hora_inicio;
        $pro->fecha_fin = $req->fecha_fin;
        $pro->hora_fin = $req->hora_fin;
        
        if($pro->save()){
            return [
                RSP::alert('success','right-top','Hecho','Se ha actualizado correctamente la promocion',true),
                RSP::viewDiv('[data-code="'.$req->code.'"]','Cliente.ActualizarPromocion',['promo'=>$pro,'code'=>$req->code])
            ];
        }else{
            return RSP::swalError();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
