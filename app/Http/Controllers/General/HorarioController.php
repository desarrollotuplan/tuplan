<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\General\Horario as H;
use App\Models\General\HorarioTipo as HT;
use App\Models\General\Dias as D;
use App\Models\General\Negocio as N;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class HorarioController extends Controller
{
    
    // Horario negocio
    public function horarioNegocio($id, Request $req){
        
        // Id del negocio consultado
        $id = N::getId($id);
        
        $horario = H::with(['dia'=>function($q){ $q->select('id','nombre'); },'horarioTipo'=>function($q){ $q->select('id','nombre'); }])->where('negocio_id',$id->id)->get();
        
        return RSP::viewDiv('#loadInformation','Usuario.Horario',['horario'=>$horario]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $horario = D::with(['horario'=>function($q){ $q->where(['negocio_id'=>User::negocioId()]); },'horario.horarioTipo'=>function($q){ $q->select('id','nombre'); }])->get();
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
            return RSP::viewDiv('.cont-main','Cliente.HorarioAtencion',['horario'=>$horario]);
        }else{
            return view('Cliente.MiSitio',['informacion'=>RSP::viewDiv('.cont-main','Cliente.HorarioAtencion',['horario'=>$horario])]);
        }
    }
    
    // Verificar si hay un horario en dicho dia
    private function getHorarioDia($id)
    {
        $dia = D::find($id);
        if($dia){
            return H::where('dia_id',$dia->id)->first();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id,$code)
    {
        // Si no hay horario un horario para ese dia se direcciona al formulario
        if(!$this->getHorarioDia($id)){
            $dia = D::find($id);
            if($dia){
                $tipo = HT::all();
                return RSP::viewModal('Cliente.CrearHorarioDia',['dia'=>$dia,'tipo'=>$tipo,'code'=>$code]);
            }else{
                return RSP::swalError();
            }
        }else{
            return RSP::swal('Error','Este dia ya tiene un horario asignado','error');
        }
    }

    
    // Confirmar el tipo de horario y devolver el formulario correspondiente al tipo - cliente
    public function confirmar(Request $req){
        $ht = HT::find($req->tipo);
        if($ht){
            switch($ht->nombre){
                case "Unica jornada";
                    $vista = 'Cliente.CrearHorarioUnicaJornada';
                break;
                case "Doble jornada";
                    $vista = 'Cliente.CrearHorarioDobleJornada';
                break;
                case "24 Horas";
                    $vista = 'Cliente.CrearHorario24Horas';
                break;
            }
            return RSP::viewDiv('.loadHorario',$vista,['horario'=>$ht]);
        }else{
            return RSP::swalError();
        }
        
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        if(!$this->getHorarioDia($req->dia)){
            if(D::find($req->dia)){
                $ht = HT::find($req->tipo);
                $hor = new H;
                $hor->negocio_id = User::negocioId();
                $hor->dia_id = $req->dia;
                $hor->horario_tipo_id = $ht->id;
                if($ht->nombre == '24 Horas'){
                    $hor->hora_inicio_1 = '00:00';
                    $hor->hora_fin_1 = '23:59';
                }else{
                    $hor->hora_inicio_1 = $req->hora_inicio_1;
                    $hor->hora_fin_1 = $req->hora_fin_1;
                    if($ht->nombre == 'Doble jornada'){
                        $hor->hora_inicio_2 = $req->hora_inicio_2;
                        $hor->hora_fin_2 = $req->hora_fin_2;
                    }
                }
                $hor->save();
                return [ 
                    RSP::alert('success','right-top','Hecho','Horario definido',true),
                    RSP::viewDiv('[data-code="'.$req->code.'"]','Cliente.ActualizarHorario',['horario'=>$hor,'code'=>$req->code,'tipo'=>$ht])
                ];
            }else{
                return RSP::swalError();
            }
        }else{
            return RSP::swalError();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$code)
    {
        $hr = H::find($id);
        $tipo = HT::all();
        return RSP::viewModal('Cliente.EditarHorario',['horario'=>$hr,'code'=>$code,'tipo'=>$tipo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $ht = HT::find($req->tipo);
        $hor = H::where(['id'=>$id, 'dia_id'=>$req->dia])->first();
        if($hor){
            $hor->horario_tipo_id = $ht->id;
            if($ht->nombre == '24 Horas'){
                $hor->hora_inicio_1 = '00:00';
                $hor->hora_fin_1 = '23:59';
            }else{
                $hor->hora_inicio_1 = $req->hora_inicio_1;
                $hor->hora_fin_1 = $req->hora_fin_1;
                if($ht->nombre == 'Doble jornada'){
                    $hor->hora_inicio_2 = $req->hora_inicio_2;
                    $hor->hora_fin_2 = $req->hora_fin_2;
                }
            }
            $hor->save();
            return [ 
                RSP::alert('success','right-top','Hecho','Horario Actualizado',true),
                RSP::viewDiv('[data-code="'.$req->code.'"]','Cliente.ActualizarHorario',['horario'=>$hor,'code'=>$req->code,'tipo'=>$ht])
            ];
        }else{
            return RSP::swalError();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
