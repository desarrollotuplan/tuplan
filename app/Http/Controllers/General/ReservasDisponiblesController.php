<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\General\ReservasDisponibles as RD;
use App\Models\General\EstadoTablas as ET;
use App\Models\General\Dias as D;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class ReservasDisponiblesController extends Controller
{
    
    // detalles Reserva - usuario
    public function detallesReservaDisponible(Request $req){
        $detalleR = RD::find($req->route()->reserva);
        return RSP::viewModal('Usuario.DetallesReserva', ['reserva'=>$detalleR]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return RSP::viewModal('Cliente.CrearReservaDisponible');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $rd = new RD;
        $rd->negocio_id = User::negocioId();
        $rd->nombre = $req->nombre;
        $rd->informacion = $req->informacion;
        $rd->numero_minimo_personas = $req->minimo;
        $rd->numero_maximo_personas = $req->maximo;
        $rd->save();
        return [
            RSP::addCont('.reservas','Cliente.AgregarReservaDisponible',['reserva'=>$rd],true),
            RSP::alert('success','right-top','Hecho','Tipo de reserva registrado')

        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Obtenemos el id del estado activo de la tabla 'horas_reserva'
        $estado = ET::getEstadoTablaId('horas_reserva','Activo');
        
        $rd = RD::find($id);
        $dias = D::with(['horasReservas'=>function($q)use($id,$estado){ $q->where(['reserva_disponible_id'=>$id,'estado_id'=>$estado->id]); }])->get();
        return RSP::viewModal('Cliente.DetallesReservaDisponible',['reserva'=>$rd,'dias'=>$dias],'modal-lg modal-dark');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$code)
    {
        $rd = RD::find($id);
        return RSP::viewModal('Cliente.EditarReservaDisponible',['reserva'=>$rd,'code'=>$code]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $rd = RD::find($id);
        $rd->nombre = $req->nombre;
        $rd->informacion = $req->informacion;
        $rd->numero_minimo_personas = $req->minimo;
        $rd->numero_maximo_personas = $req->maximo;
        $rd->save();
        return [
            RSP::alert('success','right-top','Hecho','Tipo de reserva actualizada correctammente',true),
            RSP::viewDiv('[data-code="'.$req->code.'"]','Cliente.ActualizarReservaDisponible',['reserva'=>$rd,'code'=>$req->code])
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
