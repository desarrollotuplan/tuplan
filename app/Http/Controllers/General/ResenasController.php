<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\General\Comentarios as CO;
use App\Models\General\Calificacion as CA;
use App\Models\General\Negocio as N;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class ResenasController extends Controller
{
    
    // Reseñas del sitio
    public function resenasSitio(Request $req)
    {
        //Id del negocio 
        $neg = N::getId($req->route()->negocio);
        
        // Obtenemos los comentarios del sitio
        $comentarios = CO::with(['usuario'=>function($q){ $q->select('id','primer_nombre','foto'); }])
            ->where('negocio_id',$neg->id)
            ->orderBy('created_at','desc')->get();
        
        // Obtener el total de puntos y el nombre del tipo de calificacion de las calificaciones agrupada por el tipo de calificacion
        $calificacion = DB::table('calificacion as ca')
            ->select(DB::raw('SUM(p.puntos) as cantidadPuntos'),'ti.nombre','ti.icono')
            ->join('puntos as p','ca.punto_id','=','p.id')
            ->join('tipo_calificacion as ti','ca.tipo_calificacion_id','=','ti.id')
            ->where('ca.negocio_id',$neg->id)
            ->groupBy('tipo_calificacion_id')
            ->get();
            
        // Usuario que calificaron el sitio 
        $usuarios = CA::select('usuario_id')
            ->where('negocio_id',$neg->id)
            ->groupBy('usuario_id')
            ->pluck('usuario_id');
        
        // Calificacion de cada usuario
        foreach($comentarios as $c)
        {
            $calificacion_usuario = DB::table('calificacion as ca')
            ->select('ti.nombre','p.puntos')
            ->join('puntos as p','ca.punto_id','=','p.id')
            ->join('tipo_calificacion as ti','ca.tipo_calificacion_id','=','ti.id')
            ->where('ca.usuario_id',$c->usuario_id)
            ->get();   
            $cal[] = ['usuario' => $c->usuario_id, 'calificacion' => $calificacion_usuario];
        }
        
        // Numero de votantes
        $votantes = $usuarios->count();
        
        // Comentario del usuario
        $coment = CO::where(['usuario_id'=>User::id(),'negocio_id'=>$neg->id])->first();
        
        // Calificacion del usuario
        $calificacion_user = CA::where(['usuario_id'=>User::id(),'negocio_id'=>$neg->id])->first();
        
        $var = [
            'comentarios' => $comentarios,
            'calificacion' => $calificacion,
            'votantes' => $votantes,
            'calificacionUsuarios' => $cal,
            'comentario_user'   =>  $coment,
            'calificacion_user'   =>  $calificacion_user
        ];
        
        return RSP::viewDiv('#loadInformation','Usuario.Resenas',$var);
    }
    
    // Filtro para ordenar comentarios
    public function filtroComentarios(Request $req){
         //Id del negocio 
        $neg = N::getId($req->route()->negocio);
        
        // Obtenemos los comentarios del sitio
        $comentarios = CO::with(['usuario'=>function($q){ $q->select('id','primer_nombre','foto'); }])
            ->where('negocio_id',$neg->id);
        
        // Usuario que calificaron el sitio 
        $usuarios = CA::select('usuario_id')
            ->where('negocio_id',$neg->id)
            ->groupBy('usuario_id')
            ->pluck('usuario_id');
        
        if($req->filtro == 'recientes'){
            $comentarios = $comentarios->orderBy('created_at','desc')->get();
        }elseif($req->filtro == 'antiguos'){
            $comentarios = $comentarios->orderBy('created_at','asc')->get();
        }else{
            $comentarios = $comentarios->orderBy('created_at','desc')->get();
        }
        
        // Calificacion de cada usuario
        foreach($comentarios as $c)
        {
            $calificacion_usuario = DB::table('calificacion as ca')
            ->select('ti.nombre','p.puntos')
            ->join('puntos as p','ca.punto_id','=','p.id')
            ->join('tipo_calificacion as ti','ca.tipo_calificacion_id','=','ti.id')
            ->where('ca.usuario_id',$c->usuario_id)
            ->get();   
            $cal[] = ['usuario' => $c->usuario_id, 'calificacion' => $calificacion_usuario];
        }
        
        return RSP::viewDiv('.comentarios','Usuario.FiltrarComentarios',['comentarios'=>$comentarios,'calificacionUsuarios'=>$cal]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
