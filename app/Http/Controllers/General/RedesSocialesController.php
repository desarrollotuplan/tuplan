<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\General\RedesSociales as RS;
use App\Models\General\Negocio as N;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class RedesSocialesController extends Controller
{
    
    public function redesNegocio($id , Request $req){
        // Id del negocio
        $id = N::getId($id);
        
        $redes = RS::select('facebook','instagram','youtube','website','app_ios','app_android','twitter')->where('negocio_id',$id->id)->first();
        
        return RSP::viewDiv('#loadInformation', 'Usuario.RedesSociales', ['redes'=>$redes]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $RS = RS::where(['negocio_id'=>User::negocioId()])->first();
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
            return RSP::viewDiv('.cont-main','Cliente.RedesSociales',['redes'=>$RS]);
        }else{
            return view('Cliente.MiSitio',['informacion'=>RSP::viewDiv('.cont-main','Cliente.RedesSociales',['redes'=>$RS])]);
        }
    }

   
    public function update(Request $req)
    {
        $RS = RS::where(['negocio_id'=>User::negocioId()])->first();
        
        if($RS){
            $RS->facebook = $req->facebook;
            $RS->instagram = $req->instagram;
            $RS->youtube = $req->youtube;
            $RS->twitter = $req->twitter;
            $RS->website = $req->website;
            $RS->app_android = $req->app_android;
            $RS->app_ios = $req->app_ios;
            $RS->save();
            
            return RSP::alert('success','right-top','Hecho','Redes Sociales Actualizadas',false,true);
        }else{
            return RSP::swalError();
        }
    }
}
