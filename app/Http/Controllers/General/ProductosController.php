<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\General\CategoriaProducto as CAP;
use App\Models\General\Producto as P;
use App\Models\General\Negocio as N;
use App\Models\General\EstadoTablas as ET;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class ProductosController extends Controller
{
    
    // Productos del negocio - usuario
    public function productos($id , Request $req){
        
        // Id del negocio
        $id = N::getId($id);
        
        $categorias = CAP::with(['productos.imagenes'=>function($q){ $q->select('id','ruta','producto_id')->groupBy('producto_id'); }])
            ->where('negocio_id',$id->id)
            ->get();
        
        return RSP::viewDiv('#loadInformation', 'Usuario.Productos', ['categorias'=>$categorias]);
    }
    
    // Detalles producto - usuario
    public function detallesProducto(Request $req){
        $producto_id = $req->route()->producto;
        $p = P::with(['imagenes'=>function($q){ $q->select('id','ruta','producto_id'); }])->where('id',$producto_id)->first();
        return RSP::viewModal('Usuario.DetallesProducto', ['producto'=>$p]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = CAP::with('productos')->where('negocio_id',User::negocioId())->get();
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
            return RSP::viewDiv('.cont-main','Cliente.Productos',['categorias'=>$categorias]);
        }else{
            return view('Cliente.MiSitio',['informacion'=>RSP::viewDiv('.cont-main','Cliente.Productos',['categorias'=>$categorias])]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($categoria,$code)
    {
        return RSP::viewModal('Cliente.CrearProducto',['categoria'=>$categoria,'code'=>$code]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $estado = ET::getEstadoTablaId('productos','Activo');
        
        $pro = new P;
        $pro->nombre = $req->nombre;
        $pro->categoria_id = $req->categoria;
        $pro->estado_id = $estado->id;
        $pro->especificaciones = $req->especificaciones;
        $pro->informacion_adicional = $req->informacion_adicional;
        $pro->precio = $req->precio;
        $pro->save();
        
        $producto = P::find($pro->id);
        $categoria = CAP::find($req->categoria);
        return [
            RSP::alert('success','right-top','Hecho','Producto Registrado',true),
            RSP::viewDiv('[data-code="'.$req->code.'"]','Cliente.ActualizarCategoriaProducto',['categoria'=>$categoria]),
            RSP::addCont('#'.$req->code,'Cliente.Producto',['producto'=>$producto])
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$code)
    {
        $producto = P::find($id);
        return RSP::viewModal('Cliente.EditarProducto',['producto'=>$producto,'code'=>$code]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $pro = P::find($id);
        $pro->nombre = $req->nombre;
        $pro->especificaciones = $req->especificaciones;
        $pro->informacion_adicional = $req->informacion_adicional;
        $pro->precio = $req->precio;
        $pro->save();
        
        $producto = P::find($id);
        
        return [
            RSP::alert('success','right-top','Hecho','Producto Actualizado',true),
            RSP::viewDiv('[data-code="'.$req->code.'"]','Cliente.ActualizarProducto',['producto'=>$producto,'code'=>$req->code])
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
