<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\General\Recomendados as RE;
use App\Models\General\Negocio as N;
use App\Models\General\NegocioReserva as NR;
use App\Models\General\Domicilio as DM;
use App\Models\General\Imagenes as IMG;
use App\Models\General\Favoritos as F;
use App\Models\Usuario\Intereses as I;
use App\Models\Usuario\Guardados as G;
use App\Models\General\Visitas as V;
use App\Models\General\Ciudad as CI;
use App\Helpers\Response as RSP;
use App\Helpers\CategoriasCiudades as CC;
use App\Helpers\User;

class RecomendadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // Recomiendame
    public function index()
    {
        // Obtener ciudad del usuario para realizar la consulta de los recomendados.
        $ciudad = User::ciudadId();
        
        // Obtener negocios solo de la ciudad donde se encuentra el usuario.
        $recomendados = DB::table('negocio as n')
            ->select('n.id_unico','n.nombre','n.logo','s.nombre as categoria','n.folder','co.color','co.icono')
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->join('colores as co','c.id','=','co.categoria_id')
            ->where('ciudad_id',$ciudad)->get();
        
        // Obtener la informacion del primer negocio para mostrarla en pantalla.
        $info = $this->detallesRecomendado($recomendados[0]->id_unico);
        
        // Obtener todas las ciudades donde hayan negocios para el filtro.
        $ciudades = CC::getCiudades();
        
        // Obtener categorias de los negocios que esten registrados
        $categorias = CC::getCategorias();
        
        return view('Usuario.Recomiendame',['recomendados'=>$recomendados,
                                            'info'=>$info,
                                            'ciudades'=>$ciudades,
                                            'categorias'=>$categorias]);
    }
    
    // Detalles del sitio recomendado
    public function detallesRecomendado($id)
    {
        // Detalles del negocio y tablas asociadas
        $negocio = DB::table('negocio as n')->select('n.*','s.nombre as categoria','co.color','co.icono')->where('id_unico',$id)
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->join('colores as co','c.id','=','co.categoria_id')
            ->first();

        if($negocio){

            // Informacion sobre reserva
            $reservas = NR::select('reserva')->where('negocio_id',$negocio->id)->first();

            // Informacion sobre el servicio domicilio
            $domicilio = DM::select('servicio')->where('negocio_id',$negocio->id)->first();

            // Imagenes del sitio
            $imagenes = IMG::select('ruta')->where('negocio_id',$negocio->id)->get();

            // Saber si el sitio se encuentra en favoritos del usuario
            $fav = F::where(['usuario_id'=>User::id(),'negocio_id'=>$negocio->id])->first();

            // Saber si el sitio esta guardado por el usuario
            $guardado = G::select('id')->where(['negocio_id'=>$negocio->id,'usuario_id'=>User::id()])->first();

            // Variables paa enviar a la vista
            $var = [
                'negocio'=>$negocio,
                'reserva'=>$reservas,
                'domicilio'=>$domicilio,
                'imagenes'=>$imagenes,
                'favorito'=>$fav,
                'guardado'=>$guardado,
            ];
            
            return RSP::viewDiv('.load-details', 'Usuario.DetallesSitioRecomendado', $var);
        }
    }
    
    // Filtrar recomendados
    public function filtrar(Request $req){
        
        
        // Realizar el filtro
        $recomendados = DB::table('negocio as n')
            ->select('n.id_unico','n.nombre','n.logo','s.nombre as categoria','n.folder','co.color','co.icono')
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->join('colores as co','c.id','=','co.categoria_id');
        
        // Aplicamos filtro a la consulta
        if($req->categoria == 'todas'){
            $filtro = $recomendados->where('n.ciudad_id',$req->ciudad)->get();
        }else{
            $filtro = $recomendados->where(['n.ciudad_id'=>$req->ciudad,'c.id'=>$req->categoria])->get();
        }
        
        // Obtener la informacion del primer negocio para mostrarla en pantalla.
        $info = $this->detallesRecomendado($filtro[0]->id_unico);
        
        return [ 
            RSP::viewDiv('.option-selected','Usuario.ActualizarListaRecomendados',['recomendados'=>$filtro],true),
            $info,
        ];
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
