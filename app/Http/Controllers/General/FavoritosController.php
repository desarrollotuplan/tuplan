<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\General\Favoritos as F;
use App\Models\General\Negocio as N;
use App\Helpers\CategoriasCiudades as CC;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class FavoritosController extends Controller
{
    
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Categorias en las que existen negocios
        $categorias = CC::getCategorias();
        
        // Ciudades en las que hay negocios
        $ciudades = CC::getCiudades();
        
        // Gavoritos del usuario
        $favoritos = DB::table('negocio as n')
            ->select('n.id','n.id_unico','n.nombre','n.descripcion','n.foto_perfil','s.nombre as categoria','n.folder','co.color','co.icono')
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->join('colores as co','c.id','=','co.categoria_id')
            ->join('favoritos as f','f.negocio_id','=','n.id')
            ->where('f.usuario_id',User::id())
            ->get();
        
        return view('Usuario.Favoritos',['favoritos'=>$favoritos,'categorias'=>$categorias,'ciudades'=>$ciudades]);
    }
    
    // Filtrar favoritos - POST
    public function filtrar(Request $req){
        
        // Obtenemos el nombre de la ciudad por su id
        if($req->ciudad !== 'todas'){
            $ciudad = CC::getNameCiudad($req->ciudad);
        }else{
            $ciudad = 'Todas';
        }
        
        // Si existe una categoria que no sea 'todas' se busca su nombre
        if($req->categoria !== 'todas'){
            $categoria = CC::getNameCategoria($req->categoria);
        }else{
            $categoria = 'Todas';
        }
        
        // Se redirecciona para mostrar resultados
        return RSP::redirect('Favoritos/Filtrar/'.$ciudad.'/'.$categoria.'/'.$req->nombre);
    }
    
     // Filtrar favoritos del usuario
    public function buscarFavorito(Request $req)
    {
        // Obtener la ciudad por su nombre
        if($req->route()->ciudad !== 'Todas'){
            $ciudad = CC::getIdCiudad($req->route()->ciudad);
        }
        
        // Obtener la categoria por su nombre
        if(!$req->route()->categoria !== 'Todas'){
            $categoria = CC::getIdCategoria($req->route()->categoria);
        }
        
        // busqueda
        $busqueda = DB::table('negocio as n')
            ->select('n.id','n.id_unico','n.nombre','n.descripcion','n.foto_perfil','s.nombre as categoria','n.folder','co.color','co.icono')
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->join('colores as co','c.id','=','co.categoria_id')
            ->join('favoritos as f','f.negocio_id','=','n.id')
            ->where('f.usuario_id',User::id());
        
        
        if(isset($categoria)){
            $busqueda = $busqueda->where(['c.id'=>$categoria->id]);
        }
        
        if(isset($ciudad)){
            $busqueda = $busqueda->where(['n.ciudad_id'=>$ciudad->id]);
        }
        
        // Aplicar filtro
        if($req->route()->busqueda){
            $filtro = $busqueda->where(['n.nombre'=>$req->route()->busqueda])->get();
        }else{
            $filtro = $busqueda->get();
        }
        
        // Obtener ciudades para la vista
        $ciudades = CC::getCiudades();
        
        // Obtener categorias para la vista
        $categorias = CC::getCategorias();
        
        // Mostrar resultado
        return view('Usuario.Favoritos',['favoritos'=>$filtro,'categorias'=>$categorias,'ciudades'=>$ciudades]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($negocio, Request $req)
    {
        // Buscamos el id del negocio por el id unico
        $neg = N::select('id')->where('id_unico',$negocio)->first();
        
        // Si no encontramos el negocio retornamos alerta
        if($neg){
            
            // Verificamos que el usuario no tenga el sitio como favorito
            $f = F::select('id')->where(['negocio_id'=>$neg->id,'usuario_id'=>User::id()])->first();

            // Si el sitio no lo tiene como favorito se crea pero si ya esta devuelve alerta
            if(!$f){
                // Creamos el favorito
                $fav = new F;
                $fav->negocio_id = $neg->id;
                $fav->usuario_id = User::id();
                $fav->save();
                
                return RSP::alertView('success','right-top','Hecho','Sitio agregado a favoritos','.favorito','Usuario.FavoritoSitio',['favorito'=>$fav,'negocio'=>$negocio]);
                
            }else{
                return RSP::alert('info','right-top','Atencion','Este sitio ya lo tienes en favoritos');
            }
        }else{
            return RSP::swalError();
        }
    }

    
    public function destroy($negocio, $id)
    {
        // Traemos el favorito del usuario
        $fav = F::select('id')->where(['id'=>$id,'usuario_id'=>User::id()])->first();
        
        // Si se encuentra el favorito se elimina y si no se retorna alerta
        if($fav){
            
            $fav->delete();
            return RSP::alertView('info','right-top','Hecho','Se elimino el sitio de tu lista de favoritos','.favorito','Usuario.FavoritoSitio',['negocio'=>$negocio]);
            
        }else{
            return RSP::swalError();
        }
    }
    
    
}
