<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use App\Helpers\Response as RSP;

class ImagenesController extends Controller
{
    public function perfilUsuario(Request $req){
        return [RSP::viewModal('Complementos.ImagenUser',['input'=>'#img-user-upload','image'=>'.image-user'],'modal-dark modal-lg'), 
                RSP::jcrop(150,150,1,['image/jpg','image/png','image/jpeg'])];
    }
    
    public function logo(){
        return [RSP::viewModal('Complementos.ImagenUser',['input'=>'#img-logo-upload','image'=>'.logo-image'],'modal-dark modal-lg'), 
                RSP::jcrop(200,200,1,['image/jpg','image/png','image/jpeg'])];
    }
    
    public function portada(){
        return [RSP::viewModal('Complementos.ImagenUser',['input'=>'#img-portada-upload','image'=>'.portada'],'modal-dark modal-lg'), 
                RSP::jcrop(1080,500,2,['image/jpg','image/png','image/jpeg'])];
    }
    
    public function imagenesNegocio(){
        return [RSP::viewDiv('.load-files','Complementos.ImagenAlbum',['route'=>'MiNegocio/saveAlbum']), 
                RSP::jcrop(1280,720,2,['image/jpg','image/png','image/jpeg'])];
    }
    
    public function eliminarImagenesNegocio(){
        return [RSP::viewDiv('.load-files','Complementos.ImagenAlbum',['route'=>'MiNegocio/saveAlbum']), 
                RSP::jcrop(1280,720,2,['image/jpg','image/png','image/jpeg'])];
    }
    
}
