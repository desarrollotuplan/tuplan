<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\General\Calificacion as CA;
use App\Models\General\TipoCalificacion as TC;
use App\Models\General\Negocio as N;
use App\Models\General\Puntos as PU;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class CalificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        $negocio = N::getId($req->route()->negocio);
        
        $calificacion = CA::where(['negocio_id'=>$negocio->id,'usuario_id'=>User::id()])->get();
        
        $puntos = PU::all()->sortByDesc('puntos');
        
        $tipos_calificacion = TC::all();
        
        // Si el usuario ya ha calificado el sitio se dirije a el formulario de editar y si no se manda al formulario de registro
        if($calificacion->isEmpty()){
            
            return RSP::viewModal('Usuario.FormularioCalificacion',['puntos'=>$puntos,'tipo_calificacion'=>$tipos_calificacion]);
            
        }else{
            return RSP::viewModal('Usuario.EditarCalificacion',['calificacion'=>$calificacion,'puntos'=>$puntos,'tipo_calificacion'=>$tipos_calificacion]);
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $negocio = N::getId($req->route()->negocio);
        
        $cali = CA::where(['negocio_id'=>$negocio->id,'tipo_calificacion_id'=>$req->tipo_calificacion,'usuario_id'=>User::id()])->first();
        
        // Si hay una calificacion del mismo tipo se edita y si no se registra
        if(!$cali){
            $tc = TC::find($req->tipo_calificacion);
            
            // si el tipo de califiacion no existe arroja error
            if($tc){
                $califi = new CA;
                $califi->negocio_id = $negocio->id;
                $califi->usuario_id = User::id();
                $califi->tipo_calificacion_id = $tc->id;
                $califi->punto_id = $req->puntos;
                $califi->save();
                return RSP::alert('success','right-top','Hecho','Haz calificado un sitio');
            }else{
                return RSP::swalError();
            }
        }else{
            $cali->punto_id = $req->puntos;
            $cali->save();
            return RSP::alert('success','right-top','Hecho','Calificacion Actualizada');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $req)
    {
        $negocio = N::getId($req->route()->negocio);
        
        $calificacion = CA::where(['negocio_id'=>$negocio->id,'usuario_id'=>User::id()])->get();
        
        $puntos = PU::all()->sortByDesc('puntos');
            
        $tipos_calificacion = TC::all();
        
        return RSP::viewModal('Usuario.EditarCalificacion',['calificacion'=>$calificacion,'puntos'=>$puntos,'tipo_calificacion'=>$tipos_calificacion]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        $negocio = N::getId($req->route()->negocio);
        $tc = TC::find($req->tipo_calificacion);
        
        // Si el tipo de calificacion no existe arroja error
        if($tc){
            $califi = CA::where(['negocio_id'=>$negocio->id,'tipo_calificacion_id'=>$req->tipo_calificacion,'id'=>$req->route()->calificacion,'usuario_id'=>User::id()])->first();
            $califi->punto_id = $req->puntos;
            $califi->save();
            return RSP::alert('success','right-top','Hecho','Calificacion Actualizada');
        }else{
            return RSP::swalError();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
