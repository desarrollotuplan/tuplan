<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\General\HorasReserva as HR;
use App\Models\General\EstadoTablas as ET;
use App\Models\General\Reserva as R;
use App\Models\General\Dias as D;
use App\Helpers\Response as RSP;
use App\Helpers\JessengersDate as JD;

class HoraReservaController extends Controller
{
    // Buscar horas disponibles de una reserva-usuario
    public function horaReserva(Request $req){
        if($req->fecha){
            // Sacamos el numero del dia de la fecha
            $day = date('w',strtotime($req->fecha));
            
            // Id de la reserva
            $reserva_id = $req->route()->reserva;
            // Obtenemos el id del estado 'Reservado' de la tabla 'reserva'
            $est = ET::getEstadoTablaId('reservas','Reservado');
            
            if($est){
            
                // Seleccionamos el id de las horas que esten en estado "reservado"
                $reservas = DB::table('reserva as r')
                    ->join('horas_reserva as h','h.id','=','r.hora_reserva_id')
                    ->where(['r.fecha'=>$req->fecha,'r.estado_id'=>$est->id,'h.reserva_disponible_id'=>$reserva_id])
                    ->pluck('r.hora_reserva_id')->values();

                // Si no hay ninguna reserva para ese dia, hora y estado se le da un valor para poder hacer la siguiente consulta
                if($reservas->isEmpty()){
                    $reservas = [0];
                }

                // Obtenemos el id del estado activo de la tabla 'horas_reserva'
                $estado = ET::getEstadoTablaId('horas_reserva','Activo');

                
                // Traer las horas disponibles del tipo de reserva que el estado concuerde con el obetnido y que no esten previamente reservadas por algun otro usuario
                $hd = DB::table('horas_reserva as h')
                    ->select('h.id','h.hora')
                    ->join('dias as d','d.id','=','h.dia_id')
                    ->where(['h.reserva_disponible_id'=>$reserva_id,'d.codigo'=>$day,'estado_id'=>$estado->id])
                    ->whereNotIn('h.id',$reservas)
                    ->get();

                // Obtenemos la fecha en español y en string.
                $fecha = JD::getDateSpanishFull($req->fecha);

                $var = ['horas'=>$hd,'fechaString'=>$fecha,'date'=>$req->fecha,'reserva'=>$reserva_id];

                return RSP::viewDiv('.horas-disponibles', 'Usuario.HorasDisponiblesReserva', $var);

            }else{
                return RSP::swalError();
            }
            
        }else{
            return RSP::swal('Atencion','Seleccione una fecha valida e intentelo de nuevo.','info',false,['fecha']);
        }
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        // Nombre del dia para agregar horas
        
        $dia = D::find($req->dia)->nombre;
        
        // Obtenemos el id del estado activo de la tabla 'horas_reserva'
        $estado = ET::getEstadoTablaId('horas_reserva','Activo');
        
        if($req->cantidad > 0){
            for($i = 0; $i < $req->cantidad; $i++){
                $hr = new HR;
                $hr->reserva_disponible_id = $req->reserva;
                $hr->estado_id = $estado->id;
                $hr->dia_id = $req->dia;
                $hr->hora = $req->hora;
                $hr->save();
                $horas[] = $hr;
            }
            return [
                RSP::alert('success','right-top','Hecho','Hora de reserva guardada',false,false,true),
                RSP::addCont('.'.$dia,'Cliente.AgregarHoraReserva',['hora'=>$horas])
            ];
        }else{
            $hr = new HR;
            $hr->reserva_disponible_id = $req->reserva;
            $hr->estado_id = $estado->id;
            $hr->dia_id = $req->dia;
            $hr->hora = $req->hora;
            $hr->save;
            return [
                RSP::alert('success','right-top','Hecho','Hora de reserva guardada',false,false),
                RSP::addCont($dia,'Cliente.AgregarHoraReserva',['hora'=>$hr])
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Obtenemos el id del estado activo de la tabla 'horas_reserva'
        $estado = ET::getEstadoTablaId('horas_reserva','Inactivo');
        
        $hr = HR::find($id);
        if($estado){ 
            $hr->estado_id = $estado->id;
            $hr->save();
            return RSP::alert('success','right-top','Hecho','Hora eliminada');
        }
    }
}
