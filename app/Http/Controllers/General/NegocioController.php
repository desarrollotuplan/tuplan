<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\General\Negocio as N;
use App\Models\General\Categoria as C;
use App\Models\General\NegocioReserva as NR;
use App\Models\General\Domicilio as DM;
use App\Models\General\Imagenes as IMG;
use App\Models\General\Favoritos as F;
use App\Models\Usuario\Intereses as I;
use App\Models\Usuario\Guardados as G;
use App\Models\General\Visitas as V;
use App\Models\General\Eventos as EV;
use App\Models\General\Promociones as PR;
use App\Models\General\EstadoTablas as ET;
use App\Models\General\GenerateImage as GI;
use App\Helpers\CategoriasCiudades as CC;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class NegocioController extends Controller
{
    // Pagina de sitios del usuario
    public function sitios($id = null){
        
        // Categorias en las que existen negocios
        $categorias = CC::getCategorias();
        
        // Ciudades en las que hay negocios
        $ciudades = CC::getCiudades();
        
        // Intereses del usuario
        $intereses = I::select('categoria_id')->where('usuario_id',User::id())->get();
        
        // Negocios recomendados segun los intereses del usuario
        $recomendados = DB::table('negocio as n')
            ->select('n.id','n.id_unico','n.nombre','n.descripcion','n.foto_perfil','s.nombre as categoria','n.folder','co.color','co.icono')
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->join('colores as co','c.id','=','co.categoria_id');
        
            foreach($intereses as $i) {
                $recomendados->orWhere('s.categoria_id', $i->categoria_id);
            }
        
        $intereses = $recomendados->get();
        
        // Si existe un id en la ruta get se buscan los detalles del sitio para mostrar en la vista
        if($id){
            $negocio = $this->detallesNegocio($id);
        }else{
            $negocio = null;
        }
        
        return view('Usuario.Sitios',['intereses'=>$intereses,'categorias'=>$categorias,'negocio'=>$negocio,'ciudades'=>$ciudades]);
    }
    
    // Detalles Sitio - usuario
    public function detallesNegocio($id){
        if($id){
            
            // Detalles del negocio y tablas asociadas
            $negocio = DB::table('negocio as n')->select('n.*','s.nombre as categoria','co.color','co.icono')->where('id_unico',$id)
                ->join('subcategoria as s','s.id','=','n.subcategoria_id')
                ->join('categoria as c','c.id','=','s.categoria_id')
                ->join('colores as co','c.id','=','co.categoria_id')
                ->first();
            
            if($negocio){
                
                if(User::check()){
                    // Se registra una visita
                    $v = new V;
                    $v->usuario_id = User::id();
                    $v->negocio_id = $negocio->id;
                    $v->save();
                }
                
                // Informacion sobre reserva
                $reservas = NR::select('reserva')->where('negocio_id',$negocio->id)->first();

                // Informacion sobre el servicio domicilio
                $domicilio = DM::select('servicio')->where('negocio_id',$negocio->id)->first();

                // Imagenes del sitio
                $imagenes = IMG::select('ruta')->where('negocio_id',$negocio->id)->get();

                // Saber si el sitio se encuentra en favoritos del usuario
                $fav = F::where(['usuario_id'=>User::id(),'negocio_id'=>$negocio->id])->first();
                
                // Saber si el sitio esta guardado por el usuario
                $guardado = G::select('id')->where(['negocio_id'=>$negocio->id,'usuario_id'=>User::id()])->first();
                
                // Variables paa enviar a la vista
                $var = [
                    'negocio'=>$negocio,
                    'reserva'=>$reservas,
                    'domicilio'=>$domicilio,
                    'imagenes'=>$imagenes,
                    'favorito'=>$fav,
                    'guardado'=>$guardado,
                ];
                
                // Variable de activacion  para el fotorama / galeria
                if($_SERVER['REQUEST_METHOD'] == 'POST'){
                    $var['post'] = true;
                }

                return RSP::viewDiv('#loadContSitio', 'Usuario.DetallesSitio', $var);
            }else{
                return RSP::swal('Error','El sitio que intenta consultar no existe','error');
            }
        }
    }
    
    
    // Informacion Sitio - usuario
    public function informacionSitio(Request $req){
        // id del negocio
        $id = $req->route()->negocio;
        
        // Detalles del negocio y tablas asociadas
        $negocio = DB::table('negocio as n')->select('n.*','s.nombre as categoria','co.color','co.icono')->where('n.id_unico',$id)
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->join('colores as co','c.id','=','co.categoria_id')
            ->first();
        
        // Imagenes del sitio
        $imagenes = IMG::select('ruta')->where('negocio_id',$negocio->id)->get();

        $var = [
            'negocio'=>$negocio,
            'imagenes'=>$imagenes
        ];

        return RSP::viewDiv('#loadInformation','Usuario.InformacionSitio', $var);
            
    }
    
    // Filtro de busqueda de sitios del usuario
    public function filtrar(Request $req){
        
         // Obtenemos el nombre de la ciudad por su id
        if($req->ciudad !== 'todas'){
            $ciudad = CC::getNameCiudad($req->ciudad);
        }else{
            $ciudad = 'Todas';
        }
        
        // Si existe una categoria que no sea 'todas' se busca su nombre
        if($req->categoria !== 'todas'){
            $categoria = CC::getNameCategoria($req->categoria);
        }else{
            $categoria = 'Todas';
        }
        
        
        // Se redirecciona para mostrar resultados
        return RSP::redirect('Sitios/Filtrar/'.$ciudad.'/'.$categoria.'/'.$req->nombre);
    }
    
    // Filtrar sitios del usuario
    public function buscarSitios(Request $req)
    {
        
        // Obtener la ciudad por su nombre
        if($req->route()->ciudad !== 'Todas'){
            $ciudad = CC::getIdCiudad($req->route()->ciudad);
        }
        
        // Obtener la categoria por su nombre
        if(!$req->route()->categoria !== 'Todas'){
            $categoria = CC::getIdCategoria($req->route()->categoria);
        }
        
        // busqueda
        $busqueda = DB::table('negocio as n')
            ->select('n.id','n.id_unico','n.nombre','n.descripcion','n.foto_perfil','s.nombre as categoria','n.folder','co.color','co.icono')
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->join('colores as co','c.id','=','co.categoria_id');
        
        if(isset($categoria)){
            $busqueda = $busqueda->where(['c.id'=>$categoria->id]);
        }
        
        // Aplicar filtro
        if(!$req->route()->busqueda){
            $filtro = $busqueda->where(['n.ciudad_id'=>$ciudad->id])->get();
        }else{
            $filtro = $busqueda->where(['n.ciudad_id'=>$ciudad->id,'n.nombre'=>$req->route()->busqueda])->get();
        }
        
        // Obtener ciudades para la vista
        $ciudades = CC::getCiudades();
        
        // Obtener categorias para la vista
        $categorias = CC::getCategorias();
        
        // Mostrar resultado
        return view('Usuario.Sitios',['intereses'=>$filtro,'categorias'=>$categorias,'ciudades'=>$ciudades]);
    }
    
    //pagina de promociones y eventos del sitio
    public function promosEventos(){
        // OBTENER EL ID DEL ESTADO PARA LOS EVENTOS VIGENTES
        $estadoe = ET::getEstadoTablaId('eventos','Activo');
        
        // OBTENER LOS EVENTOS CON ESE ESTADO VIGENTE
        $eventos = DB::table('eventos as e')
            ->join('negocio as n','n.id','=','e.negocio_id')
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->select('n.folder','n.id_unico as id_negocio','e.*','s.nombre as nombre_categoria')
            ->where(['e.estado_id'=>$estadoe->id])->get();
        
        // OBTENER EL ID DEL ESTADO PARA LAS PROMOCIONES VIGENTES
        $estadop = ET::getEstadoTablaId('promociones','Activo');
        
        // OBTENER LAS PROMOCIONES CON ESE ESTADO VIGENTE
        $promociones = DB::table('promociones as p')
            ->join('negocio as n','n.id','=','p.negocio_id')
            ->join('subcategoria as s','s.id','=','n.subcategoria_id')
            ->join('categoria as c','c.id','=','s.categoria_id')
            ->select('n.folder','n.id_unico as id_negocio','p.*','s.nombre as nombre_categoria')
            ->where(['p.estado_id'=>$estadop->id])->get();
        
        return RSP::viewDiv('#loadInformation','Usuario.PromosEventosSitio',['eventos'=>$eventos,'promociones'=>$promociones]);
    }
    
    // Pagina del sitio del usuario 'cliente'
    public function miSitio(){
        return view('Cliente.MiSitio');
    }
    
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Negocio
        $negocio = User::negocio();
        
        // Imagenes del sitio
        $imagenes = IMG::select('id','ruta')->where('negocio_id',$negocio->id)->get();
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
            return RSP::viewDiv('.cont-main','Cliente.InformacionNegocio',['imagenes'=>$imagenes]);
        }else{
            return view('Cliente.MiSitio',['informacion'=>RSP::viewDiv('.cont-main','Cliente.InformacionNegocio',['imagenes'=>$imagenes])]);
        }
    }
    
    // Pagina de servicios del cliente
    public function servicios(){
        return view('Cliente.Servicios');
    }
    
    // Guardar imagenes de portada y logo del negocio
    public function guardarImagenes(Request $req){
        $negocio = N::find(User::negocioId());
        $logo = GI::convertToImageFile($req->logo,$negocio->folder,'.jpg',$negocio->folder.$negocio->logo,true);
        $portada = GI::convertToImageFile($req->portada,$negocio->folder,'.jpg',$negocio->folder.$negocio->foto_perfil,true);
        if($logo){
            $negocio->logo = $logo;
        }
        if($portada){
            $negocio->foto_perfil = $portada;
        }
        $negocio->save();
        return RSP::alert('success','top-right','Hecho','Potada y logo actualizados',false,true);
    }
    
    // Guardar imagenes del album de los negocios
    public function guardarAlmbum(Request $req){
        $negocio = User::negocio();
        $imagen = GI::convertToImageFile($req->imageAlbum,$negocio->folder,'.jpg');
        if($imagen){
            $newImg = new IMG;
            $newImg->negocio_id = $negocio->id;
            $newImg->ruta = $imagen;
            $newImg->save();
            
            // Imagenes del sitio
            $imagenes = IMG::select('id','ruta')->where('negocio_id',$negocio->id)->get();
            
            return [
                RSP::viewDiv('.load-files','Cliente.ImagenesNegocio',['imagenes'=>$imagenes]),
                RSP::alert('success','top-right','Hecho','Imagen subida correctamente',false,true)
            ];
        }
    }
    
    // Eliminar imagenes del album de los negocios
    public function eliminarImagenes(Request $req){
        $imagen = IMG::where(['id'=>$req->route()->imagen,'negocio_id'=>User::negocioId()])->first();
        if($imagen){
            GI::deleteFile($imagen->ruta);
            $imagen->delete();
            
            // Imagenes del sitio
            $imagenes = IMG::select('id','ruta')->where('negocio_id',User::negocioId())->get();
            
            return [
                RSP::viewDiv('.load-files','Cliente.ImagenesNegocio',['imagenes'=>$imagenes]),
                RSP::alert('success','top-right','Hecho','Imagen eliminada correctamente',false,true)
            ];
        }
    }
    
    // Actualiar el sitio - cliente
    public function actualizarNegocio(Request $req)
    {
        $negocio = N::find(User::negocioId());
        
        if($negocio){
            $negocio->nombre = $req->nombre;
            $negocio->eslogan = $req->eslogan;
            $negocio->descripcion = $req->descripcion;
            $negocio->direccion = $req->direccion;
            $negocio->latitud = $req->latitud;
            $negocio->longitud = $req->longitud;
            $negocio->telefono = $req->telefono;
            $negocio->celular = $req->celular;
            $negocio->whatsapp = $req->whatsapp;
            $negocio->nit = $req->nit;
            $negocio->rut = $req->rut;
            $negocio->save();
            
            return RSP::alert('success','top-right','Hecho','Informacion actualizada',false,true);
            
        }else{
            return RSP::ewalError();
        }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
