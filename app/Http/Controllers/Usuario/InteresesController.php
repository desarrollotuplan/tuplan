<?php

namespace App\Http\Controllers\Usuario;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\General\Categoria as C;
use App\Models\Usuario\Intereses as I;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class InteresesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Buscar categorias que ya tiene como interes para no mostrarlas
        $int = I::where('usuario_id',User::id())->pluck('categoria_id')->values();
        
        // Buscar categorias menos las de interes
        $cat = C::whereNotIn('id',$int)->get();
        
        if($cat->isEmpty()){
            return RSP::alert('info','right-top','Atencion','Has agregado todas las categorias a tu lista de intereses');
        }else{
            return RSP::viewModal('Usuario.FormularioIntereses',['categorias'=>$cat]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $cont = 0;
        
        // Recorrer las categorias que halla checkeado
        foreach($req->all() as $r)
        {
            // Se determina si existe la categoria
            $c = C::find($r);
            
            // Si existe se verifica  que no este dentro de los intereses para evitar duplicidad si no existe devolvemos error
            if($c){
                $int = I::where(['usuario_id'=>User::id(),'categoria_id'=>$r])->first();
                // Si ya esta dentro de los intereses del usuario simplemente se ignora y si no se agrega
                if(!$int){
                    $interes = new I;
                    $interes->usuario_id = User::id();
                    $interes->categoria_id = $r;
                    $interes->save();
                    
                    $cont = $cont+1;
                }
            }else{
                return RSP::swalError();
            }
        }
    
        if($cont > 0){
            // Intereses del usuario
            $intereses = I::with('categoria')->where('usuario_id',User::id())->get();

            return RSP::alertView('success','right-top','Hecho','Se han agregado las categorias a tu lista intereses','.intereses','Usuario.InteresesUsuario',['intereses'=>$intereses],true);
        }else{
            return RSP::swal('Error','Debes seleccionar almenos una categoria','info');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $interes = I::where(['usuario_id'=>User::id(),'id'=>$id])->first();
        
        if($interes){
            $interes->delete();
            
            // Intereses del usuario
            $intereses = I::with('categoria')->where('usuario_id',User::id())->get();
        
            return RSP::alertView('success','right-top','Hecho','Categoria Eliminada de tu lista de intereses','.intereses','Usuario.InteresesUsuario',['intereses'=>$intereses]);
            
        }else{
            return RSP::alert('warning','right-top','Atencion','Esta categoria no esta dentro de tus intereses');
        }
    }
}
