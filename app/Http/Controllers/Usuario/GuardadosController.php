<?php

namespace App\Http\Controllers\Usuario;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Usuario\Guardados as G;
use App\Models\General\Negocio as N;
use App\Models\General\Promociones as PR;
use App\Models\General\Eventos as EV;
use App\Helpers\Response as RSP;
use App\Helpers\User;

class GuardadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guardados = G::where('usuario_id',User::id())->get();
        return view('Usuario.Guardados',['guardados'=>$guardados]);
    }
    
    // Formulario para guardar una promocion
    public function formSavePromo(Request $req)
    {   
        // SE CONSULTA PARA SABER SI YA EXISTE O NO
        $g = G::where(['promocion_id'=>$req->promo->id,'usuario_id'=>User::id()])->first();
        
        // SI EXISTE SE LE DA UNA OPCION PARA DEJAR DE GUARDARLO Y SI NO SE LE DA EL FORMULARIO PARA GUARDARLO
        if(!$g){
            return RSP::viewDiv('.savePromo','Usuario.FormSavePromo',['promo'=>$req->promo]);
        }else{
            return RSP::viewDiv('.savePromo','Usuario.FormSavePromo',['guardado'=>$g]);
        }
    }
    
    // Guardar promo
    public function savePromo(Request $req)
    {   
        // Verificamos que no este guardada la promocion - evitar duplicidad
        $gd = G::where(['promocion_id'=>$req->promo->id,'usuario_id'=>User::id()])->first();
        if(!$gd){
            $g = new G;
            $g->usuario_id = User::id();
            $g->promocion_id = $req->promo->id;
            $g->nombre = $req->nombre;
            $g->save();
            return RSP::alertView('success','right-top','Hecho','Promocion Guardada como <b>'.$req->nombre.'<b>','.savePromo','Usuario.FormSavePromo',['guardado'=>$g]);
        }
    }
    
    // Eliminar promocion guardada
    public function deleteSavePromo($id){
        $g = G::where(['id'=>$id,'usuario_id'=>User::id()])->first();
        
        $g->delete();
        
        $promo = PR::find($g->promocion_id);
        
        return RSP::alertView('info','right-top','Hecho','Dejaste de guardar una promocion','.savePromo','Usuario.FormSavePromo',['promo'=>$promo]);
    }
    
    // Mostrar formulario para los guardar un evento
    public function formSaveEvento(Request $req){
        // SE CONSULTA PARA SABER SI YA EXISTE O NO
        $g = G::where(['evento_id'=>$req->evento->id,'usuario_id'=>User::id()])->first();
        
        // SI EXISTE SE LE DA UNA OPCION PARA DEJAR DE GUARDARLO Y SI NO SE LE DA EL FORMULARIO PARA GUARDARLO
        if(!$g){
            return RSP::viewDiv('.saveEvento','Usuario.FormSaveEvento',['evento'=>$req->evento]);
        }else{
            return RSP::viewDiv('.saveEvento','Usuario.FormSaveEvento',['guardado'=>$g]);
        }
    }
    
    public function saveEvento(Request $req){
        // Verificamos que no este guardada la promocion - evitar duplicidad
        $gd = G::where(['evento_id'=>$req->evento->id,'usuario_id'=>User::id()])->first();
        if(!$gd){
            $g = new G;
            $g->usuario_id = User::id();
            $g->evento_id = $req->evento->id;
            $g->nombre = $req->nombre;
            $g->save();
            return RSP::alertView('success','right-top','Hecho','Evento Guardado como <b>'.$req->nombre.'<b>','.saveEvento','Usuario.FormSaveEvento',['guardado'=>$g]);
        }
    }
    
    // Eliminar promocion guardada
    public function deleteSaveEvento($id){
        $g = G::where(['id'=>$id,'usuario_id'=>User::id()])->first();
        
        $g->delete();
        
        $evento = EV::find($g->evento_id);
        
        return RSP::alertView('info','right-top','Hecho','Dejaste de guardar un evento','.saveEvento','Usuario.FormSaveEvento',['evento'=>$evento]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        // Si se encuentra el negocio sigue el proceso si no retornamos alerta
        if($req->route()->negocio){
            
            // Seleccionar el id y el nombre del negocio
            $n = N::select('id','nombre','id_unico')->where('id_unico',$req->route()->negocio)->first();
            
            // Se verifica que el sitio este guardado por el usuario
            $g = G::select('nombre')->where(['negocio_id'=>$n->id,'usuario_id'=>User::id()])->first();
            
            if(!$g){
                return RSP::viewModal('Usuario.GuardarSitio',['negocio'=>$n,'id'=>$req->data]);
            }else{
                return RSP::alert('warning','right-top','Atencion','Ya se encuentra guardado como <b>'.$g->nombre.'</b>');
            }
        }else{
            return RSP::swalError();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $n = N::select('id')->where('id_unico',$req->route()->negocio)->first();
        
        // Si el negocio existe lo guardamos
        if($n){
            
            // Se verifica que el sitio no este guardado por el usuario
            $g = G::select('nombre')->where(['negocio_id'=>$n->id,'usuario_id'=>User::id()])->first();
            
            if(!$g){
                $g = new G;
                $g->usuario_id = User::id();
                $g->negocio_id = $n->id;
                $g->nombre = $req->nombre;
                $g->save();

                return RSP::alertView('success','right-top','Hecho','Sitio Guardado como <b>'.$req->nombre.'</b>','.save','Usuario.GuardadoSitio',['guardado'=>$g,'negocio'=>$req->route()->negocio],true);
            }else{
                return RSP::swal('Atencion','Este sitio ya se encuentra guardado.','info',true);
            }
            
        }else{
            return RSP::swalError();
        }
    }
    
    // Filtrar y redireccionar
    public function filtrar(Request $req){
        if($req->tipo == 'sitios'){
            $tipo = 'Sitios';
        }elseif($req->tipo == 'promociones'){
            $tipo = 'Promociones';
        }elseif($req->tipo == 'eventos'){
            $tipo = 'Eventos';
        }else{
            $tipo = 'Todos';
        }
         // Se redirecciona para mostrar resultados
        return RSP::redirect('Guardados/Filtrar/'.$tipo.'/'.$req->nombre);
    }
    
    // Buscar 
    public function buscar(Request $req){
        if($req->route()->tipo == 'Sitios'){
            $tipo = 'negocio_id';
        }elseif($req->route()->tipo == 'Promociones'){
            $tipo = 'promocion_id';
        }elseif($req->route()->tipo == 'Eventos'){
            $tipo = 'evento_id';
        }else{
            $tipo = 'usuario_id';
        }
        
        $busqueda = G::where('usuario_id',User::id())->whereNotNull($tipo);
        
        if($req->route()->busqueda){
            $filtro = $busqueda->where('nombre',$req->route()->busqueda)->get();
        }else{
            $filtro = $busqueda->get();
        }
        
        return view('Usuario.Guardados',['guardados'=>$filtro]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $req)
    {
        $guardado = G::where(['usuario_id'=>User::id(),'id'=>$req->route()->guardado])->first();
        return RSP::viewModal('Usuario.EditarGuardado',['guardado'=>$guardado]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        $guardado = G::where(['usuario_id'=>User::id(),'id'=>$req->route()->guardado])->first();
        $guardado->nombre = $req->nombre;
        $guardado->save();
        return RSP::alert('success','right-top','Hecho','Se actualizo un registro guardado',true);
    }
    
    // Eliminar guardados en general
    public function eliminarGuardado(Request $req)
    {
        // Verificamos que el sitio este guardado por el usuario para proceder a eliminar
        $g = G::where(['id'=>$req->route()->guardado,'usuario_id'=>User::id()])->first();
        
        // Si existe se procede
        if($g){
            $g->delete();
            return [
                RSP::deleteCont('#'.$req->route()->div),
                RSP::alert('info','right-top','Hecho','Se elimino un registro dentro de tus guardados')
                ];
        }else{
            return RSP::swalError();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        // Verificamos que el sitio este guardado por el usuario para proceder a eliminar
        $g = G::where(['id'=>$req->route()->guardado,'usuario_id'=>User::id()])->first();
        
        // Si existe se procede
        if($g->negocio_id){
            
            $g->delete();
            return RSP::viewDiv('.save','Usuario.GuardadoSitio',['negocio'=>$req->route()->negocio]);
            
        }else{
            return RSP::swalError();
        }
    }
    
    
}
