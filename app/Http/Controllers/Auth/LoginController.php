<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\General\DatosLoginController as DL;
use App\Http\Controllers\General\UsuarioController as UC;
use Illuminate\Http\Request;
use App\Models\General\Usuario as U;
use App\Models\General\EstadoTablas as ET;
use App\Models\General\UsuarioRol as UR;
use App\Helpers\Response as RSP;
use App\Helpers\Facebook as FB;
use App\Helpers\GmailAuth as GG;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    
    protected $username = 'email';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('login','cerrarSesion');
    }
    
    // Login
    public function login(Request $req){
        // Email
        $u = U::getEmail($req->user);
        if($u){
            // Estado
            $e = ET::getEstado('usuarios',$u->estado_id);
            if($e->estado == 'Activo'){
                // Rol
                $r = UR::with('rol')->where('usuario_id',$u->id)->first();
                if(!is_null($r)){
                    // Autenticacion
                    if(Auth::attempt(['email' => $req->user,'password' => $req->password],true)){
                        // Datos de login
                        DL::datosLogin();
                        
                        return RSP::redirect();
                    }else{
                        return RSP::swal('Error','La contraseña digitada es incorrecta','info',false,['password']);
                    }
                }else{
                    return RSP::redirect();
                }
            }else{
                return RSP::swal('Error','El usuario se encuentra bloqueado si cree que se trata de un error contacte a soporte','info',false,['user']);
            }
        }else{
            return RSP::swal('Error','No se encontro ningun usuario con el correo electronico digitado','error',false,['user']);
        }
    }
    
    /**
    * Cerrar sesion
    */
    public function cerrarSesion(){
        session()->flush();
        Auth::logout();
        return redirect()->route('/');
    }
    
    // Inicio de sesion con facebook
    public function facebook(Request $req){
        $fb = new FB;
        $fb->facebookIni();
        $user = $fb->getUser();
        
        if($user){
            
            // Se verifica que sea un usuario de facebook
            $u = U::where(['email'=>$user['email'],'facebook_id'=>$user['id']])->first();
            if($u){
                // obtener el nombre del estado
                $e = ET::getEstado('usuarios',$u->estado_id);
                if($e->estado == 'Activo'){
                    // Rol
                    $r = UR::with('rol')->where('usuario_id',$u->id)->first();
                    if(!is_null($r)){
                        // Autenticacion
                        Auth::login($u);
                        // Datos de login
                        DL::datosLogin();

                        return RSP::redirectWith('/');
                    }else{
                        return RSP::redirect();
                    }
                }else{
                    return RSP::swal('Error','El usuario se encuentra bloqueado si cree que se trata de un error contacte a soporte','info',false,['user']);
                }
            }else{
                $email = U::getEmail($user['email']);
                if(!$email){
                    // Registrar usuario de facebook si no existe
                    $user = UC::RegistrarUsuarioFacebook($user);
                }else{
                    // Registrar id del facebook si existe
                    $user = UC::registrarIdFacebookUser($user);
                }

                if($user){
                    // Autenticacion
                    Auth::login($user);
                    // Datos de login
                    DL::datosLogin();

                    return RSP::redirectWith('/');
                }else{
                    return RSP::redirectWith('/',RSP::swalError());
                }
            }
            
        }else{
            return RSP::redirectWith('/',RSP::swalError());
        }
    }
    
    public function google(){
        $gg = new GG;
        $gg->googleIni();
        // Se verifica que alla iniciado sesion con google en nuestra aplicacion
        if($gg->checkRedirectCode()){
            
            // Se obtiene el usuario de google
            $userGg = $gg->getUser();
            
            // Si existen los datos se procede
            if($userGg){
                // Se verifica si existe un usuario con el id de google y el email
                $userBd = U::where(['email'=>$userGg['email'],'google_id'=>$userGg['sub']])->first();
                
                // Si existe se procede a verificar su estado
                if($userBd){
                    // obtener el nombre del estado
                    $e = ET::getEstado('usuarios',$userBd->estado_id);
                    if($e->estado == 'Activo'){
                         // Rol
                        $r = UR::with('rol')->where('usuario_id',$userBd->id)->first();
                        if(!is_null($r)){
                            // Autenticacion
                            Auth::login($userBd);
                            // Datos de login
                            DL::datosLogin();

                            return RSP::redirectWith('/');
                        }else{
                            return RSP::redirect();
                        }
                    }else{
                        return RSP::swal('Error','El usuario se encuentra bloqueado si cree que se trata de un error contacte a soporte','info',false,['user']);
                    }
                    
                }else{
                    // Como no existe el usuario con el id de google se procede a buscar solo el correo
                    $u = U::getEmail($userGg['email']);
                    // Si el correo no existe se procede a registrar si existe solo se le actualiza el valor de google id en la db
                    if(!$u){
                        $user = UC::registrarUsuarioGoogle($userGg);
                    }else{
                        $user = UC::registrarIdGoogleUser($userGg);
                    }
                    
                    if($user){
                        // Autenticacion
                        Auth::login($user);
                        // Datos de login
                        DL::datosLogin();

                        return RSP::redirectWith('/');
                    }else{
                        return RSP::redirectWith('/',RSP::swalError());
                    }
                }
                
            }else{
                return RSP::redirectWith('/',RSP::swalError());
            }
            
        }else{
            return RSP::redirectWith('/',RSP::swalError());
        }
    }
    
}
