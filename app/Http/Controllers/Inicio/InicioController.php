<?php

namespace App\Http\Controllers\Inicio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Response as RSP;
use App\Helpers\Facebook as FB;
use App\Helpers\GmailAuth as GG;
use App\Helpers\User;

class InicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(User::check() && User::rolName()){
            switch(User::rolName()){
                case 'usuario':
                    return view('Usuario.Inicio');
                break;
                case 'cliente':
                    return view('Cliente.Inicio');
                break;
                case 'empleado':
                    return view('Empleado.Inicio');
                break;
            }
        }else{
            return view('Inicio.Inicio');
        }
    }
    
    /**
     * Mostrar formulario de login 
    */
    public function formLogin(){
        $fb = new FB;
        $fb->facebookIni();
        $facebookUrl = $fb->getAuthUrl();

        $google = new GG;
        $google->googleIni();
        $googleUrl = $google->getAuthUrl();
        
        return RSP::viewModal('Inicio.FormularioLogin',['FBurl'=>$facebookUrl,'GGurl'=>$googleUrl]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
