<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\General\Domicilio as DM;
use App\Helpers\Response as RSP;
use App\Helpers\User;


class DomicilioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $domicilio = DM::where(['negocio_id'=>User::negocioId()])->first();
        
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
            return RSP::viewDiv('.cont-main','Cliente.Domicilios',['domicilio'=>$domicilio]);
        }else{
            return view('Cliente.MiSitio',['informacion'=>RSP::viewDiv('.cont-main','Cliente.Domicilios',['domicilio'=>$domicilio])]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        $DM = DM::where(['negocio_id'=>User::negocioId()])->first();
        $DM->servicio = $req->servicio;
        $DM->valor_minimo = $req->valor_minimo;
        $DM->valor_domicilio = $req->valor_domicilio;
        $DM->hora_inicio = $req->hora_inicio;
        $DM->hora_final = $req->hora_final;
        $DM->save();
        
        return RSP::alert('success','right-top','Hecho','Informacion Actualizada',false,true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
