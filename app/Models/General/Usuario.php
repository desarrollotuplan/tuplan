<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property int $id
 * @property int $ciudad_id
 * @property boolean $genero_id
 * @property int $estado_id
 * @property string $nombres
 * @property string $apellidos
 * @property string $fecha_nacimiento
 * @property string $email
 * @property int $movil
 * @property string $password
 * @property string $palabra_clave
 * @property int $visitas
 * @property string $created_at
 * @property string $updated_at
 * @property Ciudad $ciudad
 * @property Estado $estado
 * @property Genero $genero
 * @property Calificacion[] $calificacions
 * @property Comentario[] $comentarios
 * @property Favorito[] $favoritos
 * @property Interese[] $intereses
 * @property Mensaje[] $mensajes
 * @property Pedido[] $pedidos
 * @property Recomendado[] $recomendados
 * @property Reserva[] $reservas
 * @property UsuarioNegocio[] $usuarioNegocios
 * @property UsuarioRol[] $usuarioRols
 * @property Visita[] $visitas
 */
class Usuario extends Authenticatable
{
    use Notifiable;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $table = 'usuario';
    
    protected $hidden = ['password'];

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';
    
    /**
     * @var array
     */
    protected $fillable = ['ciudad_id', 'genero_id', 'estado_id', 'primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'fecha_nacimiento', 'email', 'telefono_movil', 'direccion', 'password', 'palabra_clave', 'visitas', 'foto', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ciudad()
    {
        return $this->belongsTo('App\Models\General\Ciudad');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estado()
    {
        return $this->belongsTo('App\Models\General\Estado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function genero()
    {
        return $this->belongsTo('App\Models\General\Genero');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function calificaciones()
    {
        return $this->hasMany('App\Models\General\Calificacion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comentarios()
    {
        return $this->hasMany('AppModels\General\Comentario');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favoritos()
    {
        return $this->hasMany('AppModels/General/\Favorito');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function intereses()
    {
        return $this->hasMany('App\Models\General\Interese');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mensajes()
    {
        return $this->hasMany('App\Models\General\Mensaje');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pedidos()
    {
        return $this->hasMany('App\Models\General\Pedido');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recomendados()
    {
        return $this->hasMany('App\Models\General\Recomendado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservas()
    {
        return $this->hasMany('App\Models\General\Reserva');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usuarioNegocios()
    {
        return $this->hasMany('App\Models\General\UsuarioNegocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usuarioRols()
    {
        return $this->hasMany('App\Models\General\UsuarioRol');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visitas()
    {
        return $this->hasMany('App\Models\General\Visita');
    }
    
    // Consulta de email 
    public static function getEmail($email){
        return static::where('email',$email)->first();
    }
}
