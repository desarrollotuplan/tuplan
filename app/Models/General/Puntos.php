<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $puntos
 * @property string $nombre
 * @property string $created_at
 * @property string $updated_at
 * @property Calificacion[] $calificacions
 */
class Puntos extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['puntos', 'nombre', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function calificacions()
    {
        return $this->hasMany('App\Models\General\Calificacion');
    }
}
