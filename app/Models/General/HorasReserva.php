<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $reserva_disponible_id
 * @property int $dia_id
 * @property string $hora
 * @property string $created_at
 * @property string $updated_at
 * @property Dia $dia
 * @property ReservasDisponible $reservasDisponible
 * @property Reserva[] $reservas
 */
class HorasReserva extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'horas_reserva';

    /**
     * @var array
     */
    protected $fillable = ['reserva_disponible_id', 'dia_id', 'hora', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dia()
    {
        return $this->belongsTo('App\Models\General\Dias', 'dia_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reservasDisponible()
    {
        return $this->belongsTo('App\Models\General\ReservasDisponibles', 'reserva_disponible_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservas()
    {
        return $this->hasMany('App\Models\General\Reserva', 'hora_reserva_id');
    }
}
