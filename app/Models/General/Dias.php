<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $codigo
 * @property string $nombre
 * @property string $created_at
 * @property string $updated_at
 * @property Horario[] $horarios
 * @property HorasReserva[] $horasReservas
 */
class Dias extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['codigo', 'nombre', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function horario()
    {
        return $this->hasMany('App\Models\General\Horario', 'dia_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function horasReservas()
    {
        return $this->hasMany('App\Models\General\HorasReserva', 'dia_id');
    }
}
