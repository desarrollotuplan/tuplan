<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $categoria_id
 * @property int $estado_id
 * @property string $nombre
 * @property int $precio
 * @property string $especificaciones
 * @property string $informacion_adicional
 * @property string $foto
 * @property string $created_at
 * @property string $updated_at
 * @property CategoriaProducto $categoriaProducto
 * @property Estado $estado
 * @property DetallePedido[] $detallePedidos
 * @property Imagene[] $imagenes
 */
class Producto extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['categoria_id', 'estado_id', 'nombre', 'precio', 'especificaciones', 'informacion_adicional', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categoriaProducto()
    {
        return $this->belongsTo('App\Models\General\CategoriaProducto', 'categoria_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estado()
    {
        return $this->belongsTo('App\Models\General\Estado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detallePedidos()
    {
        return $this->hasMany('App\Models\General\DetallePedido');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function imagenes()
    {
        return $this->hasMany('App\Models\General\Imagenes');
    }
}
