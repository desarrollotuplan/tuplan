<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class GenerateImage extends Model
{
    //Función para convertir imagen de base64 a archivo de imagen
    public static function convertToImageFile($imageInBase64,$path,$format,$existingImage=null,$name=null){
        
        if(isset($imageInBase64)){
            
            $base64ToPhp = explode(',',$imageInBase64);
            
            $imageData = base64_decode($base64ToPhp[1]);

            $nameFile = static::nameGenerator().$format;
            
            $arrayPath = explode('/',$path);
            
            if($arrayPath[0] === 'storage'){
                $newPath = substr($path,8,strlen($path));
            }else{
                $newPath = $path;
            }
            
            $filePath = '/'.$newPath.$nameFile;

            file_put_contents(public_path('storage').$filePath,$imageData);

            if($existingImage){
                static::deleteFile($existingImage);
            }
            
            if(!$name){
                return 'storage'.$filePath;
            }else{
                return $nameFile;
            }
        }
        
    }
    
    //Función para generar nombres unicos para imagenes
    static function nameGenerator(){
        $date = new \DateTime();
        $name = $date->format('YmdHis').rand(000,999);
        return $name;
    }
    
    //Función para eliminar archivos si existen
    public static function deleteFile($filePath){
        if(!static::verifyExeption($filePath)){
            if(File::exists(public_path($filePath))){
                File::delete(public_path($filePath));
            }
        }
    }
    public static function verifyExeption($file){
        $exeption = [
            'storage/imagenes/usuarios/20190422104331507.jpg',
            'storage/imagenes/negocios/123456789/arquitectura.jpg',
            'storage/imagenes/negocios/123456789/adventure.jpg',
            'storage/imagenes/negocios/123456789/cafe.jpg',
            'storage/imagenes/negocios/123456789/camping.jpg',
            'storage/imagenes/negocios/123456789/food.jpg',
        ];
        if(!in_array($file,$exeption) === false){
            return true;
        }else{
            return false;
        }
    }
}