<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $tipo_publico_id
 * @property string $nombre
 * @property string $created_at
 * @property string $updated_at
 * @property TipoPublico $tipoPublico
 * @property Colore[] $colores
 * @property Evento[] $eventos
 * @property Interese[] $intereses
 * @property Promocione[] $promociones
 * @property Subcategorium[] $subcategorias
 */
class Categoria extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'categoria';

    /**
     * @var array
     */
    protected $fillable = ['tipo_publico_id', 'nombre', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoPublico()
    {
        return $this->belongsTo('App\Models\General\TipoPublico');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function colores()
    {
        return $this->hasMany('App\Models\General\Colore', 'categoria_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventos()
    {
        return $this->hasMany('App\Models\General\Evento', 'categoria_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function intereses()
    {
        return $this->hasMany('App\Models\General\Interese', 'categoria_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promociones()
    {
        return $this->hasMany('App\Models\General\Promocione', 'categoria_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subcategorias()
    {
        return $this->hasMany('App\Models\General\Subcategorium', 'categoria_id');
    }
}
