<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $negocio_id
 * @property string $nombre
 * @property string $created_at
 * @property string $updated_at
 * @property Negocio $negocio
 * @property Producto[] $productos
 */
class CategoriaProducto extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'categoria_producto';

    /**
     * @var array
     */
    protected $fillable = ['negocio_id', 'nombre', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productos()
    {
        return $this->hasMany('App\Models\General\Producto', 'categoria_id');
    }
}
