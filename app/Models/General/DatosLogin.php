<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use Request as R;
use App\Models\General\Navegador as N;
use App\Models\General\Dispositivo as D;
use App\Models\General\SistemaOperativo as SO;

/**
 * @property integer $id
 * @property int $usuario_id
 * @property boolean $navegador_id
 * @property boolean $dispositivo_id
 * @property boolean $sistema_operativo_id
 * @property string $ip
 * @property string $modelo
 * @property string $created_at
 * @property string $updated_at
 * @property Dispositivo $dispositivo
 * @property Navegador $navegador
 * @property SistemaOperativo $sistemaOperativo
 * @property Usuario $usuario
 */
class DatosLogin extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'datos_login';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['usuario_id', 'navegador_id', 'dispositivo_id', 'sistema_operativo', 'ip', 'modelo', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dispositivo()
    {
        return $this->belongsTo('App\Models\General\Dispositivo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function navegador()
    {
        return $this->belongsTo('App\Models\General\Navegador');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\General\Usuario');
    }
    
    // Metodo para consultar datos del login
    static function getDataLogin(){
        
        //Se detecta la ip
        $ip = R::ip();
        
        //Se detecta el navegador
        if(\Browser::browserFamily() == 'Unknown'){
            $b = N::where('nombre','Otro')->select('id')->first();
        }else{
            $b = N::where('nombre',\Browser::browserFamily())->select('id')->first();
        }
        
        //Se detecta el dispositivo
        if(\Browser::isDesktop()){
            $d = D::where('nombre','Desktop')->select('id')->first();
        }elseif(\Browser::isTablet()){
            $d = D::where('nombre','Tablet')->select('id')->first();
        }elseif(\Browser::isMobile()){
            $d = D::where('nombre','Smartphone')->select('id')->first();
        }else{
            $d = D::where('nombre','Otro')->select('id')->first();
        }
        
        //Se detecta el sistema operativo
        $so = \Browser::platformFamily();
        
        return ['ip'=>$ip,'browser'=>$b->id,'device'=>$d->id,'so'=>$so];
    }
}
