<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $negocio_id
 * @property int $estado_id
 * @property string $nombre
 * @property string $descripcion
 * @property string $informacion_adicional
 * @property string $fecha_inicio
 * @property string $hora_inicio
 * @property string $fecha_fin
 * @property string $hora_fin
 * @property string $imagen
 * @property string $created_at
 * @property string $updated_at
 * @property Estado $estado
 * @property Negocio $negocio
 * @property Imagene[] $imagenes
 * @property Visita[] $visitas
 */
class Promociones extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['negocio_id', 'estado_id', 'nombre', 'descripcion', 'informacion_adicional', 'fecha_inicio', 'hora_inicio', 'fecha_fin', 'hora_fin', 'imagen', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estado()
    {
        return $this->belongsTo('App\Models\General\Estado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function imagenes()
    {
        return $this->hasMany('App\Models\General\Imagene', 'promocion_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visitas()
    {
        return $this->hasMany('App\Models\General\Visita', 'promo_id');
    }
}
