<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $negocio_id
 * @property string $servicio
 * @property int $valor_minimo
 * @property int $valor_domicilio
 * @property string $hora_inicio
 * @property string $hora_final
 * @property string $created_at
 * @property string $updated_at
 * @property Negocio $negocio
 */
class Domicilio extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'domicilio';

    /**
     * @var array
     */
    protected $fillable = ['negocio_id', 'servicio', 'valor_minimo', 'valor_domicilio', 'hora_inicio', 'hora_final', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }
}
