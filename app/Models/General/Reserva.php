<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $usuario_id
 * @property int $hora_reserva_id
 * @property int $estado_id
 * @property int $tipo_reserva_id
 * @property string $fecha
 * @property int $numero_personas
 * @property string $nombre_usuario
 * @property string $informacion_adicional
 * @property string $created_at
 * @property string $updated_at
 * @property Estado $estado
 * @property HorasReserva $horasReserva
 * @property TipoReserva $tipoReserva
 * @property Usuario $usuario
 */
class Reserva extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'reserva';

    /**
     * @var array
     */
    protected $fillable = ['usuario_id', 'hora_reserva_id', 'estado_id', 'tipo_reserva_id', 'fecha', 'numero_personas', 'nombre_usuario', 'informacion_adicional', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estado()
    {
        return $this->belongsTo('App\Models\General\Estado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function horasReserva()
    {
        return $this->belongsTo('App\Models\General\HorasReserva', 'hora_reserva_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoReserva()
    {
        return $this->belongsTo('App\Models\General\TipoReserva');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\General\Usuario');
    }
}
