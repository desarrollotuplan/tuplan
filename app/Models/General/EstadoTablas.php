<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id
 * @property int $estado_id
 * @property int $tabla_id
 * @property string $created_at
 * @property string $updated_at
 * @property Estado $estado
 * @property Tabla $tabla
 * @property Usuario[] $usuarios
 */
class EstadoTablas extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'estado-tablas';

    /**
     * @var array
     */
    protected $fillable = ['estado_id', 'tabla_id', 'created_at', 'updated_at'];
    
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estado()
    {
        return $this->belongsTo('App\Models\General\Estado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tabla()
    {
        return $this->belongsTo('App\Models\General\Tabla');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usuarios()
    {
        return $this->hasMany('App\Models\General\Usuario', 'estado_id');
    }
    
    // OBTENER NOMBRE DEL ESTADO - GENERAL
    static function getEstado($tabla,$idEstado){
        return DB::table('estado-tablas as et')
                   ->select('e.nombre as estado')
                   ->join('estado as e','et.estado_id','=','e.id')
                   ->join('tablas as t','et.tabla_id','=','t.id')
                   ->where([
                       ['et.id',$idEstado],
                       ['t.nombre',$tabla]
                   ])->first();
    }
    
    // OBTENER EL ID DE ESTADO-TABLAS DONDE SE CUMPLA LA CONDICION
    static function getEstadoTablaId($tabla,$nombreEstado){
        return DB::table('estado-tablas as et')
                   ->select('et.id')
                   ->join('estado as e','et.estado_id','=','e.id')
                   ->join('tablas as t','et.tabla_id','=','t.id')
                   ->where([
                       ['e.nombre',$nombreEstado],
                       ['t.nombre',$tabla]
                   ])->first();
    }
    
}
