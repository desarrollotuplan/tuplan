<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $negocio_id
 * @property int $estado_id
 * @property string $id_unico
 * @property string $nombre
 * @property string $descripcion
 * @property string $informacion_adicional
 * @property string $fecha_inicio
 * @property string $hora_inicio
 * @property string $fecha_fin
 * @property string $hora_fin
 * @property string $imagen
 * @property string $created_at
 * @property string $updated_at
 * @property EstadoTabla $estadoTabla
 * @property Negocio $negocio
 * @property Comentario[] $comentarios
 * @property Guardado[] $guardados
 * @property Imagene[] $imagenes
 * @property Visita[] $visitas
 */
class Eventos extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['negocio_id', 'estado_id', 'id_unico', 'nombre', 'descripcion', 'informacion_adicional', 'fecha_inicio', 'hora_inicio', 'fecha_fin', 'hora_fin', 'imagen', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estadoTabla()
    {
        return $this->belongsTo('App\Models\General\EstadoTabla', 'estado_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comentarios()
    {
        return $this->hasMany('App\Models\General\Comentario');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function guardados()
    {
        return $this->hasMany('App\Models\General\Guardado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function imagenes()
    {
        return $this->hasMany('App\Models\General\Imagene');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visitas()
    {
        return $this->hasMany('App\Models\General\Visita');
    }
}
