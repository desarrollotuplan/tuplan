<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property boolean $id
 * @property string $nombre
 * @property string $seudo
 * @property string $created_at
 * @property string $updated_at
 * @property Usuario[] $usuarios
 */
class Genero extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'genero';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'seudo', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usuarios()
    {
        return $this->hasMany('App\Models\General\Usuario');
    }
}
