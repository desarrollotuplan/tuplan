<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $usuario_id
 * @property int $negocio_id
 * @property int $promo_id
 * @property int $evento_id
 * @property string $created_at
 * @property string $updated_at
 * @property Evento $evento
 * @property Negocio $negocio
 * @property Promocione $promocione
 * @property Usuario $usuario
 */
class Visitas extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['usuario_id', 'negocio_id', 'promo_id', 'evento_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function evento()
    {
        return $this->belongsTo('App\Models\General\Evento');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function promocione()
    {
        return $this->belongsTo('App\Models\General\Promocione', 'promo_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\General\Usuario');
    }
}
