<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $departamento_id
 * @property int $codigo
 * @property string $nombre
 * @property string $created_at
 * @property string $updated_at
 * @property Departamento $departamento
 * @property Evento[] $eventos
 * @property Negocio[] $negocios
 * @property Usuario[] $usuarios
 */
class Ciudad extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'ciudad';

    /**
     * @var array
     */
    protected $fillable = ['departamento_id', 'codigo', 'nombre', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function departamento()
    {
        return $this->belongsTo('App\Models\General\Departamento');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventos()
    {
        return $this->hasMany('App\Models\General\Evento');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function negocios()
    {
        return $this->hasMany('App\Models\General\Negocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usuarios()
    {
        return $this->hasMany('App\Models\General\Usuario');
    }
}
