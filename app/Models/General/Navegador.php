<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property boolean $id
 * @property string $nombre
 * @property string $created_at
 * @property string $updated_at
 * @property DatosLogin[] $datosLogins
 */
class Navegador extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'navegador';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function datosLogins()
    {
        return $this->hasMany('App\Models\General\DatosLogin');
    }
}
