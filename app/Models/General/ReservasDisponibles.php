<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $negocio_id
 * @property string $nombre
 * @property string $informacion
 * @property string $imagen
 * @property string $created_at
 * @property string $updated_at
 * @property Negocio $negocio
 * @property HorasReserva[] $horasReservas
 */
class ReservasDisponibles extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['negocio_id', 'nombre', 'informacion', 'numero_minimo_personas', 'numero_maximo_personas', 'imagen', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function horasReservas()
    {
        return $this->hasMany('App\Models\General\HorasReserva', 'reserva_disponible_id');
    }
}
