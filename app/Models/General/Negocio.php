<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $subcategoria_id
 * @property int $ciudad_id
 * @property int $estado_id
 * @property string $id_unico
 * @property string $foto_perfil
 * @property string $logo
 * @property string $nombre
 * @property string $eslogan
 * @property string $descripcion
 * @property string $direccion
 * @property int $telefono
 * @property string $celular
 * @property string $whatsapp
 * @property string $servicios
 * @property string $latitud
 * @property string $longitud
 * @property string $nit
 * @property string $rut
 * @property string $folder
 * @property string $created_at
 * @property string $updated_at
 * @property Ciudad $ciudad
 * @property Estado $estado
 * @property Subcategorium $subcategorium
 * @property Calificacion[] $calificacions
 * @property CategoriaProducto[] $categoriaProductos
 * @property Comentario[] $comentarios
 * @property Domicilio[] $domicilios
 * @property Evento[] $eventos
 * @property Favorito[] $favoritos
 * @property Horario[] $horarios
 * @property Mensaje[] $mensajes
 * @property MensajesAutomatico[] $mensajesAutomaticos
 * @property OpcionesPago[] $opcionesPagos
 * @property Pago[] $pagos
 * @property Pedido[] $pedidos
 * @property Promocione[] $promociones
 * @property Recomendado[] $recomendados
 * @property RedesSociale[] $redesSociales
 * @property Reserva[] $reservas
 * @property ReservasDisponible[] $reservasDisponibles
 * @property UsuarioNegocio[] $usuarioNegocios
 * @property Visita[] $visitas
 */
class Negocio extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'negocio';

    /**
     * @var array
     */
    protected $fillable = ['subcategoria_id', 'ciudad_id', 'estado_id', 'id_unico', 'foto_perfil', 'logo', 'nombre', 'eslogan', 'descripcion', 'direccion', 'telefono', 'celular', 'whatsapp', 'servicios', 'latitud', 'longitud', 'nit', 'rut', 'folder', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ciudad()
    {
        return $this->belongsTo('App\Models\General\Ciudad');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estado()
    {
        return $this->belongsTo('App\Models\General\Estado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subcategorium()
    {
        return $this->belongsTo('App\Models\General\Subcategorium', 'subcategoria_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function calificacions()
    {
        return $this->hasMany('App\Models\General\Calificacion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoriaProductos()
    {
        return $this->hasMany('App\Models\General\CategoriaProducto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comentarios()
    {
        return $this->hasMany('App\Models\General\Comentario');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function domicilios()
    {
        return $this->hasMany('App\Models\General\Domicilio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventos()
    {
        return $this->hasMany('App\Models\General\Evento');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favoritos()
    {
        return $this->hasMany('App\Models\General\Favorito');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function horarios()
    {
        return $this->hasMany('App\Models\General\Horario');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mensajes()
    {
        return $this->hasMany('App\Models\General\Mensaje');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mensajesAutomaticos()
    {
        return $this->hasMany('App\Models\General\MensajesAutomatico');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function opcionesPagos()
    {
        return $this->hasMany('App\Models\General\OpcionesPago');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pagos()
    {
        return $this->hasMany('App\Models\General\Pago');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pedidos()
    {
        return $this->hasMany('App\Models\General\Pedido');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promociones()
    {
        return $this->hasMany('App\Models\General\Promocione');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recomendados()
    {
        return $this->hasMany('App\Models\General\Recomendado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function redesSociales()
    {
        return $this->hasMany('App\Models\General\RedesSociale');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservas()
    {
        return $this->hasMany('App\Models\General\Reserva');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservasDisponibles()
    {
        return $this->hasMany('App\Models\General\ReservasDisponible');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usuarioNegocios()
    {
        return $this->hasMany('App\Models\General\UsuarioNegocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visitas()
    {
        return $this->hasMany('App\Models\General\Visita');
    }
    
    // Obtener el id del negocio por el id unico
    public static function getId($id_unico)
    {
        return self::select('id')->where('id_unico',$id_unico)->first();
    }
    
}
