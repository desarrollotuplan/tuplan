<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $negocio_id
 * @property int $usuario_id
 * @property string $created_at
 * @property string $updated_at
 * @property Negocio $negocio
 * @property Usuario $usuario
 */
class UsuarioNegocio extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'usuario-negocio';

    /**
     * @var array
     */
    protected $fillable = ['negocio_id', 'usuario_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\General\Usuario');
    }
}
