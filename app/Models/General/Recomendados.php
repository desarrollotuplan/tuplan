<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $usuario_id
 * @property int $negocio_id
 * @property string $created_at
 * @property string $updated_at
 * @property Negocio $negocio
 * @property Usuario $usuario
 */
class Recomendados extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'recomendados';

    /**
     * @var array
     */
    protected $fillable = ['usuario_id', 'negocio_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\General\Usuario');
    }
}
