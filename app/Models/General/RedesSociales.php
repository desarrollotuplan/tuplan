<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $negocio_id
 * @property string $facebook
 * @property string $youtube
 * @property string $instagram
 * @property string $twitter
 * @property string $website
 * @property string $app_android
 * @property string $app_ios
 * @property string $created_at
 * @property string $updated_at
 * @property Negocio $negocio
 */
class RedesSociales extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['negocio_id', 'facebook', 'youtube', 'instagram', 'twitter', 'website', 'app_android', 'app_ios', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }
}
