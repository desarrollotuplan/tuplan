<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $negocio_id
 * @property int $promocion_id
 * @property int $evento_id
 * @property int $producto_id
 * @property string $ruta
 * @property string $created_at
 * @property string $updated_at
 * @property Evento $evento
 * @property Negocio $negocio
 * @property Producto $producto
 * @property Promocione $promocione
 */
class Imagenes extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['negocio_id', 'promocion_id', 'evento_id', 'producto_id', 'ruta', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function evento()
    {
        return $this->belongsTo('App\Models\General\Evento');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producto()
    {
        return $this->belongsTo('App\Models\General\Producto', 'producto_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function promocione()
    {
        return $this->belongsTo('App\Models\General\Promocione', 'promocion_id');
    }
}
