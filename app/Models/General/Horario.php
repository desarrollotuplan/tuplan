<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $negocio_id
 * @property int $dia_id
 * @property int $horario_tipo_id
 * @property string $hora_inicio_1
 * @property string $hora_fin_1
 * @property string $hora_inicio_2
 * @property string $hora_fin_2
 * @property string $created_at
 * @property string $updated_at
 * @property Dia $dia
 * @property HorarioTipo $horarioTipo
 * @property Negocio $negocio
 */
class Horario extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'horario';

    /**
     * @var array
     */
    protected $fillable = ['negocio_id', 'dia_id', 'horario_tipo_id', 'hora_inicio_1', 'hora_fin_1', 'hora_inicio_2', 'hora_fin_2', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dia()
    {
        return $this->belongsTo('App\Models\General\Dias');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function horarioTipo()
    {
        return $this->belongsTo('App\Models\General\HorarioTipo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }
}
