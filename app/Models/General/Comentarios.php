<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $usuario_id
 * @property int $negocio_id
 * @property int $evento_id
 * @property int $estado_id
 * @property string $fecha
 * @property string $comentario
 * @property string $created_at
 * @property string $updated_at
 * @property Estado $estado
 * @property Evento $evento
 * @property Negocio $negocio
 * @property Usuario $usuario
 */
class Comentarios extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['usuario_id', 'negocio_id', 'evento_id', 'estado_id', 'comentario', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estado()
    {
        return $this->belongsTo('App\Models\General\Estado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function evento()
    {
        return $this->belongsTo('App\Models\General\Evento');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\General\Usuario');
    }
}
