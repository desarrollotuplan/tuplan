<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $usuario_id
 * @property int $negocio_id
 * @property int $tipo_calificacion_id
 * @property int $punto_id
 * @property string $created_at
 * @property string $updated_at
 * @property Negocio $negocio
 * @property Punto $punto
 * @property TipoCalificacion $tipoCalificacion
 * @property Usuario $usuario
 */
class Calificacion extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'calificacion';

    /**
     * @var array
     */
    protected $fillable = ['usuario_id', 'negocio_id', 'tipo_calificacion_id', 'punto_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function punto()
    {
        return $this->belongsTo('App\Models\General\Punto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoCalificacion()
    {
        return $this->belongsTo('App\Models\General\TipoCalificacion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\General\Usuario');
    }
}
