<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre
 * @property string $icono
 * @property string $created_at
 * @property string $updated_at
 * @property Calificacion[] $calificacions
 */
class TipoCalificacion extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tipo_calificacion';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'icono', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function calificacion()
    {
        return $this->hasMany('App/Models/General/Calificacion');
    }
    
    public static function getByName(string $name){
        return self::where('nombre',$name)->first();
    }
    
    public static function getNameAll(){
        return self::select('nombre')->get();
    }
}
