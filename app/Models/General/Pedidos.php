<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $negocio_id
 * @property int $usuario_id
 * @property int $estado_id
 * @property int $tipo_pago_id
 * @property int $id_generado
 * @property string $fecha
 * @property string $hora
 * @property int $total
 * @property int $valor_domicilio
 * @property string $comentario
 * @property string $direccion
 * @property string $barrio
 * @property int $telefono
 * @property int $efectivo_pago
 * @property string $factura_empresa
 * @property string $nombre_empresa
 * @property string $nit_empresa
 * @property string $created_at
 * @property string $updated_at
 * @property Estado $estado
 * @property Negocio $negocio
 * @property TipoPago $tipoPago
 * @property Usuario $usuario
 * @property DetallePedido[] $detallePedidos
 */
class Pedidos extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'pedido';

    /**
     * @var array
     */
    protected $fillable = ['negocio_id', 'usuario_id', 'estado_id', 'tipo_pago_id', 'id_generado', 'fecha', 'hora', 'total', 'valor_domicilio', 'comentario', 'direccion', 'barrio', 'telefono', 'efectivo_pago', 'factura_empresa', 'nombre_empresa', 'nit_empresa', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estado()
    {
        return $this->belongsTo('App\Models\General\Estado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\General\Negocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoPago()
    {
        return $this->belongsTo('App\Models\General\TipoPago');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\General\Usuario');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detallePedidos()
    {
        return $this->hasMany('App\Models\General\DetallePedido');
    }
}
