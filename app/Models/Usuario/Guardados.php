<?php

namespace App\Models\Usuario;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $usuario_id
 * @property int $negocio_id
 * @property int $promocion_id
 * @property int $evento_id
 * @property string $nombre
 * @property string $created_at
 * @property string $updated_at
 * @property Evento $evento
 * @property Negocio $negocio
 * @property Promocione $promocione
 * @property Usuario $usuario
 */
class Guardados extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['usuario_id', 'negocio_id', 'promocion_id', 'evento_id', 'nombre', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function evento()
    {
        return $this->belongsTo('App\Models\Usuario\Evento');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\Usuario\Negocio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function promociones()
    {
        return $this->belongsTo('App\Models\Usuario\Promociones', 'promocion_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\Usuario\Usuario');
    }
}
