<?php

namespace App\Models\Usuario;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $usuario_id
 * @property int $categoria_id
 * @property string $created_at
 * @property string $updated_at
 * @property Categorium $categorium
 * @property Usuario $usuario
 */
class Intereses extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['usuario_id', 'categoria_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categoria()
    {
        return $this->belongsTo('App\Models\General\Categoria', 'categoria_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\General\Usuario');
    }
}
