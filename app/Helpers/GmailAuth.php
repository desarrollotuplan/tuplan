<?php
namespace App\Helpers;

use Google_Client as GC;

class GmailAuth {
    
    protected $google;
    private $appId;
    private $appSecret;
    private $url;
    private $client;
    
    // set de variables
    public function __construct(){
        $this->appId = env('GOOGLE_APP_ID');
        $this->appSecret = env('GOOGLE_APP_SECRET');
        $this->url = env('GOOGLE_APP_URL');
        $this->client = $this->google = new GC;
    }
    
    // Inicializacion de objetos
    public function googleIni(){
        if($this->client){
            $this->client->setClientId($this->appId);
            $this->client->setClientSecret($this->appSecret);
            $this->client->setRedirectUri($this->url);
            $this->client->setScopes(['profile','email']);
            return $this->client;
        }
    }
    
    // Verificacion de inicio de sesion en la aplicacion
    public function isLoggedIn(){
        return isset($_SESSION['access_token']);
    }
    
    // Obtener la url de direcionamiento 
    public function getAuthUrl(){
        return $this->client->createAuthUrl();
    }
    
    // Chekear si existe el 'code' en la ruta get
    public function checkRedirectCode(){
        if(isset($_GET['code'])){
            $this->client->authenticate($_GET['code']);
            $this->setToken($this->client->getAccessToken());
            return true;
        }else{
            return false;
        }
    }
    
    // Setear el token 
    public function setToken($token){
        $_SESSION['access_token'] = $token;
        $this->client->setAccessToken($token);
    }
    
    // Cerrar session con google
    public function logout(){
        unset($_SESSION['access_token']);
    }
    
    // Obtener datos del usuario
    public function getUser(){
        if ($this->client->getAccessToken()) {
            return $this->client->verifyIdToken();
        }else{
            return false;
        }
    }
    
}