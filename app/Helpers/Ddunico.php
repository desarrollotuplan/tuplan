<?php 
namespace App\Helpers;

use Jenssegers\Date\Date;
use App\Models\General\Negocio as N;
use App\Models\General\Eventos as E;
use App\Models\General\Promociones as P;

class Ddunico {
    
    // Id unico de un evento 
    public static function evento($id) {
        $evento = E::find($id);
        return static::retornar($evento);
    }
    
    // Id unico de un promo 
    public static function promo($id) {
        $promo = P::find($id);
        return static::retornar($promo);
    }
    
    // Id unico de un negocio
    public static function negocio($id) {
        $negocio = N::find($id);
        return static::retornar($negocio);
    }
    
    // Si existe la consulta retorna el id unico
    public static function retornar($clase){
        if($clase){
            return $clase->id_unico;
        }else{
            return 'hh';
        }
    }
    
}