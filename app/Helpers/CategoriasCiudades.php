<?php 

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class CategoriasCiudades {
    
    // Obtener las ciudades solo donde hay negocios registrados
    public static function getCiudades(){
        return DB::table('ciudad as c')
            ->whereExists(function($query){ 
                $query->select('n.ciudad_id')
                    ->from('negocio as n')
                    ->whereRaw('n.ciudad_id = c.id');
            })->get();
    }
    
    // Obtener las categorias donde hayan negocios
    public static function getCategorias(){
        
        // Obetener subcategorias solo donde hay negocios
        $subcategorias = DB::table('subcategoria as s')
            ->whereExists(function($query){ 
                $query->select('n.subcategoria_id')
                    ->from('negocio as n')
                    ->whereRaw('n.subcategoria_id = s.id');
            })->get();
        
        if(!$subcategorias->isEmpty()){
            // Devolver categorias solo donde hay negocios de sus subcategorias
            return DB::table('categoria as c')
                ->where('c.id', $subcategorias->pluck('categoria_id')->values())
                ->get();
        }
    }
    
    // Obtener solo el nombre de una ciudad por su id
    public static function getNameCiudad($id)
    {
        return DB::table('ciudad')->select('nombre')->where('id',$id)->first()->nombre;
    }
    
    // Obtener solo el nombre de una categoria por su id
    public static function getNameCategoria($id)
    {
        return DB::table('categoria')->select('nombre')->where('id',$id)->first()->nombre;
    }
    
    // Obtener solo el id de una ciudad por su nombre
    public static function getIdCiudad($name)
    {
        return DB::table('ciudad')->select('id')->where('nombre',$name)->first();
    }
    
    // Obtener solo el id de una categoria por su nombre
    public static function getIdCategoria($name)
    {
        return DB::table('categoria')->select('id')->where('nombre',$name)->first();
    }
    
}