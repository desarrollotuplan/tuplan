<?php
namespace App\Helpers;

use Facebook\Facebook as FB;

class Facebook {
    
    protected $facebook;
    private $appId;
    private $appSecret;
    private $url;
    
    public function __construct(){
        $this->appId = env('FACEBOOK_APP_ID');
        $this->appSecret = env('FACEBOOK_APP_SECRET');
        $this->url = env('FACEBOOK_APP_URL');
    }
    
    public function facebookIni(){
        session_start();
        $this->facebook = new FB([
            'app_id' => $this->appId,
            'app_secret' => $this->appSecret,
            'default_graph_version' => 'v2.10'
        ]);
    }
    
    public function getHelper(){
        return $this->facebook->getRedirectLoginHelper();
    }
    
    public function getAccessToken(){
        return $this->getHelper()->getAccessToken();
    }
    
    public function getAuthUrl(){
        return $this->getHelper()->getLoginUrl($this->url,array('email'));
    }
    
    public function getUser(){
        $response = $this->facebook->get('/me?fields=id,first_name,last_name,email,gender,birthday',$this->getAccessToken());
        return $response->getGraphUser();
    }
    
    
}