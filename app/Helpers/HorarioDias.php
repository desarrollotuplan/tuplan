<?php

namespace App\Helpers;

use App\Models\General\Dias as D;

class HorarioDias {
    
    // Funcion para saber si el negocio esta abierto o no
    public static function getHorarioDia($id_negocio)
    {
        // Obtener el codigo del dia
        $dia = date('w');
        
        // Obtener hora actual
        $hora = date("H:i:s");
        
        // verificar si ese dia hay un horario definido por el negocio
        $estado = D::with(['horario'=>function($q)use($id_negocio){ $q->where('negocio_id',$id_negocio); }])
            ->where('codigo',$dia)
            ->first();
        
        // si hay un horario se verifica que este abierto
        if(isset($estado->horario[0])){
            $horario = $estado->horario[0];
            
            // Deacuerdo a la hora devuelve el texto correspondiente
            if($horario->hora_inicio_1 <= $hora && $horario->hora_fin_1 >= $hora){
                return "Abierto";
            }elseif($horario->hora_inicio_2 <= $hora && $horario->hora_fin_2 >= $hora){
                return "Abierto";
            }else{
                return "Cerrado";
            }
        }
    }
    
}