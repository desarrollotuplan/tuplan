<?php 

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use App\Models\General\Genero as G;
use App\Models\General\Ciudad as C;
use App\Models\General\Negocio as N;
use App\Models\General\UsuarioRol as UR;
use App\Models\General\UsuarioNegocio as UN;

class User {
    
    // id del usuario
    public static function id()
    {
        return Auth::user()->id;
    }
    
    // id del rol del usuario
    public static function rolId()
    {
        return UR::with('rol')->where('usuario_id',static::id())->first()->rol->id;
    }
    
    // nombre del rol del usuario
    public static function rolName()
    {
        return UR::with('rol')->where('usuario_id',static::id())->first()->rol->nombre;
    }
    
    // Verificar que el usuario es de tipo 'usuario' para acceder a fumciones especificas
    public static function userCheckFunction(){
        if(static::check() && static::rolName() == 'usuario'){
            return true;
        }else{
            return false;
        }
    }
    
    // id del negocio al cual esta vinculado
    public static function negocioId()
    {
        return UN::where('usuario_id',static::id())->first()->negocio_id;
    }
    
    // detalles del negocio al cual pertenece 
    public static function negocio()
    {
        return N::find(static::negocioId());
    }
    
    // primer nombre
    public static function primerNombre()
    {
        return Auth::user()->primer_nombre;
    }
    
    // segundo nombre
    public static function segundoNombre()
    {
        return Auth::user()->segundo_nombre;
    }
    
    // primer apellido
    public static function primerApellido()
    {
        return Auth::user()->primer_apellido;   
    }
    
    // segundo apellido
    public static function segundoApellido()
    {
        return Auth::user()->segundo_apellido;   
    }
    
    // sexo id
    public static function sexoId()
    {
        return Auth::user()->genero_id;   
    }
    
    // sexo nombre
    public static function sexoNombre()
    {
        return G::select('nombre')->where('id',static::sexoId())->first()->nombre;
    }
    
    // fecha de nacimiento
    public static function fechaNacimiento()
    {
        return Auth::user()->fecha_nacimiento;
    }
    
    // celular
    public static function celular()
    {
        return Auth::user()->telefono_movil;
    }
    
    // direccion
    public static function direccion()
    {
        return Auth::user()->direccion;
    }
    
    // id de la ciudad
    public static function ciudadId()
    {
        return Auth::user()->ciudad_id;
    }
    
    // nombre de la ciudad
    public static function ciudadNombre()
    {
        return C::select('nombre')->where('id',static::ciudadId())->first()->nombre;   
    }
    
    // email
    public static function email()
    {
        return Auth::user()->email;   
    }
    
    // foto
    public static function foto()
    {
        return Auth::user()->foto;
        
    }
    
    public static function check()
    {
        return Auth::check();
    }
    
    //Método con rand()
    public static function randomString($length = 4)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return str_shuffle($randomString);
    }
    
}