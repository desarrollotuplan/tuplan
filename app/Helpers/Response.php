<?php
namespace App\Helpers;

class Response {
    
    // Alerta personalizada
    public static function swal(string $title, string $text, $icon = false, bool $closeModal = false, array $errorInput = [], bool $disabledForm = false)
    {
        return [
            'type'  =>  'swal',
            'icon'  =>  $icon,
            'title' =>  $title,
            'text'  =>  $text,
            'closeModal' => $closeModal,
            'errorInput' => $errorInput,
            'disabledForm' => $disabledForm,
        ];
    }
    
    // Error que se puede dar por manipulacion de datos sensibles como id y que no concuerdan con las consultas
    public static function swalError()
    {
        return [
            'type'  =>  'swal',
            'icon'  =>  'error',
            'title' =>  'Atencion',
            'text'  =>  'Ha ocurrido un error, actualize la pagina y si el error persiste contacte con soporte'
        ];
    }
    
    // Mensaje de verificacion exitosa de alguna accion
    public static function swalSuccess(string $text, bool $title = false , bool $closeModal = false, bool $disabledForm = false)
    {
        if(!$title){ $title = 'Hecho';  }
        return [
            'type'  =>  'swal',
            'icon'  =>  'success',
            'title' =>  $title,
            'text'  =>  $text,
            'closeModal'    =>  $closeModal,
            'disabledForm'  =>  $disabledForm,
        ];
    }
    
    // Respuesta para mostrar vistas en divs
    public static function viewDiv(string $idDiv, string $vista, array $variables = [], bool $closeModal = false)
    {
        return [
            'type'  =>  'div',
            'idDiv' =>  $idDiv,
            'view'  =>  view($vista,$variables)->render(),
            'closeModal'  =>  $closeModal
        ];
    }
    
    // Respuesta para mostrar vista en modal
    public static function viewModal(string $vista, array $variables = [], string $modalType = 'modal-dark')
    {
        return [
            'type'  =>  'modal',
            'view'  =>  view($vista,$variables)->render(),
            'modalType' =>  $modalType
        ];
    }
    
    // Respuesta para redireccionar
    public static function redirect(string $route = '/')
    {
        return [
            'type'  =>  'redirect',
            'route' =>  $route
        ];
    }
    
    // Alertas breves con vista
    public static function alertView(string $typeAlert, string $position, string $title, string $descripcion, string $idDiv, string $vista, array $variables = [], bool $closeModal = false)
    {
        return [
            'type'  =>  'alertView',
            'typeAlert' =>  $typeAlert,
            'position'  =>  $position,
            'title' =>  $title,
            'text'   =>  $descripcion,
            'idDiv' =>  $idDiv,
            'view'  =>  view($vista,$variables)->render(),
            'closeModal'    =>  $closeModal
        ];
    }
    
    // Alertas breves 
    public static function alert(string $typeAlert, string $position, string $title, string $descripcion, bool $closeModal = false, bool $disabledForm = false, bool $resetForm = false)
    {
        return [
            'type'  =>  'alert',
            'typeAlert' =>  $typeAlert,
            'position'  =>  $position,
            'title' =>  $title,
            'text'   =>  $descripcion,
            'closeModal'    =>  $closeModal,
            'disabledForm'  =>  $disabledForm,
            'resetForm'  =>  $resetForm
        ];
    }
    
    // Redireccionar con mensaje
    public static function redirectWith(string $route = '/', array $message = [])
    {
        return redirect($route)->with('message',$message);
    }
    
    // Agregar elementos del DOM
    public static function addCont(string $div, string $view, array $variables = [], bool $closeModal = false)
    {
        return [
            'type'  =>  'addCont',
            'div' =>  $div,
            'view'  =>  view($view,$variables)->render(),
            'closeModal'    =>  $closeModal
        ];
    }
    
    // Eliminar elementos del DOM
    public static function deleteCont(string $div, bool $closeModal = false)
    {
        return [
            'type'  =>  'deleteCont',
            'div' =>  $div,
            'closeModal'    =>  $closeModal
        ];
    }
    
    // Funcion para los jcrop
    public static function jcrop($width,$height,$aspectRatio = 1, array $tipo)
    {
        return [
            'type'  =>  'jcrop',
            'width' => $width,
            'height' =>  $height,
            'aspectRatio'   =>  $aspectRatio,
            'typeImages'  => $tipo
        ];
    }
    
}