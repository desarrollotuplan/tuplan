<?php 
namespace App\Helpers;

use Jenssegers\Date\Date;

class JessengersDate {
    
    static function getDateSpanishFull($fecha)
    {
        $fecha = Date::parse($fecha);
        return $fecha->format('l j').' de '.$fecha->format('F').' del '.$fecha->format('Y');
    }
    
    static function getTimeFormat($hora)
    {
        return date('g:i a',strtotime($hora));
    }
    
}