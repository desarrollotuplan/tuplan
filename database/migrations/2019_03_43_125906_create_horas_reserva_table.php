<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorasReservaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horas_reserva', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reserva_disponible_id');
            $table->unsignedInteger('dia_id');
            $table->unsignedInteger('estado_id');
            $table->time('hora');
            $table->timestamps();
            $table->foreign('reserva_disponible_id')->references('id')->on('reservas_disponibles');
            $table->foreign('dia_id')->references('id')->on('dias');
            $table->foreign('estado_id')->references('id')->on('estado-tablas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horas_reserva');
    }
}
