<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('usuario_id');
            $table->unsignedInteger('negocio_id')->nullable();
            $table->unsignedInteger('evento_id')->nullable();
            $table->unsignedInteger('estado_id');
            $table->string('comentario',500);
            $table->timestamps();
            $table->foreign('usuario_id')->references('id')->on('usuario');
            $table->foreign('negocio_id')->references('id')->on('negocio');
            $table->foreign('evento_id')->references('id')->on('eventos');
            $table->foreign('estado_id')->references('id')->on('estado-tablas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentarios');
    }
}
