<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNegocioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('negocio', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('subcategoria_id');
            $table->unsignedInteger('ciudad_id');
            $table->unsignedInteger('estado_id');
            $table->string('id_unico',200)->unique();
            $table->string('foto_perfil',300)->nullable();
            $table->string('logo',300)->nullable();
            $table->string('nombre',200);
            $table->string('eslogan',500)->nullable();
            $table->string('descripcion',500)->nullable();
            $table->string('direccion',300);
            $table->integer('telefono')->nullable();
            $table->string('celular',30)->nullable();
            $table->string('whatsapp',30)->nullable();
            $table->string('servicios',500)->nullable();
            $table->string('latitud',100)->nullable();
            $table->string('longitud',100)->nullable();
            $table->string('nit',50)->nullable();
            $table->string('rut',50)->nullable();
            $table->string('folder',50);
            $table->timestamps();
            $table->foreign('subcategoria_id')->references('id')->on('subcategoria');
            $table->foreign('ciudad_id')->references('id')->on('ciudad');
            $table->foreign('estado_id')->references('id')->on('estado-tablas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('negocio');
    }
}
