<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuardadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guardados', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('usuario_id');
            $table->unsignedInteger('negocio_id')->nullable();
            $table->unsignedInteger('promocion_id')->nullable();
            $table->unsignedInteger('evento_id')->nullable();
            $table->string('nombre',200);
            $table->timestamps();
            $table->foreign('usuario_id')->references('id')->on('usuario');
            $table->foreign('negocio_id')->references('id')->on('negocio');
            $table->foreign('promocion_id')->references('id')->on('promociones');
            $table->foreign('evento_id')->references('id')->on('eventos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guardados');
    }
}
