<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasDisponiblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas_disponibles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('negocio_id');
            $table->string('nombre',50);
            $table->string('informacion',200);
            $table->integer('numero_minimo_personas');
            $table->integer('numero_maximo_personas');
            $table->text('imagen');
            $table->timestamps();
            $table->foreign('negocio_id')->references('id')->on('negocio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas_disponibles');
    }
}
