<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserva', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('usuario_id');
            $table->unsignedInteger('hora_reserva_id');
            $table->unsignedInteger('estado_id');
            $table->unsignedInteger('tipo_reserva_id')->nullable();
            $table->date('fecha');
            $table->integer('numero_personas');
            $table->string('nombre_usuario',50);
            $table->string('informacion_adicional',500)->nullable();
            $table->timestamps();
            $table->foreign('usuario_id')->references('id')->on('usuario');
            $table->foreign('hora_reserva_id')->references('id')->on('horas_reserva');
            $table->foreign('estado_id')->references('id')->on('estado-tablas');
            $table->foreign('tipo_reserva_id')->references('id')->on('tipo_reserva');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserva');
    }
}
