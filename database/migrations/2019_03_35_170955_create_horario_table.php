<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horario', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('negocio_id');
            $table->unsignedInteger('dia_id');
            $table->unsignedInteger('horario_tipo_id');
            $table->time('hora_inicio_1');
            $table->time('hora_fin_1');
            $table->time('hora_inicio_2')->nullable();
            $table->time('hora_fin_2')->nullable();
            $table->timestamps();
            $table->foreign('negocio_id')->references('id')->on('negocio');
            $table->foreign('dia_id')->references('id')->on('dias');
            $table->foreign('horario_tipo_id')->references('id')->on('horario_tipo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horario');
    }
}
