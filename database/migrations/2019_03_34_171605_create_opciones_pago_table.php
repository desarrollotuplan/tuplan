<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpcionesPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opciones_pago', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('negocio_id');
            $table->unsignedInteger('tipo_pago_id');
            $table->timestamps();
            $table->foreign('negocio_id')->references('id')->on('negocio');
            $table->foreign('tipo_pago_id')->references('id')->on('tipo_pago');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opciones_pago');
    }
}
