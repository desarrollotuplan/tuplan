<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomicilioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domicilio', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('negocio_id');
            $table->string('servicio',2);
            $table->integer('valor_minimo')->nullable();
            $table->integer('valor_domicilio')->nullable();
            $table->time('hora_inicio');
            $table->time('hora_final');
            $table->timestamps();
            $table->foreign('negocio_id')->references('id')->on('negocio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domicilio');
    }
}
