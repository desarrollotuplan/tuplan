<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioNegocioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario-negocio', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('negocio_id');
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->foreign('negocio_id')->references('id')->on('negocio');
            $table->foreign('usuario_id')->references('id')->on('usuario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario-negocio');
    }
}
