<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedesSocialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redes_sociales', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('negocio_id');
            $table->string('facebook',300)->nullable();
            $table->string('youtube',300)->nullable();
            $table->string('instagram',300)->nullable();
            $table->string('twitter',300)->nullable();
            $table->string('website',300)->nullable();
            $table->string('app_android',300)->nullable();
            $table->string('app_ios',300)->nullable();
            $table->timestamps();
            $table->foreign('negocio_id')->references('id')->on('negocio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redes_sociales');
    }
}
