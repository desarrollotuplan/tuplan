<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ciudad_id')->nullable();
            $table->unsignedTinyInteger('genero_id');
            $table->unsignedInteger('estado_id');
            $table->string('facebook_id')->unique()->nullable();
            $table->string('google_id')->unique()->nullable();
            $table->string('primer_nombre',40);
            $table->string('segundo_nombre',40)->nullable();
            $table->string('primer_apellido',40)->nullable();
            $table->string('segundo_apellido',40)->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('direccion',300)->nullable();
            $table->string('email',100);
            $table->string('telefono_movil',20)->nullable();
            $table->text('password');
            $table->string('palabra_clave')->nullable();
            $table->text('foto');
            $table->timestamps();
            $table->rememberToken();
            $table->foreign('ciudad_id')->references('id')->on('ciudad');
            $table->foreign('genero_id')->references('id')->on('genero');
            $table->foreign('estado_id')->references('id')->on('estado-tablas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
