<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagenes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('negocio_id')->nullable();
            $table->unsignedInteger('promocion_id')->nullable();
            $table->unsignedInteger('evento_id')->nullable();
            $table->unsignedInteger('producto_id')->nullable();
            $table->string('ruta',300);
            $table->timestamps();
            $table->foreign('negocio_id')->references('id')->on('negocio');
            $table->foreign('promocion_id')->references('id')->on('promociones');
            $table->foreign('evento_id')->references('id')->on('eventos');
            $table->foreign('producto_id')->references('id')->on('productos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagenes');
    }
}
