<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('negocio_id');
            $table->unsignedInteger('usuario_id');
            $table->unsignedInteger('estado_id');
            $table->unsignedInteger('tipo_pago_id');
            $table->unsignedInteger('id_generado');
            $table->date('fecha');
            $table->time('hora');
            $table->unsignedInteger('total');
            $table->unsignedInteger('valor_domicilio');
            $table->string('comentario',500)->nullable();
            $table->string('direccion',300);
            $table->string('barrio',100);
            $table->unsignedInteger('telefono');
            $table->unsignedInteger('efectivo_pago')->nullable();
            $table->string('factura_empresa',2)->nullable();
            $table->string('nombre_empresa',300)->nullable();
            $table->string('nit_empresa',30)->nullable();
            $table->timestamps();
            $table->foreign('negocio_id')->references('id')->on('negocio');
            $table->foreign('usuario_id')->references('id')->on('usuario');
            $table->foreign('estado_id')->references('id')->on('estado');
            $table->foreign('tipo_pago_id')->references('id')->on('tipo_pago');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido');
    }
}
