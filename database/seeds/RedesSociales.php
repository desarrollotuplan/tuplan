<?php

use Illuminate\Database\Seeder;

class RedesSociales extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/redesSociales.json');
        $rol = file_get_contents($file);
        
        foreach(json_decode($rol) as $row){
            DB::table('redes_sociales')->insert([
                'negocio_id' => $row->negocio,
                'facebook' => $row->facebook, 
            ]);
        }
    }
}
