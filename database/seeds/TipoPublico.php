<?php

use Illuminate\Database\Seeder;

class TipoPublico extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/tipoPublico.json');
        $tip = file_get_contents($file);
        
        foreach(json_decode($tip) as $t){
            DB::table('tipo_publico')->insert([
                'nombre' => $t->nombre,
                'edad_minima' => $t->edad_minima,
                'edad_maxima' => $t->edad_maxima
            ]);
        }
    }
}
