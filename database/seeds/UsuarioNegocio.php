<?php

use Illuminate\Database\Seeder;

class UsuarioNegocio extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/usuarioNegocio.json');
        $rol = file_get_contents($file);
        
        foreach(json_decode($rol) as $row){
            DB::table('usuario-negocio')->insert([
                'negocio_id' => $row->negocio,
                'usuario_id' => $row->usuario,
            ]);
        }
    }
}
