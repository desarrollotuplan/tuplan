<?php

use Illuminate\Database\Seeder;

class Calificacion extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/calificacion.json');
        $gen = file_get_contents($file);
        
        foreach(json_decode($gen) as $row){
            DB::table('calificacion')->insert([
                'usuario_id' => $row->usuario,
                'negocio_id' => $row->negocio,
                'tipo_calificacion_id' => $row->calificacion,
                'punto_id' => $row->punto
            ]);
        }
    }
}
