<?php

use Illuminate\Database\Seeder;

class Dispositivo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/dispositivo.json');
        $gen = file_get_contents($file);
        
        foreach(json_decode($gen) as $row){
            DB::table('dispositivo')->insert([
                'nombre' => $row->nombre,
            ]);
        }
    }
}
