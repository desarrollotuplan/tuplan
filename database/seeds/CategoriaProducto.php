<?php

use Illuminate\Database\Seeder;

class CategoriaProducto extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/categoriaProducto.json');
        $cat = file_get_contents($file);
        
        foreach(json_decode($cat) as $row){
            DB::table('categoria_producto')->insert([
                'negocio_id' => $row->negocio,
                'nombre' => $row->nombre,
            ]);
        }
    }
}
