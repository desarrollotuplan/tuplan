<?php

use Illuminate\Database\Seeder;

class NegocioReserva extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/negocioReserva.json');
        $tip = file_get_contents($file);
        
        foreach(json_decode($tip) as $t){
            DB::table('negocio-reserva')->insert([
                'negocio_id' => $t->negocio,
                'reserva' => $t->reserva,
                'hora_inicio' => $t->hora_inicio,
                'hora_final' => $t->hora_final
            ]);
        }
    }
}
