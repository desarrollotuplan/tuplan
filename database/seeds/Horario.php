<?php

use Illuminate\Database\Seeder;

class Horario extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/horario.json');
        $gen = file_get_contents($file);
        
        foreach(json_decode($gen) as $row){
            DB::table('horario')->insert([
                'negocio_id' => $row->negocio,
                'dia_id' => $row->dia,
                'horario_tipo_id' => $row->horario_tipo,
                'hora_inicio_1' => $row->hora_inicio_1,
                'hora_fin_1' => $row->hora_fin_1,
                'hora_inicio_2' => $row->hora_inicio_2,
                'hora_fin_2' => $row->hora_fin_2,
            ]);
        }
    }
}
