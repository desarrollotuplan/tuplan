<?php

use Illuminate\Database\Seeder;

class Domicilio extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/domicilio.json');
        $tip = file_get_contents($file);
        
        foreach(json_decode($tip) as $t){
            DB::table('domicilio')->insert([
                'negocio_id' => $t->negocio,
                'servicio' => $t->servicio,
                'valor_minimo' => $t->valor_minimo,
                'valor_domicilio' => $t->valor_domicilio,
                'hora_inicio' => $t->hora_inicio,
                'hora_final' => $t->hora_final
            ]);
        }
    }
}
