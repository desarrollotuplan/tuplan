<?php

use Illuminate\Database\Seeder;

class HorarioTipo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/horarioTipo.json');
        $gen = file_get_contents($file);
        
        foreach(json_decode($gen) as $row){
            DB::table('horario_tipo')->insert([
                'nombre' => $row->nombre
            ]);
        }
    }
}
