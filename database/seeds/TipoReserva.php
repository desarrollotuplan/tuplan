<?php

use Illuminate\Database\Seeder;

class TipoReserva extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/tipoReserva.json');
        $tip = file_get_contents($file);
        
        foreach(json_decode($tip) as $t){
            DB::table('tipo_reserva')->insert([
                'nombre' => $t->nombre,
            ]);
        }
    }
}
