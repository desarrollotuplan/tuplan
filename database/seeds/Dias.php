<?php

use Illuminate\Database\Seeder;

class Dias extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/dias.json');
        $dep = file_get_contents($file);
        
        foreach(json_decode($dep) as $d){
            DB::table('dias')->insert([
                'codigo' => $d->codigo,
                'nombre' => $d->nombre,
            ]);
        }
    }
}
