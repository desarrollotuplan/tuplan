<?php

use Illuminate\Database\Seeder;

class Tablas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/tablas.json');
        $tabla = file_get_contents($file);
        
        foreach(json_decode($tabla) as $row){
            DB::table('tablas')->insert([
                'nombre' => $row->nombre, 
            ]);
        }
    }
}
