<?php

use Illuminate\Database\Seeder;

class Pais extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/pais.json');
        $pais = file_get_contents($file);
        
        foreach(json_decode($pais) as $row){
            DB::table('pais')->insert([
                'codigo' => $row->codigo,
                'nombre' => $row->nombre, 
            ]);
        }
    }
}
