<?php

use Illuminate\Database\Seeder;

class Negocio extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/negocio.json');
        $gen = file_get_contents($file);
        
        foreach(json_decode($gen) as $row){
            DB::table('negocio')->insert([
                'subcategoria_id' => $row->sub_id,
                'ciudad_id' => $row->ciudad,
                'estado_id' =>$row->estado,
                'id_unico' => md5(uniqid(mt_rand(), true)),
                'nombre' => $row->nombre,
                'direccion' => $row->direccion,
                'foto_perfil' => $row->foto_perfil,
                'logo' => $row->logo,
                'eslogan' => $row->eslogan,
                'descripcion' =>$row->descripcion,
                'telefono' => $row->telefono,
                'celular' => $row->celular,
                'whatsapp' => $row->whatsapp,
                'folder' => $row->folder
            ]);
        }
    }
}
