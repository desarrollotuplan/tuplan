<?php

use Illuminate\Database\Seeder;

class UsuarioRol extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/usuarioRol.json');
        $rol = file_get_contents($file);
        
        foreach(json_decode($rol) as $row){
            DB::table('usuario-rol')->insert([
                'usuario_id' => $row->usuario,
                'rol_id' => $row->rol,
            ]);
        }
    }
}
