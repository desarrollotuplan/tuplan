<?php

use Illuminate\Database\Seeder;

class Departamento extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/departamento.json');
        $dep = file_get_contents($file);
        
        foreach(json_decode($dep) as $d){
            DB::table('departamento')->insert([
                'codigo' => $d->codigo,
                'nombre' => $d->nombre,
                'pais_id' => $d->pais_id
            ]);
        }
    }
}
