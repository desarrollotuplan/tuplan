<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Navegador::class);
        $this->call(Dispositivo::class);
        $this->call(TipoPublico::class);
        $this->call(Pais::class);
        $this->call(Departamento::class);
        $this->call(Ciudad::class);
        $this->call(Categoria::class);
        $this->call(Colores::class);
        $this->call(Subcategoria::class);
        $this->call(Estado::class);
        $this->call(Tablas::class);
        $this->call(EstadoTablas::class);
        $this->call(Genero::class);
        $this->call(Usuario::class);
        $this->call(Rol::class);
        $this->call(UsuarioRol::class);
        $this->call(Negocio::class);
        $this->call(Intereses::class);
        $this->call(Dias::class);
        $this->call(TipoReserva::class);
        $this->call(TipoCalificacion::class);
        $this->call(NegocioReserva::class);
        $this->call(Domicilio::class);
        $this->call(HorarioTipo::class);
        $this->call(Horario::class);
        $this->call(CategoriaProducto::class);
        $this->call(Producto::class);
        $this->call(Imagenes::class);
        $this->call(RedesSociales::class);
        $this->call(ReservasDisponibles::class);
        $this->call(HorasReserva::class);
        $this->call(Puntos::class);
        $this->call(Calificacion::class);
        $this->call(Comentarios::class);
        $this->call(Promociones::class);
        $this->call(Eventos::class);
        $this->call(UsuarioNegocio::class);
    }
}
