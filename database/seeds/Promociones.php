<?php

use Illuminate\Database\Seeder;

class Promociones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/promociones.json');
        $pr = file_get_contents($file);
        
        foreach(json_decode($pr) as $row){
            DB::table('promociones')->insert([
                'negocio_id' => $row->negocio,
                'estado_id' => $row->estado,
                'id_unico' => md5(uniqid(mt_rand(), true)),
                'nombre' => $row->nombre,
                'descripcion' => $row->descripcion,
                'informacion_adicional' => $row->informacion_adicional,
                'fecha_inicio' => $row->fecha_inicio,
                'hora_inicio' => $row->hora_inicio,
                'fecha_fin' => $row->fecha_fin,
                'hora_fin' => $row->hora_fin,
                'imagen' => $row->imagen
            ]);
        }
    }
}
