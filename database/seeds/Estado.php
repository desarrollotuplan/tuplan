<?php

use Illuminate\Database\Seeder;

class Estado extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/estado.json');
        $esta = file_get_contents($file);
        
        foreach(json_decode($esta) as $row){
            DB::table('estado')->insert([
                'nombre' => $row->nombre, 
            ]);
        }
    }
}
