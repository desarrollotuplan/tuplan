<?php

use Illuminate\Database\Seeder;

class TipoCalificacion extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/tipoCalificacion.json');
        $tip = file_get_contents($file);
        
        foreach(json_decode($tip) as $t){
            DB::table('tipo_calificacion')->insert([
                'nombre' => $t->nombre,
                'icono' => $t->icono
            ]);
        }
    }
}
