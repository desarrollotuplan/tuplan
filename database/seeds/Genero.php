<?php

use Illuminate\Database\Seeder;

class Genero extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/genero.json');
        $gen = file_get_contents($file);
        
        foreach(json_decode($gen) as $row){
            DB::table('genero')->insert([
                'nombre' => $row->nombre,
                'seudo' => $row->seudo,
            ]);
        }
    }
}
