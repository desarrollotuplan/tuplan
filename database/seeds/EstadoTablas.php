<?php

use Illuminate\Database\Seeder;

class EstadoTablas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/estadoTablas.json');
        $esta = file_get_contents($file);
        
        foreach(json_decode($esta) as $row){
            DB::table('estado-tablas')->insert([
                'estado_id' => $row->estado, 
                'tabla_id' => $row->tabla
            ]);
        }
    }
}
