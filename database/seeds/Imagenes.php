<?php

use Illuminate\Database\Seeder;

class Imagenes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/imagenes.json');
        $gen = file_get_contents($file);
        
        foreach(json_decode($gen) as $row){
            DB::table('imagenes')->insert([
                'negocio_id' => $row->negocio,
                'producto_id' => $row->producto,
                'ruta' => $row->ruta
            ]);
        }
    }
}
