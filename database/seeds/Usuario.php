<?php

use Illuminate\Database\Seeder;

class Usuario extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fUsuario = database_path('Json/usuario.json');
        $jUsuario = file_get_contents($fUsuario);
        
        foreach(json_decode($jUsuario) as $row){
            DB::table('usuario')->insert([
                'ciudad_id' => $row->ciudad_id,
                'genero_id' => $row->genero_id, 
                'estado_id' => $row->estado_id,
                'facebook_id' => $row->facebook_id,
                'google_id' => $row->google_id,
                'primer_nombre' => $row->primer_nombre,
                'segundo_nombre' => $row->segundo_nombre,
                'primer_apellido' => $row->primer_apellido,
                'segundo_apellido' => $row->segundo_apellido,
                'fecha_nacimiento' => $row->fecha_nacimiento,
                'email' => $row->email,
                'telefono_movil' => $row->telefono_movil,
                'direccion' => $row->direccion,
                'password' => bcrypt($row->password),
                'palabra_clave' => $row->palabra_clave,
                'foto' => $row->foto
            ]);
        }
    }
}
