<?php

use Illuminate\Database\Seeder;

class Colores extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/colores.json');
        $col = file_get_contents($file);
        
        foreach(json_decode($col) as $row) 
        {
            DB::table('colores')->insert([
                'categoria_id' => $row->categoria,
                'color' => $row->color,
                'icono' => $row->icono
            ]);
        }
    }
}
