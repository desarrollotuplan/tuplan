<?php

use Illuminate\Database\Seeder;

class Comentarios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/comentarios.json');
        $gen = file_get_contents($file);
        
        foreach(json_decode($gen) as $row){
            DB::table('comentarios')->insert([
                'usuario_id' => $row->usuario,
                'negocio_id' => $row->negocio,
                'evento_id' => $row->evento,
                'estado_id' => $row->estado,
                'comentario' => $row->comentario
            ]);
        }
    }
}
