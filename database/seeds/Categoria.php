<?php

use Illuminate\Database\Seeder;

class Categoria extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/categoria.json');
        $cat = file_get_contents($file);
        
        foreach(json_decode($cat) as $row){
            DB::table('categoria')->insert([
                'tipo_publico_id' => $row->tipo_publico,
                'nombre' => $row->nombre, 
            ]);
        }
    }
}
