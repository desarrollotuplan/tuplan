<?php

use Illuminate\Database\Seeder;

class Navegador extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/navegador.json');
        $gen = file_get_contents($file);
        
        foreach(json_decode($gen) as $row){
            DB::table('navegador')->insert([
                'nombre' => $row->nombre,
            ]);
        }
    }
}
