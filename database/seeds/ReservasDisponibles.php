<?php

use Illuminate\Database\Seeder;

class ReservasDisponibles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/reservasDisponibles.json');
        $rol = file_get_contents($file);

        foreach(json_decode($rol) as $row){
            DB::table('reservas_disponibles')->insert([
                'negocio_id' => $row->negocio,
                'nombre' => $row->nombre, 
                'informacion' => $row->informacion, 
                'imagen' => $row->imagen,
                'numero_minimo_personas' => $row->minimo_personas,
                'numero_maximo_personas' => $row->maximo_personas
            ]);
        }
    }
}
