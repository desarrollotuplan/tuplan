<?php

use Illuminate\Database\Seeder;

class HorasReserva extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/horasReserva.json');
        $gen = file_get_contents($file);
        
        foreach(json_decode($gen) as $row){
            DB::table('horas_reserva')->insert([
                'reserva_disponible_id' => $row->reserva,
                'dia_id' => $row->dia,
                'hora' => $row->hora,
                'estado_id' => $row->estado
            ]);
        }
    }
}
