<?php

use Illuminate\Database\Seeder;

class Ciudad extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/ciudad.json');
        $ciudad = file_get_contents($file);
        
        foreach(json_decode($ciudad) as $c){
            DB::table('ciudad')->insert([
                'codigo' => $c->codigo,
                'nombre' => $c->nombre,
                'departamento_id' => $c->departamento_id
            ]);
        }
    }
}
