<?php

use Illuminate\Database\Seeder;

class Intereses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/intereses.json');
        $gen = file_get_contents($file);
        
        foreach(json_decode($gen) as $row){
            DB::table('intereses')->insert([
                'usuario_id' => $row->usuario,
                'categoria_id' => $row->categoria,
            ]);
        }
    }
}
