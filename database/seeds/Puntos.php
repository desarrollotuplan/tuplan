<?php

use Illuminate\Database\Seeder;

class Puntos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/puntos.json');
        $gen = file_get_contents($file);
        
        foreach(json_decode($gen) as $row){
            DB::table('puntos')->insert([
                'puntos' => $row->punto,
                'nombre' => $row->nombre
            ]);
        }
    }
}
