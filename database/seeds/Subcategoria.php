<?php

use Illuminate\Database\Seeder;

class Subcategoria extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/subcategoria.json');
        $sub = file_get_contents($file);
        
        foreach(json_decode($sub) as $row) 
        {
            DB::table('subcategoria')->insert([
                'categoria_id' => $row->categoria,
                'nombre' => $row->nombre,
            ]);
        }
    }
}
