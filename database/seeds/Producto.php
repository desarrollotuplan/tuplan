<?php

use Illuminate\Database\Seeder;

class Producto extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/producto.json');
        $gen = file_get_contents($file);
        
        foreach(json_decode($gen) as $row){
            DB::table('productos')->insert([
                'categoria_id' => $row->categoria,
                'estado_id' => $row->estado,
                'nombre' => $row->nombre,
                'precio' => $row->precio,
                'especificaciones' => $row->especificaciones,
                'informacion_adicional' => $row->info_adicional,
            ]);
        }
    }
}
