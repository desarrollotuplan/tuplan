<?php

use Illuminate\Database\Seeder;

class Rol extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('Json/rol.json');
        $rol = file_get_contents($file);
        
        foreach(json_decode($rol) as $row){
            DB::table('rol')->insert([
                'nombre' => $row->nombre, 
            ]);
        }
    }
}
